package com.hcmute.slayz;

import com.hcmute.slayz.data.entities.*;
import com.hcmute.slayz.data.repositories.*;
import com.hcmute.slayz.dto.response.statistic.LineChartElement;
import com.hcmute.slayz.utils.Constants;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;

@SpringBootTest
class SlayzApplicationTests {

//  @Autowired
//  InvoiceRepository invoiceRepository;
//
//  @Autowired
//  ProductRepository productRepository;
//
//  @Autowired
//  AddressRepository addressRepository;
//
//  @Autowired
//  UserRepository userRepository;
//
//  @Autowired
//  ImageRepository imageRepository;
//  @Autowired
//  VariantRepository variantRepository;
//
//  @Autowired
//  PasswordEncoder passwordEncoder;
//
//  @Test
//  void contextLoads() {
//  }
//
//  @Test
//  void test() {
//    Optional<Image> image = this.imageRepository.findByPath("0ea5d3436e4c4168b75d90b5324372a7.jpg");
//    image.ifPresent(value -> System.out.println(value.getId()));
//  }
//
//  @Test
//  void TestMethod() {
//    List<Variant> getList = this.variantRepository.findAllColorVariantByProductId(12L);
//    for (Variant variant : getList) {
//      System.out.println(variant.getId());
//    }
//  }
//
//  @Test
//  void TestMethod2() {
//    List<Variant> getList = this.variantRepository.findAllSizeVariantByProductId(12L);
//    for (Variant variant : getList) {
//      System.out.println(variant.getId());
//    }
//  }
//
//  @Test
//  void TestMethod3() {
//    Optional<Variant> getList = this.variantRepository.findVariantByProductIdAndColorIdAndSizeId(12L, 5L, 2L);
//    System.out.println(getList.isPresent() ? getList.get().getId() : "Fail");
//  }
//
//  @Test
//  void TestMethod4() {
//    List<User> all = userRepository.findAll();
//    for(User user : all){
//      user.setPassword(passwordEncoder.encode(user.getEmail()));
//      userRepository.save(user);
//    }
//  }
//
//  @Test
//  void TestMethod5() {
//    Matcher matcher = Constants.pattern.matcher("Quochoa@123");
//    System.out.println(matcher.matches());
//  }
//
//  @Test
//  void TestMethod6() {
//    Optional<User> userFound = this.userRepository.findById(5L);
//    if (userFound.isPresent()) {
//      User user = userFound.get();
//      user.setPassword(passwordEncoder.encode("Huong@04022004"));
//      this.userRepository.save(user);
//    }
//  }
//
//  @Test
//  void TestMethod8() {
//    List<Address> result = this.addressRepository.findAll();
//    result.removeIf(value -> value.getCreatedDate() == null);
//    for (Address address: result){
//      System.out.println(address.getId());
//    }
//  }
//
//  @Test
//  void TestMethod9() {
//
//  }
//  @Test
//  void TestMethod10() {
//    LocalDateTime now = LocalDateTime.now();
//    List<LocalDateTime> sevenDayRecent = new ArrayList<>();
//    List<LineChartElement> data = new ArrayList<>();
//    for(int i = 6;i >=0;i--){
//      sevenDayRecent.add(now.minusDays(i));
//    }
//    List<Invoice> invoiceList = this.invoiceRepository.findAll();
//    invoiceList.removeIf(value -> value.getStatus() == "CANCELLED");
//    for(LocalDateTime date : sevenDayRecent){
//      Long sum = 0L;
//      for(Invoice invoice : invoiceList){
//        LocalDateTime invoiceDate =
//                Instant.ofEpochMilli(invoice.getCreatedDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
//        if(date.getDayOfMonth() == invoiceDate.getDayOfMonth()){
//          sum += invoice.getGrandTotal();
//        }
//      }
//      data.add(new LineChartElement(date.getDayOfMonth() + "-" + date.getMonthValue() + "-" + date.getYear(),sum));
//    }
//  }
//  @Test
//  void TestMethod11() {
//    List<Product> productList = this.productRepository.findTop12NewestProduct();
//    for(Product product: productList){
//      System.out.println(product.getId());
//    }
//  }
}
