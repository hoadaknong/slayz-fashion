package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Invoice;
import com.hcmute.slayz.data.entities.InvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InvoiceDetailRepository extends JpaRepository<InvoiceDetail, Long> {
  List<InvoiceDetail> findAllByInvoiceId(Long invoiceId);
}
