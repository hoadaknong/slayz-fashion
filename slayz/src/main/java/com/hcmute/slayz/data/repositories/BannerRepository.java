package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Banner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BannerRepository extends JpaRepository<Banner, Long> {
  List<Banner> findAllByType(String type);
}
