package com.hcmute.slayz.data.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_review")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Review {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Long invoiceDetailId;

  private Long variantId;

  private Integer rating;

  private String comment;

  private Boolean enable;

  private Long userId;

  @CreationTimestamp
  private Date createdDate;

  @UpdateTimestamp
  private Date updateDate;

  private Date deletedDate;
}
