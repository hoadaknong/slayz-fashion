package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Review;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {

  @Query(value="SELECT * " +
          "FROM tbl_review r " +
          "WHERE r.variant_id IN " +
          "(SELECT v.id " +
          "FROM tbl_product p JOIN tbl_variant v ON p.id = v.product_id " +
          "WHERE p.id = :productId) AND r.rating IN :ratingList AND r.enable = TRUE AND r.deleted_date IS NULL " +
          "ORDER BY CASE WHEN :sortOption = 'time_desc' THEN r.created_date END DESC, " +
          "CASE WHEN :sortOption = 'rating_desc' THEN r.rating END DESC, " +
          "CASE WHEN :sortOption = 'rating_asc' THEN r.rating END ASC, " +
          "CASE WHEN :sortOption = 'time_asc' THEN r.created_date END ASC", nativeQuery = true)
  List<Review> filterReviewForEachProduct(@Param("ratingList") Integer[] ratingList,
                                  @Param("sortOption")String sortOption,
                                  @Param("productId")Long productId,
                                  Pageable pageable);
  @Query(value="SELECT * " +
          "FROM tbl_review r " +
          "WHERE r.variant_id IN " +
          "(SELECT v.id " +
          "FROM tbl_product p JOIN tbl_variant v ON p.id = v.product_id " +
          "WHERE p.id = :productId) AND r.rating IN :ratingList AND r.enable = TRUE AND r.deleted_date IS NULL " +
          "ORDER BY CASE WHEN :sortOption = 'time_desc' THEN r.created_date END DESC, " +
          "CASE WHEN :sortOption = 'rating_desc' THEN r.rating END DESC, " +
          "CASE WHEN :sortOption = 'rating_asc' THEN r.rating END ASC, " +
          "CASE WHEN :sortOption = 'time_asc' THEN r.created_date END ASC", nativeQuery = true)
  List<Review> getTotalRecordOfFilterReviewForEachProduct(@Param("ratingList") Integer[] ratingList,
                                          @Param("sortOption")String sortOption,
                                          @Param("productId")Long productId);

  @Query(value="SELECT * " +
          "FROM tbl_review r " +
          "WHERE r.variant_id IN " +
          "(SELECT v.id " +
          "FROM tbl_product p JOIN tbl_variant v ON p.id = v.product_id " +
          "WHERE p.id = :productId) AND r.enable = TRUE AND r.deleted_date IS NULL", nativeQuery = true)
  List<Review> findAllByProductId(@Param("productId")Long productId);

  @Query(value="SELECT COUNT(*) " +
          "FROM tbl_review r " +
          "WHERE r.variant_id IN " +
          "(SELECT v.id " +
          "FROM tbl_product p JOIN tbl_variant v ON p.id = v.product_id " +
          "WHERE p.id = :productId) AND r.rating = :rating AND r.enable = TRUE AND r.deleted_date IS NULL", nativeQuery = true)
  List<?> getAmountOfRatingByProductId(@Param("productId") Long productId, @Param("rating")Integer rating);
  List<Review> findAllByUserId(Long userId);

  List<Review> findByInvoiceDetailIdAndUserId(Long id,Long userId);

  @Query(value = "SELECT R FROM Review R WHERE R.deletedDate IS NULL")
  List<Review> findAllReview(Pageable pageable);

  @Query(value = "SELECT R FROM Review R WHERE R.deletedDate IS NULL")
  List<Review> findAllReview();

  List<Review> findAllByRating(Integer rating);
}
