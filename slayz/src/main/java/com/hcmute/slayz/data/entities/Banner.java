package com.hcmute.slayz.data.entities;


import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_banner")
public class Banner {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String type;
  private String image;
  private String linkTo;
  private Date startDate;
  private Date endDate;
  @CreationTimestamp
  private Date createdDate;
  private Date deletedDate;
  @UpdateTimestamp
  private Date updateDate;
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Banner banner = (Banner) o;
    return id != null && Objects.equals(id, banner.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
