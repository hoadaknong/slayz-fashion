package com.hcmute.slayz.data.entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_address")
public class Address {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String addressLine;

  private String ward;

  private String district;

  private String province;

  private Long userId;

  private Boolean isDefault;

  private Long provinceId;

  private Long districtId;

  private String wardId;

  private String phone;

  private String fullName;

  @CreationTimestamp
  private Date createdDate;

  @UpdateTimestamp
  private Date updateDate;

  private Date deletedDate;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Address address = (Address) o;
    return id != null && Objects.equals(id, address.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
