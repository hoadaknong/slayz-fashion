package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Address;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {
  @Query(value = "SELECT A FROM Address A WHERE A.deletedDate IS NULL AND A.userId =:id")
  List<Address> findAllByUserId(@Param("id") Long id);

  @Query(value = "SELECT A FROM Address A WHERE A.deletedDate IS NULL AND A.userId =:id")
  List<Address> findAllByUserId(@Param("id") Long id, Pageable pageable);
}
