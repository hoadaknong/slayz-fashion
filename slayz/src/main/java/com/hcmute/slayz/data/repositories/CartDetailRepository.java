package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.CartDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartDetailRepository extends JpaRepository<CartDetail, Long> {
  List<CartDetail> findByCartId(Long id);

  List<CartDetail> findAllByVariantId(Long variantId);
}
