package com.hcmute.slayz.data.entities;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="tbl_product")
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Long categoryId;

  private String name;

  private String barcode;

  private String gender;

  private Long listPrice;

  private Long standCost;

  private String image;

  @Column(length = 65555)
  private String description;

  @CreationTimestamp
  private Date createdDate;

  @UpdateTimestamp
  private Date updateDate;

  private Date deletedDate;

}
