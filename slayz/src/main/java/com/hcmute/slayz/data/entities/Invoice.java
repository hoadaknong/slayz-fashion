package com.hcmute.slayz.data.entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_invoice")
public class Invoice {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Long userId;

  private Long grandTotal;

  private String paymentStatus;

  private String paymentMethod;

  private String deliveryStatus;

  private String status;

  private Long addressId;

  private String note;

  private Long deliveryFee;
  private String deliveryMethod;

  @CreationTimestamp
  private Date createdDate;

  @UpdateTimestamp
  private Date updateDate;

  private Date deletedDate;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Invoice invoice = (Invoice) o;
    return id != null && Objects.equals(id, invoice.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

}
