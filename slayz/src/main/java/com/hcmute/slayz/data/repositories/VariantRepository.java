package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Variant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface VariantRepository extends JpaRepository<Variant, Long> {
  List<Variant> findByProductId(Long productId);

  List<Variant> findByColorId(Long colorId);

  List<Variant> findBySizeId(Long sizeId);
  List<Variant> findAllByProductIdAndColorId(Long productId, Long colorId);

  @Query(value = "SELECT * FROM tbl_variant v WHERE v.product_id = :productId GROUP BY v.color_id",nativeQuery = true)
  List<Variant> findAllColorVariantByProductId(@Param("productId")Long productId);

  @Query(value = "SELECT * FROM tbl_variant v WHERE v.product_id = :productId GROUP BY v.size_id",nativeQuery = true)
  List<Variant> findAllSizeVariantByProductId(@Param("productId")Long productId);

  List<Variant> findAllByProductIdAndColorIdAndSizeId(Long productId, Long colorId, Long sizeId);

  Optional<Variant> findVariantByProductIdAndColorIdAndSizeId(Long productId, Long colorId, Long sizeId);
}
