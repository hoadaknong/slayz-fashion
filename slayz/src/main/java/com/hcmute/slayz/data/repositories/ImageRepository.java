package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ImageRepository extends JpaRepository<Image, Long> {
  List<Image> findByProductId(Long productId);

  Optional<Image> findByPath(String path);
}
