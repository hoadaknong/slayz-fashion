package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Size;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SizeRepository extends JpaRepository<Size, Long> {

  @Query(value="SELECT S FROM Size S ORDER BY S.size")
  List<Size> findAllSize();
}
