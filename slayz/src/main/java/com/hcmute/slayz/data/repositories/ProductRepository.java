package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product, Long> {

  Optional<Product> findByBarcode(String barcode);

  @Query(value = "SELECT * " +
          "FROM tbl_product p " +
          "WHERE p.deleted_date is null " +
          "ORDER BY p.id DESC LIMIT 12", nativeQuery = true)
  List<Product> findTop12NewestProduct();

  @Query(value = "SELECT * " +
          "FROM tbl_product p " +
          "WHERE p.id = (SELECT v.product_id FROM tbl_variant v WHERE v.id = :variantId)", nativeQuery = true)
  Optional<Product> findByVariantId(@Param("variantId") Long variantId);

  @Query(value = "SELECT * FROM tbl_product p " +
          "WHERE p.category_id IN :category AND " +
          "(p.list_price >= :minPrice AND p.list_price <= :maxPrice) AND " +
          "p.name LIKE %:name% AND p.gender IN :gender AND " +
          "p.deleted_date IS NULL AND " +
          "p.id IN " + "(SELECT v.product_id FROM tbl_variant v WHERE v.color_id IN :colors AND v.size_id IN :sizes) " +
          "ORDER BY " + "CASE WHEN :sortOption = 'name_asc' THEN p.name END ASC, " +
          "CASE WHEN :sortOption = 'name_desc' THEN p.name END DESC, " +
          "CASE WHEN :sortOption = 'price_asc' THEN p.list_price END ASC, " +
          "CASE WHEN :sortOption = 'price_desc' THEN p.list_price END DESC, " +
          "CASE WHEN :sortOption = 'newest' THEN p.created_date END DESC", nativeQuery = true)
  List<Product> filterProduct(@Param("category") Long[] category, @Param("colors") Long[] colors, @Param("sizes") Long[] sizes, @Param("minPrice") Long minPrice, @Param("maxPrice") Long maxPrice, @Param("gender") String[] gender, @Param("name") String name, @Param("sortOption") String nameSortOption, Pageable pageable);

  @Query(value = "SELECT * FROM tbl_product p " +
          "WHERE p.category_id IN :category AND " +
          "(p.list_price >= :minPrice AND p.list_price <= :maxPrice) AND " +
          "p.name LIKE %:name% AND p.gender IN :gender AND " +
          "p.deleted_date IS NULL AND " +
          "p.id IN " + "(SELECT v.product_id FROM tbl_variant v WHERE v.color_id IN :colors AND v.size_id IN :sizes) " +
          "ORDER BY " + "CASE WHEN :sortOption = 'name_asc' THEN p.name END ASC, " +
          "CASE WHEN :sortOption = 'name_desc' THEN p.name END DESC, " +
          "CASE WHEN :sortOption = 'price_asc' THEN p.list_price END ASC, " +
          "CASE WHEN :sortOption = 'price_desc' THEN p.list_price END DESC, " +
          "CASE WHEN :sortOption = 'newest' THEN p.created_date END DESC", nativeQuery = true)
  List<Product> getSizeOfFilter(@Param("category") Long[] category,
                                @Param("colors") Long[] colors,
                                @Param("sizes") Long[] sizes,
                                @Param("minPrice") Long minPrice,
                                @Param("maxPrice") Long maxPrice,
                                @Param("gender") String[] gender,
                                @Param("name") String name,
                                @Param("sortOption") String nameSortOption);

  @Query(value = "select sum(quantity) as sum " +
          "from (select variant_id, product_id " +
          "from (tbl_product p join (select id as variant_id, product_id from tbl_variant) v on p.id = v.product_id) " +
          "where product_id = :productId) x join tbl_invoice_detail inde on x.variant_id = inde.variant_id", nativeQuery = true)
  List<?> findSalesProduct(@Param("productId") Long productId);

  @Query(value = "SELECT p.* " +
          "FROM tbl_product p " +
          "WHERE p.category_id = :categoryId AND p.id <> :id AND p.deleted_date IS NULL" + " LIMIT 4", nativeQuery = true)
  List<Product> findRelatedProduct(@Param("id") Long id, @Param("categoryId") Long categoryId);
}
