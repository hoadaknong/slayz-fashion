package com.hcmute.slayz.data.entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_cart")
public class Cart {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private Long userId;
  private Long grandTotal;

  @CreationTimestamp
  private Date createdDate;

  @UpdateTimestamp
  private Date updateDate;

  private Date deletedDate;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Cart cart = (Cart) o;
    return id != null && Objects.equals(id, cart.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
