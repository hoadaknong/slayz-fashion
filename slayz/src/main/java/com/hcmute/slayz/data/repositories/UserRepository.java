package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
  Optional<User> findByPhone(String phone);

  Optional<User> findByEmail(String email);

  @Query(value = "select * from tbl_user u WHERE u.email =:key OR u.phone=:key", nativeQuery = true)
  Optional<User> findByEmailOrPhone(@Param("key") String key);

  List<User> findAllByEnable(Boolean status);

  @Query(value = "SELECT U FROM User U WHERE " +
          "U.deletedDate IS NULL AND " +
          "U.fullName LIKE %:fullName% AND " +
          "U.phone LIKE %:phone% AND " +
          "U.gender IN (:gender) AND " +
          "U.email LIKE %:email% AND " +
          "U.enable IN (:status)")
  List<User> findAllUser(Pageable pageable,
                         @Param("fullName") String fullName,
                         @Param("phone") String phone,
                         @Param("gender") String[] gender,
                         @Param("email") String email,
                         @Param("status") Boolean[] status);

  @Query(value = "SELECT U FROM User U WHERE " +
          "U.deletedDate IS NULL AND " +
          "U.fullName LIKE %:fullName% AND " +
          "U.phone LIKE %:phone% AND " +
          "U.gender IN (:gender) AND " +
          "U.email LIKE %:email% AND " +
          "U.enable IN (:status)")
  List<User> findAllUser(@Param("fullName") String fullName,
                         @Param("phone") String phone,
                         @Param("gender") String[] gender,
                         @Param("email") String email,
                         @Param("status") Boolean[] status);
}
