package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Long> {
  Optional<Cart> findByUserId(Long id);
}
