package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Color;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ColorRepository extends JpaRepository<Color, Long> {
  @Query(value="SELECT C FROM Color C WHERE C.deletedDate IS NULL")
  List<Color> findAllColor(Pageable pageable);
  @Query(value="SELECT C FROM Color C WHERE C.deletedDate IS NULL")
  List<Color> findAllColor();
}
