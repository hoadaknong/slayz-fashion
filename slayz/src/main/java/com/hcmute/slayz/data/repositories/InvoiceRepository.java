package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Invoice;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {


  @Query(value = "SELECT I FROM Invoice I WHERE I.deletedDate IS NULL")
  List<Invoice> findAllInvoice(Pageable pageable);

  @Query(value = "SELECT I FROM Invoice I WHERE I.userId = :userId AND I.status IN (:statuses) AND I.paymentStatus IN (:paymentStatuses) AND I.deletedDate IS NULL ORDER BY I.createdDate DESC")
  List<Invoice> findAllByUserId(@Param("userId") Long userId, Pageable pageable, @Param("statuses")String[] statuses, @Param("paymentStatuses") String[] paymentStatuses);

  @Query(value = "SELECT I FROM Invoice I WHERE I.userId = :userId AND I.status IN (:statuses) AND I.paymentStatus IN (:paymentStatuses) AND I.deletedDate IS NULL ORDER BY I.createdDate DESC")
  List<Invoice> findAllByUserId(@Param("userId") Long userId, @Param("statuses")String[] statuses, @Param("paymentStatuses") String[] paymentStatuses);


  @Query(value = "SELECT I FROM Invoice I WHERE I.status = :status AND I.deletedDate IS NULL ORDER BY I.createdDate DESC")
  List<Invoice> findAllByStatus(@Param("status") String status, Pageable pageable);

  @Query(value = "SELECT I FROM Invoice I WHERE I.status = :status AND I.deletedDate IS NULL ORDER BY I.createdDate DESC")
  List<Invoice> findAllByStatus(@Param("status") String status);

  @Query(value = "SELECT I FROM Invoice I WHERE I.status like %:status% AND I.deletedDate IS NULL AND I.createdDate >= :startDate AND I.createdDate <= :endDate")
  List<Invoice> findAllByStatusAndRangeDate(@Param("status") String status, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

  @Query(value = "SELECT I FROM Invoice I WHERE I.deletedDate IS NULL AND I.createdDate >= :startDate AND I.createdDate <= :endDate")
  List<Invoice> findAllByStatusAndRangeDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
