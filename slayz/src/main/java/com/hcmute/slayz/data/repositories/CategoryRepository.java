package com.hcmute.slayz.data.repositories;

import com.hcmute.slayz.data.entities.Category;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
  @Query(value = "SELECT C FROM Category C WHERE C.deletedDate IS NULL")
  List<Category> findAllCategory(Pageable pageable);

  @Query(value = "SELECT C FROM Category C WHERE C.deletedDate IS NULL")
  List<Category> findAllCategory();
}
