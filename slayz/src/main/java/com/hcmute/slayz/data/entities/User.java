package com.hcmute.slayz.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tbl_user")
public class User implements UserDetails {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "role")
  private Role role;

  private String fullName;

  private String gender;

  private String dob;

  private String phone;

  private String email;

  @JsonIgnore
  private String password;

  private String photo;

  @JsonIgnore
  private Boolean enable;

  @JsonIgnore
  private String resetPasswordToken;

  @JsonIgnore
  @CreationTimestamp
  private Date createdDate;

  @JsonIgnore
  @UpdateTimestamp
  private Date updateDate;

  @JsonIgnore
  private Date deletedDate;

  @Override
  @JsonIgnore
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<SimpleGrantedAuthority> authorise = new ArrayList<>();
    authorise.add(new SimpleGrantedAuthority(role.getName()));
    return authorise;
  }

  @Override
  @JsonIgnore
  public String getUsername() {
    return this.phone;
  }

  @Override
  @JsonIgnore
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  @JsonIgnore
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  @JsonIgnore
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  @JsonIgnore
  public boolean isEnabled() {
    return this.enable;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    User user = (User) o;
    return id != null && Objects.equals(id, user.id);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
