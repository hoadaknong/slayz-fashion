package com.hcmute.slayz.services;

import com.hcmute.slayz.data.entities.Variant;
import com.hcmute.slayz.dto.request.variant.VariantCreation;
import com.hcmute.slayz.dto.request.variant.VariantUpdateRequest;
import com.hcmute.slayz.dto.response.variant.VariantResponse;

import java.util.List;

public interface VariantService {

  void createNewVariant(VariantCreation creation);

  void updateVariant(VariantCreation creation);

  List<VariantResponse> getAllVariantByProductId(Long productId);

  List<VariantResponse> getListVariantByProductIdAndColorId(Long productId, Long colorId);

  List<VariantResponse> getAllColorByProductId(Long productId);

  List<VariantResponse> getAllSizeByProductId(Long productId);

  Variant getVariantByProductIdAndColorIdAndSizeId(Long productId, Long colorId, Long sizeId);

  Variant updateVariant(Long id, VariantUpdateRequest request);
}
