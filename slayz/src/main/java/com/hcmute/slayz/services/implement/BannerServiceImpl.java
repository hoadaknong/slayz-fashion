package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Banner;
import com.hcmute.slayz.data.repositories.BannerRepository;
import com.hcmute.slayz.dto.request.banner.BannerCreation;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.services.BannerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class BannerServiceImpl implements BannerService {
  private final BannerRepository bannerRepository;

  @Override
  public List<Banner> findAllBanner() {
    return bannerRepository
            .findAll()
            .stream()
            .filter(banner -> banner.getDeletedDate() == null)
            .collect(Collectors.toList());
  }

  @Override
  public List<Banner> findAllBannerByType(String type) {
    return bannerRepository
            .findAllByType(type)
            .stream()
            .filter(banner -> banner.getDeletedDate() == null)
            .collect(Collectors.toList());
  }

  @Override
  public Banner findBannerById(Long id) {
    return bannerRepository
            .findById(id)
            .orElseThrow(()-> new ResourceNotFoundException("Không tìm thấy banner"));
  }

  @Override
  public void deleteBannerById(Long id) {
    Banner banner = bannerRepository
            .findById(id)
            .orElseThrow(()-> new ResourceNotFoundException("Không tìm thấy banner"));
    banner.setDeletedDate(new Date());
    bannerRepository.save(banner);
  }

  @Override
  public void updateBannerById(Long id, BannerCreation creation) {
    Banner banner = bannerRepository
            .findById(id)
            .orElseThrow(()-> new ResourceNotFoundException("Không tìm thấy banner"));
    banner.setImage(creation.getImage());
    banner.setType(creation.getType());
    banner.setLinkTo(creation.getLinkTo());
    banner.setStartDate(creation.getStartDate());
    banner.setEndDate(creation.getEndDate());
    bannerRepository.save(banner);
  }

  @Override
  public void createNewBanner(BannerCreation creation) {
    Banner banner = new Banner();
    banner.setImage(creation.getImage());
    banner.setType(creation.getType());
    banner.setLinkTo(creation.getLinkTo());
    banner.setStartDate(creation.getStartDate());
    banner.setEndDate(creation.getEndDate());
    bannerRepository.save(banner);
  }
}
