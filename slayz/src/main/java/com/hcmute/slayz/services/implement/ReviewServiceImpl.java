package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.*;
import com.hcmute.slayz.data.repositories.*;
import com.hcmute.slayz.dto.request.filter.FilterReviewRequest;
import com.hcmute.slayz.dto.request.review.ReviewCreationRequest;
import com.hcmute.slayz.dto.response.filter.FilterReviewResponse;
import com.hcmute.slayz.dto.response.review.ReviewOverViewResponse;
import com.hcmute.slayz.dto.response.review.ReviewResponse;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.exceptions.UnauthorizedException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.services.ReviewService;
import com.hcmute.slayz.utils.CommonFunctions;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ReviewServiceImpl implements ReviewService {

  private final ReviewRepository reviewRepository;

  private final VariantRepository variantRepository;

  private final UserRepository userRepository;

  private final ProductRepository productRepository;

  private final SizeRepository sizeRepository;

  private final ColorRepository colorRepository;

  @Override
  public void createReview(ReviewCreationRequest request) {
    Authentication authentication;
    User user;
    try {
      authentication = SecurityContextHolder.getContext().getAuthentication();
      user = (User) authentication.getPrincipal();
    } catch (Exception e) {
      throw new UnauthorizedException("Không tìm thấy người dùng!");
    }
    Variant variant = variantRepository.findById(request.getVariantId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy biến thể sản phẩm!"));
    Review review = new Review();
    review.setEnable(true);
    review.setComment(request.getComment());
    review.setRating(request.getRating());
    review.setVariantId(variant.getId());
    review.setUserId(user.getId());
    review.setInvoiceDetailId(request.getInvoiceDetailId());
    reviewRepository.save(review);
  }

  @Override
  public void changeReviewStatus(Long reviewId, Boolean status) {
    Review review = reviewRepository.findById(reviewId).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy bài đánh giá!"));
    review.setEnable(status);
    reviewRepository.save(review);
  }

  @Override
  public void deleteReview(Long reviewId) {
    Review review = reviewRepository.findById(reviewId).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy bài đánh giá!"));
    review.setDeletedDate(new Date());
    reviewRepository.save(review);
  }

  @Override
  public ReviewResponse getReviewById(Long id) {
    Review review = reviewRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy bài đánh giá!"));
    return reviewToDto(review);
  }

  @Override
  public PageResponse getAllReview(Pageable pageable) {
    int totalRecord = reviewRepository.findAllReview().size();
    List<ReviewResponse> reviews = reviewRepository.findAllReview(pageable)
            .stream()
            .map(this::reviewToDto)
            .collect(Collectors.toList());
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1, pageable.getPageSize(), totalRecord, reviews);
  }

  @Override
  public FilterReviewResponse getAllReviewByProductId(FilterReviewRequest request) {
    Pageable pageable = null;
    try {
      pageable = PageRequest.of(request.getPage() - 1, request.getLimit());
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    List<ReviewResponse> reviewList = reviewRepository.filterReviewForEachProduct(request.getRatingList(), request.getSortOption(), request.getProductId(), pageable).stream().map(this::reviewToDto).collect(Collectors.toList());
    Integer totalRecord = reviewRepository.getTotalRecordOfFilterReviewForEachProduct(request.getRatingList(), request.getSortOption(), request.getProductId()).size();
    FilterReviewResponse response = new FilterReviewResponse();
    response.setPage(request.getPage());
    response.setTotalRecord(totalRecord);
    Double totalPageCal = totalRecord.doubleValue() / request.getLimit();
    if (totalPageCal > Double.parseDouble(String.valueOf(totalPageCal.intValue()))) {
      response.setTotalPage(totalPageCal.intValue() + 1);
    } else {
      response.setTotalPage(totalPageCal.intValue());
    }
    response.setReviewListData(reviewList);
    return response;
  }

  @Override
  public List<ReviewResponse> getAllReviewByUserId(Long userId) {
    List<Review> reviewList = reviewRepository.findAllByUserId(userId);
    return reviewList.stream().filter(value -> value.getDeletedDate() == null).map(this::reviewToDto).collect(Collectors.toList());
  }

  @Override
  public List<ReviewResponse> findAllReviewByInvoiceDetailId(Long invoiceDetailId) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = (User) authentication.getPrincipal();
    List<Review> reviewList = reviewRepository.findByInvoiceDetailIdAndUserId(invoiceDetailId, currentUser.getId());
    return reviewList.stream().filter(value -> value.getDeletedDate() == null && value.getEnable() == true).map(this::reviewToDto).collect(Collectors.toList());
  }

  @Override
  public BigDecimal getAverageRatingByProductId(Long productId) {
    List<Review> reviewList = reviewRepository.findAllByProductId(productId);
    reviewList.removeIf(value -> value.getDeletedDate() != null && value.getEnable() == false);
    Double avgRaring = 0.0;
    for (Review review : reviewList) {
      avgRaring += review.getRating();
    }
    if (reviewList.isEmpty()) {
      return BigDecimal.valueOf(0.0);
    }
    avgRaring /= reviewList.size();
    return BigDecimal.valueOf(avgRaring).setScale(1, BigDecimal.ROUND_HALF_EVEN);
  }

  @Override
  public List<ReviewOverViewResponse> getOverviewReviewByProductId(Long productId) {
    List<Review> reviewList = reviewRepository.findAllByProductId(productId);
    reviewList.removeIf(value -> value.getDeletedDate() != null && value.getEnable() == false);
    List<ReviewOverViewResponse> result = new ArrayList<>();
    for (int i = 5; i >= 1; i--) {
      ReviewOverViewResponse response = new ReviewOverViewResponse();
      List<?> query = reviewRepository.getAmountOfRatingByProductId(productId, i);
      if (!query.isEmpty()) {
        response.setRating(i);
        response.setAmountOfRating(Integer.parseInt(query.get(0).toString()));
        Float percent = (response.getAmountOfRating().floatValue() / reviewList.size()) * 100;
        response.setPercentage(percent.intValue());
      } else {
        response.setRating(i);
        response.setAmountOfRating(0);
        response.setPercentage(0);
      }
      result.add(response);
    }
    return result;
  }

  private ReviewResponse reviewToDto(Review review) {
    ReviewResponse response = new ReviewResponse();
    response.setId(review.getId());
    response.setCreatedDate(review.getCreatedDate() == null ? null : review.getCreatedDate().getTime());
    response.setRating(review.getRating());
    response.setComment(review.getComment());
    User user = this.userRepository.findById(review.getUserId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    response.setFullName(user.getFullName());
    response.setUserPhoto(user.getPhoto());
    Product product = this.productRepository.findByVariantId(review.getVariantId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    response.setProductName(product.getName());
    response.setProductId(product.getId());
    Variant variant = this.variantRepository.findById(review.getVariantId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy biến thể sản phẩm!"));
    Size size = this.sizeRepository.findById(variant.getSizeId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy kích cỡ sản phẩm!"));
    response.setVariantSize(size.getSize());
    Color color = this.colorRepository.findById(variant.getColorId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy biến thể(màu sắc) của sản phẩm!"));
    response.setVariantColor(color.getColor());
    response.setUserId(user.getId());
    response.setProductImage(variant.getImage());
    response.setEnable(review.getEnable());
    return response;
  }
}
