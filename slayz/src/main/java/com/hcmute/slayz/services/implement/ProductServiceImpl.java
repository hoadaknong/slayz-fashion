package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Category;
import com.hcmute.slayz.data.entities.Image;
import com.hcmute.slayz.data.entities.Product;
import com.hcmute.slayz.data.entities.Variant;
import com.hcmute.slayz.data.repositories.*;
import com.hcmute.slayz.dto.request.filter.FilterRequest;
import com.hcmute.slayz.dto.request.product.ProductCreation;
import com.hcmute.slayz.dto.request.product.ProductUpdate;
import com.hcmute.slayz.dto.response.filter.FilterResponse;
import com.hcmute.slayz.dto.response.product.ProductReponse;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.services.ImageStorageService;
import com.hcmute.slayz.services.ProductService;
import com.hcmute.slayz.utils.CommonFunctions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.hcmute.slayz.utils.Constants.IMAGES_PATH;
import static com.hcmute.slayz.utils.Constants.THUMBNAIL_PATH;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;

  private final VariantRepository variantRepository;

  private final ImageRepository imageRepository;

  private final CategoryRepository categoryRepository;

  private final ImageStorageService imageStorageService;

  private final CartRepository cartRepository;

  private final CartDetailRepository cartDetailRepository;

  private final CartServiceImpl cartService;

  private final AddressRepository addressRepository;

  @Override
  public List<ProductReponse> getAllProductGrid() {
    List<Product> productList = this.productRepository.findAll();
    return productList.stream().filter(product -> product.getDeletedDate() == null).map(this::productToDto).sorted((product1, product2) -> product2.getProductId().compareTo(product1.getProductId())).collect(Collectors.toList());
  }

  @Override
  public ProductReponse createProduct(ProductCreation creation) {
    Product p = new Product();
    Optional<Product> productFound = productRepository.findByBarcode(creation.getBarcode());
    if (productFound.isPresent()) {
      p.setBarcode(creation.getBarcode() + "-" + UUID.randomUUID());
    } else {
      p.setBarcode(creation.getBarcode());
    }
    p.setCategoryId(creation.getCategory());
    p.setDescription(creation.getDescription());
    if (creation.getThumbnail() != null) {
      try {
        String fileName = this.imageStorageService.storeFile(creation.getThumbnail());
        p.setImage(fileName);
      } catch (Exception exception) {
        throw new ResourceNotFoundException(exception.getMessage());
      }
    }
    p.setListPrice(creation.getListPrice());
    p.setStandCost(creation.getStandCost());
    p.setName(creation.getTitle());
    p.setGender(creation.getGender());
    Product savedProduct = this.productRepository.save(p);

    //Save variant form
    int variantLength = creation.getColorVariants().length;
    for (int i = 0; i < variantLength; i++) {
      try {
        Variant variant = new Variant();
        String fileName = this.imageStorageService.storeFile(creation.getPhotoVariants()[i]);
        variant.setProductId(savedProduct.getId());
        variant.setImage(fileName);
        variant.setColorId(creation.getColorVariants()[i]);
        variant.setInventory(creation.getQuantityVariants()[i]);
        variant.setSizeId(creation.getSizeVariants()[i]);
        this.variantRepository.save(variant);
      } catch (Exception exception) {
        List<Variant> variantList = this.variantRepository.findByProductId(savedProduct.getId());
        for (Variant variant : variantList) {
          this.imageStorageService.deleteFile(variant.getImage());
          this.variantRepository.delete(variant);
        }
        this.imageStorageService.deleteFile(savedProduct.getImage());
        this.productRepository.delete(savedProduct);
        throw new ResourceNotFoundException("Lỗi biến thể sản phẩm!");
      }
    }

    // Save photos
    if (creation.getPhotos() != null) {
      MultipartFile[] photos = creation.getPhotos();
      try {
        for (int i = 0; i < photos.length; i++) {
          String fileName = this.imageStorageService.storeFile(photos[i]);
          Image image = new Image();
          image.setPath(fileName);
          image.setProductId(savedProduct.getId());
          this.imageRepository.save(image);
        }
      } catch (Exception exception) {
        this.productRepository.delete(savedProduct);
        throw new ResourceNotFoundException("Lỗi khi lưu dữ liệu hình ảnh!");
      }
    }
    return productToDto(savedProduct);
  }

  @Override
  public void updateProduct(Long id, ProductUpdate update) {
    log.info(update.toString());
    Optional<Product> productFound = this.productRepository.findById(id);
    if (!productFound.isPresent()) {
      throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
    }
    Product product = productFound.get();
    product.setName(update.getTitle());
    Optional<Category> categoryFound = this.categoryRepository.findById(update.getCategory());
    if (!categoryFound.isPresent()) {
      throw new ResourceNotFoundException("Không tìm thấy loại sản phẩm!");
    }
    product.setCategoryId(update.getCategory());
    Optional<Product> pdFound = productRepository.findByBarcode(update.getBarcode());
    if (pdFound.isPresent()) {
      product.setBarcode(update.getBarcode() + "-" + UUID.randomUUID());
    } else {
      product.setBarcode(update.getBarcode());
    }
    product.setBarcode(update.getBarcode());
    product.setStandCost(update.getStandCost());
    product.setListPrice(update.getListPrice());
    product.setDescription(update.getDescription());
    product.setGender(update.getGender());
    try {
      MultipartFile thumbnail = (MultipartFile) update.getThumbnail();
      String fileNameNewThumbnail = this.imageStorageService.storeFile(thumbnail);
      product.setImage(fileNameNewThumbnail);
    } catch (Exception e) {
      log.info("Thumbnail does not change");
    }

    // Update quantity of old variant
    List<Variant> variantList = this.variantRepository.findByProductId(id);
    if (update.getIdVariantsOld() == null || update.getQuantityVariantsOld() == null) {
      throw new ResourceNotFoundException("Không tìm thấy biến thể sản phẩm!");
    } else {
      Long[] variantIdOld = update.getIdVariantsOld();
      Long[] quantityVariantIdOld = update.getQuantityVariantsOld();
      for (int i = 0; i < quantityVariantIdOld.length; i++) {
        for (Variant variant : variantList) {
          if (variant.getId().equals(variantIdOld[i])) {
            variant.setInventory(quantityVariantIdOld[i]);
            this.variantRepository.save(variant);
          }
        }
      }
    }

    // Update new variant
    if (update.getQuantityVariants() != null && update.getColorVariants() != null && update.getSizeVariants() != null && update.getPhotoVariants() != null) {
      log.info("Add new variant");
      for (int i = 0; i < update.getSizeVariants().length; i++) {
        Variant variant = new Variant();
        String fileName = this.imageStorageService.storeFile(update.getPhotoVariants()[i]);
        variant.setProductId(id);
        variant.setImage(fileName);
        variant.setColorId(update.getColorVariants()[i]);
        variant.setInventory(update.getQuantityVariants()[i]);
        variant.setSizeId(update.getSizeVariants()[i]);
        this.variantRepository.save(variant);
      }
    }

    //Update old list image
    List<Image> imageList = this.imageRepository.findByProductId(id);
    String[] oldImages = update.getPhotosOld();
    if (oldImages != null) {
      for (int i = 0; i < oldImages.length; i++) {
        oldImages[i] = oldImages[i].substring(IMAGES_PATH.length(), oldImages[i].length());
      }
      imageList.forEach(image -> {
        boolean isImagePresent = false;
        for (int i = 0; i < oldImages.length; i++) {
          if (oldImages[i].equals(image.getPath())) {
            isImagePresent = true;
            break;
          }
        }
        if (!isImagePresent) {
          image.setDeletedDate(new Date());
          this.imageRepository.save(image);
          this.imageStorageService.deleteFile(image.getPath());
        }
      });
    } else {
      imageList.forEach(image -> {
        this.imageRepository.deleteById(image.getId());
        this.imageStorageService.deleteFile(image.getPath());
      });
    }

    //Update new images
    if (update.getPhotos() != null) {
      for (MultipartFile file : update.getPhotos()) {
        String fileName = this.imageStorageService.storeFile(file);
        Image image = new Image();
        image.setProductId(id);
        image.setPath(fileName);
        this.imageRepository.save(image);
      }
    }
    this.productRepository.save(product);
  }

  @Override
  public ProductReponse getProductById(Long id) {
    Product productFound = this.productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    return productToDto(productFound);
  }

  @Override
  public ProductReponse getProductByVariantId(Long variantId) {
    Product productFound = this.productRepository.findByVariantId(variantId).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    return productToDto(productFound);
  }

  @Override
  public ProductReponse getProductByBarcode(String barcode) {
    Product productFound = this.productRepository.findByBarcode(barcode).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    return productToDto(productFound);
  }

  @Override
  public void deleteProductById(Long id) {
    Product productFound = this.productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    productFound.setDeletedDate(new Date());
    this.productRepository.save(productFound);
  }

  @Override
  public PageResponse filterProduct(FilterRequest request) {
    Pageable pageable = null;
    try {
      pageable = PageRequest.of(request.getOffset() - 1, request.getLimit());
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    List<Product> listToGetNumberRecord = this.productRepository.getSizeOfFilter(request.getCategories(), request.getColors(), request.getSizes(), request.getMinPrice(), request.getMaxPrice(), request.getGender(), request.getName(), request.getSortOption());
    List<Product> productList = this.productRepository.filterProduct(request.getCategories(), request.getColors(), request.getSizes(), request.getMinPrice(), request.getMaxPrice(), request.getGender(), request.getName(), request.getSortOption(), pageable);
    List<ProductReponse> resultList = productList.stream().map(this::productToDto).collect(Collectors.toList());
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1, pageable.getPageSize(), listToGetNumberRecord.size(), resultList);
  }

  @Override
  public Long salesProduct(Long productId) {
    if (this.productRepository.findById(productId).isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
    }
    List<?> result = this.productRepository.findSalesProduct(productId);
    if (result.get(0) == null) {
      return 0L;
    }
    return Long.parseLong(result.get(0).toString());
  }

  @Override
  public List<ProductReponse> findTop12NewestProduct() {
    List<Product> productList = this.productRepository.findTop12NewestProduct();
    return productList.stream().filter(product -> product.getDeletedDate() == null).map(this::productToDto).collect(Collectors.toList());
  }

  @Override
  public List<ProductReponse> findRelatedProduct(Long id) {
    Product productFound = this.productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    List<Product> productList = this.productRepository.findRelatedProduct(productFound.getId(), productFound.getCategoryId());
    return productList.stream().filter(product -> product.getDeletedDate() == null).map(this::productToDto).collect(Collectors.toList());
  }

  private ProductReponse productToDto(Product product) {
    ProductReponse productReponse = new ProductReponse();
    productReponse.setProductId(product.getId());
    productReponse.setTitle(product.getName());
    productReponse.setProductImage(THUMBNAIL_PATH + product.getImage());
    if (product.getCategoryId() != null) {
      Optional<Category> categoryFound = this.categoryRepository.findById(product.getCategoryId());
      if (categoryFound.isPresent()) {
        productReponse.setCategory(categoryFound.get().getName());
        productReponse.setCategoryId(categoryFound.get().getId());
      }
    }
    productReponse.setStandCost(product.getStandCost());
    productReponse.setListPrice(product.getListPrice());
    productReponse.setDescription(product.getDescription());
    Long inventory = 0L;
    List<Variant> variantList = this.variantRepository.findByProductId(product.getId());
    for (Variant variant : variantList) {
      inventory += variant.getInventory();
    }
    productReponse.setInventory(inventory);
    productReponse.setBarcode(product.getBarcode());
    productReponse.setGender(product.getGender());
    return productReponse;
  }
}
