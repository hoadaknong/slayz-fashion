package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.authentication.RegisterRequest;
import com.hcmute.slayz.dto.request.user.ChangePassword;
import com.hcmute.slayz.dto.request.user.ResetPasswordRequest;
import com.hcmute.slayz.dto.request.user.UserUpdate;
import com.hcmute.slayz.dto.response.user.UserListResponse;
import com.hcmute.slayz.dto.response.user.UserResponse;
import com.hcmute.slayz.models.PageResponse;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

  PageResponse getAllUser(Pageable pageable, String fullName, String phone, String gender, String email, String status);

  UserResponse register(RegisterRequest request);

  UserListResponse getAllUserByStatus(Boolean status);

  UserResponse getUserByUserId(Long id);

  void updateUser(Long id, UserUpdate update);

  void updateUserStatus(Long id, Boolean status);

  void updateRoleUser(Long idUser, Long roleId);

  void updatePassword(Long idUser, ChangePassword request);

  void sendMailToResetPassword(String key);

  void resetPassword(ResetPasswordRequest request);
}
