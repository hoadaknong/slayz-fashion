package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.User;
import com.hcmute.slayz.dto.request.authentication.AuthenticationRequest;
import com.hcmute.slayz.dto.request.authentication.RegisterRequest;
import com.hcmute.slayz.dto.response.authentication.AuthenticationResponse;
import com.hcmute.slayz.dto.response.authentication.UserInfo;
import com.hcmute.slayz.dto.response.user.UserResponse;
import com.hcmute.slayz.exceptions.ForbiddenException;
import com.hcmute.slayz.exceptions.UnauthorizedException;
import com.hcmute.slayz.services.AuthenticationService;
import com.hcmute.slayz.services.UserService;
import com.hcmute.slayz.utils.Constants;
import com.hcmute.slayz.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {
  private final AuthenticationManager authManager;

  private final JwtTokenUtil jwtUtil;

  private final UserService userService;

  private final UserDetailsService userDetailsService;

  @Override
  public UserResponse register(RegisterRequest request) {
    return this.userService.register(request);
  }

  @Override
  public AuthenticationResponse login(AuthenticationRequest request) {
    try {
      log.info(request.getUsername() + " login...");
      Authentication authentication = authManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
      User user = (User) authentication.getPrincipal();
      String accessToken = jwtUtil.generateAccessToken(user);
      String refreshToken = jwtUtil.generateRefreshToken(user);
      AuthenticationResponse authenticationResponse = new AuthenticationResponse(accessToken, refreshToken);
      return authenticationResponse;
    } catch (BadCredentialsException exception) {
      throw new UnauthorizedException("Tài khoản hoặc mật khẩu không đúng!");
    } catch (UsernameNotFoundException exception) {
      throw new UnauthorizedException("Không tìm thấy người dùng!");
    } catch (Exception exception) {
      throw new UnauthorizedException("Người dùng đã bị vô hiệu hóa!\n Vui lòng liên hệ SlayZ để biết thêm thông tin chi tiết!");
    }
  }

  @Override
  public AuthenticationResponse loginForAdmin(AuthenticationRequest request) {
    try {
      log.info(request.getUsername() + " login...");
      Authentication authentication = authManager.authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));
      User user = (User) authentication.getPrincipal();
      if (user.getRole().getName().equals("ROLE_ADMIN") || user.getRole().getName().equals("ROLE_STAFF")) {
        String accessToken = jwtUtil.generateAccessToken(user);
        String refreshToken = jwtUtil.generateRefreshToken(user);
        AuthenticationResponse authenticationResponse = new AuthenticationResponse(accessToken, refreshToken);
        return authenticationResponse;
      }
      throw new ForbiddenException("Tài khoản không được phép truy cập!");
    } catch (BadCredentialsException exception) {
      throw new UnauthorizedException("Tài khoản hoặc mật khẩu không đúng!");
    } catch (UsernameNotFoundException exception) {
      throw new UnauthorizedException("Không tìm thấy người dùng!");
    } catch (ForbiddenException exception) {
      throw new ForbiddenException("Tài khoản không được phép truy cập!");
    } catch (Exception exception) {
      throw new UnauthorizedException("Người dùng đã bị vô hiệu hóa!\n Vui lòng liên hệ SlayZ để biết thêm thông tin chi tiết!");
    }
  }

  @Override
  public AuthenticationResponse refreshToken(String refreshTokenRequest) {
    if (jwtUtil.validateAccessToken(refreshTokenRequest)) {
      UserDetails userDetails = jwtUtil.getUserDetails(refreshTokenRequest);
      User user = (User) userDetails;
      String accessToken = jwtUtil.generateAccessToken(user);
      String refreshToken = jwtUtil.generateRefreshToken(user);
      AuthenticationResponse authenticationResponse = new AuthenticationResponse(accessToken, refreshToken);
      return authenticationResponse;
    }
    throw new UnauthorizedException("Đã xảy ra lỗi xác thực!");
  }

  @Override
  public Boolean checkResetPasswordTokenAvailable(String token) {
    try {
      User user = null;
      String[] jwtSubject = jwtUtil.getSubject(token).split(",");
      user = (User) userDetailsService.loadUserByUsername(jwtSubject[1]);
      if (token.equals(user.getResetPasswordToken()) && jwtUtil.validateAccessToken(token)) {
        return true;
      }
    } catch (Exception exception) {
      return false;
    }
    return false;
  }
}
