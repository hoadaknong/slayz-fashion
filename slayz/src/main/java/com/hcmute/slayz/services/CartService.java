package com.hcmute.slayz.services;

import com.hcmute.slayz.data.entities.Cart;
import com.hcmute.slayz.dto.request.cart.CartDetailCreation;
import com.hcmute.slayz.dto.request.cart.UpdateQuantity;
import com.hcmute.slayz.dto.response.cart.CartDetailGrid;
import com.hcmute.slayz.dto.response.cart.CartGrid;

import java.util.List;

public interface CartService {
  List<CartGrid> getAllCart();

  Cart getCartByUserId(Long userId);

  CartGrid getCartById(Long id);

  Cart addProductToCart(CartDetailCreation creation);

  void removeProductFromCart(Long cartDetailId);

  List<CartDetailGrid> getListCartDetailByCartId(Long id);

  void updateQuantity(UpdateQuantity request);
}
