package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.filter.FilterRequest;
import com.hcmute.slayz.dto.request.product.ProductCreation;
import com.hcmute.slayz.dto.request.product.ProductUpdate;
import com.hcmute.slayz.dto.response.filter.FilterResponse;
import com.hcmute.slayz.dto.response.product.ProductReponse;
import com.hcmute.slayz.models.PageResponse;

import java.util.List;

public interface ProductService {
  List<ProductReponse> getAllProductGrid();

  ProductReponse createProduct(ProductCreation creation);

  void updateProduct(Long id, ProductUpdate update);

  ProductReponse getProductById(Long id);

  ProductReponse getProductByVariantId(Long variantId);

  ProductReponse getProductByBarcode(String barcode);

  void deleteProductById(Long id);

  PageResponse filterProduct(FilterRequest request);

  Long salesProduct(Long productId);

  List<ProductReponse> findTop12NewestProduct();

  List<ProductReponse> findRelatedProduct(Long id);

}
