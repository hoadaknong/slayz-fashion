package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Category;
import com.hcmute.slayz.data.repositories.CategoryRepository;
import com.hcmute.slayz.dto.request.category.CategoryCreationRequest;
import com.hcmute.slayz.dto.response.category.CategoryResponse;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.services.CategoryService;
import com.hcmute.slayz.utils.CommonFunctions;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

  private final CategoryRepository categoryRepository;

  @Override
  public CategoryResponse save(CategoryCreationRequest request) {
    Category newCategory = new Category();
    newCategory.setName(request.getName());
    newCategory.setDescription(request.getDescription());
    if (request.getParentId() != null) {
      Optional<Category> getById = this.categoryRepository.findById(request.getParentId());
      if (getById.isPresent()) {
        Category parentCategory = getById.get();
        newCategory.setParentId(parentCategory.getId());
      }
    }
    Category save = this.categoryRepository.save(newCategory);
    return categoryToDto(save);
  }

  @Override
  public PageResponse getAll(Pageable pageable) {
    int totalRecord = this.categoryRepository.findAll().size();
    List<CategoryResponse> categories = categoryRepository.findAllCategory(pageable)
            .stream()
            .map(this::categoryToDto)
            .sorted((category1, category2) -> category2.getId().compareTo(category1.getId()))
            .collect(Collectors.toList());
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1,pageable.getPageSize(),totalRecord,categories);
  }

  @Override
  public void deleteById(Long id) {
    Category category = this.categoryRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy loại sản phẩm!"));
    category.setDeletedDate(new Date());
    this.categoryRepository.save(category);
  }

  @Override
  public void update(Long id, CategoryCreationRequest request) {
    Category category = this.categoryRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy phân loại sản phẩm!"));
    category.setName(request.getName());
    category.setDescription(request.getDescription());
    category.setParentId(request.getParentId());
    this.categoryRepository.save(category);
  }

  @Override
  public CategoryResponse getCategoryById(Long id) {
    Category category = this.categoryRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy phân loại!"));
    CategoryResponse response = new CategoryResponse();
    response.setId(category.getId());
    response.setName(category.getName());
    response.setDescription(category.getDescription());
    if (category.getParentId() != null) {
      Category categoryParent = this.categoryRepository
              .findById(category.getParentId())
              .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy!"));
      response.setParentId(categoryParent.getId());
      response.setParentName(categoryParent.getName());
    }
    return response;
  }

  private CategoryResponse categoryToDto(Category save) {
    CategoryResponse response = new CategoryResponse();
    response.setId(save.getId());
    response.setDescription(save.getDescription());
    response.setName(save.getName());
    if (save.getParentId() != null) {
      Optional<Category> getById = this.categoryRepository.findById(save.getParentId());
      response.setParentName(getById.get().getName());
      response.setParentId(getById.get().getId());
    } else {
      response.setParentName("Mục lớn");
    }
    return response;
  }
}
