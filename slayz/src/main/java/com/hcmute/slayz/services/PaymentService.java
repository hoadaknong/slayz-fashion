package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.payment.PaymentRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

public interface PaymentService {
  String createMomoPaymentLink(PaymentRequest request);

  String createVnpayLink(PaymentRequest paymentRequest, HttpServletRequest req ) throws UnsupportedEncodingException;
}
