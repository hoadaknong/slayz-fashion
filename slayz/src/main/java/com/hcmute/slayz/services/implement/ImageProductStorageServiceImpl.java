package com.hcmute.slayz.services.implement;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.hcmute.slayz.exceptions.FileException;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.services.ImageStorageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class ImageProductStorageServiceImpl implements ImageStorageService {

  private final Cloudinary cloudinary;
  @Override
  public String storeFile(MultipartFile file) {
    String fileName = UUID.randomUUID().toString();
    try{
      Map fileMap = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.asMap("public_id",fileName));
      return fileMap.get("secure_url").toString();
    }catch (Exception e){
      log.info(e.getMessage());
    }
    String urlErrorImage = "http://www.setra.com/hubfs/Sajni/crc_error.jpg";
    return urlErrorImage;
  }

  @Override
  public byte[] readFileContent(String fileName) {
    return null;
  }

  @Override
  public void deleteFile(String name) {
  }
}