package com.hcmute.slayz.services;

import com.hcmute.slayz.data.entities.Address;
import com.hcmute.slayz.dto.request.address.AddressCreation;
import com.hcmute.slayz.models.PageResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AddressService {
  Address createAddress(AddressCreation creation);

  void updateAddress(Long id, AddressCreation creation);

  void deleteAddressById(Long id);

  void setDefaultAddress(Long id);

  PageResponse getAllAddressByUserId(Long id, Pageable pageable);

  List<Address> getAllAddress();

  Address getDefaultAddressByUserId(Long userId);

  Address getAddressById(Long id);
}
