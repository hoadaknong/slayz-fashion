package com.hcmute.slayz.services.implement;


import com.google.gson.JsonObject;
import com.hcmute.slayz.dto.request.payment.PaymentRequest;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.payment.momo.config.Environment;
import com.hcmute.slayz.payment.momo.enums.RequestType;
import com.hcmute.slayz.payment.momo.models.PaymentResponse;
import com.hcmute.slayz.payment.momo.processor.CreateOrderMoMo;
import com.hcmute.slayz.payment.momo.shared.utils.LogUtils;
import com.hcmute.slayz.payment.vnpay.Config;
import com.hcmute.slayz.services.PaymentService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.hcmute.slayz.utils.Constants.WEBHOOK;

@Service
public class PaymentServiceImpl implements PaymentService {
  @Override
  public String createMomoPaymentLink(PaymentRequest request) {
    LogUtils.init();
    Environment environment = Environment.selectEnv("dev");
    String requestId = String.valueOf(System.currentTimeMillis());
    String orderId = String.valueOf(System.currentTimeMillis());
    String notifyURL = WEBHOOK;
    try {
      PaymentResponse captureWalletMoMoResponse = null;
      if (request.getType().equals(0)) {
        captureWalletMoMoResponse = CreateOrderMoMo.process(environment,
                orderId,
                requestId,
                Long.toString(request.getAmount()),
                request.getOrderInfo(),
                request.getReturnUrl(),
                notifyURL,
                request.getExtraData(),
                RequestType.CAPTURE_WALLET,
                Boolean.TRUE
        );
      }
      if (request.getType().equals(1)) {
        captureWalletMoMoResponse = CreateOrderMoMo.process(environment,
                orderId,
                requestId,
                Long.toString(request.getAmount()),
                request.getOrderInfo(),
                request.getReturnUrl(),
                notifyURL,
                request.getExtraData(),
                RequestType.PAY_WITH_ATM,
                Boolean.TRUE
        );
      }
      if (request.getType().equals(2)) {
        captureWalletMoMoResponse = CreateOrderMoMo.process(environment,
                orderId,
                requestId,
                Long.toString(request.getAmount()),
                request.getOrderInfo(),
                request.getReturnUrl(),
                notifyURL,
                request.getExtraData(),
                RequestType.PAY_WITH_CREDIT,
                Boolean.TRUE
        );
      }
      if (request.getType().equals(3)) {
        captureWalletMoMoResponse = CreateOrderMoMo.process(environment,
                orderId,
                requestId,
                Long.toString(request.getAmount()),
                request.getOrderInfo(),
                request.getReturnUrl(),
                notifyURL,
                request.getExtraData(),
                RequestType.PAY_WITH_METHOD,
                Boolean.TRUE
        );
      }
      return captureWalletMoMoResponse.getPayUrl();
    } catch (Exception exception) {
      throw new InvalidFieldException(exception.getMessage());
    }
  }

  @Override
  public String createVnpayLink(PaymentRequest paymentRequest, HttpServletRequest req ) throws UnsupportedEncodingException {
    String vnp_Version = "2.1.0";
    String vnp_Command = "pay";
    String orderType = "NCB";
    long amount = paymentRequest.getAmount() * 100;
    String bankCode = "";

    String vnp_TxnRef = paymentRequest.getExtraData();
    String vnp_IpAddr = Config.getIpAddress(req);
    String vnp_TmnCode = Config.vnp_TmnCode;

    Map<String, String> vnp_Params = new HashMap<>();
    vnp_Params.put("vnp_Version", vnp_Version);
    vnp_Params.put("vnp_Command", vnp_Command);
    vnp_Params.put("vnp_TmnCode", vnp_TmnCode);
    vnp_Params.put("vnp_Amount", String.valueOf(amount));
    vnp_Params.put("vnp_CurrCode", "VND");

    if (bankCode != null && !bankCode.isEmpty()) {
      vnp_Params.put("vnp_BankCode", bankCode);
    }
    vnp_Params.put("vnp_TxnRef", paymentRequest.getExtraData() );
    vnp_Params.put("vnp_OrderInfo", paymentRequest.getOrderInfo());
    vnp_Params.put("vnp_OrderType", orderType);

    String locate = req.getParameter("language");
    if (locate != null && !locate.isEmpty()) {
      vnp_Params.put("vnp_Locale", locate);
    } else {
      vnp_Params.put("vnp_Locale", "sg");
    }
    vnp_Params.put("vnp_ReturnUrl", paymentRequest.getReturnUrl());
    vnp_Params.put("vnp_IpAddr", vnp_IpAddr);

    Calendar cld = Calendar.getInstance(TimeZone.getTimeZone("Etc/GMT+8"));
    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
    String vnp_CreateDate = formatter.format(cld.getTime());
    vnp_Params.put("vnp_CreateDate", vnp_CreateDate);

    cld.add(Calendar.MINUTE, 6000);
    String vnp_ExpireDate = formatter.format(cld.getTime());
    vnp_Params.put("vnp_ExpireDate", vnp_ExpireDate);

    List fieldNames = new ArrayList(vnp_Params.keySet());
    Collections.sort(fieldNames);
    StringBuilder hashData = new StringBuilder();
    StringBuilder query = new StringBuilder();
    Iterator itr = fieldNames.iterator();
    while (itr.hasNext()) {
      String fieldName = (String) itr.next();
      String fieldValue = (String) vnp_Params.get(fieldName);
      if ((fieldValue != null) && (fieldValue.length() > 0)) {
        //Build hash data
        hashData.append(fieldName);
        hashData.append('=');
        hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
        //Build query
        query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
        query.append('=');
        query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
        if (itr.hasNext()) {
          query.append('&');
          hashData.append('&');
        }
      }
    }
    String queryUrl = query.toString();
    String vnp_SecureHash = Config.hmacSHA512(Config.vnp_HashSecret, hashData.toString());
    queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
    return Config.vnp_PayUrl + "?" + queryUrl;
  }
}
