package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Cart;
import com.hcmute.slayz.data.entities.Role;
import com.hcmute.slayz.data.entities.User;
import com.hcmute.slayz.data.repositories.CartRepository;
import com.hcmute.slayz.data.repositories.RoleRepository;
import com.hcmute.slayz.data.repositories.UserRepository;
import com.hcmute.slayz.dto.request.authentication.RegisterRequest;
import com.hcmute.slayz.dto.request.user.ChangePassword;
import com.hcmute.slayz.dto.request.user.ResetPasswordRequest;
import com.hcmute.slayz.dto.request.user.UserUpdate;
import com.hcmute.slayz.dto.response.user.UserListResponse;
import com.hcmute.slayz.dto.response.user.UserResponse;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.exceptions.UnauthorizedException;
import com.hcmute.slayz.exceptions.UpdateInfoUserException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.services.ImageStorageService;
import com.hcmute.slayz.services.UserService;
import com.hcmute.slayz.utils.Constants;
import com.hcmute.slayz.utils.JwtTokenUtil;
import com.hcmute.slayz.utils.RoleName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static com.hcmute.slayz.utils.Constants.*;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

  @Value("${host.domain.fe-client}")
  public String HOST_FRONT_END;

  private final UserRepository userRepository;

  private final RoleRepository roleRepository;

  private final PasswordEncoder passwordEncoder;

  private final ImageStorageService imageStorageService;

  private final CartRepository cartRepository;

  private final JwtTokenUtil jwtTokenUtil;

  private final UserDetailsService userDetailsService;

  @Override
  public PageResponse getAllUser(Pageable pageable, String fullName, String phone, String gender, String email, String status) {
    String genders[] = new String[]{"Nam", "Nữ", "Khác", ""};
    Boolean statuses[] = new Boolean[]{true, false};
    if (gender.equals("Nam")) {
      genders = new String[]{"Nam"};
    }
    if (gender.equals("Nữ")) {
      genders = new String[]{"Nữ"};
    }
    if (gender.equals("Khác")) {
      genders = new String[]{"Khác"};
    }
    if (status.equals("active")) {
      statuses = new Boolean[]{true};
    }
    if (status.equals("disable")) {
      statuses = new Boolean[]{false};
    }
    int totalRecord = userRepository.findAllUser(fullName, phone, genders, email, statuses).size();
    List<UserResponse> userList = userRepository.findAllUser(pageable, fullName, phone, genders, email, statuses)
            .stream()
            .map(this::userToDto)
            .collect(Collectors.toList());
    PageResponse pageResponse = new PageResponse();
    pageResponse.setCurrentPage(pageable.getPageNumber() + 1);
    pageResponse.setPageSize(pageable.getPageSize());
    pageResponse.setTotalPage((int) Math.ceil((double) totalRecord / pageResponse.getPageSize()));
    pageResponse.setDetails(userList);
    return pageResponse;
  }

  @Override
  public UserResponse register(RegisterRequest request) {
    Boolean isEmailExist = isEmailExisted(request.getEmail());
    if (isEmailExist) {
      throw new ResourceNotFoundException("Email đã được sử dụng!");
    }
    if (!request.getPassword().equals(request.getConfirmPassword())) {
      throw new ResourceNotFoundException("Mật khẩu xác thực không đúng!");
    }
    User user = new User();
    Role role = roleRepository
            .findByName(RoleName.ROLE_USER.name())
            .orElseThrow(() -> new UnauthorizedException("Lỗi phân quyền hệ thống!"));
    user.setFullName(request.getFullName());
    user.setEmail(request.getEmail());
    user.setPassword(passwordEncoder.encode(request.getPassword()));
    user.setEnable(true);
    user.setRole(role);
    user.setPhone("");
    user.setGender("");
    user.setPhoto(DEFAULT_USER_PHOTO_PATH);
    User save = this.userRepository.save(user);
    Cart cart = new Cart();
    cart.setUserId(save.getId());
    cart.setGrandTotal(0L);
    this.cartRepository.save(cart);
    return userToDto(save);
  }

  @Override
  public UserListResponse getAllUserByStatus(Boolean status) {
    List<User> userList = this.userRepository.findAllByEnable(status);
    List<UserResponse> result = new ArrayList<>();
    Long amountMale = 0L;
    Long amountFemale = 0L;
    Long amountUndefined = 0L;
    for (User user : userList) {
      result.add(userToDto(user));
      try {
        if (user.getGender().equals("Nam")) {
          amountMale++;
        } else if (user.getGender().equals("Nữ")) {
          amountFemale++;
        } else {
          amountUndefined++;
        }
      } catch (Exception exception) {
        amountUndefined++;
      }
    }
    Collections.reverse(result);
    UserListResponse userListResponse = new UserListResponse();
    userListResponse.setListUser(result);
    userListResponse.setAmount((long) result.size());
    userListResponse.setAmountFemale(amountFemale);
    userListResponse.setAmountMale(amountMale);
    userListResponse.setAmountUndefined(amountUndefined);
    return userListResponse;
  }

  @Override
  public UserResponse getUserByUserId(Long id) {
    User userFound = this.userRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    return userToDto(userFound);
  }

  @Override
  public void updateUser(Long id, UserUpdate update) {
    User userFound = this.userRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    if (isPhoneNumberExisted(id, update.getPhone())) {
      throw new UpdateInfoUserException("Số điện thoại đã được sử dụng!");
    }
    userFound.setPhone(update.getPhone());
    userFound.setFullName(update.getFullName());
    userFound.setGender(update.getGender());
    userFound.setDob(update.getDob());
    try {
      MultipartFile file = (MultipartFile) update.getPhoto();
      String fileName = this.imageStorageService.storeFile(file);
      userFound.setPhoto(fileName);
    } catch (Exception exception) {
      log.info(exception.getMessage());
    }
    this.userRepository.save(userFound);
  }

  @Override
  public void updateUserStatus(Long id, Boolean status) {
    User userFound = this.userRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    userFound.setEnable(status);
    this.userRepository.save(userFound);
  }

  @Override
  public void updateRoleUser(Long idUser, Long roleId) {
    User userFound = this.userRepository
            .findById(idUser)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    Role roleFound = this.roleRepository
            .findById(roleId)
            .orElseThrow(() -> new ResourceNotFoundException("Lỗi phần quyền!"));
    userFound.setRole(roleFound);
    this.userRepository.save(userFound);
  }

  @Override
  public void updatePassword(Long idUser, ChangePassword request) {
    User user = this.userRepository
            .findById(idUser)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    //Validate password
    if (!passwordEncoder.matches(request.getOldPassword(), user.getPassword())) {
      throw new UpdateInfoUserException("Mật khẩu cũ không đúng!");
    }
    if (!request.getNewPassword().equals(request.getConfirmPassword())) {
      throw new UpdateInfoUserException("Mật khẩu xác nhận không đúng!");
    }
    if (passwordEncoder.matches(request.getNewPassword(), user.getPassword())) {
      throw new UpdateInfoUserException("Mật khẩu mới không được giống mật khẩu cũ!");
    }
    Matcher matcher = Constants.pattern.matcher(request.getConfirmPassword());
    if (!matcher.matches()) {
      throw new UpdateInfoUserException("Mật khẩu chưa đủ mạnh.\n" +
              "Mật khẩu phải chứa các kí tự chữ thường, chữ in hoa, số 0-9 và kí tự đặc biệt !,@,#,&,(,)\n" +
              "Vui lòng thử lại!");
    }
    user.setPassword(passwordEncoder.encode(request.getConfirmPassword()));
    this.userRepository.save(user);
  }

  @Override
  public void sendMailToResetPassword(String key) {
    User user = this.userRepository
            .findByEmailOrPhone(key)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng trong sử dụng email: " + key));
    String token = this.jwtTokenUtil.generateAccessToken(user);
    user.setResetPasswordToken(token);
    this.userRepository.save(user);
    String link = HOST_FRONT_END + "/reset_password?token=" + token;
    ExecutorService emailExecutor = Executors.newSingleThreadExecutor();
    emailExecutor.execute(() -> {
      try {
        MailSenderServiceImpl.sendMailToResetPassword(user.getEmail(), "ĐĂT LẠI MẬT KHẨU", user.getFullName(), link);
      } catch (MessagingException e) {
        throw new RuntimeException(e);
      }
    });
    emailExecutor.shutdown();
  }

  @Override
  public void resetPassword(ResetPasswordRequest request) {
    User user;
    String[] jwtSubject = jwtTokenUtil.getSubject(request.getToken()).split(",");
    user = (User) userDetailsService.loadUserByUsername(jwtSubject[1]);
    user.setPassword(passwordEncoder.encode(request.getPasswordNew()));
    user.setResetPasswordToken(null);
    this.userRepository.save(user);
  }

  private Boolean isPhoneNumberExisted(Long id, String phone) {
    Optional<User> userByPhone = this.userRepository.findByPhone(phone);
    if (userByPhone.isPresent()) {
      User user = userByPhone.get();
      return !user.getId().equals(id);
    }
    return false;
  }

  private Boolean isEmailExisted(String email) {
    Optional<User> userByEmail = this.userRepository.findByEmail(email);
    return userByEmail.isPresent();
  }

  private UserResponse userToDto(User user) {
    UserResponse userResponse = new UserResponse();
    userResponse.setId(user.getId());
    userResponse.setDob(user.getDob());
    userResponse.setEmail(user.getEmail());
    userResponse.setFullName(user.getFullName());
    userResponse.setGender(user.getGender());
    userResponse.setPhone(user.getPhone());
    userResponse.setStatus(user.getEnable());
    userResponse.setRoleId(user.getRole().getId());
    userResponse.setRoleName(user.getRole().getDescription());
    if (user.getPhoto() == null || user.getPhoto().trim().equals(DEFAULT_USER_PHOTO_PATH)) {
      if (user.getGender() != null) {
        if (user.getGender().equals("Nam")) {
          userResponse.setPhoto(MAN_USER_PHOTO_PATH);
        } else if (user.getGender().equals("Nữ")) {
          userResponse.setPhoto(WOMAN_USER_PHOTO_PATH);
        } else {
          userResponse.setPhoto(DEFAULT_USER_PHOTO_PATH);
        }
      } else {
        userResponse.setPhoto("https://res.cloudinary.com/dfnnmsofg/image/upload/v1677652646/win10-user-account-default-picture_zponae.png");
      }
    } else {
      userResponse.setPhoto(user.getPhoto());
    }
    return userResponse;
  }
}
