package com.hcmute.slayz.services;

import com.hcmute.slayz.data.entities.Banner;
import com.hcmute.slayz.dto.request.banner.BannerCreation;

import java.util.List;

public interface BannerService {
  List<Banner> findAllBanner();
  List<Banner> findAllBannerByType(String type);
  Banner findBannerById(Long id);
  void deleteBannerById(Long id);
  void updateBannerById(Long id, BannerCreation creation);
  void createNewBanner(BannerCreation creation);
}
