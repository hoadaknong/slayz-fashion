package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.color.ColorCreation;
import com.hcmute.slayz.dto.response.color.ColorResponse;
import com.hcmute.slayz.models.PageResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ColorService {
  ColorResponse createColor(ColorCreation obj);

  void updateColor(Long id, ColorCreation obj);

  ColorResponse getColorById(Long id);

  void deleteColorById(Long id);

  PageResponse getAllColor(Pageable pageable);
}
