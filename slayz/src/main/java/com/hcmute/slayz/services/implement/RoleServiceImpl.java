package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Role;
import com.hcmute.slayz.data.repositories.RoleRepository;
import com.hcmute.slayz.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

  private final RoleRepository repository;

  @Override
  public List<Role> getAllRoles() {
    return repository.findAll();
  }
}
