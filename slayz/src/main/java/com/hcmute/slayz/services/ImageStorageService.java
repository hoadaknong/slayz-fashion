package com.hcmute.slayz.services;

import org.springframework.web.multipart.MultipartFile;

public interface ImageStorageService {
  String storeFile(MultipartFile file);

  byte[] readFileContent(String fileName);

  void deleteFile(String name);
}
