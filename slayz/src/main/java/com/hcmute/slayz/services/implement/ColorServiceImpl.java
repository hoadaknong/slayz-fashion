package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Color;
import com.hcmute.slayz.data.repositories.ColorRepository;
import com.hcmute.slayz.dto.request.color.ColorCreation;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.dto.response.color.ColorResponse;
import com.hcmute.slayz.services.ColorService;
import com.hcmute.slayz.utils.CommonFunctions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ColorServiceImpl implements ColorService {

  private final ColorRepository colorRepository;

  @Override
  public ColorResponse createColor(ColorCreation obj) {
    Color newColor = new Color();
    newColor.setColor(obj.getName());
    newColor.setCode(obj.getCode());
    Color save = this.colorRepository.save(newColor);
    return colorToDto(save);
  }

  @Override
  public void updateColor(Long id, ColorCreation obj) {
    Color newColor = this.colorRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy màu sắc!"));
    newColor.setColor(obj.getName());
    newColor.setCode(obj.getCode());
    Color save = this.colorRepository.save(newColor);
  }

  @Override
  public ColorResponse getColorById(Long id) {
    Color color = this.colorRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException());
    return colorToDto(color);
  }

  @Override
  public void deleteColorById(Long id) {
    Color color = this.colorRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy màu sắc!"));
    color.setDeletedDate(new Date());
    this.colorRepository.save(color);
  }

  @Override
  public PageResponse getAllColor(Pageable pageable) {
    int totalRecord = this.colorRepository.findAll().size();
    List<ColorResponse> colors = colorRepository.findAllColor(pageable)
            .stream()
            .map(this::colorToDto)
            .collect(Collectors.toList());
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1,pageable.getPageSize(),totalRecord,colors);
  }

  private ColorResponse colorToDto(Color color) {
    ColorResponse response = new ColorResponse();
    response.setId(color.getId());
    response.setCode(color.getCode());
    response.setName(color.getColor());
    return response;
  }
}
