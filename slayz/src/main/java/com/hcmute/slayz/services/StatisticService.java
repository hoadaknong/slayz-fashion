package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.response.statistic.LineChartElement;
import com.hcmute.slayz.dto.response.statistic.PieChartElement;
import com.hcmute.slayz.dto.response.statistic.ReviewMetricResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface StatisticService {
  Integer getTotalUserData();

  Long getTotalQuantityProductData();

  Integer getTotalProductData();

  Integer getTotalQuantityInvoiceByStatusData(String status);

  Integer getTotalQuantityInvoiceData();

  Long getTotalQuantitySaleProductData();

  List<PieChartElement> getPieChartAllInvoiceData();

  List<LineChartElement> getGrandTotalInvoiceBySevenDayRecentData();

  List<LineChartElement> getQuantityInvoiceBySevenDayRecentData();

  Long getRevenueData();

  ReviewMetricResponse getReviewMetric();
}