package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.invoice.InvoiceCreation;
import com.hcmute.slayz.dto.request.invoice.UpdatePaymentStatus;
import com.hcmute.slayz.dto.response.invoice.InvoiceDetailResponse;
import com.hcmute.slayz.dto.response.invoice.InvoiceResponse;
import com.hcmute.slayz.models.PageResponse;
import org.springframework.data.domain.Pageable;

import java.io.ByteArrayInputStream;
import java.io.Writer;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface InvoiceService {
  InvoiceResponse createNewInvoice(InvoiceCreation creation);

  void updatePaymentStatus(Long id, UpdatePaymentStatus request);

  PageResponse getAllInvoice(Pageable pageable);

  InvoiceResponse getInvoiceById(Long id);

  PageResponse getAllInvoiceByStatus(String status, Pageable pageable);

  PageResponse getAllInvoiceByUserId(Long userId,Pageable pageable,String status, String paymentStatus);

  List<InvoiceDetailResponse> getAllInvoiceDetailByInvoiceId(Long invoiceId);

  void updateInvoiceStatus(Long id, UpdatePaymentStatus request);

  ByteArrayInputStream writeDataToExcel(String status);
  ByteArrayInputStream writeDataToExcel(String status, LocalDateTime startDate, LocalDateTime endDate);
}
