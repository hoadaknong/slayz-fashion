package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.*;
import com.hcmute.slayz.data.repositories.*;
import com.hcmute.slayz.dto.response.statistic.LineChartElement;
import com.hcmute.slayz.dto.response.statistic.PieChartElement;
import com.hcmute.slayz.dto.response.statistic.ReviewMetricResponse;
import com.hcmute.slayz.services.StatisticService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class StatisticServiceImpl implements StatisticService {

  private final ProductRepository productRepository;

  private final InvoiceRepository invoiceRepository;

  private final InvoiceDetailRepository invoiceDetailRepository;

  private final UserRepository userRepository;

  private final VariantRepository variantRepository;
  private final ReviewRepository reviewRepository;

  @Override
  public Integer getTotalUserData() {
    List<User> userList = this.userRepository.findAll();
    userList
            .stream()
            .filter(user -> user.getDeletedDate() == null);
    return userList.size();
  }

  @Override
  public Long getTotalQuantityProductData() {
//    List<Variant> variantList = this.variantRepository.findAll();
//    Long sum = 0L;
//    for (Variant variant : variantList) {
//      sum += variant.getInventory();
//    }
//    return sum;
    return (long) productRepository.findAll().size();
  }

  @Override
  public Integer getTotalProductData() {
    List<Product> productList = this.productRepository
            .findAll()
            .stream()
            .filter(product -> product.getDeletedDate() == null)
            .collect(Collectors.toList());
    return productList.size();
  }

  @Override
  public Integer getTotalQuantityInvoiceByStatusData(String status) {
    List<Invoice> invoiceList = this.invoiceRepository
            .findAllByStatus(status)
            .stream()
            .filter(invoice -> invoice.getDeletedDate() == null)
            .collect(Collectors.toList());
    return invoiceList.size();
  }

  @Override
  public Integer getTotalQuantityInvoiceData() {
    List<Invoice> invoiceList = this.invoiceRepository
            .findAll()
            .stream()
            .filter(invoice -> invoice.getDeletedDate() == null)
            .collect(Collectors.toList());
    return invoiceList.size();
  }

  @Override
  public Long getTotalQuantitySaleProductData() {
    Long sum = 0L;
    List<InvoiceDetail> invoiceDetailList = this.invoiceDetailRepository.findAll();
    for (InvoiceDetail invoiceDetail : invoiceDetailList) {
      sum += invoiceDetail.getQuantity();
    }
    return sum;
  }

  @Override
  public List<PieChartElement> getPieChartAllInvoiceData() {
    List<Invoice> allInvoice = this.invoiceRepository.findAll();
    List<Invoice> invoicePending = this.invoiceRepository.findAllByStatus("PENDING");
    List<Invoice> invoiceInProgress = this.invoiceRepository.findAllByStatus("IN PROGRESS");
    List<Invoice> invoiceCompleted = this.invoiceRepository.findAllByStatus("COMPLETED");
    List<Invoice> invoiceCancelled = this.invoiceRepository.findAllByStatus("CANCELLED");
    List<PieChartElement> result = new ArrayList<>();
    double pendingPercent = (((double) invoicePending.size() / allInvoice.size()) * 100);
    result.add(new PieChartElement("Đang chờ xử lý",
            Long.parseLong(String.valueOf(invoicePending.size())),
            round(pendingPercent, 0) + "%"));
    double inProgressPercent = (((double) invoiceInProgress.size() / allInvoice.size()) * 100);
    result.add(new PieChartElement("Đã xử lý",
            Long.parseLong(String.valueOf(invoiceInProgress.size())),
            round(inProgressPercent, 0) + "%"));
    double completedPercent = (((double) invoiceCompleted.size() / allInvoice.size()) * 100);
    result.add(new PieChartElement("Đã hoàn thành",
            Long.parseLong(String.valueOf(invoiceCompleted.size())),
            round(completedPercent, 0) + "%"));
    double cancelledPercent = (((double) invoiceCancelled.size() / allInvoice.size()) * 100);
    result.add(new PieChartElement("Đã hủy",
            Long.parseLong(String.valueOf(invoiceCancelled.size())),
            round(cancelledPercent, 0) + "%"));
    return result;
  }

  @Override
  public List<LineChartElement> getGrandTotalInvoiceBySevenDayRecentData() {
    LocalDateTime now = LocalDateTime.now();
    List<LocalDateTime> sevenDayRecent = new ArrayList<>();
    List<LineChartElement> data = new ArrayList<>();
    for (int i = 6; i >= 0; i--) {
      sevenDayRecent.add(now.minusDays(i));
    }
    List<Invoice> invoiceList = this.invoiceRepository.findAll();
    invoiceList.removeIf(value -> value.getStatus() == "CANCELLED");
    for (LocalDateTime date : sevenDayRecent) {
      Long sum = 0L;
      for (Invoice invoice : invoiceList) {
        LocalDateTime invoiceDate =
                Instant.ofEpochMilli(invoice.getCreatedDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        if (date.getDayOfMonth() == invoiceDate.getDayOfMonth()) {
          sum += invoice.getGrandTotal();
        }
      }
      data.add(new LineChartElement(date.getDayOfMonth() + "-" + date.getMonthValue() + "-" + date.getYear(), sum));
    }
    return data;
  }

  @Override
  public List<LineChartElement> getQuantityInvoiceBySevenDayRecentData() {
    LocalDateTime now = LocalDateTime.now();
    List<LocalDateTime> sevenDayRecent = new ArrayList<>();
    List<LineChartElement> data = new ArrayList<>();
    for (int i = 6; i >= 0; i--) {
      sevenDayRecent.add(now.minusDays(i));
    }
    List<Invoice> invoiceList = this.invoiceRepository.findAll();
    invoiceList.removeIf(value -> value.getStatus() == "CANCELLED");
    for (LocalDateTime date : sevenDayRecent) {
      Long sum = 0L;
      for (Invoice invoice : invoiceList) {
        LocalDateTime invoiceDate =
                Instant.ofEpochMilli(invoice.getCreatedDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
        if (date.getDayOfMonth() == invoiceDate.getDayOfMonth()) {
          sum++;
        }
      }
      data.add(new LineChartElement(date.getDayOfMonth() + "-" + date.getMonthValue() + "-" + date.getYear(), sum));
    }
    return data;
  }

  @Override
  public Long getRevenueData() {
    Long sum = 0L;
    List<Invoice> invoiceList = this.invoiceRepository.findAllByStatus("COMPLETED");
    for (Invoice invoice : invoiceList) {
      sum += invoice.getGrandTotal();
    }
    return sum;
  }

  @Override
  public ReviewMetricResponse getReviewMetric() {
    ReviewMetricResponse response = new ReviewMetricResponse();
    response.setOneStar(reviewRepository.findAllByRating(1).size());
    response.setTwoStar(reviewRepository.findAllByRating(2).size());
    response.setThreeStar(reviewRepository.findAllByRating(3).size());
    response.setFourStar(reviewRepository.findAllByRating(4).size());
    response.setFiveStar(reviewRepository.findAllByRating(5).size());
    response.setReviewCount(reviewRepository.findAll().size());
    double total = 0;
    for(Review review : reviewRepository.findAll()){
      total += Double.parseDouble(review.getRating().toString());
    }
    response.setAvgVote(round((double)total / response.getReviewCount(),1));

    return response;
  }

  public static double round(double value, int places) {
    long factor = (long) Math.pow(10, places);
    return (double) Math.round(value * factor) / factor;
  }
}
