package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.*;
import com.hcmute.slayz.data.repositories.*;
import com.hcmute.slayz.dto.request.cart.CartDetailCreation;
import com.hcmute.slayz.dto.request.cart.UpdateQuantity;
import com.hcmute.slayz.dto.response.cart.CartDetailGrid;
import com.hcmute.slayz.dto.response.cart.CartGrid;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.services.CartService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.hcmute.slayz.utils.Constants.*;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class CartServiceImpl implements CartService {

  private final CartRepository cartRepository;

  private final CartDetailRepository cartDetailRepository;

  private final UserRepository userRepository;

  private final ProductRepository productRepository;

  private final VariantRepository variantRepository;

  private final SizeRepository sizeRepository;

  private final ColorRepository colorRepository;

  private final CategoryRepository categoryRepository;

  @Override
  public List<CartGrid> getAllCart() {
    List<Cart> cartList = this.cartRepository.findAll();
    Collections.reverse(cartList);
    return cartList
            .stream()
            .filter(cart -> cart.getDeletedDate() == null)
            .map(this::cartToCartGrid)
            .collect(Collectors.toList());
  }

  @Override
  public Cart getCartByUserId(Long userId) {
    User userFound = this.userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    Optional<Cart> cartFoundByUser = this.cartRepository.findByUserId(userId);
    if (!cartFoundByUser.isPresent()) {
      Cart cart = new Cart();
      cart.setUserId(userId);
      Cart save = this.cartRepository.save(cart);
      return save;
    }
    return cartFoundByUser.get();
  }

  @Override
  public CartGrid getCartById(Long id) {
    Cart cart = this.cartRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy giỏ hàng!"));
    return cartToCartGrid(cart);
  }

  @Override
  public Cart addProductToCart(CartDetailCreation creation) {
    Optional<Cart> cartFound = this.cartRepository.findById(creation.getCartId());
    if (cartFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy giỏ hàng!");
    }
    Optional<Variant> variantFound = this.variantRepository.findById(creation.getVariantId());
    if (variantFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
    }
    Optional<Product> productFound = findProductByVariantId(creation.getVariantId());
    if (productFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
    }
    Variant variant = variantFound.get();
    if (variant.getInventory() <= creation.getQuantity()) {
      throw new ResourceNotFoundException("Sản phẩm hiện không đủ!");
    }
    List<CartDetail> cartDetailList = this.cartDetailRepository.findByCartId(creation.getCartId());
    Boolean isProductExist = false;
    for (CartDetail cartDetail : cartDetailList) {
      if (cartDetail.getVariantId().equals(creation.getVariantId()) && cartDetail.getDeletedDate() == null) {
        log.info("Check item existed in cart...");
        if(cartDetail.getQuantity() + creation.getQuantity() > variant.getInventory()){
          throw new ResourceNotFoundException("Sản phẩm hiện không đủ!");
        }
        cartDetail.setQuantity(cartDetail.getQuantity() + creation.getQuantity());
        cartDetail.setTotal(productFound.get().getStandCost() * cartDetail.getQuantity());
        this.cartDetailRepository.save(cartDetail);
        isProductExist = true;
      }
    }
    if (!isProductExist) {
      CartDetail cartDetail = new CartDetail();
      cartDetail.setCartId(creation.getCartId());
      cartDetail.setQuantity(creation.getQuantity());
      cartDetail.setTotal(productFound.get().getStandCost() * creation.getQuantity());
      cartDetail.setVariantId(creation.getVariantId());
      this.cartDetailRepository.save(cartDetail);
    }
    updateGrandTotalOfCart(creation.getCartId());
    return this.cartRepository.findById(creation.getCartId()).orElseThrow(()-> new ResourceNotFoundException("Không tìm thấy giỏ hàng!"));
  }

  @Override
  public void removeProductFromCart(Long cartDetailId) {
    CartDetail cartDetail = this.cartDetailRepository
            .findById(cartDetailId)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    cartDetail.setDeletedDate(new Date());
    this.cartDetailRepository.save(cartDetail);
  }

  @Override
  public List<CartDetailGrid> getListCartDetailByCartId(Long id) {
    List<CartDetail> cartDetailList = this.cartDetailRepository.findByCartId(id);
    Collections.reverse(cartDetailList);
    return cartDetailList
            .stream()
            .filter(cartDetail -> cartDetail.getDeletedDate() == null && isProductAvailable(cartDetail))
            .map(this::cartDetailToCartDetailGrid)
            .collect(Collectors.toList());
  }

  @Override
  public void updateQuantity(UpdateQuantity request) {
    CartDetail cartDetail = this.cartDetailRepository.findById(request.getCartDetailId()).orElseThrow(() -> new ResourceNotFoundException("Sản phẩm không có sẵn!"));
    Product product = this.findProductByVariantId(cartDetail.getVariantId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm hoặc sản phẩm đã hết hàng!"));
    Variant variant = variantRepository.findById(cartDetail.getVariantId()).orElseThrow(()->new ResourceNotFoundException("Không tìm thấy sản phẩm"));
    if(request.getQuantity() >= variant.getInventory()){
      throw new ResourceNotFoundException("Sản phẩm trong kho không đủ!!!");
    }
    cartDetail.setQuantity(request.getQuantity());
    cartDetail.setTotal(cartDetail.getQuantity() * product.getStandCost());
    this.cartDetailRepository.save(cartDetail);
    updateGrandTotalOfCart(cartDetail.getCartId());
  }

  private CartDetailGrid cartDetailToCartDetailGrid(CartDetail cartDetail) {
    CartDetailGrid cartDetailGrid = new CartDetailGrid();
    cartDetailGrid.setId(cartDetail.getId());
    Optional<Product> product = findProductByVariantId(cartDetail.getVariantId());
    product.ifPresent(value -> {
      cartDetailGrid.setPrice(value.getStandCost());
      cartDetailGrid.setProductId(value.getId());
      cartDetailGrid.setProductName(value.getName());
      cartDetailGrid.setBarcode(value.getBarcode());
      cartDetailGrid.setImageProduct(THUMBNAIL_PATH + value.getImage());
    });
    Optional<Variant> variant = this.variantRepository.findById(cartDetail.getVariantId());
    variant.ifPresent(value -> {
      Optional<Size> size = this.sizeRepository.findById(value.getSizeId());
      size.ifPresent(valueSize -> cartDetailGrid.setSize(valueSize.getSize()));
      Optional<Color> color = this.colorRepository.findById(value.getColorId());
      color.ifPresent(valueColor -> cartDetailGrid.setStyle(valueColor.getColor()));
      cartDetailGrid.setImageProduct(VARIANT_PATH + value.getImage());
    });
    cartDetailGrid.setQuantity(cartDetail.getQuantity());
    cartDetailGrid.setTotal(cartDetail.getTotal());
    return cartDetailGrid;
  }

  private CartGrid cartToCartGrid(Cart cart) {
    CartGrid cartGrid = new CartGrid();
    List<CartDetail> cartDetailList = this.cartDetailRepository.findByCartId(cart.getId());
    cartDetailList.removeIf(value -> value.getDeletedDate() != null);
    Long totalQuantity = 0L;
    Long grandTotal = 0L;
    for (CartDetail cartDetail : cartDetailList) {
      Product productFound = findProductByVariantId(cartDetail.getVariantId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
      grandTotal += productFound.getStandCost() * cartDetail.getQuantity();
      totalQuantity += cartDetail.getQuantity();
    }
    cartGrid.setId(cart.getId());
    cartGrid.setTotalQuantity(totalQuantity);
    cartGrid.setQuantityProduct((long) cartDetailList.size());
    cartGrid.setGrandTotal(grandTotal);
    User userFound = this.userRepository.findById(cart.getUserId()).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    cartGrid.setUserPhoto(USER_PHOTO_PATH + userFound.getPhoto());
    cartGrid.setFullName(userFound.getFullName());
    cartGrid.setUserId(userFound.getId());

    return cartGrid;
  }

  public Optional<Product> findProductByVariantId(Long id) {
    Optional<Variant> variantFound = this.variantRepository.findById(id);
    Optional<Product> productFound = Optional.empty();
    if (variantFound.isPresent()) {
      Optional<Product> productFoundByVariantId = this.productRepository.findById(variantFound.get().getProductId());
      if (productFoundByVariantId.isPresent()) {
        productFound = productFoundByVariantId;
      }
    }
    return productFound;
  }

  public void updateGrandTotalOfCart(Long cartId) {
    Long grandTotal = 0L;
    Optional<Cart> cartFound = this.cartRepository.findById(cartId);
    if (cartFound.isPresent()) {
      Cart cart = cartFound.get();
      List<CartDetail> cartDetailListAfterAdd = this.cartDetailRepository.findByCartId(cart.getId());
      cartDetailListAfterAdd.removeIf(value -> value.getDeletedDate() != null);
      for (CartDetail cartDetail : cartDetailListAfterAdd) {
        grandTotal += cartDetail.getTotal();
      }
      cart.setGrandTotal(grandTotal);
      this.cartRepository.save(cart);
    }
  }

  private Boolean isProductAvailable(CartDetail cartDetail) {
    Optional<Product> product = this.findProductByVariantId(cartDetail.getVariantId());
    if (product.isPresent()) {
      if (product.get().getDeletedDate() == null) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }
}
