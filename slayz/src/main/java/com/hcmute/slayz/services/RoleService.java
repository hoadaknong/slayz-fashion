package com.hcmute.slayz.services;

import com.hcmute.slayz.data.entities.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAllRoles();
}
