package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Color;
import com.hcmute.slayz.data.entities.Product;
import com.hcmute.slayz.data.entities.Size;
import com.hcmute.slayz.data.entities.Variant;
import com.hcmute.slayz.data.repositories.ColorRepository;
import com.hcmute.slayz.data.repositories.ProductRepository;
import com.hcmute.slayz.data.repositories.SizeRepository;
import com.hcmute.slayz.data.repositories.VariantRepository;
import com.hcmute.slayz.dto.request.variant.SizeVariant;
import com.hcmute.slayz.dto.request.variant.VariantCreation;
import com.hcmute.slayz.dto.request.variant.VariantUpdateRequest;
import com.hcmute.slayz.dto.response.variant.VariantResponse;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.services.ImageStorageService;
import com.hcmute.slayz.services.VariantService;
import com.hcmute.slayz.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class VariantServiceImpl implements VariantService {


  private final VariantRepository variantRepository;

  private final ProductRepository productRepository;

  private final ColorRepository colorRepository;

  private final SizeRepository sizeRepository;

  private final ImageStorageService imageStorageService;

  @Override
  public void createNewVariant(VariantCreation creation) {
    Variant variant;
    for(SizeVariant size: creation.getSizeVariants()){
      variant = new Variant();
      variant.setColorId(creation.getColorId());
      variant.setSizeId(size.getSizeId());
      variant.setInventory(size.getInventory());
      variant.setImage(creation.getImage());
      variant.setProductId(creation.getProductId());
      variantRepository.save(variant);
    }
  }

  @Override
  public void updateVariant(VariantCreation creation) {
    Variant variant;
    for(SizeVariant size: creation.getSizeVariants()){
      variant = variantRepository
              .findVariantByProductIdAndColorIdAndSizeId(creation.getProductId(), creation.getColorId(), size.getSizeId())
              .orElseThrow(()-> new ResourceNotFoundException("Không tìm thấy biến sản phẩm!"));
      variant.setInventory(size.getInventory());
      variant.setImage(creation.getImage());
      variantRepository.save(variant);
    }
  }

  @Override
  public List<VariantResponse> getAllVariantByProductId(Long productId) {
    Product productFound = this.productRepository
            .findById(productId)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
    List<Variant> variantList = this.variantRepository.findByProductId(productFound.getId());
    return variantList.stream()
            .filter(variant -> variant.getDeletedDate() == null)
            .map(this::variantToVariantResponse)
            .collect(Collectors.toList());
  }

  @Override
  public List<VariantResponse> getListVariantByProductIdAndColorId(Long productId, Long colorId) {
    try {
      Product productFound = this.productRepository
              .findById(productId)
              .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy sản phẩm!"));
      Color colorFound = this.colorRepository
              .findById(colorId)
              .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy màu sắc"));
      List<VariantResponse> result = new ArrayList<>();
      List<Variant> sizeVariant = this.variantRepository.findAllSizeVariantByProductId(productId);
      List<Variant> sizeListByProductIdAndColorId = this.variantRepository.findAllByProductIdAndColorId(productId, colorId);
      for (Variant variant : sizeVariant) {
        VariantResponse sizeChosen = variantToVariantResponse(variant);
        for (Variant availableVariant : sizeListByProductIdAndColorId) {
          if (availableVariant.getColorId() == colorId &&
                  availableVariant.getSizeId() == variant.getSizeId() &&
                  availableVariant.getInventory() > 0) {
            sizeChosen.setIsChosen(true);
          }
        }
        result.add(sizeChosen);
      }
      return result;
    } catch (Exception exception) {
      throw new ResourceNotFoundException(exception.getMessage());
    }
  }

  @Override
  public List<VariantResponse> getAllColorByProductId(Long productId) {
    try {
      Optional<Product> productFound = this.productRepository.findById(productId);
      if (productFound.isPresent()) {
        List<Variant> variantList = this.variantRepository.findAllColorVariantByProductId(productId);
        List<VariantResponse> result = new ArrayList<>();
        for (Variant variant : variantList) {
          result.add(variantToVariantResponse(variant));
        }
        return result;
      }
    } catch (Exception exception) {
      throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
    }
    throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
  }

  @Override
  public List<VariantResponse> getAllSizeByProductId(Long productId) {
    try {
      Optional<Product> productFound = this.productRepository.findById(productId);
      if (productFound.isPresent()) {
        List<Variant> variantList = this.variantRepository.findAllSizeVariantByProductId(productId);
        List<VariantResponse> result = new ArrayList<>();
        for (Variant variant : variantList) {
          result.add(variantToVariantResponse(variant));
        }
        return result;
      }
    } catch (Exception exception) {
      throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
    }
    throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
  }

  @Override
  public Variant getVariantByProductIdAndColorIdAndSizeId(Long productId, Long colorId, Long sizeId) {
    List<Variant> result = this.variantRepository.findAllByProductIdAndColorIdAndSizeId(productId, colorId, sizeId);
    if (result.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
    }
    return result.get(0);
  }

  @Override
  public Variant updateVariant(Long id, VariantUpdateRequest request) {
    Variant variantFound = this.variantRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy biến thể sản phẩm"));
    variantFound.setInventory(request.getQuantity());
    variantFound.setColorId(request.getColorId());
    variantFound.setSizeId(request.getSizeId());
    try {
      MultipartFile file = (MultipartFile) request.getImage();
      String fileName = this.imageStorageService.storeFile(file);
      variantFound.setImage(fileName);
    } catch (Exception e) {
    }
    return this.variantRepository.save(variantFound);
  }

  private VariantResponse variantToVariantResponse(Variant variant) {
    VariantResponse response = new VariantResponse();
    response.setId(variant.getId());
    response.setColorId(variant.getColorId());
    response.setImage(Constants.VARIANT_PATH + variant.getImage());
    response.setInventory(variant.getInventory());
    response.setProductId(variant.getProductId());
    response.setSizeId(variant.getSizeId());
    response.setIsChosen(false);
    Optional<Size> sizeFound = this.sizeRepository.findById(variant.getSizeId());
    Optional<Color> colorFound = this.colorRepository.findById(variant.getColorId());
    if (sizeFound.isPresent() && colorFound.isPresent()) {
      response.setColorName(colorFound.get().getColor());
      response.setSizeName(sizeFound.get().getSize());
    }
    return response;
  }
}
