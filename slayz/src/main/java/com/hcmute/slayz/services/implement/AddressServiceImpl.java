package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Address;
import com.hcmute.slayz.data.entities.User;
import com.hcmute.slayz.data.repositories.AddressRepository;
import com.hcmute.slayz.data.repositories.UserRepository;
import com.hcmute.slayz.dto.request.address.AddressCreation;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.services.AddressService;
import com.hcmute.slayz.utils.CommonFunctions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class AddressServiceImpl implements AddressService {

  private final AddressRepository addressRepository;

  private final UserRepository userRepository;

  @Override
  public Address createAddress(@NotNull AddressCreation creation) {
    Optional<User> user = this.userRepository.findById(creation.getUserId());
    if (user.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thất người dùng!");
    }
    Address address = new Address();
    address.setIsDefault(false);
    address.setFullName(creation.getFullName());
    address.setPhone(creation.getPhone());
    address.setAddressLine(creation.getAddressLine());
    address.setDistrict(creation.getDistrict());
    address.setProvince(creation.getProvince());
    address.setWard(creation.getWard());
    address.setUserId(creation.getUserId());
    address.setProvinceId(creation.getProvinceId());
    address.setDistrictId(creation.getDistrictId());
    address.setWardId(creation.getWardId());
    Address save = this.addressRepository.save(address);
    return save;
  }

  @Override
  public void updateAddress(Long id, AddressCreation creation) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = (User) authentication.getPrincipal();
    Optional<User> user = this.userRepository.findById(creation.getUserId());
    if (user.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy người dùng");
    }
    Optional<Address> addressFound = this.addressRepository.findById(id);
    if (addressFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy địa chỉ ");
    }
    if (addressFound.get().getUserId() != currentUser.getId() && currentUser.getRole().getId() != 2L) {
      throw new ResourceNotFoundException("Không tìm thấy địa chỉ của người dùng");
    }
    Address address = addressFound.get();
    address.setFullName(creation.getFullName());
    address.setPhone(creation.getPhone());
    address.setAddressLine(creation.getAddressLine());
    address.setDistrict(creation.getDistrict());
    address.setProvince(creation.getProvince());
    address.setWard(creation.getWard());
    address.setUserId(creation.getUserId());
    address.setProvinceId(creation.getProvinceId());
    address.setDistrictId(creation.getDistrictId());
    address.setWardId(creation.getWardId());
    Address save = this.addressRepository.save(address);
  }

  @Override
  public void deleteAddressById(Long id) {
    Address address = this.addressRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy địa chỉ!"));
    address.setDeletedDate(new Date());
    this.addressRepository.save(address);
  }

  @Override
  public void setDefaultAddress(Long id) {
    Optional<Address> addressFound = this.addressRepository.findById(id);
    if (addressFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy địa chỉ!");
    }
    Address address = addressFound.get();
    Long userId = address.getUserId();
    List<Address> addressListByUser = this.addressRepository.findAllByUserId(userId);
    if (addressListByUser.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy địa chỉ!");
    }
    for (Address addressItem : addressListByUser) {
      addressItem.setIsDefault(false);
      this.addressRepository.save(addressItem);
    }
    address.setIsDefault(true);
    this.addressRepository.save(address);
  }

  @Override
  public PageResponse getAllAddressByUserId(Long id, Pageable pageable) {
    int totalRecord = addressRepository.findAllByUserId(id).size();
    List<Address> addresses = addressRepository.findAllByUserId(id, pageable);
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1,pageable.getPageSize(),totalRecord,addresses);
  }

  @Override
  public List<Address> getAllAddress() {
    List<Address> result = this.addressRepository.findAll();
    return result
            .stream()
            .filter(address -> address.getDeletedDate() == null)
            .collect(Collectors.toList());
  }

  @Override
  public Address getDefaultAddressByUserId(Long userId) {
    List<Address> addressListByUser = this.addressRepository.findAllByUserId(userId);
    if (addressListByUser.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy địa chỉ!");
    }
    for (Address address : addressListByUser) {
      if (address.getIsDefault() == true) {
        return address;
      }
    }
    return addressListByUser.get(0);
  }

  @Override
  public Address getAddressById(Long id) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = (User) authentication.getPrincipal();
    Optional<Address> addressFound = this.addressRepository.findById(id);
    if (addressFound.isPresent()) {
      if (addressFound.get().getUserId() == currentUser.getId() || currentUser.getRole().getName().equals("ROLE_ADMIN")) {
        return addressFound.get();
      }
    }
    throw new ResourceNotFoundException("Không tìm thấy địa chỉ!");
  }
}
