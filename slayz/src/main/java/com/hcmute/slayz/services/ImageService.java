package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.image.ImageCreationRequest;
import com.hcmute.slayz.models.KeyValue;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ImageService {
  List<KeyValue<Long, String>> getAllImageByProductId(Long productId);

  void saveImageProduct(ImageCreationRequest request);

  void deleteImageById(Long id);
}
