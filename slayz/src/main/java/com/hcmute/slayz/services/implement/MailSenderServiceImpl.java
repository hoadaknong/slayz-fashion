package com.hcmute.slayz.services.implement;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
@RequiredArgsConstructor
public class MailSenderServiceImpl {
  private static JavaMailSender emailSender;
  private static SpringTemplateEngine templateEngine;

  @Autowired
  public MailSenderServiceImpl(JavaMailSender emailSender,SpringTemplateEngine templateEngine) {
    MailSenderServiceImpl.emailSender = emailSender;
    MailSenderServiceImpl.templateEngine = templateEngine;
  }

  public static void sendMailToResetPassword(String to, String subject, String fullName, String link) throws MessagingException {
    MimeMessage message = emailSender.createMimeMessage();
    MimeMessageHelper helper = new MimeMessageHelper(message,
            MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
            StandardCharsets.UTF_8.name());
    Context context = new Context();
    Map<String, Object> props = new HashMap<String, Object>();
    props.put("fullName",fullName);
    props.put("link",link);
    context.setVariables(props);
    String html = templateEngine.process("mail_reset_pasword", context);
    helper.setTo(to);
    helper.setText(html, true);
    helper.setSubject(subject);
    helper.setFrom("phamdinhquochoa101@gmail.com");
    emailSender.send(message);
  }
}
