package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.*;
import com.hcmute.slayz.data.repositories.*;
import com.hcmute.slayz.dto.request.invoice.InvoiceCreation;
import com.hcmute.slayz.dto.request.invoice.UpdatePaymentStatus;
import com.hcmute.slayz.dto.response.invoice.InvoiceDetailResponse;
import com.hcmute.slayz.dto.response.invoice.InvoiceResponse;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.services.InvoiceService;
import com.hcmute.slayz.utils.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class InvoiceServiceImpl implements InvoiceService {
  private String pattern = "dd-MM-yyyy";
  private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
  private final CommonFunctions commonFunctions;

  private final InvoiceRepository invoiceRepository;

  private final UserRepository userRepository;

  private final InvoiceDetailRepository invoiceDetailRepository;

  private final VariantRepository variantRepository;

  private final CartRepository cartRepository;

  private final CartDetailRepository cartDetailRepository;

  private final AddressRepository addressRepository;

  private final CartServiceImpl cartService;

  private final SizeRepository sizeRepository;

  private final ColorRepository colorRepository;

  private final RoleRepository roleRepository;

  @Override
  public InvoiceResponse createNewInvoice(InvoiceCreation creation) {
    Optional<User> userFound = this.userRepository.findById(creation.getUserId());
    if (userFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy người dùng!");
    }
    Optional<Cart> cartFound = this.cartRepository.findByUserId(creation.getUserId());
    if (cartFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy giỏ hàng!");
    }
    Optional<Address> addressFound = this.addressRepository.findById(creation.getAddressId());
    if (addressFound.isEmpty()) {
      throw new ResourceNotFoundException("Không tìm thấy địa chỉ!");
    }
    Invoice invoice = new Invoice();
    Invoice saveInvoice = null;
    try {
      invoice.setId(creation.getUserId());
      invoice.setStatus(InvoiceStatus.PENDING.name());
      invoice.setDeliveryStatus(DeliveryStatus.PENDING.name());
      invoice.setPaymentMethod(creation.getPaymentMethod());
      invoice.setPaymentStatus(creation.getPaymentStatus());
      invoice.setUserId(creation.getUserId());
      saveInvoice = this.invoiceRepository.save(invoice);
    } catch (Exception exception) {
      log.error(exception.getMessage());
      throw new InvalidFieldException("Lỗi đặt hàng!");
    }
    Long invoiceId = saveInvoice.getId();
    Long[] listDetail = creation.getListDetailId();
    if (listDetail == null) {
      this.invoiceRepository.deleteById(invoiceId);
      throw new ResourceNotFoundException("Hãy chọn sản phẩm để thanh toán!");
    }
    for (int i = 0; i < listDetail.length; i++) {
      Optional<CartDetail> cartDetailFound = this.cartDetailRepository.findById(listDetail[i]);
      if (cartDetailFound.isEmpty()) {
        this.invoiceRepository.deleteById(invoiceId);
        throw new ResourceNotFoundException("Hãy chọn sản phẩm để thanh toán!");
      }
      CartDetail cartDetail = cartDetailFound.get();
      Optional<Variant> variantFound = this.variantRepository.findById(cartDetail.getVariantId());
      if (variantFound.isEmpty()) {
        this.invoiceRepository.deleteById(invoiceId);
        throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
      }
      Variant variant = variantFound.get();
      if (cartDetail.getQuantity() >= variant.getInventory()) {
        this.invoiceRepository.deleteById(invoiceId);
        throw new ResourceNotFoundException("Sản phẩm trong kho không đủ!");
      }
      Optional<Product> productFound = commonFunctions.getProductByVariantId(variant.getId());
      Product product = null;
      if (productFound.isEmpty() || productFound.get().getDeletedDate() != null) {
        this.invoiceRepository.deleteById(invoiceId);
        throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
      }
    }
    Long grandTotalInvoice = 0L;
    for (int i = 0; i < listDetail.length; i++) {
      Optional<CartDetail> cartDetailFound = this.cartDetailRepository.findById(listDetail[i]);
      if (cartDetailFound.isEmpty()) {
        throw new ResourceNotFoundException("Không tìm thấy sản phẩm!");
      }
      CartDetail cartDetail = cartDetailFound.get();
      Optional<Variant> variantFound = this.variantRepository.findById(cartDetail.getVariantId());
      Variant variant = variantFound.get();
      Optional<Product> productFound = commonFunctions.getProductByVariantId(variant.getId());
      Product product = productFound.get();
      InvoiceDetail invoiceDetail = new InvoiceDetail();
      invoiceDetail.setInvoiceId(invoiceId);
      invoiceDetail.setQuantity(cartDetail.getQuantity());
      invoiceDetail.setUnitPrice(product.getStandCost());
      invoiceDetail.setTotal(invoiceDetail.getQuantity() * invoiceDetail.getUnitPrice());
      invoiceDetail.setVariantId(cartDetail.getVariantId());
      this.invoiceDetailRepository.save(invoiceDetail);
      this.cartDetailRepository.deleteById(cartDetail.getId());
      grandTotalInvoice += invoiceDetail.getTotal();
      variant.setInventory(variant.getInventory() - cartDetail.getQuantity());
      this.variantRepository.save(variant);
    }
    saveInvoice.setGrandTotal(grandTotalInvoice);
    saveInvoice.setAddressId(creation.getAddressId());
    saveInvoice.setNote(creation.getNote());
    saveInvoice.setDeliveryFee(creation.getDeliveryFee());
    saveInvoice.setDeliveryMethod(creation.getDeliveryMethodName());
    this.invoiceRepository.save(saveInvoice);
    Long grandTotal = 0L;
    Cart cart = cartFound.get();
    List<CartDetail> cartDetailList = this.cartDetailRepository.findByCartId(cart.getId());
    for (CartDetail cartDetail : cartDetailList) {
      grandTotal += cartDetail.getTotal();
    }
    cart.setGrandTotal(grandTotal);
    this.cartRepository.save(cart);
    return invoiceToDto(saveInvoice);
  }

  @Override
  public void updatePaymentStatus(Long id, UpdatePaymentStatus request) {
    Invoice invoice = this.invoiceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy đơn hàng!"));
    invoice.setPaymentStatus(request.getStatus());
    this.invoiceRepository.save(invoice);
  }

  @Override
  public PageResponse getAllInvoice(Pageable pageable) {
    Integer totalRecord = this.invoiceRepository.findAll().size();
    List<Invoice> invoiceList = this.invoiceRepository.findAllInvoice(pageable);
    List<InvoiceResponse> invoices = invoiceList.stream().filter(value -> value.getDeletedDate() == null).map(this::invoiceToDto).sorted((invoice1, invoice2) -> invoice2.getId().compareTo(invoice1.getId())).collect(Collectors.toList());
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1, pageable.getPageSize(), totalRecord, invoices);
  }

  @Override
  public InvoiceResponse getInvoiceById(Long id) {
    Optional<Role> role = this.roleRepository.findById(2L);
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = (User) authentication.getPrincipal();
    Invoice invoiceFound = this.invoiceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy đơn hàng!"));
    if (invoiceFound.getUserId() == currentUser.getId() || currentUser.getRole().getId() == role.get().getId()) {
      InvoiceResponse response = invoiceToDto(invoiceFound);
      return response;
    }
    throw new ResourceNotFoundException("Không tìm thấy hóa đơn!");
  }

  @Override
  public PageResponse getAllInvoiceByStatus(String status, Pageable pageable) {
    Integer totalRecord = this.invoiceRepository.findAllByStatus(status).size();
    List<Invoice> invoiceList = this.invoiceRepository.findAllByStatus(status, pageable);
    List<InvoiceResponse> invoices = invoiceList.stream().map(this::invoiceToDto).sorted((invoice1, invoice2) -> invoice2.getId().compareTo(invoice1.getId())).collect(Collectors.toList());
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1, pageable.getPageSize(), totalRecord, invoices);
  }

  @Override
  public PageResponse getAllInvoiceByUserId(Long userId, Pageable pageable, String status, String paymentStatus) {
    User userFound = this.userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy người dùng!"));
    String[] statuses = new String[]{"PENDING", "IN PROGRESS", "COMPLETED", "CANCELLED"};
    String[] paymentStatuses = new String[]{"PAID", "PENDING"};
    if (!status.equals("all") && !status.equals("ALL")) {
      statuses = new String[]{status};
    }
    if (!paymentStatus.equals("all") && !paymentStatus.equals("ALL")) {
      paymentStatuses = new String[]{paymentStatus};
    }
    Integer totalRecord = this.invoiceRepository.findAllByUserId(userId, statuses, paymentStatuses).size();
    List<Invoice> invoiceList = this.invoiceRepository.findAllByUserId(userId, pageable, statuses, paymentStatuses);
    List<InvoiceResponse> invoices = invoiceList.stream().map(this::invoiceToDto).collect(Collectors.toList());
    return CommonFunctions.pageResponse(pageable.getPageNumber() + 1, pageable.getPageSize(), totalRecord, invoices);
  }

  @Override
  public List<InvoiceDetailResponse> getAllInvoiceDetailByInvoiceId(Long invoiceId) {
    Invoice invoiceFound = this.invoiceRepository.findById(invoiceId).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy hóa đơn!"));
    List<InvoiceDetail> invoiceDetailList = this.invoiceDetailRepository.findAllByInvoiceId(invoiceId);
    return invoiceDetailList.stream().filter(invoiceDetail -> invoiceDetail.getDeletedDate() == null).map(this::invoiceDetailToResponse).collect(Collectors.toList());
  }

  @Override
  public void updateInvoiceStatus(Long id, UpdatePaymentStatus request) {
    Invoice invoiceFound = this.invoiceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy hóa đơn!"));
    invoiceFound.setStatus(request.getStatus());
    if (request.getStatus().equals("COMPLETED")) {
      invoiceFound.setPaymentStatus("PAID");
    }
    this.invoiceRepository.save(invoiceFound);
  }

  @Override
  public ByteArrayInputStream writeDataToExcel(String status) {
    List<Invoice> invoiceList = new ArrayList<>();
    if (status.equals("all")) {
      invoiceList = this.invoiceRepository.findAll();
    } else {
      invoiceList = this.invoiceRepository.findAllByStatus(status);
    }
    List<InvoiceResponse> invoices = invoiceList.stream().map(this::invoiceToDto).collect(Collectors.toList());
    return ExcelHelper.invoiceToExcel(invoices, status);
  }

  @Override
  public ByteArrayInputStream writeDataToExcel(String status, LocalDateTime startDate, LocalDateTime endDate) {
    Date _startDate = Date.from(startDate.atZone(ZoneId.systemDefault()).toInstant());
    Date _endDate = Date.from(endDate.atZone(ZoneId.systemDefault()).toInstant());
    System.out.println(_startDate.toString());
    System.out.println(_endDate.toString());
    List<Invoice> invoiceList = new ArrayList<>();
    if (status.equals("all")) {
      invoiceList = this.invoiceRepository.findAllByStatusAndRangeDate(_startDate, _endDate);
    } else {
      invoiceList = this.invoiceRepository.findAllByStatusAndRangeDate(status, _startDate, _endDate);
    }
    List<InvoiceResponse> invoices = invoiceList.stream().map(this::invoiceToDto).collect(Collectors.toList());
    return ExcelHelper.invoiceToExcel(invoices, status);
  }

  private InvoiceResponse invoiceToDto(Invoice invoice) {
    InvoiceResponse invoiceResponse = new InvoiceResponse();
    invoiceResponse.setId(invoice.getId());
    Optional<Address> addressFound = this.addressRepository.findById(invoice.getAddressId());
    if (addressFound.isPresent()) {
      Address address = addressFound.get();
      invoiceResponse.setAddress(address.getProvince() + ", " + address.getDistrict() + ", " + address.getWard() + ", " + address.getAddressLine());
      invoiceResponse.setFullName(address.getFullName());
      invoiceResponse.setPhone(address.getPhone());
    }
    Optional<User> userFound = this.userRepository.findById(invoice.getUserId());
    if (userFound.isPresent()) {
      User user = userFound.get();
      invoiceResponse.setFullNameUser(user.getFullName());
      invoiceResponse.setPhoneUser(user.getPhone());
      invoiceResponse.setEmailUser(user.getEmail());
      invoiceResponse.setUserPhoto(Constants.USER_PHOTO_PATH + user.getPhoto());
    }
    invoiceResponse.setPaymentMethod(invoice.getPaymentMethod());
    invoiceResponse.setPaymentStatus(invoice.getPaymentStatus());
    invoiceResponse.setStatus(invoice.getStatus());
    invoiceResponse.setDelivery(invoice.getDeliveryMethod());
    invoiceResponse.setDate(simpleDateFormat.format(invoice.getCreatedDate()));
    invoiceResponse.setDeliveryFee(invoice.getDeliveryFee());
    invoiceResponse.setUserId(invoice.getUserId());
    invoiceResponse.setAddressId(invoice.getAddressId());
    invoiceResponse.setNote(invoice.getNote());
    invoiceResponse.setUpdateDate(invoice.getUpdateDate());
    try {
      invoiceResponse.setTotal(invoice.getGrandTotal() + invoice.getDeliveryFee());
    } catch (Exception exception) {
      invoiceResponse.setTotal(invoice.getGrandTotal());
    }
    return invoiceResponse;
  }

  private InvoiceDetailResponse invoiceDetailToResponse(InvoiceDetail detail) {
    InvoiceDetailResponse response = new InvoiceDetailResponse();
    Optional<Product> productFound = this.cartService.findProductByVariantId(detail.getVariantId());
    if (productFound.isPresent()) {
      Product product = productFound.get();
      response.setImage(Constants.THUMBNAIL_PATH + product.getImage());
      response.setName(product.getName());
      response.setSlug(product.getBarcode());
    }
    response.setPrice(detail.getUnitPrice());
    response.setQuantity(detail.getQuantity());
    response.setTotal(detail.getTotal());
    response.setId(detail.getId());
    response.setVariantId(detail.getVariantId());
    Optional<Variant> variantFound = this.variantRepository.findById(detail.getVariantId());
    if (variantFound.isPresent()) {
      Variant variant = variantFound.get();
      Optional<Color> colorFound = this.colorRepository.findById(variant.getColorId());
      Optional<Size> sizeFound = this.sizeRepository.findById(variant.getSizeId());
      if (colorFound.isPresent()) {
        response.setColor(colorFound.get().getColor());
      }
      if (sizeFound.isPresent()) {
        response.setSize(sizeFound.get().getSize());
      }
    }
    return response;
  }
}
