package com.hcmute.slayz.services;

import com.hcmute.slayz.data.entities.Category;
import com.hcmute.slayz.dto.request.category.CategoryCreationRequest;
import com.hcmute.slayz.dto.response.category.CategoryResponse;
import com.hcmute.slayz.models.PageResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CategoryService {
  CategoryResponse save(CategoryCreationRequest request);

  PageResponse getAll(Pageable pageable);

  void deleteById(Long id);

  void update(Long id, CategoryCreationRequest request);

  CategoryResponse getCategoryById(Long id);
}
