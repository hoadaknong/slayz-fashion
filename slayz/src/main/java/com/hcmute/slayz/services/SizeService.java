package com.hcmute.slayz.services;

import com.hcmute.slayz.data.entities.Size;
import com.hcmute.slayz.dto.request.size.SizeCreation;
import com.hcmute.slayz.dto.response.size.SizeResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SizeService {
  SizeResponse createSize(SizeCreation obj);

  void updateSize(Long id, SizeCreation obj);

  SizeResponse getSizeById(Long id);

  void deleteSizeById(Long id);

  List<SizeResponse> getAllSize();
}
