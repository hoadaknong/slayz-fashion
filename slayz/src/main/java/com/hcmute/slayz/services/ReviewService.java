package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.filter.FilterReviewRequest;
import com.hcmute.slayz.dto.request.review.ReviewCreationRequest;
import com.hcmute.slayz.dto.response.filter.FilterReviewResponse;
import com.hcmute.slayz.dto.response.review.ReviewOverViewResponse;
import com.hcmute.slayz.dto.response.review.ReviewResponse;
import com.hcmute.slayz.models.PageResponse;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;

public interface ReviewService {
  void createReview(ReviewCreationRequest request);

  void changeReviewStatus(Long reviewId, Boolean status);

  void deleteReview(Long reviewId);

  ReviewResponse getReviewById(Long id);

  PageResponse getAllReview(Pageable pageable);

  FilterReviewResponse getAllReviewByProductId(FilterReviewRequest request);

  List<ReviewResponse> getAllReviewByUserId(Long userId);

  List<ReviewResponse> findAllReviewByInvoiceDetailId(Long invoiceDetailId);

  BigDecimal getAverageRatingByProductId(Long productId);

  List<ReviewOverViewResponse> getOverviewReviewByProductId(Long productId);
}
