package com.hcmute.slayz.services;

import com.hcmute.slayz.dto.request.authentication.AuthenticationRequest;
import com.hcmute.slayz.dto.request.authentication.RegisterRequest;
import com.hcmute.slayz.dto.response.authentication.AuthenticationResponse;
import com.hcmute.slayz.dto.response.user.UserResponse;

public interface AuthenticationService {
  UserResponse register(RegisterRequest request);

  AuthenticationResponse login(AuthenticationRequest request);

  AuthenticationResponse loginForAdmin(AuthenticationRequest request);

  AuthenticationResponse refreshToken(String refreshToken);

  Boolean checkResetPasswordTokenAvailable(String token);
}
