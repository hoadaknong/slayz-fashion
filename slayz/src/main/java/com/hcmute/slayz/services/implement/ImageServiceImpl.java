package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Image;
import com.hcmute.slayz.data.repositories.ImageRepository;
import com.hcmute.slayz.dto.request.image.ImageCreationRequest;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.models.KeyValue;
import com.hcmute.slayz.services.ImageService;
import com.hcmute.slayz.utils.Constants;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

  private final ImageRepository imageRepository;

  @Override
  public List<KeyValue<Long, String>> getAllImageByProductId(Long productId) {
    List<Image> imageListByProduct = this.imageRepository.findByProductId(productId);
    imageListByProduct.removeIf(value -> value.getDeletedDate() != null);
    List<KeyValue<Long, String>> result = new ArrayList<>();
    for (Image image : imageListByProduct) {
      result.add(new KeyValue<>(image.getId(), Constants.IMAGES_PATH + image.getPath()));
    }
    return result;
  }

  @Override
  public void saveImageProduct(ImageCreationRequest request) {
    Image image = new Image();
    image.setPath(request.getImageName());
    image.setProductId(request.getProductId());
    this.imageRepository.save(image);
  }

  @Override
  public void deleteImageById(Long id) {
    Image imageFound = this.imageRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy hình ảnh!"));
    imageFound.setDeletedDate(new Date());
    this.imageRepository.save(imageFound);
  }
}
