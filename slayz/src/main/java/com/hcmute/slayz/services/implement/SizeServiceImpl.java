package com.hcmute.slayz.services.implement;

import com.hcmute.slayz.data.entities.Size;
import com.hcmute.slayz.data.repositories.SizeRepository;
import com.hcmute.slayz.dto.request.size.SizeCreation;
import com.hcmute.slayz.dto.response.size.SizeResponse;
import com.hcmute.slayz.exceptions.ResourceNotFoundException;
import com.hcmute.slayz.services.SizeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class SizeServiceImpl implements SizeService {

  private final SizeRepository sizeRepository;

  @Override
  public SizeResponse createSize(SizeCreation obj) {
    Size size = new Size();
    size.setSize(obj.getName());
    size.setDescription(obj.getDescription());
    Size save = this.sizeRepository.save(size);
    return sizeToDto(save);
  }

  @Override
  public void updateSize(Long id, SizeCreation obj) {
    Size sizeFound = this.sizeRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy kích cỡ!"));
    sizeFound.setSize(obj.getName());
    sizeFound.setDescription(obj.getDescription());
    this.sizeRepository.save(sizeFound);
  }

  @Override
  public SizeResponse getSizeById(Long id) {
    Size sizeFound = this.sizeRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy kích cỡ!"));
    return sizeToDto(sizeFound);
  }

  @Override
  public void deleteSizeById(Long id) {
    Size sizeFound = this.sizeRepository
            .findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Không tìm thấy kích cỡ!"));
    sizeFound.setDeletedDate(new Date());
    this.sizeRepository.save(sizeFound);
  }

  @Override
  public List<SizeResponse> getAllSize() {
    List<Size> listSize = this.sizeRepository.findAllSize();
    return listSize
            .stream()
            .filter(size -> size.getDeletedDate() == null)
            .map(this::sizeToDto)
            .collect(Collectors.toList());
  }

  private SizeResponse sizeToDto(Size size) {
    SizeResponse response = new SizeResponse();
    response.setId(size.getId());
    response.setName(size.getSize());
    response.setDescription(size.getDescription());
    return response;
  }

}
