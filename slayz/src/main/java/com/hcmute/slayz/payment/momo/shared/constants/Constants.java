package com.hcmute.slayz.payment.momo.shared.constants;

public final class Constants {
  public static final String LANGUAGE_VI = "vi";
  public static final String LANGUAGE_EN = "en";
}
