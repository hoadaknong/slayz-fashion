package com.hcmute.slayz.payment.momo.models;


import com.hcmute.slayz.payment.momo.enums.ConfirmRequestType;

public class ConfirmResponse extends Response {
  private Long amount;
  private Long transId;
  private String requestId;
  private ConfirmRequestType requestType;
}
