package com.hcmute.slayz.controllers;

import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.StatisticService;
import com.hcmute.slayz.utils.Constants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("api/v1/statistic")
@Api(tags = "Statistic API")
@RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
@RequiredArgsConstructor
public class StatisticController {

  private final StatisticService statisticService;

  @ApiOperation(value = "Get total user account")
  @GetMapping("/total_user")
  public ResponseEntity<?> getTotalUserData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Số lượng người dùng!",
                    this.statisticService.getTotalUserData())
    );
  }

  @ApiOperation(value = "Get total product quantity")
  @GetMapping("/total_product_quantity")
  public ResponseEntity<?> getTotalQuantityProductData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Số lượng tất cả sản phẩm!",
                    this.statisticService.getTotalQuantityProductData())
    );
  }

  @ApiOperation(value = "Get total product")
  @GetMapping("/total_product")
  public ResponseEntity<?> getTotalProductData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Số lượng đầu sản phẩm!",
                    this.statisticService.getTotalProductData())
    );
  }

  @ApiOperation(value = "Get total quantity invoice by status")
  @GetMapping("/quantity_invoice/{status}")
  public ResponseEntity<?> getTotalQuantityInvoiceByStatusData(@PathVariable("status") String status) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Số lượng đơn hàng theo trạng thái!",
                    this.statisticService.getTotalQuantityInvoiceByStatusData(status))
    );
  }

  @ApiOperation(value = "Get total invoice quantity data")
  @GetMapping("/total_quantity_invoice")
  public ResponseEntity<?> getTotalQuantityInvoiceData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Số lượng tất cả đơn hàng!",
                    this.statisticService.getTotalQuantityInvoiceData())
    );
  }

  @ApiOperation(value = "Get total product quantity sale")
  @GetMapping("/total_quantity_sale")
  public ResponseEntity<?> getTotalQuantitySaleProductData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Số lượng sản phẩm đã bán!",
                    this.statisticService.getTotalQuantitySaleProductData())
    );
  }

  @ApiOperation(value = "Get pie chart invoice data")
  @GetMapping("/pie_chart_invoice")
  public ResponseEntity<?> getPieChartAllInvoiceData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Dữ liệu biểu đồ tròn của tất cả đơn hàng!",
                    this.statisticService.getPieChartAllInvoiceData())
    );
  }

  @ApiOperation(value = "Get data grand total invoice seven day recent line chart")
  @GetMapping("/line_chart/grand_total")
  public ResponseEntity<?> getGrandTotalInvoiceBySevenDayRecentData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Dữ liệu doanh thu 7 ngày gần đây!",
                    this.statisticService.getGrandTotalInvoiceBySevenDayRecentData())
    );
  }

  @ApiOperation(value = "Get data quantity nvoice seven day recent line chart")
  @GetMapping("/line_chart/quantity_invoice")
  public ResponseEntity<?> getQuantityInvoiceBySevenDayRecentData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Dữ liệu số lượng đơn hàng 7 ngày gần đây!",
                    this.statisticService.getQuantityInvoiceBySevenDayRecentData())
    );
  }

  @ApiOperation(value = "Get revenue data")
  @GetMapping("/revenue")
  public ResponseEntity<?> getRevenueData() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Dữ liệu doanh thu!",
                    this.statisticService.getRevenueData())
    );
  }

  @ApiOperation(value = "Get review metric")
  @GetMapping("/review")
  public ResponseEntity<?> getReviewMetric() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Dữ liệu doanh thu!",
                    this.statisticService.getReviewMetric())
    );
  }
}
