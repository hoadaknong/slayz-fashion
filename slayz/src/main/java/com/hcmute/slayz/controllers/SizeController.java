package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.size.SizeCreation;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.SizeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

import static com.hcmute.slayz.utils.Constants.PATH_VERSION;

@RestController
@RequestMapping("api/v1/size")
@Api(tags = "Size API")
@RequiredArgsConstructor
public class SizeController {

  private final SizeService sizeService;

  @ApiOperation(value = "Get all size")
  @GetMapping()
  public ResponseEntity<?> getAllSize() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách kích cỡ!",
                    this.sizeService.getAllSize()
            ));
  }

  @ApiOperation(value = "Get size by id")
  @GetMapping("{id}")
  public ResponseEntity<?> getSizeById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin kích cỡ!",
                    this.sizeService.getSizeById(id)
            ));
  }

  @ApiOperation(value = "Create new size")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @PostMapping()
  public ResponseEntity<?> createSize(@RequestBody SizeCreation obj) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo mới thông tin kích cỡ thành công!",
                    this.sizeService.createSize(obj)
            ));
  }

  @ApiOperation(value = "Update size information")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @PutMapping("{id}")
  public ResponseEntity<?> updateSize(@PathVariable("id") Long id, @RequestBody SizeCreation obj) {
    this.sizeService.updateSize(id, obj);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật thông tin kích cỡ thành công!",
                    null
            ));
  }

  @ApiOperation(value = "Delete size by id")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @DeleteMapping("{id}")
  public ResponseEntity<?> deleteSize(@PathVariable("id") Long id) {
    this.sizeService.deleteSizeById(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Xóa thông tin kích cỡ thành công!",
                    null
            ));
  }
}
