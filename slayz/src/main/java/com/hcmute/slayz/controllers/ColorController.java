package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.color.ColorCreation;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.ColorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

import static com.hcmute.slayz.utils.Constants.PATH_VERSION;

@RestController
@RequestMapping("api/v1/color")
@Api(tags = "Color API")
@RequiredArgsConstructor
public class ColorController {

  private final ColorService colorService;

  @ApiOperation(value = "Get all category")
  @GetMapping()
  public ResponseEntity<?> getAllColor(@RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                       @RequestParam(required = false, defaultValue = "100") Integer pageSize) {
    Pageable pageable;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách màu sắc!",
                    this.colorService.getAllColor(pageable)
            )
    );
  }

  @ApiOperation(value = "Get category by id")
  @GetMapping(path = "{id}")
  public ResponseEntity<?> getColorById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin màu sắc!",
                    this.colorService.getColorById(id)
            )
    );
  }

  @ApiOperation(value = "Create new color")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @PostMapping()
  public ResponseEntity<?> createColor(@RequestBody ColorCreation obj) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo mới thông tin màu sắc thành công!",
                    this.colorService.createColor(obj)
            )
    );
  }

  @ApiOperation(value = "Update color color information")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @PutMapping("{id}")
  public ResponseEntity<?> updateColor(@PathVariable("id") Long id, @RequestBody ColorCreation obj) {
    this.colorService.updateColor(id, obj);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật thông tin màu sắc thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Delete color by id")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @DeleteMapping("{id}")
  public ResponseEntity<?> deleteColorById(@PathVariable("id") Long id) {
    this.colorService.deleteColorById(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Xóa thông tin màu sắc thành công",
                    null
            )
    );
  }
}
