package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.filter.FilterReviewRequest;
import com.hcmute.slayz.dto.request.review.ReviewCreationRequest;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.ReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping(path = "api/v1/review")
@Api(tags = "Review API")
@RequiredArgsConstructor
public class ReviewController {

  private final ReviewService reviewService;

  @ApiOperation(value = "Get all review")
  @GetMapping
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> getAllReview(@RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                        @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
    Pageable pageable;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách review sản phẩm!",
                    reviewService.getAllReview(pageable))
    );
  }

  @ApiOperation(value = "Get by id")
  @GetMapping("{id}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity getById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin chi tiết đánh giá!",
                    reviewService.getReviewById(id))
    );
  }

  @ApiOperation(value = "Get all review by user id")
  @GetMapping("user/{userId}")
  public ResponseEntity<?> getAllReviewByUserId(@PathVariable("userId") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách review sản phẩm theo người dùng!",
                    reviewService.getAllReviewByUserId(id))
    );
  }

  @ApiOperation(value = "Get all review by product id")
  @PostMapping("product")
  public ResponseEntity<?> getAllReviewByProductId(@RequestBody FilterReviewRequest request) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách review sản phẩm theo từng sản phẩm!",
                    reviewService.getAllReviewByProductId(request))
    );
  }

  @ApiOperation(value = "Create new review")
  @PostMapping
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF","ROLE_USER"})
  public ResponseEntity<?> createNewReview(@RequestBody ReviewCreationRequest request) {
    reviewService.createReview(request);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo mới review!",
                    null)
    );
  }

  @ApiOperation(value = "Enable review")
  @PostMapping("{id}/enable")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> enableReview(@PathVariable("id") Long reviewId) {
    reviewService.changeReviewStatus(reviewId, true);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thay đổi trạng thái review thành công!",
                    null)
    );
  }

  @ApiOperation(value = "Disable review")
  @PostMapping("{id}/disable")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> disableReview(@PathVariable("id") Long reviewId) {
    reviewService.changeReviewStatus(reviewId, false);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thay đổi trạng thái review thành công!",
                    null)
    );
  }

  @ApiOperation(value = "Delete review")
  @DeleteMapping("{id}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> deleteReview(@PathVariable("id") Long id) {
    reviewService.deleteReview(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "",
                    null)
    );
  }

  @ApiOperation(value = "Get average of product's rating")
  @GetMapping("avg_rating/{productId}")
  public ResponseEntity<?> getAvgRatingOfProduct(@PathVariable("productId") Long productId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Giá trị đánh giá sản phẩm trung bình!",
                    reviewService.getAverageRatingByProductId(productId))
    );
  }

  @ApiOperation(value = "Get overview of product's rating")
  @GetMapping("overview_rating/{productId}")
  public ResponseEntity<?> getOverviewRatingOfProduct(@PathVariable("productId") Long productId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thống kê đánh giá tổng quan của sản phẩm!",
                    reviewService.getOverviewReviewByProductId(productId))
    );
  }

  @ApiOperation(value = "Get all review by invoice detail id")
  @GetMapping("invoice_detail_id/{invoiceDetailId}")
  public ResponseEntity<?> getAllReviewByInvoiceDetailId(@PathVariable("invoiceDetailId") Long invoiceDetailId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách đánh giá sản phẩm!",
                    reviewService.findAllReviewByInvoiceDetailId(invoiceDetailId))
    );
  }
}
