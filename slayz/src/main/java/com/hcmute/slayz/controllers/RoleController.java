package com.hcmute.slayz.controllers;

import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("api/v1/role")
@Api(tags = "Role API")
@RequiredArgsConstructor
@RolesAllowed({"ROLE_ADMIN"})
public class RoleController {

  private final RoleService roleService;

  @ApiOperation(value = "Get all roles")
  @GetMapping
  public ResponseEntity<?> getAllRoles() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách phân quyền!",
                    roleService.getAllRoles()
            )
    );
  }
}
