package com.hcmute.slayz.controllers;

import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.ImageService;
import com.hcmute.slayz.services.ImageStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/v1")
@Api(tags = "Image API")
@RequiredArgsConstructor
public class ImageController {

  private final ImageService imageService;
  private final ImageStorageService imageStorageService;

  @ApiOperation(value = "Get link image product by id")
  @GetMapping(path = "image_product/{id}")
  public ResponseEntity<?> getListImageByProductId(@PathVariable("id") Long id) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách đường dẫn hình ảnh!",
                    this.imageService.getAllImageByProductId(id)
            ));
  }

  @ApiOperation(value = "Save image")
  @PostMapping(path = "image")
  public ResponseEntity<?> saveImage(@RequestPart("file") MultipartFile file) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.OK,
                    "Đường dẫn hình ảnh!",
                    imageStorageService.storeFile(file)
            ));
  }
}
