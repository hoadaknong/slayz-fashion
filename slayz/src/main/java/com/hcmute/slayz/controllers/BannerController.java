package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.banner.BannerCreation;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.BannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/banner")
@RequiredArgsConstructor
@Api(tags = "Banner API")
@Slf4j
public class BannerController {
  private final BannerService bannerService;

  @ApiOperation(value = "Get all banner")
  @GetMapping
  public ResponseEntity<?> getAllBanner() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách banner!",
                    this.bannerService.findAllBanner())
    );
  }

  @ApiOperation(value = "Get all banner by type")
  @GetMapping("type")
  public ResponseEntity<?> getAllBannerByType(@RequestParam String type) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách banner!",
                    this.bannerService.findAllBannerByType(type))
    );
  }

  @ApiOperation(value = "Create new banner")
  @PostMapping
  public ResponseEntity<?> createNewBanner(@RequestBody BannerCreation request) {
    bannerService.createNewBanner(request);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo mới banner thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Get banner by id")
  @GetMapping("{id}")
  public ResponseEntity<?> getBannerById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin banner!",
                    bannerService.findBannerById(id)
            )
    );
  }

  @ApiOperation(value = "Update banner information")
  @PutMapping("{id}")
  public ResponseEntity<?> updateBannerInformation(@PathVariable("id") Long id,@RequestBody BannerCreation request) {
    bannerService.updateBannerById(id, request);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật thông tin banner thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Delete banner")
  @DeleteMapping("{id}")
  public ResponseEntity<?> deleteBanner(@PathVariable Long id) {
    bannerService.deleteBannerById(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Xóa thông tin banner thành công!",
                    null
            )
    );
  }
}