package com.hcmute.slayz.controllers;

import com.hcmute.slayz.data.entities.User;
import com.hcmute.slayz.dto.request.cart.CartDetailCreation;
import com.hcmute.slayz.dto.request.cart.UpdateQuantity;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.CartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("api/v1/cart")
@RolesAllowed({"ROLE_ADMIN", "ROLE_USER"})
@Api(tags = "Cart API")
@RequiredArgsConstructor
public class CartController {

  private final CartService cartService;

  @ApiOperation(value = "Get all cart")
  @GetMapping()
  public ResponseEntity<?> getAllCart() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách giỏ hàng!",
                    cartService.getAllCart()
            )
    );
  }

  @ApiOperation(value = "Get cart by user id")
  @GetMapping("/user")
  public ResponseEntity<?> getCartByUserId() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = (User) authentication.getPrincipal();
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách giỏ hàng!",
                    cartService.getCartByUserId(currentUser.getId())
            )
    );
  }

  @ApiOperation(value = "Add product to cart")
  @PostMapping("/add_to_cart")
  public ResponseEntity<?> addProductToCart(@RequestBody CartDetailCreation creation) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thêm sản phẩm vào giỏ hàng thành công!",
                    cartService.addProductToCart(creation)
            )
    );
  }

  @ApiOperation(value = "Get all cart detail by cart id")
  @GetMapping("cart_detail/{id}")
  public ResponseEntity<?> getAllCartDetail(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách sản phẩm trong giỏ hàng!",
                    cartService.getListCartDetailByCartId(id)
            )
    );
  }

  @ApiOperation(value = "Remove cart detail by id")
  @DeleteMapping("cart_detail/{cartDetailId}")
  public ResponseEntity<?> removeProductFromCart(@PathVariable("cartDetailId") Long id) {
    this.cartService.removeProductFromCart(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Xóa sản phẩm khỏi giỏ hàng thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Get cart by id")
  @GetMapping("{id}")
  public ResponseEntity<?> getCartById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin giỏ hàng!",
                    cartService.getCartById(id)
            )
    );
  }

  @ApiOperation(value = "Update quantity product in cart")
  @PutMapping("update_quantity")
  public ResponseEntity<?> updateQuantity(@RequestBody UpdateQuantity request) {
    this.cartService.updateQuantity(request);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật số lượng sản phẩm thành công!",
                    null
            )
    );
  }
}
