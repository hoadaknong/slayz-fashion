package com.hcmute.slayz.controllers;

import com.hcmute.slayz.data.entities.User;
import com.hcmute.slayz.dto.request.user.ChangePassword;
import com.hcmute.slayz.dto.request.user.UserUpdate;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;

import static com.hcmute.slayz.utils.Constants.PATH_VERSION;

@RestController
@RequestMapping("api/v1/user")
@Api(tags = "User API")
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;


  @GetMapping
  @ApiOperation(value = "Get all user")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> getAllUser(@RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                      @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                                      @RequestParam(required = false, defaultValue = "")String fullName,
                                      @RequestParam(required = false, defaultValue = "")String phone,
                                      @RequestParam(required = false, defaultValue = "all")String gender,
                                      @RequestParam(required = false, defaultValue = "")String email,
                                      @RequestParam(required = false, defaultValue = "all")String status
                                      ) {
    Pageable pageable;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách người dùng!",
                    this.userService.getAllUser(pageable, fullName,phone,gender,email,status))
    );
  }

  @GetMapping("/active")
  @ApiOperation(value = "Get all active user")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> getAllUserActive() {
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Danh sách người dùng còn hoạt động!",
            this.userService.getAllUserByStatus(true))
    );
  }

  @GetMapping("/disable")
  @ApiOperation(value = "Get all disable user")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> getAllUserDisable() {
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Danh sách người dùng bị cấm!",
            this.userService.getAllUserByStatus(false))
    );
  }

  @GetMapping("{id}")
  @ApiOperation(value = "Get user by id")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getUserById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Thông tin người dùng!",
            this.userService.getUserByUserId(id))
    );
  }

  @GetMapping("status")
  @ApiOperation(value = "Get all user by status")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> getAllUserByStatus(@RequestParam("status") Boolean status) {
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Danh sách người dùng " + status + " !",
            this.userService.getAllUserByStatus(status))
    );
  }

  @PutMapping("{id}")
  @ApiOperation(value = "Update user information")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> updateUserInfo(@ModelAttribute UserUpdate update, @PathVariable Long id) {
    this.userService.updateUser(id, update);
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Cập nhật thông tin người dùng thành công!",
            null)
    );
  }

  @PatchMapping("/active/{id}")
  @ApiOperation(value = "Active user")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> activeUser(@PathVariable("id") Long id) {
    this.userService.updateUserStatus(id, true);
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Kích hoạt tài khoản thành công!",
            null)
    );
  }

  @PatchMapping("/disable/{id}")
  @ApiOperation(value = "Disable user")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  public ResponseEntity<?> disableUser(@PathVariable("id") Long id) {
    this.userService.updateUserStatus(id, false);
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Vô hiệu hóa tài khoản thành công!",
            null)
    );
  }

  @PatchMapping("/{userId}/role/{roleId}")
  @ApiOperation(value = "Update role of user")
  @RolesAllowed({"ROLE_ADMIN"})
  public ResponseEntity<?> updateRoleUser(@PathVariable("userId") Long userId, @PathVariable("roleId") Long roleId) {
    this.userService.updateRoleUser(userId, roleId);
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Cập nhật vai trò người dùng thành công!",
            null)
    );
  }

  @PostMapping("/{userId}/password")
  @ApiOperation(value = "Update user password")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> updatePassword(@PathVariable("userId") Long userId, @RequestBody ChangePassword request) {
    this.userService.updatePassword(userId, request);
    return ResponseEntity.ok(new ResponseObject(
            HttpStatus.OK,
            "Cập nhật mật khẩu thành công!",
            null)
    );
  }

  @GetMapping("current_user")
  @ApiOperation(value = "Get current user")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getCurrentUser(HttpServletRequest request) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    User currentUser = (User) authentication.getPrincipal();
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin người dùng!",
                    currentUser)
    );
  }
}
