package com.hcmute.slayz.controllers;


import com.hcmute.slayz.dto.request.variant.VariantCreation;
import com.hcmute.slayz.dto.request.variant.VariantUpdateRequest;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.VariantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.hcmute.slayz.utils.Constants.PATH_VERSION;

@RestController
@RequestMapping("api/v1/variant")
@Api(tags = "Variant API")
@RequiredArgsConstructor
public class VariantController {

  private final VariantService variantService;

  @ApiOperation(value="Create new variant")
  @PostMapping
  public ResponseEntity<?> createNewVariant(@RequestBody VariantCreation variantCreation){
    variantService.createNewVariant(variantCreation);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo mới sản phẩm thành công!",
                    null
            )
    );
  }

  @ApiOperation(value="Create new variant")
  @PutMapping
  public ResponseEntity<?> updateVariant(@RequestBody VariantCreation variantCreation){
    variantService.updateVariant(variantCreation);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo mới sản phẩm thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Get all variant by product id")
  @GetMapping("product/{productId}")
  public ResponseEntity<?> getAllVariantByProductId(@PathVariable("productId") Long productId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách biến thể sản phẩm theo mã sản phẩm!",
                    this.variantService.getAllVariantByProductId(productId)
            )
    );
  }

  @ApiOperation(value = "Update variants")
  @PutMapping("{id}")
  public ResponseEntity<?> updateVariant(@PathVariable("id") Long id,
                                         VariantUpdateRequest request) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách biến thể sản phẩm theo mã sản phẩm!",
                    this.variantService.updateVariant(id, request))
    );
  }

  @ApiOperation(value = "Get all color")
  @GetMapping("color")
  public ResponseEntity<?> getAllColorByProductId(@RequestParam("productId") Long productId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách màu sắc của sản phẩm!",
                    this.variantService.getAllColorByProductId(productId))
    );
  }

  @ApiOperation(value = "Get all size by product id")
  @GetMapping("size")
  public ResponseEntity<?> getAllSizeByProductId(@RequestParam("productId") Long productId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách size của của sản phẩm!",
                    this.variantService.getAllSizeByProductId(productId))
    );
  }

  @ApiOperation(value = "Get size chosen by product id and color id")
  @GetMapping("size_chosen")
  public ResponseEntity<?> getSizeChosenByProductIdAndColorId(@RequestParam("productId") Long productId,
                                                              @RequestParam("colorId") Long colorId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách size sẵn có của sản phẩm!",
                    this.variantService.getListVariantByProductIdAndColorId(productId, colorId))
    );
  }

  @ApiOperation(value = "Get variant by product id, color id and size id")
  @GetMapping("find")
  public ResponseEntity<?> getVariantByProductIdAndColorIdAndSizeId(@RequestParam("productId") Long productId,
                                                                    @RequestParam("colorId") Long colorId,
                                                                    @RequestParam("sizeId") Long sizeId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin biến thể!",
                    this.variantService.getVariantByProductIdAndColorIdAndSizeId(productId, colorId, sizeId))
    );
  }
}
