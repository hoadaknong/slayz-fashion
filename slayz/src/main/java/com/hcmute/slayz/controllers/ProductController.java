package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.filter.FilterRequest;
import com.hcmute.slayz.dto.request.product.ProductCreation;
import com.hcmute.slayz.dto.request.product.ProductUpdate;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("api/v1/product")
@Api(tags = "Product API")
@RequiredArgsConstructor
public class ProductController {

  private final ProductService productService;

  @ApiOperation(value = "Create new product")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @PostMapping()
  public ResponseEntity<?> createProduct(@ModelAttribute ProductCreation creation) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo sản phẩm mới thành công!",
                    this.productService.createProduct(creation)
            )
    );
  }

  @ApiOperation(value = "Update product information")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @PutMapping("{id}")
  public ResponseEntity<?> updateProduct(@PathVariable("id") Long id,
                                         @ModelAttribute ProductUpdate update) {
    this.productService.updateProduct(id, update);
    return ResponseEntity.ok(new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật thông tin sản phẩm thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Get product by id")
  @GetMapping("{id}")
  public ResponseEntity<?> getProductById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin sản phẩm!",
                    this.productService.getProductById(id)
            )
    );
  }

  @ApiOperation(value = "Get product by variant id")
  @GetMapping("variant/{variantId}")
  public ResponseEntity<?> getProductByVariantId(@PathVariable("variantId") Long variantId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin sản phẩm!",
                    this.productService.getProductByVariantId(variantId)
            )
    );
  }

  @ApiOperation(value = "Get product by barcode")
  @GetMapping("/barcode/{barcode}")
  public ResponseEntity<?> getProductByBarcode(@PathVariable("barcode") String barcode) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin sản phẩm theo barcode!",
                    this.productService.getProductByBarcode(barcode)
            )
    );
  }

  @ApiOperation(value = "Get all product")
  @GetMapping("product_grid")
  public ResponseEntity<?> getAllProductGrid() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách sản phẩm!",
                    this.productService.getAllProductGrid()
            )
    );
  }

  @ApiOperation(value = "Delete product by id")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @DeleteMapping("{id}")
  public ResponseEntity<?> deleteProductById(@PathVariable("id") Long id) {
    this.productService.deleteProductById(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Xóa sản phẩm thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Filter product")
  @PostMapping("filter")
  public ResponseEntity<?> filterProduct(@RequestBody FilterRequest request) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách sản phẩm theo bộ lọc!",
                    this.productService.filterProduct(request)
            )
    );
  }

  @ApiOperation(value = "Get sale quantity by product id")
  @GetMapping("/sale_quantity/{id}")
  public ResponseEntity<?> getSaleQuantityByProductId(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Số lượng sản phẩm đã bán!",
                    this.productService.salesProduct(id)
            )
    );
  }

  @ApiOperation(value = "Get 12 newest product")
  @GetMapping("/newest_product")
  public ResponseEntity<?> findTop12NewestProduct() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách 12 sản phẩm mới nhất!",
                    this.productService.findTop12NewestProduct()
            )
    );
  }

  @ApiOperation(value = "Get related product")
  @GetMapping("/{id}/related")
  public ResponseEntity<?> findRelatedProducts(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách 4 sản phẩm liên quan!",
                    this.productService.findRelatedProduct(id)
            )
    );
  }
}
