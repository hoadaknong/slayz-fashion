package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.category.CategoryCreationRequest;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("api/v1/category")
@Api(tags = "Category API")
@RequiredArgsConstructor
public class CategoryController {

  private final CategoryService categoryService;

  @ApiOperation(value = "Create new category")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @RequestMapping(path = "", method = RequestMethod.POST)
  public ResponseEntity<?> create(@RequestBody CategoryCreationRequest request) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thêm phân loại mới thành công!",
                    this.categoryService.save(request)
            )
    );
  }

  @ApiOperation(value = "Get all category")
  @RequestMapping(path = "", method = RequestMethod.GET)
  public ResponseEntity<?> findAll(@RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                   @RequestParam(required = false, defaultValue = "100") Integer pageSize) {
    Pageable pageable;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách phân loại",
                    this.categoryService.getAll(pageable)
            )
    );
  }

  @ApiOperation(value = "Update information category")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @RequestMapping(path = "{id}", method = RequestMethod.PUT)
  public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestBody CategoryCreationRequest request) {
    this.categoryService.update(id, request);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật thông tin phân loại thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Get category by id")
  @RequestMapping(path = "{id}", method = RequestMethod.GET)
  public ResponseEntity<?> getCategory(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin phân loại",
                    this.categoryService.getCategoryById(id)
            )
    );
  }

  @ApiOperation(value = "Delete category by id")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF"})
  @RequestMapping(path = "{id}", method = RequestMethod.DELETE)
  public ResponseEntity<?> deleteCategoryById(@PathVariable("id") Long id) {
    this.categoryService.deleteById(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Xóa phân loại thành công!",
                    null
            )
    );
  }
}
