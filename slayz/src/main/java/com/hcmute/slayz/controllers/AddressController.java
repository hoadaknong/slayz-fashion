package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.address.AddressCreation;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

@RestController
@RequestMapping("api/v1/address")
@RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
@RequiredArgsConstructor
@Api(tags = "Address API")
public class AddressController {

  private final AddressService addressService;

  @ApiOperation(value = "Get all address")
  @GetMapping
  public ResponseEntity<?> getAllAddress() {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách địa chỉ",
                    this.addressService.getAllAddress())
    );
  }

  @ApiOperation(value = "Get address by id")
  @GetMapping("{id}")
  public ResponseEntity<?> getAddressById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách địa chỉ",
                    this.addressService.getAddressById(id))
    );
  }

  @ApiOperation(value = "Get all address by user id")
  @GetMapping("user/{id}")
  public ResponseEntity<?> getAllAddressByUserId(@RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                                 @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                                                 @PathVariable("id") Long userId) {
    Pageable pageable;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách địa chỉ",
                    this.addressService.getAllAddressByUserId(userId, pageable))
    );
  }

  @ApiOperation(value = "Get default address by user id")
  @GetMapping("/default_address/{id}")
  public ResponseEntity<?> getDefaultAddressByUserId(@PathVariable("id") Long userId) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Địa chỉ mặc định của người dùng.",
                    this.addressService.getDefaultAddressByUserId(userId))
    );
  }

  @ApiOperation(value = "Create new address")
  @PostMapping
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> createNewAddress(@RequestBody AddressCreation creation) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo địa chỉ mới thành công!",
                    this.addressService.createAddress(creation))
    );
  }

  @ApiOperation(value = "Update address")
  @PutMapping("{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> updateNewAddress(@PathVariable("id") Long id,
                                            @RequestBody AddressCreation creation) {
    this.addressService.updateAddress(id, creation);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật địa chỉ thành công!",
                    null)
    );
  }

  @ApiOperation(value = "Delete address by id")
  @DeleteMapping("{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> deleteAddressById(@PathVariable("id") Long id) {
    this.addressService.deleteAddressById(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Xóa địa chỉ thành công!",
                    null)
    );
  }

  @ApiOperation(value = "Set address to default address")
  @PatchMapping("{id}")
  @ResponseStatus(HttpStatus.OK)
  public ResponseEntity<?> setDefaultAddress(@PathVariable("id") Long id) {
    this.addressService.setDefaultAddress(id);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thiết lập địa chỉ mặc định thành công!",
                    null)
    );
  }
}
