package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.payment.PaymentRequest;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.PaymentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;

import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("api/v1/payment")
@Api(tags = "Payment API")
@RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
@RequiredArgsConstructor
public class PaymentController {

  private final PaymentService paymentService;

  @ApiOperation(value = "Momo payment")
  @PostMapping("momo")
  public ResponseEntity<?> getLinkPaymentMomo(@RequestBody PaymentRequest request) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Liên kết thanh toán!",
                    this.paymentService.createMomoPaymentLink(request)
            )
    );
  }
  @ApiOperation(value = "VNPay payment")
  @PostMapping("vnpay")
  public ResponseEntity<?> getLinkPaymentVnpay(@RequestBody PaymentRequest paymentRequest, HttpServletRequest req ) throws UnsupportedEncodingException {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Liên kết thanh toán!",
                    this.paymentService.createVnpayLink(paymentRequest,req)
            )
    );
  }
}
