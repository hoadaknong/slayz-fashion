package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.authentication.AuthenticationRequest;
import com.hcmute.slayz.dto.request.authentication.RegisterRequest;
import com.hcmute.slayz.dto.request.user.ResetPasswordRequest;
import com.hcmute.slayz.dto.response.authentication.AuthenticationResponse;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.AuthenticationService;
import com.hcmute.slayz.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import static com.hcmute.slayz.utils.Constants.*;

@RestController
@RequestMapping("api/v1")
@Slf4j
@Api(tags = "Authentication API")
@RequiredArgsConstructor
public class AuthenticationController {

  private final AuthenticationService authenticationService;

  private final UserService userService;

  @ApiOperation(value = "Login for all user")
  @PostMapping("auth/login")
  public ResponseEntity<?> loginForAllUser(@RequestBody @Valid AuthenticationRequest request, HttpServletRequest req, HttpServletResponse response) {
    String origin = req.getHeader("Origin");
    String domain = "";
    origin = origin.split("//")[1];
    if (!origin.contains("localhost")) {
      domain = origin;
    }
    AuthenticationResponse authenticationResponse = authenticationService.login(request);
    Cookie cookie = new Cookie("refresh_token", authenticationResponse.getRefreshToken());
    cookie.setPath("/");
    cookie.setDomain(domain);
    cookie.setMaxAge(24 * 60 * 60);
    cookie.setHttpOnly(true);
    response.addCookie(cookie);
    return ResponseEntity.status(HttpStatus.OK)
            .body(new ResponseObject(HttpStatus.OK, "Đăng nhập thành công!", authenticationResponse));
  }

  @ApiOperation(value = "Login for admin")
  @PostMapping("auth/admin/login")
  public ResponseEntity<?> loginForAdminPage(@RequestBody @Valid AuthenticationRequest request, HttpServletResponse response) {
    AuthenticationResponse authenticationResponse = authenticationService.loginForAdmin(request);
    ResponseCookie springCookie = ResponseCookie.from("refresh_token", authenticationResponse.getRefreshToken()).domain("").path("/").httpOnly(true).maxAge(24 * 60 * 60).build();
    return ResponseEntity.status(HttpStatus.OK).header(HttpHeaders.SET_COOKIE, springCookie.toString()).body(new ResponseObject(HttpStatus.OK, "Đăng nhập thành công!", authenticationResponse));
  }

  @ApiOperation(value = "Register")
  @PostMapping("auth/register")
  public ResponseEntity<?> register(@RequestBody @Valid RegisterRequest request) {
    return ResponseEntity.ok(new ResponseObject(HttpStatus.OK, "Đăng ký thành công!", authenticationService.register(request)));
  }

  @ApiOperation(value = "Refresh token")
  @GetMapping("auth/refresh")
  public ResponseEntity<?> refreshToken(@CookieValue(value = "refresh_token", defaultValue = "unknown") String refreshToken) {

    AuthenticationResponse authenticationResponse = authenticationService.refreshToken(refreshToken);
    ResponseCookie springCookie = ResponseCookie.from("refresh_token", authenticationResponse.getRefreshToken()).domain("").path("/").httpOnly(true).maxAge(7 * 24 * 60 * 60).build();
    return ResponseEntity.status(HttpStatus.OK).header(HttpHeaders.SET_COOKIE, springCookie.toString()).body(new ResponseObject(HttpStatus.OK, "Làm mới token thành công!", authenticationResponse));
  }

  @ApiOperation(value = "Send link reset passwrod by email")
  @GetMapping("reset_password/send_mail")
  public ResponseEntity<?> sendMailToResetPassword(@RequestParam("key") String key) {
    this.userService.sendMailToResetPassword(key);
    return ResponseEntity.ok(new ResponseObject(HttpStatus.OK, "Gửi thư thành công!", null));
  }

  @ApiOperation(value = "Check reset password token")
  @GetMapping("reset_password/check_token/{token}")
  public ResponseEntity<?> checkResetPasswordToken(@PathVariable("token") String token) {
    Boolean isAvailable = this.authenticationService.checkResetPasswordTokenAvailable(token);
    if (isAvailable) {
      return ResponseEntity.ok(new ResponseObject(HttpStatus.OK, "Token hợp lệ", null));
    }
    return ResponseEntity.ok(new ResponseObject(HttpStatus.NOT_ACCEPTABLE, "Token không hợp lệ!", null));
  }

  @ApiOperation(value = "Reset password")
  @PostMapping("reset_password")
  public ResponseEntity<?> resetPassword(@RequestBody ResetPasswordRequest request) {
    this.userService.resetPassword(request);
    return ResponseEntity.ok(new ResponseObject(HttpStatus.OK, "Đặt lại mật khẩu thành công!", null));
  }

  @ApiOperation(value = "Logout")
  @GetMapping("auth/logout")
  public ResponseEntity<?> logout(@CookieValue(name = "refresh_token", defaultValue = "_") String refreshToken) {
    ResponseCookie springCookie = ResponseCookie.from("refresh_token", refreshToken).domain("").path("/").httpOnly(true).maxAge(0).build();
    return ResponseEntity.status(HttpStatus.OK).header(HttpHeaders.SET_COOKIE, springCookie.toString()).body(new ResponseObject(HttpStatus.OK, "Đăng xuất thành công!", null));
  }
}
