package com.hcmute.slayz.controllers;

import com.hcmute.slayz.dto.request.invoice.ExportExcelRequest;
import com.hcmute.slayz.dto.request.invoice.InvoiceCreation;
import com.hcmute.slayz.dto.request.invoice.UpdatePaymentStatus;
import com.hcmute.slayz.dto.response.invoice.InvoiceResponse;
import com.hcmute.slayz.exceptions.InvalidFieldException;
import com.hcmute.slayz.models.PageResponse;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.services.InvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.hcmute.slayz.utils.Constants.PATH_VERSION;

@RestController
@RequestMapping("api/v1/invoice")
@Api(tags = "Invoice API")
@RequiredArgsConstructor
public class InvoiceController {

  private final InvoiceService invoiceService;

  @ApiOperation(value = "Create invoice")
  @PostMapping
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> createInvoice(@RequestBody InvoiceCreation creation) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Tạo đơn hàng thành công!",
                    this.invoiceService.createNewInvoice(creation)
            )
    );
  }

  @ApiOperation(value = "Update payment status by invoice id")
  @PatchMapping("payment_status/{id}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> updatePaymentStatus(@PathVariable("id") Long id,
                                               @RequestBody UpdatePaymentStatus request) {
    this.invoiceService.updatePaymentStatus(id, request);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật trạng thái thanh toán đơn hàng thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Update invoice status by invoice id")
  @PatchMapping("status/{id}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> updateInvoiceStatus(@PathVariable("id") Long id,
                                               @RequestBody UpdatePaymentStatus request) {
    this.invoiceService.updateInvoiceStatus(id, request);
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Cập nhật trạng thái đơn hàng thành công!",
                    null
            )
    );
  }

  @ApiOperation(value = "Get invoice by id")
  @GetMapping("{id}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getInvoiceById(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Thông tin hóa đơn",
                    this.invoiceService.getInvoiceById(id)
            )
    );
  }

  @ApiOperation(value = "Get all invoice")
  @GetMapping()
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getAllInvoice(@RequestParam(required = false, defaultValue = "10") Integer pageSize,
                                         @RequestParam(required = false, defaultValue = "1") Integer pageIndex) {
    Pageable pageable = null;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách đơn hàng!",
                    this.invoiceService.getAllInvoice(pageable)
            )
    );
  }

  @ApiOperation(value = "Get all address by user id")
  @GetMapping("users/{userId}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getAllInvoiceByUserId(@PathVariable("userId") Long userId,
                                                 @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                                                 @RequestParam(required = false, defaultValue = "1") Integer pageIndex,
                                                 @RequestParam(required = false, defaultValue = "all") String status,
                                                 @RequestParam(required = false, defaultValue = "all") String paymentStatus) {
    Pageable pageable = null;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách đơn hàng theo người dùng!",
                    this.invoiceService.getAllInvoiceByUserId(userId, pageable, status, paymentStatus)
            )
    );
  }

  @ApiOperation(value = "Get list invoice detail by invoice id")
  @GetMapping("invoice_detail/{id}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getAllInvoiceDetailByInvoiceId(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách đơn hàng theo người dùng!",
                    this.invoiceService.getAllInvoiceDetailByInvoiceId(id)
            )
    );
  }

  @ApiOperation(value = "Get invoice by status")
  @GetMapping("status/{status}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getAllInvoiceByStatus(@PathVariable("status") String status,
                                                 @RequestParam(required = false, defaultValue = "10") Integer pageSize,
                                                 @RequestParam(required = false, defaultValue = "1") Integer pageIndex) {
    Pageable pageable = null;
    try {
      pageable = PageRequest.of(pageIndex - 1, pageSize);
    } catch (Exception e) {
      throw new InvalidFieldException("Số trang không hợp lệ!");
    }
    return ResponseEntity.ok(
            new ResponseObject(
                    HttpStatus.OK,
                    "Danh sách đơn hàng theo người dùng!",
                    this.invoiceService.getAllInvoiceByStatus(status, pageable)
            )
    );
  }

  @ApiOperation(value = "Get file invoice .xlsx")
  @GetMapping("export_xlsx/status/{status}")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getAllInvoiceFileByStatus(@PathVariable("status") String status) {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
    String currentDateTime = dateFormatter.format(new Date());
    String filename = String.format("invoice_%s.xlsx", currentDateTime);
    InputStreamResource file = new InputStreamResource(invoiceService.writeDataToExcel(status));
    return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
            .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
            .body(file);
  }

  @ApiOperation(value = "Get file invoice .xlsx (include range created date and status)")
  @PostMapping("export_xlsx_range_date")
  @RolesAllowed({"ROLE_ADMIN", "ROLE_STAFF", "ROLE_USER"})
  public ResponseEntity<?> getAllInvoiceFileByStatus(@RequestBody ExportExcelRequest request) {
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
    String currentDateTime = dateFormatter.format(new Date());
    String filename = String.format("invoice_%s.xlsx", currentDateTime);
    System.out.println(request.toString());
    InputStreamResource file = new InputStreamResource(invoiceService.writeDataToExcel(request.getStatus(), request.getStartDate(), request.getEndDate()));
    return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
            .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
            .body(file);
  }
}
