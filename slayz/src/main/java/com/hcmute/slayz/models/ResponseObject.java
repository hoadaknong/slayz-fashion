package com.hcmute.slayz.models;

import org.springframework.http.HttpStatus;

public class ResponseObject {
  private HttpStatus status;
  private String message;
  private Object data;


  public ResponseObject() {
  }

  public ResponseObject(HttpStatus status, String message, Object data) {
    this.status = status;
    this.message = message;
    this.data = data;
  }

  public ResponseObject(HttpStatus status, String message) {
    this.status = status;
    this.message = message;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public void setStatus(HttpStatus status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }


  @Override
  public String toString() {
    return "{" +
            "status=" + status +
            ", message='" + message + '\'' +
            ", data=" + data +
            '}';
  }
}
