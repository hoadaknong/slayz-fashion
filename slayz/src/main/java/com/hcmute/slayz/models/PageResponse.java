package com.hcmute.slayz.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResponse {
  private Integer totalPage;
  private Integer currentPage;
  private Integer pageSize;
  private Integer totalRecord;
  private Object details;
}
