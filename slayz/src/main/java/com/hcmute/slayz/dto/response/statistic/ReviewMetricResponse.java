package com.hcmute.slayz.dto.response.statistic;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReviewMetricResponse {
  private Integer reviewCount;
  private Integer oneStar;
  private Integer twoStar;
  private Integer threeStar;
  private Integer fourStar;
  private Integer fiveStar;
  private Double avgVote;

}
