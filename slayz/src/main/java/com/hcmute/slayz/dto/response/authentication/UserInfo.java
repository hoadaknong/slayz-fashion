package com.hcmute.slayz.dto.response.authentication;

import com.hcmute.slayz.data.entities.Role;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
  private Long id;
  private Role role;
  private String fullName;
  private String gender;
  private String dob;
  private String email;
  private String photo;
  private String phone;
}
