package com.hcmute.slayz.dto.request.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;
import java.util.Arrays;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductUpdate {
  private String title;
  private Long category;
  private String barcode;
  private String gender;
  private Long standCost;
  private Long listPrice;
  private String description;
  private Object thumbnail;
  private Long[] idVariantsOld;
  private Long[] quantityVariantsOld;
  private Long[] colorVariants;
  private Long[] sizeVariants;
  private Long[] quantityVariants;
  private MultipartFile[] photoVariants;
  private String[] photosOld;
  private MultipartFile[] photos;

  @Override
  public String toString() {
    return "ProductUpdate{" +
            "title='" + title + '\'' +
            ", category=" + category +
            ", barcode='" + barcode + '\'' +
            ", standCost=" + standCost +
            ", listPrice=" + listPrice +
            ", description='" + description + '\'' +
            ", thumbnail=" + thumbnail +
            ", idVariantsOld=" + Arrays.toString(idVariantsOld) +
            ", quantityVariantsOld=" + Arrays.toString(quantityVariantsOld) +
            ", colorVariants=" + Arrays.toString(colorVariants) +
            ", sizeVariants=" + Arrays.toString(sizeVariants) +
            ", quantityVariants=" + Arrays.toString(quantityVariants) +
            ", photoVariants=" + Arrays.toString(photoVariants) +
            ", photosOld=" + Arrays.toString(photosOld) +
            ", photos=" + Arrays.toString(photos) +
            '}';
  }
}
