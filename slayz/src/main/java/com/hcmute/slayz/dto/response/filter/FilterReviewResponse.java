package com.hcmute.slayz.dto.response.filter;

import com.hcmute.slayz.dto.response.review.ReviewResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterReviewResponse {
  private Integer page;
  private Integer totalRecord;
  private Integer totalPage;
  List<ReviewResponse> reviewListData;
}
