package com.hcmute.slayz.dto.response.variant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VariantResponse {
  private Long id;

  private Long sizeId;

  private String sizeName;

  private Long productId;

  private Long colorId;

  private String colorName;

  private Long inventory;

  private String image;

  private Boolean isChosen;
}
