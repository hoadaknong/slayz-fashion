package com.hcmute.slayz.dto.request.variant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VariantCreation {
  private String image;
  private Long colorId;
  private SizeVariant[] sizeVariants;

  private Long productId;
  @Override
  public String toString() {
    return "VariantCreation{" +
            "image='" + image + '\'' +
            ", colorId=" + colorId +
            ", sizeVariants=" + Arrays.toString(sizeVariants) +
            '}';
  }
}
