package com.hcmute.slayz.dto.response.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FilterResponse {
  private Integer totalRecord;
  private Integer currentPage;
  private List<?> data;
}
