package com.hcmute.slayz.dto.response.voucher;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VoucherResponse {
  private Long id;
  private String title;
  private String type;
  private String code;
  private String description;
  private Long valueRequirement;
  private Long maxValue;
  private BigDecimal value;
  private Date expiredDate;
  private String image;
}
