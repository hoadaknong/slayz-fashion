package com.hcmute.slayz.dto.request.invoice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDetailProp {
  private Long id;
  private Long variantId;
  private Long quantity;
}
