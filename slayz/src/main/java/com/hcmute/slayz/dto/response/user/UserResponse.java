package com.hcmute.slayz.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
  private Long id;
  private String dob;
  private String email;
  private String fullName;
  private String gender;
  private String phone;
  private String photo;
  private Long roleId;
  private String roleName;
  private Boolean status;
}
