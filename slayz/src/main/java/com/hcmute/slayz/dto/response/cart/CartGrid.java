package com.hcmute.slayz.dto.response.cart;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CartGrid {
  private Long id;
  private Long userId;
  private String userPhoto;
  private String fullName;
  private Long quantityProduct;
  private Long totalQuantity;
  private Long grandTotal;
}
