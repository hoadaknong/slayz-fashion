package com.hcmute.slayz.dto.request.size;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SizeCreation {
  private String name;
  private String description;
}
