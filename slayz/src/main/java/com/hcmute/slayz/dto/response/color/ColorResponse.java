package com.hcmute.slayz.dto.response.color;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ColorResponse {
  private Long id;
  private String name;
  private String code;
}
