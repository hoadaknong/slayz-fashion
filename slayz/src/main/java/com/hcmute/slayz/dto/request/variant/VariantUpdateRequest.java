package com.hcmute.slayz.dto.request.variant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VariantUpdateRequest {
  private Long colorId;
  private Long sizeId;
  private Long quantity;
  private Object image;
}
