package com.hcmute.slayz.dto.request.voucher;

import lombok.*;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VoucherCreation {
  private String title;
  private String type;
  private String code;
  private String description;
  private Long maxValue;
  private Long valueRequirement;
  private BigDecimal value;
  private Date expiredDate;
  private String image;
}
