package com.hcmute.slayz.dto.request.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterReviewRequest {
  private Integer[] ratingList;
  private Integer page;
  private Integer limit;
  private String sortOption;
  private Long productId;
}
