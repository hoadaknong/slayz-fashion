package com.hcmute.slayz.dto.response.invoice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceResponse {
  private Long id;
  private String fullName;
  private String phone;
  private String address;
  private String paymentStatus;
  private String status;
  private String delivery;
  private String date;
  private Long total;
  private Long deliveryFee;
  private String fullNameUser;
  private String phoneUser;
  private String paymentMethod;
  private Long userId;
  private Long addressId;
  private String emailUser;
  private String userPhoto;
  private String note;
  private Date updateDate;
}
