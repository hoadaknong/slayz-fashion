package com.hcmute.slayz.dto.response.review;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewResponse {
  private Long id;

  private String fullName;

  private String userPhoto;

  private Integer rating;

  private Long userId;

  private String comment;

  private String productImage;

  private String productName;

  private Long productId;

  private String variantSize;

  private String variantColor;

  private Boolean enable;

  private Long createdDate;
}
