package com.hcmute.slayz.dto.request.authentication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {

  @Size(message = "Họ tên đầy đủ phải có ít nhất 6 ký tự! Vui lòng thử lại.", max = 50, min = 6)
  private String fullName;

  //@gmail.com
  @Size(message = "Địa chỉ email không hợp lệ!", max = 100, min = 6)
  private String email;

  @Size(message = "Mật khẩu phải có ít nhất 6 ký tự!",min = 6)
  private String password;

  @Size(message = "Mật khẩu phải có ít nhất 6 ký tự!",min = 6)
  private String confirmPassword;


}
