package com.hcmute.slayz.dto.request.filter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FilterRequest {
  private String name;
  private Long[] categories;
  private Long[] colors;
  private Long[] sizes;
  private String[] gender;
  private Long minPrice;
  private Long maxPrice;
  private Integer offset;
  private Integer limit;
  private String sortOption;
}
