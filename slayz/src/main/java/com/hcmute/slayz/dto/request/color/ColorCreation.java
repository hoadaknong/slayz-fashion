package com.hcmute.slayz.dto.request.color;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ColorCreation {
  private String name;
  private String code;
}
