package com.hcmute.slayz.dto.response.category;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponse {
  private Long id;
  private String name;
  private String parentName;
  private String description;
  private Long parentId;
}
