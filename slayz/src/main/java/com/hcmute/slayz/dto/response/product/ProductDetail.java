package com.hcmute.slayz.dto.response.product;

import com.hcmute.slayz.data.entities.Variant;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetail {
  private Long id;

  private Long categoryId;

  private String name;

  private String barcode;

  private Long listPrice;

  private Long standCost;
  private String gender;

  private String image;

  private String description;

  private List<String> photos;

  private List<Variant> variants;
}
