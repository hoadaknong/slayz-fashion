package com.hcmute.slayz.dto.request.variant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SizeVariant {
  private Long sizeId;
  private Long inventory;

  @Override
  public String toString() {
    return "SizeVariant{" +
            "sizeId=" + sizeId +
            ", inventory=" + inventory +
            '}';
  }
}
