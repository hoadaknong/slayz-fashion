package com.hcmute.slayz.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdate {
  private String dob;
  private String phone;
  private String fullName;
  private String gender;
  private Object photo;
}
