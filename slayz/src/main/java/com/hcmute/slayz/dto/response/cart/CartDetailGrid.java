package com.hcmute.slayz.dto.response.cart;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CartDetailGrid {
  private Long id;
  private Long productId;
  private String productName;
  private String imageProduct;
  private String barcode;
  private String size;
  private String style;
  private Long quantity;
  private Long total;
  private Long price;
}
