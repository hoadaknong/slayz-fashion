package com.hcmute.slayz.dto.response.notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationResponse {
  private Long id;
  private Long idOfType;
  private String type;
  private String userPhoto;
  private String message;
  private Long createdDate;
  private Boolean isChecked;
}
