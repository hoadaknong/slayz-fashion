package com.hcmute.slayz.dto.response.size;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SizeResponse {
  private Long id;
  private String name;
  private String description;
}
