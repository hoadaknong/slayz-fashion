package com.hcmute.slayz.dto.request.review;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewCreationRequest {
  private Integer rating;
  private String comment;
  private Long variantId;
  private Long invoiceDetailId;
}
