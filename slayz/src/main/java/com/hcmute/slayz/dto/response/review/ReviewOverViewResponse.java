package com.hcmute.slayz.dto.response.review;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ReviewOverViewResponse {
  private Integer rating;
  private Integer amountOfRating;
  private Integer percentage;
  private Integer totalAmountOfReview;
}
