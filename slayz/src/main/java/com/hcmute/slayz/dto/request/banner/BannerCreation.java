package com.hcmute.slayz.dto.request.banner;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BannerCreation {
  private String type;
  private String image;
  private String linkTo;
  private Date startDate;
  private Date endDate;
}
