package com.hcmute.slayz.dto.request.address;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AddressCreation {
  private String fullName;
  private String phone;
  private String addressLine;
  private String province;
  private String district;
  private String ward;
  private Long provinceId;
  private Long districtId;
  private String wardId;
  private Long userId;
}
