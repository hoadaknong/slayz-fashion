package com.hcmute.slayz.dto.response.product;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductReponse {
  private Long productId;
  private String title;
  private String category;
  private Long categoryId;
  private Long standCost;
  private Long listPrice;
  private Long inventory;
  private String productImage;
  private String barcode;
  private String description;
  private String gender;
}
