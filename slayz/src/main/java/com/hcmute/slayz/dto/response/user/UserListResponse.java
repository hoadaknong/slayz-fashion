package com.hcmute.slayz.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserListResponse {
  private List<UserResponse> listUser;
  private Long amount;
  private Long amountMale;
  private Long amountFemale;
  private Long amountUndefined;
}
