package com.hcmute.slayz.dto.request.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentRequest {
  private String orderInfo;
  private Long amount;
  private String returnUrl;
  private String extraData;
  private Integer type;

}
