package com.hcmute.slayz.dto.request.notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotificationPayload {
  private Long id;

  @Override
  public String toString() {
    return "NotificationPayload{" +
            "id=" + id +
            '}';
  }
}
