package com.hcmute.slayz.dto.request.product;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductCreation {
  private String title;
  private Long category;
  private String barcode;
  private String gender;
  private Long standCost;
  private Long listPrice;
  private String description;
  private MultipartFile thumbnail;
  private MultipartFile[] photos;
  private Long[] colorVariants;
  private Long[] sizeVariants;
  private Long[] quantityVariants;
  private MultipartFile[] photoVariants;
}
