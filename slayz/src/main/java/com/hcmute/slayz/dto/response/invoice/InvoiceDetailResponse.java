package com.hcmute.slayz.dto.response.invoice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDetailResponse {

  private Long id;
  private String image;
  private String name;
  private String slug;
  private Long price;
  private String color;
  private String size;
  private Long quantity;
  private Long total;
  private Long variantId;
}
