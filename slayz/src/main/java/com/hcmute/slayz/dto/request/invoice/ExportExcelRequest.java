package com.hcmute.slayz.dto.request.invoice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExportExcelRequest {
  private String status;

  private LocalDateTime startDate;

  private LocalDateTime endDate;

  @Override
  public String toString() {
    return "ExportExcelRequest{" +
            "status='" + status + '\'' +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            '}';
  }
}
