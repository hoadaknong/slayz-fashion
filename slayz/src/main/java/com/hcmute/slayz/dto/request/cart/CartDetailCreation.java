package com.hcmute.slayz.dto.request.cart;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CartDetailCreation {
  private Long cartId;
  private Long variantId;
  private Long quantity;
}
