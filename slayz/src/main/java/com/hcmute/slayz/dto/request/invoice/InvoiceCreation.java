package com.hcmute.slayz.dto.request.invoice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceCreation {
  private Long userId;
  private String paymentMethod;
  private String paymentStatus;

  private Long addressId;
  private Long[] listDetailId;

  private String note;
  private Long deliveryFee;

  private String deliveryMethodName;
  @Override
  public String toString() {
    return "InvoiceCreation{" +
            "userId=" + userId +
            ", paymentMethod='" + paymentMethod + '\'' +
            ", paymentStatus='" + paymentStatus + '\'' +
            ", listDetailId=" + Arrays.toString(listDetailId) +
            '}';
  }
}
