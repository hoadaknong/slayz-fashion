package com.hcmute.slayz.dto.request.category;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCreationRequest {
  private String name;
  private String description;
  private Long parentId;
}
