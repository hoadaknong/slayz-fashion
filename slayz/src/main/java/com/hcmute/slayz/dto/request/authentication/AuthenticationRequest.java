package com.hcmute.slayz.dto.request.authentication;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AuthenticationRequest {
  @Size(message = "Số điện thoại hoặc email không hợp lệ!", max = 100, min = 6)
  @NotNull(message = "Vui lòng nhập Email hoặc số điện thoại!")
  private String username;

  @Length(message = "Mật khẩu không hợp lệ!", min = 6, max = 100)
  @NotNull(message = "Vui lòng nhập mật khẩu!")
  private String password;
}
