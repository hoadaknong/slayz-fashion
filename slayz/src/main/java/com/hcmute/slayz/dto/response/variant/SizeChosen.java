package com.hcmute.slayz.dto.response.variant;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SizeChosen {
  private Long colorId;
  private Long sizeId;
  private String sizeName;
  private Boolean isChosen;
}
