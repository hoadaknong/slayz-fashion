package com.hcmute.slayz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class SlayzApplication {

  public static void main(String[] args) {
    SpringApplication.run(SlayzApplication.class, args);
  }

}
