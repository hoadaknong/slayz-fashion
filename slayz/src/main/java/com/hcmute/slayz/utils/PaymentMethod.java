package com.hcmute.slayz.utils;

public enum PaymentMethod {
  MOMO, PAYPAL, QR, COD, CASH
}
