package com.hcmute.slayz.utils;

public enum DeliveryStatus {
  PENDING, CONFIRMED, DELIVERING, DELIVERED, CANCELLED
}
