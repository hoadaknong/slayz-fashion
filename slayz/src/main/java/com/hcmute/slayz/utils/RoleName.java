package com.hcmute.slayz.utils;

public enum RoleName {
  ROLE_ADMIN, ROLE_STAFF, ROLE_USER
}