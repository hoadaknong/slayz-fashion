package com.hcmute.slayz.utils;

import com.hcmute.slayz.data.entities.Product;
import com.hcmute.slayz.data.entities.Variant;
import com.hcmute.slayz.data.repositories.ProductRepository;
import com.hcmute.slayz.data.repositories.VariantRepository;
import com.hcmute.slayz.models.PageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class CommonFunctions {
  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private VariantRepository variantRepository;

  public Optional<Product> getProductByVariantId(Long variantId) {
    Optional<Product> result = Optional.empty();
    Optional<Variant> variantFound = variantRepository.findById(variantId);
    Variant variant = new Variant();
    if (variantFound.isPresent()) {
      variant = variantFound.get();
    }
    if (variant.getProductId() != null) {
      result = productRepository.findById(variant.getProductId());
    }
    return result;
  }

  public static PageResponse pageResponse(int currentPage,int pageSize, int totalRecord, Object data){
    PageResponse pageResponse = new PageResponse();
    pageResponse.setCurrentPage(currentPage);
    pageResponse.setPageSize(pageSize);
    pageResponse.setTotalPage((int) Math.ceil((double) totalRecord / pageResponse.getPageSize()));
    pageResponse.setDetails(data);
    pageResponse.setTotalRecord(totalRecord);
    return pageResponse;
  }
}
