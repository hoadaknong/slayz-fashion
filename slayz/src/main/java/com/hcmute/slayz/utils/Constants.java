package com.hcmute.slayz.utils;

import org.springframework.beans.factory.annotation.Value;

import java.util.regex.Pattern;

public class Constants {
  public static final String HOST = "";

  public static final String PATH_VERSION = "";

  public static String THUMBNAIL_PATH = HOST + "";

  public static String IMAGES_PATH = HOST + "";

  public static String VARIANT_PATH = HOST + "";

  public static String USER_PHOTO_PATH = HOST + "";

  public static String MAN_USER_PHOTO_PATH = "https://res.cloudinary.com/dfnnmsofg/image/upload/v1677132869/d2ee9ad9-ef75-4f71-8f9b-40125f335d0e.png";
  public static String WOMAN_USER_PHOTO_PATH = "https://res.cloudinary.com/dfnnmsofg/image/upload/v1677132869/d2ee9ad9-ef75-4f71-8f9b-40125f335d0e.png";
  public static String DEFAULT_USER_PHOTO_PATH = "https://res.cloudinary.com/dfnnmsofg/image/upload/v1677132869/d2ee9ad9-ef75-4f71-8f9b-40125f335d0e.png";

  public static String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";

  public static String WEBHOOK = "http://slayz-payment-env.eba-kr2dndhb.ap-southeast-1.elasticbeanstalk.com/api/v1/payment";

  public static Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

  public static String[] ORIGINS = new String[]{
          "http://localhost:3000",
          "http://localhost:3006",
          "http://localhost:8080",
          "http://localhost:8088",
          "http://116.99.232.51:3000",
          "http://116.99.232.51:3006",
          "http://116.99.232.51:8080",
          "http://192.168.1.104:3006",
          "http://192.168.1.104:3000",
          "http://192.168.1.104:8080",
          "https://slayz.netlify.app",
          "https://test-payment.momo.vn",
          "https://webhook.site",
          "http://slayz.s3-website-ap-southeast-1.amazonaws.com",
          "http://slayz-admin.s3-website-ap-southeast-1.amazonaws.com"
  };

}
