package com.hcmute.slayz.utils;

import com.hcmute.slayz.data.entities.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
@RequiredArgsConstructor
public class JwtTokenUtil {

  public static final long EXPIRE_DURATION_TOKEN = 24 * 60 * 60 * 1000; // 1 day
  public static final long EXPIRE_DURATION_REFRESH_TOKEN = 7 * 24 * 60 * 60 * 1000; // 7 day

  private final UserDetailsService userDetailsService;
  @Value("${app.jwt.secret}")
  private String SECRET_KEY;

  public String generateAccessToken(User user) {
    return Jwts.builder().setSubject(String.format("%s,%s", user.getId(), user.getEmail()))
            .setIssuedAt(new Date()).setExpiration(new Date(System.currentTimeMillis() + EXPIRE_DURATION_TOKEN))
            .signWith(SignatureAlgorithm.HS512, SECRET_KEY).compact();
  }

  public String generateRefreshToken(User user) {
    return Jwts.builder().setSubject(String.format("%s,%s", user.getId(), user.getEmail()))
            .setIssuedAt(new Date()).setExpiration(new Date(System.currentTimeMillis() + EXPIRE_DURATION_REFRESH_TOKEN))
            .signWith(SignatureAlgorithm.HS512, SECRET_KEY).compact();
  }

  //used to verify a given JWT. It returns true if the JWT is verified, or false otherwise.
  public boolean validateAccessToken(String token) {
    try {
      Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);
      return true;
    } catch (Exception ex) {
      return false;
    }
  }

  public UserDetails getUserDetails(String token) {
    User userDetails;
    String[] jwtSubject = getSubject(token).split(",");
    userDetails = (User) userDetailsService.loadUserByUsername(jwtSubject[1]);
    return userDetails;
  }

  //gets the value of the subject field of a given token.
  //The subject contains User ID and email, which will be used to recreate a User object.
  public String getSubject(String token) {
    return parseClaims(token).getSubject();
  }

  private Claims parseClaims(String token) {
    return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
  }
}
