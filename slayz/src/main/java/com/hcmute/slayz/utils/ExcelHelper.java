package com.hcmute.slayz.utils;

import com.hcmute.slayz.dto.response.invoice.InvoiceResponse;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class ExcelHelper {
  private static String[] HEADER_BILL = new String[]{"Mã hóa đơn", "Khách hàng", "SĐT", "Email", "Địa chỉ", "Thanh toán", "Trạng thái", "Giao hàng", "Ngày đặt", "Tổng cộng", "Phí vận chuyển", "Ghi chú"};

  public static ByteArrayInputStream invoiceToExcel(List<InvoiceResponse> data, String sheetName) {
    try {
      Workbook workbook = new XSSFWorkbook();
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      Sheet sheet = workbook.createSheet(sheetName);

      // Header
      Row headerRow = sheet.createRow(0);
      headerRow.setHeight((short) 600);
      XSSFCellStyle headerStyle = (XSSFCellStyle) workbook.createCellStyle();
      headerStyle.setBorderBottom(BorderStyle.THIN);
      headerStyle.setBorderTop(BorderStyle.THIN);
      headerStyle.setBorderRight(BorderStyle.THIN);
      headerStyle.setBorderLeft(BorderStyle.THIN);
      headerStyle.setAlignment(HorizontalAlignment.CENTER);
      headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
      headerStyle.setFillBackgroundColor(IndexedColors.BLUE.index);
      for (int col = 0; col < HEADER_BILL.length; col++) {
        Cell cell = headerRow.createCell(col);
        cell.setCellValue(HEADER_BILL[col]);
        cell.setCellStyle(headerStyle);
        if (col == 4) {
          sheet.setColumnWidth(col, 10000);
        } else if (col == 1 || col == 11) {
          sheet.setColumnWidth(col, 8000);
        } else {
          sheet.setColumnWidth(col, 5000);
        }
      }
      XSSFCellStyle rowStyle = (XSSFCellStyle) workbook.createCellStyle();
      rowStyle.setBorderBottom(BorderStyle.THIN);
      rowStyle.setBorderTop(BorderStyle.THIN);
      rowStyle.setBorderRight(BorderStyle.THIN);
      rowStyle.setBorderLeft(BorderStyle.THIN);
      rowStyle.setAlignment(HorizontalAlignment.LEFT);
      rowStyle.setVerticalAlignment(VerticalAlignment.CENTER);
      rowStyle.setWrapText(true);
      int rowIdx = 1;
      for (InvoiceResponse invoice : data) {
        Row row = sheet.createRow(rowIdx++);
        row.setHeight((short) 600);
        Cell cell = row.createCell(0, CellType.NUMERIC);
        cell.setCellValue(invoice.getId());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(1, CellType.STRING);
        cell.setCellValue(invoice.getFullNameUser());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(2, CellType.STRING);
        cell.setCellValue(invoice.getPhone());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(3, CellType.NUMERIC);
        cell.setCellValue(invoice.getEmailUser());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(4, CellType.STRING);
        cell.setCellValue(String.format("%s, %s, %s", invoice.getFullName(), invoice.getPhone(), invoice.getAddress()));
        cell.setCellStyle(rowStyle);

        cell = row.createCell(5, CellType.STRING);
        cell.setCellValue(invoice.getPaymentStatus());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(6, CellType.STRING);
        cell.setCellValue(invoice.getStatus());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(7, CellType.STRING);
        cell.setCellValue(invoice.getDelivery());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(8, CellType.STRING);
        cell.setCellValue(invoice.getDate());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(9, CellType.NUMERIC);
        cell.setCellValue(invoice.getTotal());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(10, CellType.NUMERIC);
        cell.setCellValue(invoice.getDeliveryFee());
        cell.setCellStyle(rowStyle);

        cell = row.createCell(11, CellType.STRING);
        cell.setCellValue(invoice.getNote());
        cell.setCellStyle(rowStyle);

      }
      workbook.write(out);
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
    }
  }
}
