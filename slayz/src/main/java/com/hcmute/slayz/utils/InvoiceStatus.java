package com.hcmute.slayz.utils;

public enum InvoiceStatus {
  PENDING, DELIVERING, COMPLETED,CANCELLED
}
