package com.hcmute.slayz.utils;

public enum PaymentStatus {
  PENDING, PAID, CANCELLED
}
