package com.hcmute.slayz.exceptions;

public class InvalidValueException extends RuntimeException{
    public InvalidValueException(){

    }
    public InvalidValueException(String message){
        super(message);
    }
}
