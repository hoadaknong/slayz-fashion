package com.hcmute.slayz.exceptions;

public class InvalidFieldException extends RuntimeException {

    private String resource;
    private String fieldName;
    private String action;

    public InvalidFieldException(){
        super();
    }

    public InvalidFieldException(String message){
        super(message);
    }

    public InvalidFieldException(String resource, String fieldName, String action) {
        super(String.format("field %s in %s has an inappropriate value for action %s", fieldName , resource, action));
        this.resource = resource;
        this.fieldName = fieldName;
        this.action = action;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
