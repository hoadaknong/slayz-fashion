package com.hcmute.slayz.exceptions;

public class UnauthorizedException extends RuntimeException{
  public UnauthorizedException() {
  }

  public UnauthorizedException(String message) {
    super(message);
  }
}
