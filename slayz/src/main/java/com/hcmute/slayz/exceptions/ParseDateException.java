package com.hcmute.slayz.exceptions;

public class ParseDateException extends RuntimeException{
    public ParseDateException() {
        super();
    }

    public ParseDateException(String message) {
        super(message);
    }
}
