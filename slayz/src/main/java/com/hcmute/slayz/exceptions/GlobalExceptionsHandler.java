package com.hcmute.slayz.exceptions;

import com.hcmute.slayz.models.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import javax.mail.SendFailedException;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import java.util.Objects;

@ControllerAdvice
public class GlobalExceptionsHandler {
  @ExceptionHandler({MethodArgumentNotValidException.class, IllegalArgumentException.class})
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handleInvalidArgument(MethodArgumentNotValidException exception,
                                                                 WebRequest webRequest) {
    webRequest.getDescription(false);
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.BAD_REQUEST,
                    Objects.requireNonNull(exception.getFieldError()).getDefaultMessage(),
                    null));
  }


  @ExceptionHandler({UpdateInfoUserException.class})
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handleUserException(RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.NOT_ACCEPTABLE,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handlerResourceNotFoundException(
          RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.NOT_FOUND,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler(UnauthorizedException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handlerUnauthorizedException(
          RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.UNAUTHORIZED,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler(
          {
                  ResourceAlreadyExistsException.class,
                  InvalidFieldException.class,
                  ConstraintViolationException.class,
                  IllegalStateException.class, SendFailedException.class
          })
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handleResourceAlreadyExistsException(
          RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.NOT_ACCEPTABLE,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler({ParseDateException.class, FileException.class})
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handleParseDateException(RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.BAD_REQUEST,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler(InvalidValueException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handleInvalidCost(RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.BAD_REQUEST,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler(PersistenceException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handlePersistenceException(RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler(NumberFormatException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handleNumberFormatException(RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.BAD_REQUEST,
                    exception.getMessage(),
                    null));
  }

  @ExceptionHandler(ForbiddenException.class)
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  protected ResponseEntity<ResponseObject> handleForbiddenException(RuntimeException exception) {
    return ResponseEntity
            .status(HttpStatus.OK)
            .body(new ResponseObject(
                    HttpStatus.FORBIDDEN,
                    exception.getMessage(),
                    null));
  }

}
