package com.hcmute.slayz.exceptions;

public class NotUniqueException extends Exception {

    public NotUniqueException() {
    }

    public NotUniqueException(String message) {
        super(message);
    }
    
}
