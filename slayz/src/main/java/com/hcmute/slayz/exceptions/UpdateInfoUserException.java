package com.hcmute.slayz.exceptions;

public class UpdateInfoUserException extends RuntimeException{
  public UpdateInfoUserException() {
  }

  public UpdateInfoUserException(String message) {
    super(message);
  }
}
