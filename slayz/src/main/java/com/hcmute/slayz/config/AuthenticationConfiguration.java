package com.hcmute.slayz.config;

import com.hcmute.slayz.data.entities.Role;
import com.hcmute.slayz.data.repositories.RoleRepository;
import com.hcmute.slayz.data.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AuthenticationConfiguration {

  private UserRepository userRepository;

  private RoleRepository roleRepository;

  @Bean
  UserDetailsService userDetailsService() {
    return username -> userRepository
            .findByEmailOrPhone(username)
            .orElseThrow(() -> new UsernameNotFoundException("Người dùng không tồn tại!"));
  }

  @Bean
  PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  AuthenticationManager authenticationManager(org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration authConfig)
          throws Exception {
    return authConfig.getAuthenticationManager();
  }

  @Bean
  void initRoleUser(){
    Role userRole = roleRepository.findByName("ROLE_USER").orElse(null);
    Role userAdmin = roleRepository.findByName("ROLE_ADMIN").orElse(null);
    Role userStaff = roleRepository.findByName("ROLE_STAFF").orElse(null);
    if(userRole == null){
      userRole = new Role();
      userRole.setName("ROLE_USER");
      userRole.setDescription("Khách hàng");
      roleRepository.save(userRole);
    }
    if(userAdmin == null){
      userAdmin = new Role();
      userAdmin.setName("ROLE_ADMIN");
      userAdmin.setDescription("Quản trị viên");
      roleRepository.save(userAdmin);
    }
    if(userStaff == null){
      userStaff = new Role();
      userStaff.setName("ROLE_STAFF");
      userStaff.setDescription("Nhân viên");
      roleRepository.save(userStaff);
    }
  }
}
