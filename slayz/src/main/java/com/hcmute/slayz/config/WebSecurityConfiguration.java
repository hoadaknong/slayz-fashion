package com.hcmute.slayz.config;

import com.hcmute.slayz.data.repositories.UserRepository;
import com.hcmute.slayz.filters.JwtTokenFilter;
import com.hcmute.slayz.models.ResponseObject;
import com.hcmute.slayz.utils.MapHelper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

import static com.hcmute.slayz.utils.Constants.ORIGINS;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
public class WebSecurityConfiguration {
  @Autowired
  private UserRepository userRepo;
  @Autowired
  private JwtTokenFilter jwtTokenFilter;

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.csrf().disable();
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    http.headers().frameOptions().disable();
    http.authorizeRequests().antMatchers("/auth/login", "/docs/**", "/swagger-ui/index.html#/").permitAll().anyRequest().permitAll();
    http.exceptionHandling().accessDeniedHandler((request, response, ex) -> {
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      response.setCharacterEncoding("UTF-8");
      response.setStatus(HttpServletResponse.SC_OK);
      Map<String, Object> map = new HashMap<String, Object>();
      ResponseObject responseObject = new ResponseObject(HttpStatus.FORBIDDEN, "Bạn không có quyền truy cập!");
      map = MapHelper.convertObject(responseObject);
      response.getWriter().write(new JSONObject(map).toString());
    });
    http.exceptionHandling().authenticationEntryPoint((request, response, ex) -> {
      response.setContentType(MediaType.APPLICATION_JSON_VALUE);
      response.setCharacterEncoding("UTF-8");
      response.setStatus(HttpServletResponse.SC_OK);
      Map<String, Object> map = new HashMap<String, Object>();
      ResponseObject responseObject = new ResponseObject(HttpStatus.UNAUTHORIZED, "Đã xảy ra lỗi xác thực! Vui lòng đăng nhập để tiếp tục!");
      map = MapHelper.convertObject(responseObject);
      response.getWriter().write(new JSONObject(map).toString());
    });
    http.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);
    return http.build();
  }

  @Bean
  public WebMvcConfigurer corsConfigurer() {
    return new WebMvcConfigurer() {
      @Override
      public void addCorsMappings(CorsRegistry registry) {
        registry
                .addMapping("/**")
                .allowedMethods("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS")
                .allowedOrigins(ORIGINS)
                .allowCredentials(true);
      }
    };
  }
}
