# WELCOME TO SLAYZ
This project was bootstrapped with [Spring Initializr](https://start.spring.io/).


## Project Overview

- Name: **SlayZ Fashion SHOP**
- Author: [Hòa Phạm](https://www.facebook.com/hoaffffff/)
- Collaborator: [Tran Anh Tien](https://www.facebook.com/teddy.tran.756)
- Main tech: **Java**, **Spring Boot**, **MySQL**,

## First script

In the project directory, you can run:
### `mvn clean install`

To install all dependencies in package.json file.\

And then you can run:
### `mvn spring-boot:run`

To start the app.\
Open [http://localhost:8080/swagger-ui/index.html#/](http://localhost:8080/swagger-ui/index.html#/) to view it in your browser.
![img.png](img.png)