# WELCOME TO SLAYZ
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Project Overview

- Name: **SlayZ Fashion Client UI**
- Author: [Hòa Phạm](https://www.facebook.com/hoaffffff/)
- Collaborator: [Tran Anh Tien](https://www.facebook.com/teddy.tran.756)
- Main tech: **ReactJS**, **TailwindCSS**,

## First script

In the project directory, you can run:
### `yarn install`

To install all dependencies in package.json file.\

And then you can run:
### `yarn start`

To start the app in the development mode.\
Open [http://localhost:3006](http://localhost:3006) to view it in your browser.\
[Click here](http://slayz.s3-website-ap-southeast-1.amazonaws.com/) to view it in your browser.
 
![Trang chu](./resources/1.png)
![Trang chu](./resources/2.png)
![Trang chu](./resources/3.png)
![Trang chu](./resources/4.png)
![Trang chu](./resources/5.png)
![Trang chu](./resources/6.png)
![Trang chu](./resources/7.png)
![Trang chu](./resources/8.png)
![Trang chu](./resources/9.png)
![Trang chu](./resources/10.png)
![Trang chu](./resources/11.png)
![Trang chu](./resources/12.png)
![Trang chu](./resources/13.png)
![Trang chu](./resources/14.png)
![Trang chu](./resources/15.png)
![Trang chu](./resources/16.png)
![Trang chu](./resources/17.png)
![Trang chu](./resources/18.png)