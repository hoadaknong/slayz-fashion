/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}"],
  theme: {
    fontFamily: {
      nunito: ["Nunito", "system-ui"],
    },
    extend: {
      backgroundColor: {
        "main-bg": "#ff5a20",
        "second-bg": "#fd8644",
        "footer-bg": "#06283D",
        "hover-main-bg": "#d64700",
        "main-dark-bg": "#20232A",
        "secondary-dark-bg": "#33373E",
        "light-gray": "#F7F7F7",
        "half-transparent": "rgba(0, 0, 0, 0.5)",
      },
      colors: {
        "primary-color": "#ff5a20",
        "second-color": "#DFF6FF",
      },
    },
  },
  plugins: [require("flowbite/plugin")],
};
