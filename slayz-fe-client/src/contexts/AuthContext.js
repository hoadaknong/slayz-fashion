import {createContext, useEffect, useState} from "react";
import {UserService} from "../services/user.service";
import Swal from "sweetalert2";
import {AuthService} from "../services/auth.service";
import {api} from "../api/axios";

export const AuthContext = createContext();

const AuthContextProvider = ({children}) => {
  const [authState, setAuthState] = useState({
    isAuthenticated: true,
    user: {},
  });
  const loginUser = async (username, password) => {
    try {
      const data = {username, password};
      const response = await AuthService.login(data);
      if (response.status === "OK") {
        setAuthState({
          ...authState,
          isAuthenticated: true,
        });
        localStorage.setItem("token", JSON.stringify(response.data.accessToken));
        api.interceptors.request.use((config) => {
          const token = response.data.accessToken;
          config.headers["Authorization"] = `Bearer ${token}`;
          return config;
        });
        const userResponse = await UserService.getCurrentUser();
        if (userResponse.status === "OK") {
          setAuthState(() => {
            return {
              isAuthenticated: true,
              user: userResponse.data,
            };
          });
        }
        return true;
      } else {
        Swal.fire({
          icon: "error",
          title: "Đăng nhập",
          text: "Sai thông tin tài khoản hoặc mật khẩu!",
          footer:
            '<a href="www.facebook.com/slayz.vn/">Liên hệ SlayZ để biêt thêm thông tin chi tiết</a>',
        });
        return false;
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Lỗi",
        text: "Sai thông tin tài khoản hoặc mật khẩu!",
        footer:
          '<a href="www.facebook.com/slayz.vn/">Liên hệ SlayZ để biêt thêm thông tin chi tiết</a>',
      });
      return false;
    }
  };

  const getCurrentUser = async () => {
    const token = JSON.parse(localStorage.getItem("token"));
    if (String(token).trim() !== "") {
      try {
        const userResponse = await UserService.getCurrentUser();
        if (userResponse.status === "OK") {
          setAuthState(() => {
            return {
              isAuthenticated: true,
              user: userResponse.data,
            };
          });
        } else {
          setAuthState({
            isAuthenticated: false,
            user: {},
          });
        }
      } catch (error) {
        console.log(error);
        setAuthState({
          isAuthenticated: false,
          user: {},
        });
      }
    } else {
      setAuthState({
        isAuthenticated: false,
        user: {},
      });
    }
  };

  const logoutUser = () => {
    AuthService.logout();
    localStorage.removeItem("token");
    setAuthState({
      user: {},
      isAuthenticated: false,
    });
  };

  const authContextData = {
    logoutUser,
    loginUser,
    authState,
    getCurrentUser,
  };

  useEffect(() => {
    (async function () {
      await getCurrentUser();
    })()
  }, []);
  return <AuthContext.Provider value={{...authContextData}}>{children}</AuthContext.Provider>;
};

export default AuthContextProvider;
