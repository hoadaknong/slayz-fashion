import React, { createContext, useContext, useState } from "react";
import { CartService } from "../services/cart.service";
const DataContext = createContext();

export const DataProvider = ({ children }) => {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user_info"))?.data?.userInfo);

  const [addressList, setAddressList] = useState([]);
  //Category data
  const [categoryData, setCategoryData] = useState([]);

  //Size data
  const [sizeData, setSizeData] = useState([]);

  //Discount data
  const [discountData, setDiscountData] = useState([]);

  //Color data
  const [colorData, setColorData] = useState([]);

  //Product data
  const [productData, setProductData] = useState([]);

  //User data
  const [userData, setUserData] = useState([]);

  const [cartData, setCartData] = useState({});

  const [cartDetails, setCartDetails] = useState([]);

  const [flagChange, setFlagChange] = useState(false);

  const [isOpenSideBarFilter, setIsOpenSideBarFilter] = useState(false);

  const fetchCartData = () => {
    const userId = JSON.parse(localStorage.getItem("user"))?.data?.userInfo?.id;
    CartService.getCartByUserId(userId).then((response) => {
      if (response.status === "OK") {
        CartService.getCartById(response.data.id).then((response) => {
          setCartData(response.data);
        });
        CartService.getAllCartDetailByCartId(response.data?.id).then((response) => {
          if (response.status === "OK") {
            setCartDetails(response.data);
          }
        });
      }
    });
  };
  const data = {
    user,
    cartData,
    cartDetails,
    categoryData,
    sizeData,
    discountData,
    colorData,
    productData,
    userData,
    addressList,
    flagChange,
    isOpenSideBarFilter,
  };
  const setData = {
    setUser,
    setCartData,
    setCategoryData,
    setSizeData,
    setDiscountData,
    setColorData,
    setProductData,
    setUserData,
    fetchCartData,
    setCartDetails,
    setAddressList,
    setFlagChange,
    setIsOpenSideBarFilter,
  };
  return <DataContext.Provider value={{ ...data, ...setData }}>{children}</DataContext.Provider>;
};

export const useDataContext = () => useContext(DataContext);
