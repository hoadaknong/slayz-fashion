/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import {useEffect, useRef, useState} from "react";
import {InvoiceService} from "../services/invoice.service";
import {UserService} from "../services/user.service";

const useInvoiceData = (id) => {
  const [invoice, setInvoice] = useState(null);
  const [invoiceList, setInvoiceList] = useState([]);
  const [statusArr, setStatusArr] = useState([]);
  const [page, setPage] = useState(1);
  const [status, setStatus] = useState("PENDING");
  const [paymentStatus, setPaymentStatus] = useState("all");
  const userId = useRef(null)
  const totalPage = useRef(100);
  const fetchInvoiceList = async () => {
    const currentUser = await UserService.getCurrentUser();
    userId.current = currentUser.data.id
    const invoiceResponseByUserId = await InvoiceService.getInvoiceByUserId(userId.current, page, 4, status, paymentStatus);
    totalPage.current = invoiceResponseByUserId.data?.totalPage;
    setInvoiceList(invoiceResponseByUserId.data.details);
  };

  const fetchInvoiceById = async () => {
    const response = await InvoiceService.getInvoiceById(id);
    setInvoice(() => {
      if (response.status === "OK") return response.data;
      return null;
    });
  };

  useEffect(() => {
    if (id !== undefined) {
      (async function () {
        await fetchInvoiceById();
      })()
    } else {
      (async function () {
        await fetchInvoiceList();
      })()
    }
  }, [status, paymentStatus]);
  useEffect(() => {
    if (page !== 1) {
      (async function () {
        const invoiceResponseByUserId = await InvoiceService.getInvoiceByUserId(userId.current, page, 4, status, paymentStatus);
        setInvoiceList((prev) => {
          return [...prev, ...invoiceResponseByUserId.data.details]
        });
      })()
    }
  }, [page]);
  return {
    invoice, invoiceList, setStatusArr, setPage, totalPage, page, setStatus, setPaymentStatus
  };
};

export default useInvoiceData;
