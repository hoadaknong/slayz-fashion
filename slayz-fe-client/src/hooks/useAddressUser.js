/* eslint-disable react-hooks/exhaustive-deps */
import {useEffect, useRef, useState} from "react";
import {AddressService} from "../services/address.service";
import Swal from "sweetalert2";
import {useNavigate} from "react-router-dom";
import {UserService} from "../services/user.service";

const useAddressUser = (id) => {
  const [address, setAddress] = useState();
  const [addresses, setAddresses] = useState([]);
  const [page, setPage] = useState(1)
  const totalPage = useRef(100)
  const navigate = useNavigate();
  const userId = useRef(null)
  const fetchAddressList = async () => {
    const currentUser = await UserService.getCurrentUser();
    userId.current = currentUser.data.id
    const addressResponse = await AddressService.getAllAddressByUserId(userId.current, page, 4)
    if (addressResponse?.status === "OK") {
      setAddresses(addressResponse?.data.details);
      totalPage.current = addressResponse?.data?.totalPage
    }
  };

  const removeAddressById = async (id) => {
    const addressDeleteResponse = await AddressService.removeAddressById(id)
    if (addressDeleteResponse.status === "OK") {
      await fetchAddressList();
    }
  };

  const getAddressById = () => {
    if (id !== null) {
      AddressService.getAddressById(id).then((response) => {
        if (response.status === "OK") {
          setAddress(response.data);
        }
      });
    }
  };

  const createAddress = (data) => {
    if (validate(data)) {
      AddressService.createAddress(data).then((response) => {
        if (response.status === "OK") {
          navigate(-1);
        } else {
          Swal.fire({
            icon: "error",
            title: "Lỗi",
            text: "Đã xảy ra lỗi!",
            footer:
              '<a href="www.facebook.com/slayz.vn/">Liên hệ SlayZ để biêt thêm thông tin chi tiết</a>',
          });
        }
      });
    }
  };

  const validate = (obj) => {
    if (obj?.province?.trim() === "" || obj?.province === undefined) {
      alert("Hãy chọn tỉnh/thành phố");
      return false;
    }
    if (obj?.district?.trim() === "" || obj?.district === undefined) {
      alert("Hãy chọn quận/huyện");
      return false;
    }
    if (obj?.ward?.trim() === "" || obj?.ward === undefined) {
      alert("Hãy chọn xã/phường!");
      return false;
    }
    if (obj?.addressLine?.trim() === "") {
      alert("Hãy nhập thông tin địa chỉ!");
      return false;
    }
    if (obj?.fullName?.trim() === "") {
      alert("Hãy nhập họ tên đầy đủ!");
      return false;
    }
    if (!obj?.phone?.match(/^\d{10}$/)) {
      alert("Sai định dạng số điện thoại!");
      return false;
    }
    return true;
  };

  useEffect(() => {
    if (id === null) {
      (async function () {
        await fetchAddressList();
      })()
    } else {
      getAddressById();
    }
  }, []);
  useEffect(() => {
    if (page !== 1) {
      (async function () {
        const addressResponse = await AddressService.getAllAddressByUserId(userId.current, page, 4)
        if (addressResponse?.status === "OK") {
          setAddresses((prev) => {
            return [...prev, ...addressResponse?.data.details]
          });
        }
      })()
    }
  }, [page]);
  return {fetchAddressList, removeAddressById, address, createAddress, page, setPage, totalPage,addresses};
};

export default useAddressUser;
