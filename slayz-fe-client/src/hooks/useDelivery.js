/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import axios from "axios";
import { useEffect, useState } from "react";

const SHOP_ID = 3502746;
const SHOP_WARD_CODE = "90742";
const SHOP_DISTRICT_CODE = 3695;
const SHOP_PROVINCE_CODE = 202;

const useDelivery = () => {
  const [services, setServices] = useState([]);
  const [deliveryFee, setFee] = useState({});
  const address = JSON.parse(localStorage.getItem("address"));
  async function getService(
    shopId = 3502746,
    fromDistrict = SHOP_DISTRICT_CODE,
    to_district = 1486,
  ) {
    try {
      const service = await axios.get(
        `https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/available-services?shop_id=${shopId}&from_district=${fromDistrict}&to_district=${to_district}`,
        { headers: { token: "60be7953-6f94-11ed-b09a-9a2a48e971b0" } },
      );
      if (service.data.code === 200) {
        setServices(service.data.data);
      }
    } catch (error) {
      console.log(error);
    }
  }

  function calculateFee(service_id, insurance_value) {
    var data = JSON.stringify({
      service_id: service_id,
      insurance_value: insurance_value,
      coupon: null,
      from_district_id: SHOP_DISTRICT_CODE,
      to_district_id: address.districtId,
      to_ward_code: address.wardId,
      height: 2,
      length: 20,
      weight: 50,
      width: 20,
    });

    var config = {
      method: "post",
      url: "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/fee",
      headers: {
        token: "60be7953-6f94-11ed-b09a-9a2a48e971b0",
        "Content-Type": "application/json",
      },
      data: data,
    };

    axios(config)
      .then((response) => {
        setFee(response.data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  useEffect(() => {
    (async function () {
      if (address !== undefined) {
        getService(SHOP_ID, 3695, address?.districtId);
      }
    })();
  }, []);

  return { services, getService, deliveryFee, calculateFee };
};

export default useDelivery;
