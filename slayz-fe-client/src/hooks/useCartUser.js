/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useEffect, useState } from "react";
import { CartService } from "../services/cart.service";
import { InvoiceService } from "../services/invoice.service";
import { AuthContext } from "../contexts/AuthContext";

const useCartUser = () => {
  const { authState } = useContext(AuthContext);
  const [cartInfo, setCartInfo] = useState(null);
  const [cartDetailList, setCartDetailList] = useState([{}]);
  const checkOut = async (
    note,
    addressId,
    paymentMethod,
    paymentStatus,
    listProduct,
    deliveryFee,
    deliveryMethodName,
  ) => {
    const data = {
      userId: authState.user?.id,
      paymentMethod: paymentMethod,
      paymentStatus: paymentStatus,
      listDetailId: listProduct,
      note: note,
      addressId: addressId,
      deliveryFee: deliveryFee,
      deliveryMethodName: deliveryMethodName,
    };
    console.log(data);
    const fetch = await InvoiceService.checkOut(data);
    return fetch.data;
  };

  useEffect(() => {
    (async function () {
      const fetchCartId = await CartService.getCartByUserId(authState.user?.id);
      const cartData = await CartService.getCartById(fetchCartId?.data?.id);
      setCartInfo(cartData.data);
    })();
    (async function () {
      const fetchCartId = await CartService.getCartByUserId(authState.user?.id);
      const fetchListCartDetail = await CartService.getAllCartDetailByCartId(fetchCartId?.data?.id);
      setCartDetailList(fetchListCartDetail.data);
    })();
  }, []);

  return { cartInfo, cartDetailList, checkOut };
};

export default useCartUser;
