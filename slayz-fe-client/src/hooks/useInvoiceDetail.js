/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react";
import { InvoiceService } from "../services/invoice.service";

const useInvoiceDetail = (invoiceId) => {
  const [invoiceDetailList, setInvoiceDetailList] = useState([]);

  const fetchInvoiceDetailByInvoiceId = () => {
    InvoiceService.getAllInvoiceDetailByInvoiceId(invoiceId).then((response) => {
      if (response.status === "OK") {
        setInvoiceDetailList(response.data);
      }
    });
  };

  useEffect(() => {
    fetchInvoiceDetailByInvoiceId();
  }, [invoiceId]);
  return { invoiceDetailList };
};

export default useInvoiceDetail;
