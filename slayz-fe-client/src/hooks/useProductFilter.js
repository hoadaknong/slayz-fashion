/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState, useRef } from "react";
import { CategoryService } from "../services/category.service";
import { ColorService } from "../services/color.service";
import { ProductService } from "../services/product.service";
import { SizeService } from "../services/size.service";

const useProductFilter = () => {
  const limit = 12;
  const isInit = useRef(true);
  const isInitPage = useRef(true);
  const [filterData, setFilterData] = useState();
  const [categories, setCategories] = useState([]);
  const [colors, setColors] = useState([]);
  const [sizes, setSizes] = useState([]);
  const [categoryFilter, setCategoryFilter] = useState([]);
  const [colorFilter, setColorFilter] = useState([]);
  const [sizeFilter, setSizeFilter] = useState([]);
  const [minPrice, setMinPrice] = useState(10);
  const [maxPrice, setMaxPrice] = useState(2000000);
  const [page, setPage] = useState(1);
  const [pageList, setPageList] = useState([1]);
  const [isCategoryInit, setCategoryInit] = useState(true);
  const [isSizeInit, setSizeInit] = useState(true);
  const [isColorInit, setColorInit] = useState(true);
  const [name, setName] = useState("");
  const [gender, setGender] = useState(["MALE", "FEMALE", "UNISEX"]);
  const [message, setMessage] = useState("");
  const [sortOption, setSortOption] = useState("newest");
  const [payload, setPayload] = useState({});
  const fetchData = async () => {
    const categoryResponse = await CategoryService.getAllCategory(1,100);
    setCategories(categoryResponse.data.details);
    const colorResponse = await ColorService.getAllColor();
    setColors(colorResponse.data.details);
    const sizeResponse = await SizeService.getAllSize();
    setSizes(sizeResponse.data);
    isInitPage.current = false;
    isInit.current = false;
    filterApplyOnClick();
  };

  const filterApplyOnClick = () => {
    if (categoryFilter.length === 0) {
      setCategoryInit(true);
      setCategoryFilter(() => {
        return categories.map((category) => category.id);
      });
    }
    if (colorFilter.length === 0) {
      setColorInit(true);
      setColorFilter(() => {
        return colors.map((color) => color.id);
      });
    }
    if (sizeFilter.length === 0) {
      setSizeFilter(() => {
        return sizes.map((size) => size.id);
      });
    }
    setPage(1);
    const data = {
      categories: categoryFilter,
      colors: colorFilter,
      limit: limit,
      maxPrice: maxPrice,
      minPrice: minPrice,
      name: name,
      offset: page,
      sizes: sizeFilter,
      gender: gender,
      sortOption: sortOption,
    };
    setPayload(data);
  };

  const pageOnChange = () => {
    if (categoryFilter.length === 0) {
      var categoryArr = [];
      for (let i = 0; i < categories.length; i++) {
        categoryArr.push(categories[i].id);
      }
      setCategoryFilter(() => {
        return [...categoryArr];
      });
    }
    if (colorFilter.length === 0) {
      var colorArr = [];
      for (let i = 0; i < colors.length; i++) {
        colorArr.push(colors[i].id);
      }
      setColorFilter(() => {
        return [...colorArr];
      });
    }
    if (sizeFilter.length === 0) {
      var sizeArr = [];
      for (let i = 0; i < sizes.length; i++) {
        sizeArr.push(sizes[i].id);
      }
      setSizeFilter(() => {
        return [...sizeArr];
      });
    }
    const data = {
      categories: categoryFilter,
      colors: colorFilter,
      limit: limit,
      maxPrice: maxPrice,
      minPrice: minPrice,
      name: name,
      offset: page,
      sizes: sizeFilter,
      gender: gender,
      sortOption: sortOption,
    };
    ProductService.filterData(data).then((response) => {
      if (response.status === "OK") {
        setFilterData(response.data);
        var pageCount = 0;
        if (response.data.totalRecord / limit > parseInt(response.data.totalRecord / limit)) {
          pageCount = parseInt(response.data.totalRecord / limit) + 1;
        } else {
          pageCount = parseInt(response.data.totalRecord / limit);
        }
        var pageArray = [];
        for (let i = 0; i < pageCount; i++) {
          pageArray.push(i + 1);
        }
        setPageList(pageArray);
      }
      window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    });
  };

  useEffect(() => {
    if (!isInitPage.current) {
      pageOnChange();
    }
  }, [page]);
  useEffect(() => {
    ProductService.filterData(payload).then((response) => {
      if (response.status === "OK") {
        setFilterData(response.data);
        var pageCount = 0;
        if (response.data.totalRecord / limit > parseInt(response.data.totalRecord / limit)) {
          pageCount = parseInt(response.data.totalRecord / limit) + 1;
        } else {
          pageCount = parseInt(response.data.totalRecord / limit);
        }
        var pageArray = [];
        for (let i = 0; i < pageCount; i++) {
          pageArray.push(i + 1);
        }
        setPageList(pageArray);
        setPage(response.data.currentPage);
      }
    });
  }, [payload]);
  useEffect(() => {
    if (!isInit.current) {
      filterApplyOnClick();
    }
  }, [categoryFilter, colorFilter, sizeFilter, maxPrice, minPrice, name, gender, sortOption]);
  useEffect(() => {
    fetchData();
  }, []);
  return {
    colors,
    categories,
    sizes,
    filterData,
    pageList,
    minPrice,
    maxPrice,
    setMinPrice,
    setMaxPrice,
    setCategoryFilter,
    setSizeFilter,
    setColorFilter,
    page,
    setPage,
    isCategoryInit,
    isColorInit,
    isSizeInit,
    setCategoryInit,
    setColorInit,
    setSizeInit,
    name,
    setName,
    gender,
    setGender,
    message,
    setMessage,
    sortOption,
    setSortOption,
    setFilterData,
  };
};

export default useProductFilter;
