import {ProductGallery, Quote, Title} from "../components";
import banner1 from "../assets/HomePage1.png";
import banner2 from "../assets/banner_2.jpg";
import banner4 from "../assets/banner_4.jpg";
import banner5 from "../assets/banner_5.jpg";
import banner6 from "../assets/banner_6.jpg";
import category_1 from "../assets/3Cate1.png";
import category_2 from "../assets/3Cate2.png";
import category_3 from "../assets/3Cate3_2.png";
import {useState} from "react";
import {ProductService} from "../services/product.service";
import {useEffect} from "react";
import {Link, useNavigate} from "react-router-dom";
import {Swiper, SwiperSlide} from "swiper/react";
import {Navigation, Pagination, Zoom, Parallax} from "swiper";
import "swiper/css";
import "swiper/css/bundle";
import useWindowSize from "../hooks/useWindowSize";
import {BannerService} from "../services/banner.service";

const categoryList = [
  {
    id: 0,
    category: "NAM",
    quantity: 30,
    image: category_1,
  },
  {
    id: 1,
    category: "NỮ",
    quantity: 50,
    image: category_2,
  },
  {
    id: 2,
    category: "UNISEX",
    quantity: 20,
    image: category_3,
  },
];
const defaultBanners = [
  {
    image: banner1,
    linkTo: "ao-vest-tay-lung-xe-sai-cai-nut-relaxed.html"
  },
  {
    image: banner2,
    linkTo: "ao-vest-tay-lung-xe-sai-cai-nut-relaxed.html"
  },
  {
    image: banner4,
    linkTo: "ao-vest-tay-lung-xe-sai-cai-nut-relaxed.html"
  },
  {
    image: banner5,
    linkTo: "ao-vest-tay-lung-xe-sai-cai-nut-relaxed.html"
  },
  {
    image: banner6,
    linkTo: "ao-vest-tay-lung-xe-sai-cai-nut-relaxed.html"
  }
]
const peopleTalks = [
  {
    author: "Trần Anh Tiến",
    content: "SlayZ đang tạo ra những bộ trang phục sản xuất trong nước hoàn toàn có thể sánh " +
      "ngang với các thương hiệu thời trang nam đến từ nước ngoài về kiểu dáng, chất lượng lẫn phong cách thời trang.",
    image: "https://res.cloudinary.com/dfnnmsofg/image/upload/c_pad,b_auto:predominant,fl_preserve_transparency/v1688917603/lq1mxkwc0oauvfqhfmr0.jpg?_s=public-apps"
  },
  {
    author: "Pho Nhật Vĩ",
    content: "SlayZ đang tạo ra những bộ trang phục sản xuất trong nước hoàn toàn có thể sánh " +
      "ngang với các thương hiệu thời trang nam đến từ nước ngoài về kiểu dáng, chất lượng lẫn phong cách thời trang.",
    image: "https://scontent.fsgn19-1.fna.fbcdn.net/v/t39.30808-6/294605453_1504413329993573_5547480957442953062_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=eQuA2zKvpTYAX8N9pyq&_nc_ht=scontent.fsgn19-1.fna&oh=00_AfD8bAMUbKqR_vud_hin-LDuRQBRYLVrnBz-i1vF_Ytp0g&oe=64AC2DB1"
  },
  {
    author: "Phạm Đinh Quốc Hòa",
    content: "SlayZ đang tạo ra những bộ trang phục sản xuất trong nước hoàn toàn có thể sánh ngang " +
      "với các thương hiệu thời trang nam đến từ nước ngoài về kiểu dáng, chất lượng lẫn phong cách thời trang.",
    image: "https://scontent.fsgn19-1.fna.fbcdn.net/v/t1.15752-9/357612872_966594414662083_3193136276054948775_n.jpg?stp=dst-jpg_p1080x2048&_nc_cat=100&ccb=1-7&_nc_sid=ae9488&_nc_ohc=albYS6YpeuQAX8RIZhN&_nc_ht=scontent.fsgn19-1.fna&oh=03_AdSr1__Xb0-6AZcf0BBb-ZZ1HSwnQqhUnN3HgqIIy9Kjgg&oe=64C684D0"
  }, {
    author: "Nguyễn Thanh Hường",
    content: "SlayZ đang tạo ra những bộ trang phục sản xuất trong nước hoàn toàn có thể sánh " +
      "ngang với các thương hiệu thời trang nam đến từ nước ngoài về kiểu dáng, chất lượng lẫn phong cách thời trang.",
    image: "https://scontent.fsgn19-1.fna.fbcdn.net/v/t1.15752-9/356189582_656383359294428_2534980550982592480_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=ae9488&_nc_ohc=1P1tAU0MsnoAX-l-Hre&_nc_ht=scontent.fsgn19-1.fna&oh=03_AdQRTLQ1qNDzKsxJ5vsSbBH-_18c0CgPWlREE9gzJX9ssg&oe=64C64ED2"
  }
]
const Home = () => {
  const [productData, setProductData] = useState([]);
  const [banners, setBanners] = useState(defaultBanners);
  const navigate = useNavigate();
  const textAblout =
    "Như ý nghĩa của tên gọi, trang phục của SlayZ hướng đến việc trở thành thói quen, lựa chọn hàng ngày cho nam giới trong mọi tình huống. Bởi SlayZ hiểu rằng, sự tự tin về phong cách ăn mặc sẽ làm nền tảng, tạo động lực cổ vũ mỗi người mạnh dạn theo đuổi những điều mình mong muốn. Trong đó, trang phục nam phải mang vẻ đẹp lịch lãm, hợp mốt và tạo sự thoải mái, quan trọng nhất là mang đến cảm giác “được là chính mình” cho người mặc.";
  const fetchData = async () => {
    const response = await ProductService.get12TheNewestProduct();
    if (response.status === "OK") {
      setProductData(response.data);
    }
    const bannerResponse = await BannerService.getAllBannerByType("slide");
    if (bannerResponse.status === "OK") {
      if (bannerResponse.data.length !== 0) {
        setBanners(bannerResponse.data);
      }
    }
  };
  const windowSize = useWindowSize();
  useEffect(() => {
    (async function () {
      await fetchData()
    })()
  }, []);
  window.scrollTo(0, 0);
  return (
    <div className="w-full flex flex-col md:justify-center mx-auto md:items-center xl:mt-[40px] mt-[33px]">
      <Title title={"Trang chủ"}/>
      <Swiper
        modules={[Navigation, Pagination, Zoom, Parallax]}
        spaceBetween={10}
        slidesPerView={1}
        navigation
        pagination={{clickable: true}}
        scrollbar={{draggable: true}}
        loop={true}
        className="w-full"
      >
        {banners.map((banner = {image: "", linkTo: ""}, index) => {
          return (
            <SwiperSlide key={index}>
              <div className="relative">
                <Link
                  className="hover:scale-[1.5] scale-[1.3] p-4 bg-white absolute top-[65%] right-[10%] text-orange-500 transition-all duration-300"
                  to={`/products/${banner.linkTo}`}> KHÁM PHÁ </Link>
                <img
                  className="overflow-hidden object-cover w-full h-[500px] xl:h-fit"
                  src={banner.image}
                  alt="banner 1"
                />
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
      {/* Danh mục hình ảnh */}
      <div className="relative w-full flex flex-col justify-center items-center">
        <div id="poly-shape-first" className=""></div>
        <div className="md:max-w-[1721px] w-[90%] md:mt-[150px] mt-[60px] py-2 px-3">
          <h1 className="font-bold text-white text-xl">PHÂN LOẠI SẢN PHẨM</h1>
          <p className="text-white text-sm mt-1">Hãy chọn sản phẩm ưa thích dành cho bạn</p>
        </div>
        <div className="flex md:max-w-[1721px] my-3 flex-wrap px-3 w-[90%]">
          {categoryList.map((item, index) => {
            return (
              <div key={index} className="md:w-1/3 w-full relative h-fit overflow-hidden bg-black">
                <div className="absolute w-full flex justify-end p-4">
                  <div className="w-fit">
                    <h1 className="text-red-500 font-bold">{item.category}</h1>
                    <p className="text-[#715F5B]">{item.quantity}+ sản phẩm</p>
                  </div>
                </div>
                <img src={item.image} className="w-full" alt="category_photo"/>
                <div
                  className="flex justify-center items-center w-full h-full bg-black absolute top-0 left-0 opacity-0 hover:opacity-60 transition-all duration-300">
                  <button
                    type="button"
                    className="p-3 border-white border-2 rounded-md text-white"
                    onClick={() => {
                      if (item.id === 0) {
                        navigate("/products/male");
                      } else if (item.id === 1) {
                        navigate("/products/female");
                      } else {
                        navigate("/products/unisex");
                      }
                    }}
                  >
                    KHÁM PHÁ
                  </button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      {/* Sản phẩm nổi bật */}
      <div className="xl:max-w-[1721px] mt-10 py-2 md:mx-0 mx-auto w-[90%]">
        <h1 className="font-bold text-[#55595C] text-xl md:text-left text-center">
          SẢN PHẨM MỚI NHẤT
        </h1>
      </div>
      <div className="w-full flex justify-center items-center">
        <div className="grid md:grid-cols-4 grid-cols-2 md:gap-4 gap-2 w-[90%] mx-auto">
          {productData.map((item, index) => (
            <ProductGallery {...item} key={index}/>
          ))}
        </div>
      </div>
      <div className="w-[100%] flex justify-center items-center py-8 my-8 md:w-[76%]">
        <div
          className="cursor-pointer flex justify-center items-center font-semibold text-gray-700 h-[40px] w-[180px] border border-gray-600 hover:text-white hover:bg-black transition-all"
          onClick={() => {
            navigate("/products");
          }}
        >
          KHÁM PHÁ
        </div>
      </div>
      {/* Về chúng tôi */}
      <div className="relative w-full mb-[130px] flex flex-col justify-center items-center mx-auto">
        <div className="md:w-[76%] flex-wrap flex mb-8 bg-[#EAEFF2] md:flex-row flex-col w-[90%]">
          <div className="md:w-1/2 bg-black w-full">
            <div className="flex opacity-75 w-fit h-full overflow-hidden justify-around">
              {categoryList.map((item) => {
                return (
                  <img
                    src={item.image}
                    key={item.id}
                    alt="not found"
                    className="w-1/3 h-full object-cover"
                  />
                );
              })}
            </div>
          </div>
          <div className="md:w-1/2 w-full pl-2 flex justify-center items-center">
            <div className="md:px-0 md:py-0 px-2 py-3">
              <h1 className="text-primary-color font-bold text-xl px-5 text-center md:text-left py-3">
                VỀ CHÚNG TÔI
              </h1>
              <p className="text-xl text-justify leading-10 px-5">{textAblout}</p>
            </div>
          </div>
        </div>
        <div className="w-full flex flex-col justify-center items-center h-fit">
          <h1 className="font-bold text-[25px] px-5 my-8 text-center text-primary-color mt-[100px]">
            MỌI NGƯỜI NÓI GÌ VỀ CHÚNG TÔI
          </h1>
          <div className="flex items-start justify-center w-[90%] gap-2 md:flex-row flex-col md:flex-wrap">
            <Swiper
              modules={[Navigation, Pagination, Zoom, Parallax]}
              spaceBetween={10}
              slidesPerView={windowSize.width > 1490 ? 3 : windowSize.width > 1042 ? 2 : 1}
              navigation
              pagination={{clickable: true}}
              scrollbar={{draggable: true}}
              loop={true}
              className="w-full"
            >
              {peopleTalks.map((item, index) => {
                return <SwiperSlide key={index}>
                  <Quote {...item} />
                </SwiperSlide>
              })}
            </Swiper>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
