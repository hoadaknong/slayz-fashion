export { default as Login } from "./LoginPage";
export { default as Register } from "./RegisterPage";
export { default as Home } from "./HomePage";
export { default as Product } from "./Product/Product";
export { default as ProductDetail } from "./Product/ProductDetail";
export { default as Cart } from "./Cart/Cart";
export { default as CheckoutAddress } from "./Checkout/CheckoutAddress";
export { default as Checkout } from "./Checkout/Checkout";
export { default as Account } from "./Account/Account";
export { default as AccountDetail } from "./Account/AccountDetail";
export { default as HistoryBill } from "./Account/HistoryBill";
export { default as HistoryBillDetail } from "./Account/HistoryBillDetail";
export { default as AddressForm } from "./Account/AddressForm";
export { default as AddressEdit } from "./Account/AddressEdit";
export { default as NotFound } from "./NotFound";
export { default as OrderResult } from "./OrderResult";
export { default as ForgotPassword } from "./ForgotPassword";
export { default as ResultSendMail } from "./ResultSendMail";
export { default as ResetPassword } from "./ResetPassword";
export { default as ResultCheckout } from "./Checkout/ResultCheckout";
export { default as ReviewForm } from "./Account/ReviewForm";
