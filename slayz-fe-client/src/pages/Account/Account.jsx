/* eslint-disable no-unused-vars */
import React, { useContext } from "react";
import { useState } from "react";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";
import { HiMenu } from "@react-icons/all-files/hi/HiMenu";
import { HiUser } from "@react-icons/all-files/hi/HiUser";
import { HiOutlineLogout } from "@react-icons/all-files/hi/HiOutlineLogout";
import { FaRegAddressBook } from "@react-icons/all-files/fa/FaRegAddressBook";
import { HiShoppingCart } from "@react-icons/all-files/hi/HiShoppingCart";
import { FaFileInvoice } from "@react-icons/all-files/fa/FaFileInvoice";
import Swal from "sweetalert2";

const Account = () => {
  const { authState, logoutUser } = useContext(AuthContext);

  const navigate = useNavigate();

  const logout = () => {
    Swal.fire({
      title: "Đăng xuất",
      text: "Bạn có muốn đăng xuất tài khoản không?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#ff4949",
      cancelButtonColor: "#a0a0a0",
      cancelButtonText: "HỦY BỎ",
      confirmButtonText: "ĐĂNG XUẤT",
    }).then((result) => {
      if (result.isConfirmed) {
        logoutUser();
        navigate("/login");
      }
    });
  };
  return (
    <div className="h-fit w-full justify-center items-center md:w-[30%] md:flex mb-10 md:mb-[100px] mt-[122px] pl-0 md:pl-[80px] hidden pb-[200px]">
      <div className="w-fit flex flex-col gap-7 justify-center items-center md:justify-center md:items-start">
        <div className="flex items-center gap-2 pb-6">
          <HiMenu className="text-[25px]" />
          <p className="font-bold text-[20px]">TÀI KHOẢN CỦA BẠN</p>
        </div>
        <Link
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg flex items-center gap-2"
          to="/account"
        >
          <HiUser className="text-[25px]" /> <p>THÔNG TIN TÀI KHOẢN</p>
        </Link>
        <Link
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg flex items-center gap-2"
          to="/account/bill"
        >
          <FaFileInvoice className="text-[25px]" />
          <p>LỊCH SỬ MUA HÀNG</p>
        </Link>
        <Link
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg flex items-center gap-2"
          to="/account/address"
        >
          <FaRegAddressBook className="text-[25px]" />
          <p>DANH SÁCH ĐỊA CHỈ</p>
        </Link>
        <div
          className="cursor-pointer transition-all duration-200 hover:bg-gray-200 p-2 rounded-lg flex items-center gap-2"
          onClick={logout}
        >
          <HiOutlineLogout className="text-[25px]" /> <p>ĐĂNG XUẤT</p>
        </div>
      </div>
    </div>
  );
};

export default Account;
