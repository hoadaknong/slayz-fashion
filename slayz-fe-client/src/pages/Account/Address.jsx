/* eslint-disable no-unused-vars */
import React from "react";
import {useNavigate} from "react-router-dom";
import {AddressItem, Title} from "../../components";
import {useDataContext} from "../../contexts/DataProvider";
import useAddressUser from "../../hooks/useAddressUser";

const Address = () => {
  const navigate = useNavigate();
  const { page, totalPage, setPage,addresses} = useAddressUser(null);
  return (
    <div
      className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-[55px] md:mt-[102px] px-4 md:pl-10 pb-[100px]">
      <Title title={"Địa chỉ giao hàng"}/>
      <div className="py-5 border-b border-b-gray-200 w-full">
        <p className="font-bold text-[20px]">DANH SÁCH ĐỊA CHỈ GIAO HÀNG</p>
        <p className="text-[14px]">Bạn có thể xem và chỉnh sửa địa chỉ giao hàng tại đây.</p>
      </div>
      <div className="flex flex-col gap-2 mt-4">
        {addresses?.map((address) => {
          return <AddressItem {...address} key={address.id}/>;
        })}
      </div>
      <div>
        {page !== totalPage.current && <button
          className="transition-all duration-200 hover:shadow-md bg-none w-full py-4 text-black border border-gray-600 hover:bg-black hover:text-white"
          type="button"
          onClick={() => {
            setPage((previousPage) => {
              return previousPage + 1;
            })
          }}
        >
          XEM THÊM
        </button>}
        <button
          type="button"
          className="w-full py-4 bg-main-bg text-white hover:bg-hover-main-bg mt-2"
          onClick={() => {
            navigate("/account/address/add");
          }}
        >
          THÊM ĐỊA CHỈ MỚI
        </button>
      </div>
    </div>
  );
};

export default Address;
