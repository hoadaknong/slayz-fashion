/* eslint-disable no-unused-vars */
import React, {useEffect, useState} from "react";
import {Invoice, Title} from "../../components";
import useInvoiceData from "../../hooks/useInvoiceData";
import {Link, useNavigate} from "react-router-dom";
import {AiOutlineShoppingCart} from "@react-icons/all-files/ai/AiOutlineShoppingCart";

const HistoryBill = () => {
  const {invoiceList, setPage, page, totalPage, setStatus, setPaymentStatus} = useInvoiceData();
  const navigate = useNavigate();
  const [buttonIndex, setButtonIndex] = useState(1);
  const statusListButton = [{
    index: 1, title: "Chờ xử lý", event: function () {
      setPage(1)
      setButtonIndex(1)
      setStatus("PENDING")
    }
  }, {
    index: 2, title: "Đang giao", event: function () {
      setPage(1)
      setButtonIndex(2)
      setStatus("IN PROGRESS")
    }
  }, {
    index: 3, title: "Đã giao", event: function () {
      setPage(1)
      setButtonIndex(3)
      setStatus("COMPLETED")
    }
  }, {
    index: 4, title: "Đã hủy", event: function () {
      setPage(1)
      setButtonIndex(4)
      setStatus("CANCELLED")
    }
  }]
  const classNameInactive = "border h-10 border-black"
  const classNameActive = "border h-10 border-black bg-black text-white"
  return (<div
    className="flex flex-col justify-center items-center gap-3 md:w-2/3 w-full h-fit mb-[300px] md:mt-[102px] mt-[55px] md:pl-10 px-3">
    <Title title={"Đơn hàng"}/>
    <div className="py-5 border-b border-b-gray-200 w-full">
      <p className="font-bold text-[20px] uppercase">Lịch sử mua hàng</p>
      <p className="text-[14px]">Bạn có thể xem lịch sử mua hàng của SlayZ tại đây.</p>
      <div className="mt-4 grid grid-flow-col justify-stretch w-full">
        {statusListButton.map((button, index) => {
          return (<button type="button" onClick={button.event}
                          className={button.index === buttonIndex ? classNameActive : classNameInactive}>
            {button.title}
          </button>)
        })}
      </div>
    </div>
    {invoiceList.map((item) => {
      return <Invoice {...item} key={item?.id}/>;
    })}
    {invoiceList.length === 0 && (<div className="w-full h-[50vh] flex justify-center items-center flex-col gap-3">
      <p>Hiện không có đơn hàng!</p>
      <Link
        className="transition-all duration-200 hover:shadow-md bg-none px-5 py-2 text-center text-black border border-gray-600 hover:bg-black hover:text-white"
        type="button"
        to="/products"
      >
        VỀ TRANG SẢN PHẨM
      </Link>
    </div>)}
    {invoiceList.length !== 0 && page !== totalPage.current && <button
      className="transition-all duration-200 hover:shadow-md bg-none w-full py-2 text-black border border-gray-600 hover:bg-black hover:text-white"
      type="button"
      onClick={() => {
        setPage((previousPage) => {
          return previousPage + 1;
        })
      }}
    >
      XEM THÊM
    </button>}
  </div>);
};

export default HistoryBill;
