/* eslint-disable no-unused-vars */
import React, { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import useLocationForm from "./useLocationForm";
import Select from "react-select";
import { AddressService } from "../../services/address.service";
import Swal from "sweetalert2";
import { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import useAddressUser from "../../hooks/useAddressUser";
import { Title } from "../../components";

const AddressForm = () => {
  const { state, onCitySelect, onDistrictSelect, onWardSelect } = useLocationForm(false, null);
  const navigate = useNavigate();
  const [province, setProvince] = useState();
  const [district, setDistrict] = useState();
  const [ward, setWard] = useState();
  const addressLine = useRef();
  const fullName = useRef();
  const phone = useRef();
  const { authState } = useContext(AuthContext);
  const { createAddress } = useAddressUser(null);
  const {
    cityOptions,
    districtOptions,
    wardOptions,
    selectedCity,
    selectedDistrict,
    selectedWard,
  } = state;
  const onSave = () => {
    const createData = {
      fullName: fullName.current.value,
      phone: phone.current.value,
      addressLine: addressLine.current.value,
      province: province?.label ? province?.label : selectedCity?.label,
      district: district?.label ? district?.label : selectedDistrict?.label,
      ward: ward?.label ? ward?.label : selectedWard?.label,
      provinceId: province?.value ? province?.value : selectedCity?.value,
      districtId: district?.value ? district?.value : selectedDistrict?.value,
      wardId: ward?.value ? ward?.value : selectedWard?.value,
      userId: authState.user.id,
    };
    createAddress(createData);
  };
  return (
    <div className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-[55px] md:mt-[102px] px-4 md:pl-10">
      <Title title={"Thêm địa chỉ"} />
      <div className="py-5 border-b border-b-gray-200 w-full">
        <p className="font-bold text-[20px] uppercase">Thêm địa chỉ mới</p>
        <p className="text-[14px]">Bạn có thể thêm địa chỉ giao hàng mới ở đây</p>
      </div>
      <div className="flex flex-col gap-6 mt-4">
        <div>
          <div className="py-2 flex flex-col gap-2 ">
            <div>
              <label className="block text-sm font-medium text-gray-700 w-full">
                <h1 className="font-bold mb-1 text-[18px]">Tỉnh/Thành phố</h1>
                <div className="inline-block relative w-full">
                  <Select
                    name="cityId"
                    key={`cityId_${selectedCity?.value}`}
                    isDisabled={cityOptions.length === 0}
                    options={cityOptions}
                    onChange={(option) => {
                      setProvince(option);
                      onCitySelect(option);
                    }}
                    placeholder="Tỉnh/Thành"
                    defaultValue={selectedCity}
                    className={`mt-1 w-full focus:outline-none text-[15px] border-gray-300 shadow-sm sm:text-[15px] `}
                  />
                </div>
              </label>
            </div>
            <div className="mt-2">
              <label className="block text-sm font-medium text-gray-700 w-full">
                <h1 className="font-bold mb-1 text-[18px]">Quận/Huyện</h1>
                <div className="inline-block relative w-full">
                  <Select
                    name="districtId"
                    key={`districtId_${selectedDistrict?.value}`}
                    isDisabled={districtOptions.length === 0}
                    options={districtOptions}
                    onChange={(option) => {
                      setDistrict(option);
                      onDistrictSelect(option);
                    }}
                    placeholder="Quận/Huyện"
                    defaultValue={selectedDistrict}
                    className={`mt-1 w-full focus:outline-none text-[15px] border-gray-300 shadow-sm sm:text-[15px] `}
                  />
                </div>
              </label>
            </div>
            <div className="mt-2">
              <label className="block text-sm font-medium text-gray-700 w-full">
                <h1 className="font-bold mb-1 text-[18px]">Xã/Phường</h1>
                <div className="inline-block relative w-full">
                  <Select
                    name="wardId"
                    key={`wardId_${selectedWard?.value}`}
                    isDisabled={wardOptions.length === 0}
                    options={wardOptions}
                    placeholder="Phường/Xã"
                    onChange={(option) => {
                      setWard(option);
                      onWardSelect(option);
                    }}
                    defaultValue={selectedWard}
                    classNamePrefix="react-select"
                    className={`react-select-container mt-1 w-full focus:outline-none border-gray-300 shadow-sm text-[15px] `}
                    size="5"
                  />
                </div>
              </label>
            </div>
            <div className="mt-2">
              <label className="block text-sm font-medium text-gray-700">
                <h1 className="font-bold mb-1 text-[18px]">Địa chỉ</h1>
                <div className="mt-1">
                  <input
                    id="address"
                    name="address"
                    type="text"
                    ref={addressLine}
                    className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                    placeholder="Ví dụ: Số 1, Đường Võ Văn Ngân"
                  />
                </div>
              </label>
            </div>
            <div className="mt-2">
              <label className="block text-sm font-medium text-gray-700">
                <h1 className="font-bold mb-1 text-[18px]">Họ và tên</h1>
                <div className="mt-1">
                  <input
                    id="name"
                    name="name"
                    type="text"
                    ref={fullName}
                    className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                    placeholder="Ví dụ: Nguyễn Văn A"
                  />
                </div>
              </label>
            </div>
            <div className="mt-2">
              <label className="block text-sm font-medium text-gray-700">
                <h1 className="font-bold mb-1 text-[18px]">Số điện thoại</h1>
                <div className="mt-1">
                  <input
                    id="phone"
                    name="phone"
                    type="tel"
                    ref={phone}
                    className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                    placeholder="Ví dụ: 0388 891 635"
                  />
                </div>
              </label>
            </div>
          </div>
          <div className="flex justify-start gap-2 py-3 text-right">
            <button
              type="button"
              className="inline-flex justify-center border border-transparent bg-orange-500 py-3 px-[70px] text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
              onClick={onSave}
            >
              Lưu
            </button>
            <button
              onClick={() => {
                navigate(-1);
              }}
              className="inline-flex justify-center items-center px-10 hover:underline"
            >
              Hủy
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddressForm;
