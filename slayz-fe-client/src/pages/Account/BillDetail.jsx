import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { FeedbackService } from "../../services/feedback.service";
import { Rating } from "@mui/material";
import Swal from "sweetalert2";

const BillDetail = ({ item, invoice }) => {
  const navigate = useNavigate();
  const [reviews, setReviews] = useState([]);
  useEffect(() => {
    FeedbackService.getReviewByInvoiceDeatailId(item?.id).then((response) => {
      setReviews(response.data);
    });
  }, [item?.id]);
  return (
    <div className="w-full flex flex-col ">
      <div className="flex gap-2 w-full py-4">
        <div className="w-1/5">
          <img src={item?.image} alt="san pham" />
        </div>
        <div className="flex flex-col gap-1 w-3/5">
          <p
            className="cursor-pointer uppercase font-semibold hover:underline"
            onClick={() => {
              navigate("/products/" + item?.slug);
            }}
          >
            {item?.name}
          </p>
          <p className="uppercase text-orange-600">{GlobalUtil.numberWithCommas(item?.price)} ₫</p>
          <p className="uppercase ">
            Màu: {item?.color}, Size: {item?.size}
          </p>
          <p className="uppercase ">Số lượng: {item.quantity}</p>
        </div>
        {invoice?.status === "COMPLETED" && reviews.length === 0 && (
          <div className="flex items-center justify-center w-1/5">
            <button
              type="button"
              className="bg-main-bg text-white hover:bg-hover-main-bg hover transition-all px-3 py-2 hover:shadow-md"
              onClick={() => {
                if (GlobalUtil.checkDateIsOverTwoWeeks(invoice.updateDate)) {
                  Swal.fire(
                    "Thông tin",
                    "Đã quá 14 ngày kể từ khi nhận sản phẩm, quý khách vui lòng liên hệ page của SlayZ để biết thêm thông tin chi tiết!",
                    "info",
                  );
                } else {
                  localStorage.setItem("invoiceDetail", JSON.stringify(item));
                  navigate("/account/review");
                }
              }}
            >
              Đánh giá
            </button>
          </div>
        )}
      </div>
      {reviews.length !== 0 && (
        <div className="w-full flex flex-col">
          <div className="w-full uppercase font-semibold my-4">Đánh giá sản phẩm</div>
          <div className="w-full border border-gray-400 p-5">
            {reviews.map((item) => {
              return (
                <Review
                  rating={item.rating}
                  comment={item.comment}
                  createdDate={item.createdDate}
                  key={item.id}
                />
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
};

const Review = ({ rating, comment, createdDate }) => {
  return (
    <div className="flex flex-col justify-start gap-3">
      <div>
        <Rating name="rating" size="large" value={rating} readOnly={true} />
      </div>
      <div className="text-lg">
        <span className="font-bold">Nội dung:</span> <p className="mt-2">{comment}</p>
      </div>
      <div className="italic">Ngày đăng: {GlobalUtil.dateTimeConvert(createdDate)}</div>
    </div>
  );
};

export default BillDetail;
