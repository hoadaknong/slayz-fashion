/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import {Tooltip} from "@material-tailwind/react";
import React, {useRef} from "react";
import {useEffect} from "react";
import {useState} from "react";
import {UserService} from "../../services/user.service";
import {TiTickOutline} from "@react-icons/all-files/ti/TiTickOutline";
import {IoMdWarning} from "@react-icons/all-files/io/IoMdWarning";
import {HiEyeOff} from "@react-icons/all-files/hi/HiEyeOff";
import {HiEye} from "@react-icons/all-files/hi/HiEye";
import {BiError} from "@react-icons/all-files/bi/BiError";
import {useNavigate} from "react-router-dom";
import {useContext} from "react";
import {AuthContext} from "../../contexts/AuthContext";
import {Title} from "../../components";

const AccountDetail = () => {
  const [isChangePassword, setChangePassword] = useState(true);
  const [isHiddenOld, setIsHiddenOld] = useState(true);
  const [isHiddenNew, setIsHiddenNew] = useState(true);
  const [isHiddenConfirm, setIsHiddenConfirm] = useState(true);
  const successStyleClassName =
    "w-full py-2 px-3 flex gap-2 bg-green-500 items-center hover:drop-shadow transition-all";
  const errorStyleClassName = "w-full py-2 px-3 flex gap-2 bg-red-500";
  const [alert, setAlert] = useState({
    show: false,
    content: "A dismissible alert for showing message.",
    icon: null,
    style: errorStyleClassName,
  });
  const fullName = useRef();
  const email = useRef();
  const dob = useRef();
  const gender = useRef();
  const photo = useRef();
  const phone = useRef();
  const file = useRef();
  const passwordOld = useRef();
  const passwordNew = useRef();
  const passwordConfirm = useRef();
  const {authState, getCurrentUser} = useContext(AuthContext);
  const fetchUserData = async () => {
    await getCurrentUser();
  };
  useEffect(() => {
    const userData = authState?.user;
    fullName.current.value = userData?.fullName;
    email.current.value = userData?.email;
    dob.current.value = userData?.dob;
    gender.current.value = userData?.gender;
    phone.current.value = userData?.phone;
    photo.current.src = userData?.photo;
  }, [authState])
  const onSavePassword = () => {
    const request = {
      oldPassword: passwordOld.current.value,
      newPassword: passwordNew.current.value,
      confirmPassword: passwordConfirm.current.value,
    };
    UserService.updatePassword(authState.user.id, request).then((response) => {
      if (response.status === "OK") {
        setChangePassword(true);
        setAlert({
          show: true,
          content: response?.message,
          icon: <TiTickOutline className="text-white"/>,
          style:
            "w-full py-2 px-3 flex gap-2 bg-blue-500 items-center hover:drop-shadow transition-all",
        });
        window.scrollTo({top: 0, left: 0, behavior: "smooth"});
      } else {
        setAlert({
          show: true,
          content: response?.message,
          icon: <IoMdWarning className="text-white"/>,
          style:
            "w-full py-2 px-3 flex gap-2 bg-red-500 items-center hover:drop-shadow transition-all",
        });
      }
    });
    setTimeout(() => {
      setAlert({
        show: false,
        content: null,
        icon: <IoMdWarning className="text-white"/>,
        style:
          "w-full py-2 px-3 flex gap-2 bg-red-500 items-center hover:drop-shadow transition-all",
      });
    }, 10000);
  };
  const onSaveInfo = async () => {
    const userData = authState?.user;
    var data = new FormData();
    data.append("dob", dob.current.value);
    data.append("phone", phone.current.value);
    data.append("fullName", fullName.current.value);
    data.append("gender", gender.current.value);
    data.append("photo", file.current.files[0] ? file.current.files[0] : null);
    const updateUserReponse = await UserService.updateUser(userData.id, data);
    await fetchUserData();
    window.scrollTo({top: 0, left: 0, behavior: "smooth"});
    setAlert({
      show: true,
      content: updateUserReponse?.message,
      icon:
        updateUserReponse?.status === "OK" ? (
          <TiTickOutline className="text-white"/>
        ) : (
          <BiError className="text-white"/>
        ),
      style: updateUserReponse?.status === "OK" ? successStyleClassName : errorStyleClassName,
    });
  };
  const onChangePhoto = (e) => {
    photo.current.src = URL.createObjectURL(e.target.files[0]);
  };
  const onClickOld = (e) => {
    e.preventDefault();
    setIsHiddenOld(!isHiddenOld);
  };
  const onClickNew = (e) => {
    e.preventDefault();
    setIsHiddenNew(!isHiddenNew);
  };
  const onClickConfirm = (e) => {
    e.preventDefault();
    setIsHiddenConfirm(!isHiddenConfirm);
  };
  useEffect(() => {
    fetchUserData();
    window.scrollTo(0, 0);
  }, []);
  return (
    <div className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-[55px] md:mt-[102px] md:pl-10 pl-0">
      <Title title={"Thông tin cá nhân"}/>
      <div className="py-5 border-b border-b-gray-200 w-full px-2 md:px-0">
        <p className="font-bold text-[20px] uppercase">Thông tin tài khoản</p>
        <p className="text-[14px]">Bạn có thể cập nhật thông tin của mình ở trang này</p>
      </div>
      <div className="h-[50px] flex justify-center items-center">
        {alert.show === true && (
          <div className={alert?.style}>
            <div>{alert.icon}</div>
            <div className="text-white">{alert.content}</div>
          </div>
        )}
      </div>
      <div className="flex h-fit mt-1 flex-wrap">
        {/* Hình ảnh và thông tin đăng nhập */}
        <div className="flex flex-col justify-start items-center md:w-1/3 w-full gap-3">
          <img
            src={null}
            alt="avatar"
            className="w-[170px] h-[170px] object-cover rounded-full"
            ref={photo}
          />
          <label>
            <div
              className="w-[180px] h-[50px] bg-main-bg text-white flex justify-center items-center hover:bg-hover-main-bg">
              Chọn hình ảnh
            </div>
            <input type="file" className="hidden" onChange={onChangePhoto} ref={file}/>
          </label>
          <div className="flex flex-col gap-3 justify-start items-center w-full">
            <label className="flex gap-2 justify-start items-center rounded-none hidden">
              <input
                type="checkbox"
                className="w-5 h-5 accent-primary-color"
                checked={isChangePassword}
                onChange={(e) => {
                  if (e.target.checked) {
                    setChangePassword(true);
                  } else {
                    setChangePassword(false);
                  }
                }}
              />
              Thay đổi mật khẩu
            </label>
            {isChangePassword === true ? (
              <div className="flex flex-col gap-8 md:mt-[25px] mt-0">
                <label>
                  <p className="mb-2">
                    Mật khẩu cũ
                    <Tooltip content="Mật khẩu cũ bắt buộc" placement="right">
                      <span className="text-red-600">*</span>
                    </Tooltip>
                  </p>

                  <div className="flex justify-start items-center bg-black">
                    <input
                      className="focus:outline-none border border-gray-500 px-2 py-3 w-[85%] font-semibold"
                      type={isHiddenOld ? "password" : "text"}
                      placeholder="******"
                      ref={passwordOld}
                    />
                    <div className="flex justify-center items-center w-[15%]" onClick={onClickOld}>
                      {!isHiddenOld ? (
                        <HiEye className="h-4 w-4 text-white"/>
                      ) : (
                        <HiEyeOff className="h-4 w-4 text-white"/>
                      )}
                    </div>
                  </div>
                </label>
                <label>
                  <p className="mb-2">
                    Mật khẩu mới
                    <Tooltip content="Mật khẩu mới bắt buộc" placement="right">
                      <span className="text-red-600">*</span>
                    </Tooltip>
                  </p>
                  <div className="flex justify-start items-center bg-black">
                    <input
                      className="focus:outline-none border border-gray-500 px-2 py-3 w-[85%] font-semibold"
                      type={isHiddenNew ? "password" : "text"}
                      placeholder="******"
                      ref={passwordNew}
                    />
                    <div className="flex justify-center items-center w-[15%]" onClick={onClickNew}>
                      {!isHiddenNew ? (
                        <HiEye className="h-4 w-4 text-white"/>
                      ) : (
                        <HiEyeOff className="h-4 w-4 text-white"/>
                      )}
                    </div>
                  </div>
                </label>
                <label>
                  <p className="mb-[6px]">
                    Nhập lại mật khẩu mới
                    <Tooltip content="Nhập lại mật khẩu mới" placement="right">
                      <span className="text-red-600">*</span>
                    </Tooltip>
                  </p>
                  <div className="flex justify-start items-center bg-black">
                    <input
                      className="focus:outline-none border border-gray-500 px-2 py-3 w-[85%] font-semibold"
                      type={isHiddenConfirm ? "password" : "text"}
                      placeholder="******"
                      ref={passwordConfirm}
                    />
                    <div
                      className="flex justify-center items-center w-[15%]"
                      onClick={onClickConfirm}
                    >
                      {!isHiddenConfirm ? (
                        <HiEye className="h-4 w-4 text-white"/>
                      ) : (
                        <HiEyeOff className="h-4 w-4 text-white"/>
                      )}
                    </div>
                  </div>
                </label>
                <button
                  className="p-3 bg-main-bg text-white font-semibold mt-[5px] mb-3 hover:bg-second-bg"
                  type="button"
                  onClick={onSavePassword}
                >
                  CẬP NHẬT MẬT KHẨU
                </button>
              </div>
            ) : (
              <div></div>
            )}
          </div>
        </div>
        {/* Thông tin chi tiết */}
        <div className="w-full md:w-2/3 flex flex-col px-[60px] border-t md:border-none mt-10 md:mt-0">
          <div className="flex md:justify-start md:items-start justify-center items-center md:mt-0 mt-8">
            <p className="font-bold uppercase">Thông tin cá nhân</p>
          </div>
          <div className="flex flex-col gap-8 mt-4">
            <label>
              <p className="mb-2">
                Họ và tên
                <Tooltip content="Họ tên bắt buộc nhập" placement="right">
                  <span className="text-red-600">*</span>
                </Tooltip>
              </p>
              <input
                className="focus:outline-none border border-gray-500 px-2 py-3 w-full font-semibold"
                type="text"
                placeholder="Ví dụ: Phạm Đinh Quốc Hòa"
                ref={fullName}
              />
            </label>
            <label>
              <p className="mb-2">
                Email
                <Tooltip content="Email không thể thay đổi" placement="right">
                  <span className="text-red-600">*</span>
                </Tooltip>
              </p>
              <input
                className="focus:outline-none border border-gray-500 px-2 py-3 w-full font-semibold text-gray-500"
                type="email"
                placeholder="Ví dụ: hoadaknong101@gmail.com"
                ref={email}
                disabled
              />
            </label>
            <label>
              <p className="mb-2">
                Số điện thoại
                <Tooltip content="Số điện thoại bắt buộc nhập" placement="right">
                  <span className="text-red-600">*</span>
                </Tooltip>
              </p>
              <input
                className="focus:outline-none border border-gray-500 px-2 py-3 w-full font-semibold"
                type="email"
                ref={phone}
              />
            </label>
            <label>
              <p className="mb-2">
                Ngày sinh
                <Tooltip content="Ngày sinh bắt buộc nhập" placement="right">
                  <span className="text-red-600">*</span>
                </Tooltip>
              </p>
              <input
                className="focus:outline-none border border-gray-500 px-2 py-3 w-full font-semibold"
                type="date"
                ref={dob}
              />
            </label>
            <label>
              <p className="mb-2">
                Giới tính
                <Tooltip content="Giới tính bắt buộc nhập" placement="right">
                  <span className="text-red-600">*</span>
                </Tooltip>
              </p>
              <div className="relative">
                <select
                  className={`block appearance-none w-full bg-white border border-gray-500 p-2 py-3 mt-1 leading-tight focus:outline-none h-[50px] text-[13px]`}
                  name="gender"
                  id="gender"
                  ref={gender}
                >
                  <option value="Nam">Nam</option>
                  <option value="Nữ">Nữ</option>
                  <option value="Khác">Khác</option>
                </select>
                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                  </svg>
                </div>
              </div>
            </label>
            <button
              className="p-3 bg-main-bg text-white font-semibold mt-[3px] hover:bg-hover-main-bg"
              type="button"
              onClick={onSaveInfo}
            >
              CẬP NHẬT THÔNG TIN
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AccountDetail;
