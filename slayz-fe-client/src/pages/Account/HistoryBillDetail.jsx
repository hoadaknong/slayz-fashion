/* eslint-disable no-unused-vars */
import React, {useRef, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import useInvoiceData from "../../hooks/useInvoiceData";
import {GlobalUtil} from "../../utils/GlobalUtil";
import useInvoiceDetail from "../../hooks/useInvoiceDetail";
import {InvoiceService} from "../../services/invoice.service";
import {PaymentService} from "../../services/payment.service";
import Swal from "sweetalert2";
import BillDetail from "./BillDetail";
import {Title} from "../../components";

const HistoryBillDetail = () => {
  const {id} = useParams();
  const {invoiceDetailList} = useInvoiceDetail(id);
  const {invoice} = useInvoiceData(id);
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);

  const paymentStatusDisplay = (status) => {
    if (status === "PENDING") {
      return "Chờ thanh toán";
    } else {
      return "Đã thanh toán";
    }
  };
  const statusDisplay = (status) => {
    if (status === "PENDING") {
      return "Đã đặt hàng";
    } else if (status === "CANCELLED") {
      return "Đã hủy";
    } else if ("COMPLETE") {
      return "Đã nhận hàng";
    } else {
      return "Đã tiếp nhận";
    }
  };
  const paymentOnClick = () => {
    Swal.fire({
      title: "Thanh Toán",
      text: "Bạn có muốn chuyển hướng sang trang thanh toán không?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#1c9cff",
      cancelButtonColor: "#b7b7b7",
      cancelButtonText: "HỦY BỎ",
      confirmButtonText: "CHUYỂN TRANG",
    }).then((result) => {
      if (result.isConfirmed) {
        const dataPayment = {
          amount: invoice?.total,
          extraData: id,
          orderInfo: invoice.fullName + " thanh toán đơn hàng",
          returnUrl: window.location.origin + "/#/checkout_result",
          type: 3,
        };
        PaymentService.createPayment(dataPayment).then((response) => {
          if (response.status === "OK") {
            window.location.href = response.data;
          }
        });
      }
    });
  };
  if (invoice === null) {
    return (
      <div
        className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-[55px] md:mt-[102px] px-4 md:pl-10 pb-[100px]">
        <div className="py-5 border-b border-b-gray-200 w-full">
          <p className="font-bold text-[20px]">Không tìm thấy đơn hàng</p>
          <p className="text-[14px]">Đã xảy ra lỗi khi tải dữ liệu đơn hàng!</p>
        </div>
      </div>
    );
  }
  return (
    <div className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-[55px] md:mt-[102px] px-4 md:pl-10">
      <Title title={"Chi tiết đơn hàng"}/>
      <div className="py-5 border-b border-b-gray-200 w-full">
        <p className="font-bold text-[20px]">Đơn hàng #{invoice?.id}</p>
        <p className="text-[14px]">
          Chi tiết đơn hàng #{invoice?.id} bao gồm các sản phẩm trong đơn hàng
        </p>
      </div>
      <div className="flex flex-col gap-6 mt-1">
        <div>
          <div className="mb-3 font-semibold">DANH SÁCH SẢN PHẨM</div>
          <div className="flex flex-col gap-3 divide-y divide-gray-400">
            {invoiceDetailList?.map((item, index) => (
              <BillDetail item={item} invoice={invoice} key={index}/>
            ))}
          </div>
        </div>
        <div className="flex flex-col">
          <div className="font-semibold">THÔNG TIN NHẬN HÀNG</div>
          <div className="w-full border border-gray-400 p-5 mt-4 py-8 flex flex-col gap-2">
            <p className="font-semibold">
              {invoice?.fullName} - {invoice?.phone}
            </p>
            <p>{invoice?.address}</p>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="font-semibold">THÔNG TIN GIAO HÀNG</div>
          <div className="w-full border border-gray-400 p-5 mt-4 py-8 flex flex-col gap-2">
            <p className="font-semibold">{invoice?.delivery}</p>
            <p>Phí vận chuyển: {GlobalUtil.numberWithCommas(invoice?.deliveryFee)}đ</p>
          </div>
        </div>
        <div className="flex flex-col">
          <div className="font-semibold">GHI CHÚ</div>
          <div className="w-full border border-gray-400 p-5 mt-4 py-8 flex flex-col gap-2">
            {invoice?.note?.trim() === "" ? (
              <p className="text-gray-400">Không có ghi chú!</p>
            ) : (
              <p className="font-semibold">{invoice?.note}</p>
            )}
          </div>
        </div>
        <div className="w-full flex flex-wrap justify-between">
          <div className="w-full xl:w-[49%] flex flex-col pr-4 bg-gray-200">
            <div className=" p-6 flex flex-col gap-2">
              <div className="flex justify-between">
                <p>Phương thức thanh toán</p>
                <p>{invoice?.paymentMethod}</p>
              </div>
              <div className="flex justify-between">
                <p>Trạng thái thanh toán</p>
                <p>{paymentStatusDisplay(invoice?.paymentStatus)}</p>
              </div>
              {invoice?.paymentMethod !== "COD" && invoice?.paymentStatus === "PENDING" && (
                <div className="flex justify-center w-full items-center py-4">
                  <p className="underline cursor-pointer" onClick={paymentOnClick}>
                    Tiến hành thanh toán
                  </p>
                </div>
              )}
            </div>
          </div>
          <div className="w-full xl:w-[49%] flex justify-end items-end flex-col bg-gray-200">
            <div className="w-full flex flex-col gap-2  p-6">
              <div className="flex justify-between">
                <p>Trạng thái đơn hàng</p>
                <p>{statusDisplay(invoice?.status)}</p>
              </div>
              <div className="flex justify-between">
                <p>Phí vận chuyển</p>
                <p className="font-semibold">
                  {GlobalUtil.numberWithCommas(invoice?.deliveryFee)}{" "}
                  <span className="underline">đ</span>
                </p>
              </div>
              <div className="flex justify-between">
                <p>Tạm tính</p>
                <p className="font-semibold">
                  {GlobalUtil.numberWithCommas(invoice?.total - invoice?.deliveryFee)}{" "}
                  <span className="underline">đ</span>
                </p>
              </div>
              <div className="flex justify-between">
                <p>Tổng</p>
                <p className="font-semibold">
                  {GlobalUtil.numberWithCommas(invoice?.total)} <span className="underline">đ</span>
                </p>
              </div>
            </div>
          </div>
          <div className="w-full flex gap-5 justify-end items-center">
            <div
              className="mt-2 flex justify-center p-4 hover:underline cursor-pointer"
              onClick={() => {
                navigate(-1);
              }}
            >
              TRỞ VỀ
            </div>
            {invoice?.status === "PENDING" || invoice?.status === "PROCCESSING" ? (
              <div
                className="mt-2 flex justify-center py-3 px-4 cursor-pointer border border-gray-600 hover:text-white hover:bg-black transition-all"
                onClick={() => {
                  if (window.confirm("Bạn có muốn hủy đơn hàng không?")) {
                    InvoiceService.updateInvoiceStatus(id, {status: "CANCELLED"}).then(
                      (response) => {
                        if (response.status === "OK") {
                          alert("Đã hủy đơn hàng thành công!");
                          window.location.reload();
                        }
                      },
                    );
                  }
                }}
              >
                HỦY ĐƠN HÀNG
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HistoryBillDetail;
