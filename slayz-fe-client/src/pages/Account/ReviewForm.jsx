/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { Rating } from "@mui/material";
import { FeedbackService } from "../../services/feedback.service";
import Swal from "sweetalert2";
import { Title } from "../../components";

const ReviewForm = () => {
  let invoiceDetail = JSON.parse(localStorage.getItem("invoiceDetail"));
  const [ratingValue, setRatingValue] = useState(0);
  const commentRef = useRef(null);
  const navigate = useNavigate();
  const onReviewClick = () => {
    if (ratingValue === 0) {
      Swal.fire({
        icon: "error",
        title: "Lỗi",
        text: "Hãy chọn điểm đánh giá cho sản phẩm!",
        footer: '<a href="https://www.facebook.com/slayz.vn">Liên hệ đội ngũ hộ trợ</a>',
      });
    } else {
      const payload = {
        comment: commentRef.current.value,
        invoiceDetailId: invoiceDetail?.id,
        rating: ratingValue,
        variantId: invoiceDetail?.variantId,
      };
      FeedbackService.createNewReview(payload).then((response) => {
        if (response.status === "OK") {
          navigate(-1);
          Swal.fire({
            icon: "success",
            title: "Đánh giá",
            text: "Đánh giá sản phẩm thành công!",
            footer: '<a href="https://www.facebook.com/slayz.vn">Liên hệ đội ngũ hộ trợ</a>',
          });
        }
      });
    }
  };
  useEffect(() => {
    window.addEventListener("beforeunload", () => localStorage.removeItem("invoiceDetail"));
    if (invoiceDetail === null) {
      navigate("/not_found");
    }
  }, []);
  window.scrollTo(0, 0);
  return (
    <div className="flex flex-col gap-3 w-full md:w-2/3 h-fit mb-[100px] mt-1 md:mt-[102px] px-4 md:pl-10">
      <Title title={"Đánh giá sản phẩm"} />
      <div className="flex gap-2 w-full py-4">
        <div className="w-1/5">
          <img src={invoiceDetail?.image} alt="san pham" />
        </div>
        <div className="flex flex-col gap-1 w-4/5">
          <p
            className="cursor-pointer uppercase font-semibold hover:underline"
            onClick={() => {
              navigate("/products/" + invoiceDetail?.slug);
            }}
          >
            {invoiceDetail?.name}
          </p>
          <p className="uppercase text-orange-600">
            {GlobalUtil.numberWithCommas(invoiceDetail?.price)} ₫
          </p>
          <p className="uppercase ">
            Màu: {invoiceDetail?.color}, Size: {invoiceDetail?.size}
          </p>
          <p className="uppercase ">Số lượng: {invoiceDetail?.quantity}</p>
        </div>
      </div>
      <div className="w-full flex flex-col">
        <div className="flex justify-start item-center gap-2">
          <p className="mt-1 font-semibold">
            Đánh giá sản phẩm <span className="text-red-600 font-semibold">*</span>
          </p>
          <Rating
            name="rating"
            size="large"
            value={ratingValue}
            onChange={(event, newValue) => {
              setRatingValue(() => {
                if (newValue === null) {
                  return 0;
                }
                return newValue;
              });
            }}
          />
        </div>
        <div className="w-full mt-2 flex flex-col gap-2">
          <p className="font-semibold">Nhận xét của bạn:</p>
          <textarea className="w-full" placeholder="Aa" rows="10" ref={commentRef}></textarea>
        </div>
        <div className="flex justify-end items-end mt-2">
          <button
            type="butotn"
            onClick={() => {
              Swal.fire({
                title: "ĐÁNH GIÁ",
                text: "Khi đánh giá sản phẩm thì bạn sẽ không thể sửa lại phần đánh giá của mình?\nBạn có chắc không?",
                icon: "question",
                showCancelButton: true,
                confirmButtonColor: "#1273de",
                cancelButtonColor: "#a0a0a0",
                cancelButtonText: "HỦY BỎ",
                confirmButtonText: "ĐÁNH GIÁ",
              }).then((result) => {
                if (result.isConfirmed) {
                  onReviewClick();
                }
              });
            }}
            className="bg-white text-black py-4 px-10 transition-all hover:bg-black hover:text-white border border-black font-semibold"
          >
            Đánh giá
          </button>
        </div>
      </div>
    </div>
  );
};

export default ReviewForm;
