import React, {useState} from "react";
import { useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import { AuthService } from "../services/auth.service";
import { Title } from "../components";
import {GlobalUtil} from "../utils/GlobalUtil";
import bg from "../assets/HomePage1.png";

const Register = () => {
  const navigate = useNavigate();
  const fullNameRef = useRef(null);
  const emailRef = useRef(null);
  const passwordRef = useRef(null);
  const confirmPasswordRef = useRef(null);
  const [isShowPassword, setIsShowPassword] = useState(false);
  const [fullNameMessage, setFullnameMessage] = useState("");
  const [emailMessage, setEmailMessage] = useState("");
  const [passwordMessage, setPasswordMessage] = useState("");
  const [confirmMessage, setConfirmMessage] = useState("");

  const showPasswordOnChange = (e) => {
    if (e.target.checked) {
      setIsShowPassword(true)
    } else {
      setIsShowPassword(false)
    }
  }

  const validate = (fullname, email, password, confirmPassword) => {
    if (fullname.trim() === "" && password.trim() === "" && email.trim() === "") {
      setFullnameMessage("Họ tên không được để trống!");
      setEmailMessage("Email không được để trống!");
      setPasswordMessage("Mật khẩu không được để trống!");
      setConfirmMessage("");
      fullNameRef.current.focus();
      return false;
    } else if (fullname.trim() === "") {
      setFullnameMessage("Họ tên không được để trống!");
      setEmailMessage("");
      setPasswordMessage("");
      setConfirmMessage("");
      fullNameRef.current.focus();
      return false;
    } else if (password.trim() === "" || password?.length < 6) {
      setFullnameMessage("");
      setEmailMessage("");
      setPasswordMessage("Mật khẩu phải có ít nhất 6 kí tự!");
      setConfirmMessage("");
      passwordRef.current.focus();
      return false;
    } else if (email.trim() === "" || !GlobalUtil.validateEmail(email)) {
      setFullnameMessage("");
      setEmailMessage("Email không hợp lệ!");
      setPasswordMessage("");
      setConfirmMessage("");
      email.current.focus();
      return false;
    } else if (password.trim() !== confirmPassword.trim()) {
      setFullnameMessage("");
      setEmailMessage("");
      setPasswordMessage("");
      setConfirmMessage("Mật khẩu xác nhận không đúng!");
      confirmPasswordRef.current.focus();
    }
    return true;
  };
  const submitHandler = async (e) => {
    e.preventDefault();
    const fullname = fullNameRef.current.value;
    const email = emailRef.current.value;
    const password = passwordRef.current.value;
    const confirmPassword = confirmPasswordRef.current.value;
    if (validate(fullname, email, password, confirmPassword)) {
      AuthService.register(fullname, email, password, confirmPassword).then((response) => {
        if (response.status === "OK") {
          navigate("/login");
        } else {
          setFullnameMessage("");
          setEmailMessage(response.message);
          setPasswordMessage("");
          setConfirmMessage("");
        }
      });
    }
  };
  return (
    <div className="flex justify-center items-start h-fit xl:pt-[152px] pt-[40px] pb-[160px]"
    style={{
      backgroundImage: `url(${bg})`,
      backgroundSize: "cover"
    }}>
      <Title title={"Đăng ký"} />
      <div className="flex flex-col xl:w-[30%] md:w-[60%] w-[90%] rounded-xl xl:border- border-0 border-gray-400 pb-[80px] gap-1 xl:drop-shadow-xl drop-shadow-none bg-white pt-10">
        <label className="text-center h-9 mb-5 text-3xl font-semibold content-center w-full">
          ĐĂNG KÝ
        </label>
        <label className="mx-auto w-[70%]">
          <input
            name="fullName"
            placeholder="Họ và tên"
            className="my-2 w-full h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
            ref={fullNameRef}
            onChange={(e) => {
              if (e.target.value.trim() !== "") {
                setFullnameMessage("");
              }
            }}
        />
          <p className="text-red-600 text-sm">{fullNameMessage}</p>
        </label>
        
        <label className="mx-auto w-[70%]">
          <input
            name="account"
            placeholder="Email"
            className="my-2 w-full h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
            ref={emailRef}
            onChange={(e) => {
              if (e.target.value.trim() !== "") {
                setEmailMessage("");
              }
            }} />
          <p className="text-red-600 text-sm">{emailMessage}</p></label>
        <label className="mx-auto w-[70%]">
          <input
            name="password"
            type={isShowPassword ? "text" : "password"}
            placeholder="Mật khẩu"
            className="my-2 w-full h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
            ref={passwordRef}
            onChange={(e) => {
              if (e.target.value.trim() !== "") {
                setPasswordMessage("");
              }
            }}/>
          <p className="text-red-600 text-sm">{passwordMessage}</p>
        </label>
        <label className="mx-auto w-[70%]">
          <input
            name="confirmPassword"
            type={isShowPassword ? "text" : "password"}
            placeholder="Nhập lại mật khẩu"
            className="my-2 w-full h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white"
            ref={confirmPasswordRef}
            onChange={(e) => {
              if (e.target.value.trim() !== "") {
                setConfirmMessage("");
              }
            }}/>
          <p className="text-red-600 text-sm">{confirmMessage}</p>
        </label>
        
        <div className="w-[70%] mx-auto my-2 ">
          <label className="flex items-center gap-2 w-fit"><input type="checkbox" name="showPassword" id="showPassword" onChange={showPasswordOnChange} />Hiện mật khẩu</label>
        </div>
        <button
          type="button"
          onClick={submitHandler}
          className="w-[70%] h-[50px] mx-auto my-3 bg-orange-500  hover:bg-orange-600 text-white text-center focus:drop-shadow-sm"
        >
          Đăng ký
        </button>
        <div className="mx-auto">
          Bạn đã có tài khoản?{" "}
          <Link to="/login" className=" underline hover:text-orange-500">
            Đăng nhập
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Register;
