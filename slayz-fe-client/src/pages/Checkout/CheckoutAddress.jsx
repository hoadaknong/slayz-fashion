/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, {useContext, useEffect, useRef} from "react";
import {AiOutlineUser} from "@react-icons/all-files/ai/AiOutlineUser";
import {GoLocation} from "@react-icons/all-files/go/GoLocation";
import {AddressItem, Title} from "../../components";
import {Navigate, useNavigate} from "react-router-dom";
import useLocationForm from "../Account/useLocationForm";
import {useState} from "react";
import Select from "react-select";
import {IoMdArrowBack} from "@react-icons/all-files/io/IoMdArrowBack";
import {AiFillCreditCard} from "@react-icons/all-files/ai/AiFillCreditCard";
import {AuthContext} from "../../contexts/AuthContext";
import {AddressService} from "../../services/address.service";
import HeaderCheckout from "./HeaderCheckout";
import useAddressUser from "../../hooks/useAddressUser";
import Swal from "sweetalert2";
import useCartUser from "../../hooks/useCartUser";

const CheckoutAddress = () => {
  const {state, onCitySelect, onDistrictSelect, onWardSelect} = useLocationForm(false, null);
  const navigate = useNavigate();
  const [province, setProvince] = useState();
  const [district, setDistrict] = useState();
  const [ward, setWard] = useState();
  const addressLine = useRef();
  const fullName = useRef();
  const phone = useRef();
  const [addressId, setAddressId] = useState(null);
  const {
    cityOptions,
    districtOptions,
    wardOptions,
    selectedCity,
    selectedDistrict,
    selectedWard,
  } = state;
  const {authState} = useContext(AuthContext);
  const {page, totalPage, setPage, addresses} = useAddressUser(null);
  const {cartDetailList} = useCartUser();
  const chooseAddressClickHandle = () => {
    if (addressId === null) {
      Swal.fire(
        "Lỗi",
        "Vui lòng chọn địa chỉ hoặc nhập địa chỉ mới!",
        "warning",
      );
    } else {
      AddressService.getAddressById(addressId).then((response) => {
        if (response.status === "OK") {
          localStorage.setItem("address", JSON.stringify(response.data));
          navigate("/checkout");
          window.scrollTo(0, 0);
        } else {
          navigate("/login");
          window.scrollTo(0, 0);
        }
      });
    }

  };

  const createAddressAndChooseClickHandle = () => {
    const createData = {
      fullName: fullName.current.value,
      phone: phone.current.value,
      addressLine: addressLine.current.value,
      province: province?.label ? province?.label : selectedCity?.label,
      district: district?.label ? district?.label : selectedDistrict?.label,
      ward: ward?.label ? ward?.label : selectedWard?.label,
      provinceId: province?.value ? province?.value : selectedCity?.value,
      districtId: district?.value ? district?.value : selectedDistrict?.value,
      wardId: ward?.value ? ward?.value : selectedWard?.value,
      userId: authState.user.id,
    };
    if (validate(createData)) {
      AddressService.createAddress(createData).then((response) => {
        if (response.status === "OK") {
          localStorage.setItem("address", JSON.stringify(response.data));
          window.scrollTo(0, 0);
          navigate("/checkout");
        }
      });
    }
  };
  const validate = (obj) => {
    if (obj.province === undefined) {
      Swal.fire(
        "Lỗi",
        "Vui lòng chọn Tỉnh/Thành phố!",
        "warning",
      );
      return false;
    }
    if (obj.district === undefined) {
      Swal.fire(
        "Lỗi",
        "Vui lòng chọn Quận/Huyện!",
        "warning",
      );
      return false;
    }
    if (obj.ward === undefined) {
      Swal.fire(
        "Lỗi",
        "Vui lòng chọn Xã/Phường!",
        "warning",
      );
      return false;
    }
    if (obj.addressLine.trim() === "") {
      Swal.fire(
        "Lỗi",
        "Vui lòng nhập chi tiết địa chỉ!",
        "warning",
      );
      addressLine.current.focus();
      return false;
    }
    if (obj.fullName.trim() === "") {
      Swal.fire(
        "Lỗi",
        "Vui lòng nhập họ tên!",
        "warning",
      );
      fullName.current.focus();
      return false;
    }
    if (!obj.phone.match(/^\d{10}$/)) {
      Swal.fire(
        "Lỗi",
        "Số điện thoại không hợp lệ!",
        "warning",
      );
      return false;
    }
    return true;
  };
  if (cartDetailList.length === 0) {
    return <Navigate to={"/user/cart"}/>
  }
  return (
    <div className="h-fit w-[80%] flex justify-start  items-center flex-col mx-auto md:mt-[102px] mt-[80px] pb-[150px]">
      <HeaderCheckout step={2}/>
      <div className="border-t border-gray-500 w-full flex gap-3 py-10">
        <div className="flex justify-center items-start mt-1">
          <AiOutlineUser className="text-[20px] font-bold"/>
        </div>
        <div className="flex flex-col justify-start items-start">
          <p className="font-bold text-[20px]">Đăng nhập</p>
          <p>{authState.user.email}</p>
        </div>
      </div>
      <div className="border-t border-gray-500 w-full flex gap-3 py-10 flex-col">
        <div className="flex gap-3 mb-6">
          <div className="flex justify-center items-start mt-1">
            <GoLocation className="text-[20px] font-bold"/>
          </div>
          <div className="flex flex-col justify-start items-start">
            <p className="font-bold text-[20px]">Địa chỉ giao hàng</p>
          </div>
        </div>
        <div
          className={
            addresses.length > 0
              ? "flex md:gap-10 gap-10 md:divide-x md:divide-gray-400 md:flex-row flex-col "
              : "flex gap-10"
          }
        >
          <div
            className={addresses.length > 0 ? "md:w-1/2 w-full flex flex-col gap-3" : "hidden"}
            onChange={(e) => {
              setAddressId(Number(e.target.value));
            }}
          >
            {addresses.map((item) => {
              return (
                <label className="flex gap-3 justify-center items-center" key={item.id}>
                  <input
                    type="radio"
                    name="address"
                    className="w-[30px] h-[30px]"
                    value={item.id}
                  />{" "}
                  <AddressItem {...item} />
                </label>
              );
            })}
            {page !== totalPage.current && <button
              className="transition-all duration-200 hover:shadow-md bg-none w-full py-4 text-black border border-gray-600 hover:bg-black hover:text-white"
              type="button"
              onClick={() => {
                setPage((previousPage) => {
                  return previousPage + 1;
                })
              }}
            >
              XEM THÊM
            </button>}
            {addresses.length > 0 && (
              <button
                type="button"
                className="inline-flex justify-center border border-transparent bg-orange-500 py-3 text-sm font-medium text-white shadow-sm hover:bg-orange-600 focus:outline-none w-full text-center"
                onClick={chooseAddressClickHandle}
              >
                GIAO ĐẾN ĐỊA CHỈ ĐÃ CHỌN
              </button>
            )}
          </div>
          <div className={addresses.length > 0 ? "md:w-1/2 w-full md:pl-[30px] pl-0" : "w-full"}>
            <div>
              <div className="pb-2 flex flex-col gap-2 ">
                <div>
                  <label className="block text-sm font-medium text-gray-700 w-full">
                    <h1 className="font-bold mb-1 text-[18px]">Tỉnh/Thành phố</h1>
                    <div className="inline-block relative w-full">
                      <Select
                        name="cityId"
                        key={`cityId_${selectedCity?.value}`}
                        isDisabled={cityOptions.length === 0}
                        options={cityOptions}
                        onChange={(option) => {
                          setProvince(option);
                          onCitySelect(option);
                        }}
                        placeholder="Tỉnh/Thành"
                        defaultValue={selectedCity}
                        className={`mt-1 w-full focus:outline-none text-[15px] border-gray-300 shadow-sm sm:text-[15px] `}
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700 w-full">
                    <h1 className="font-bold mb-1 text-[18px]">Quận/Huyện</h1>
                    <div className="inline-block relative w-full">
                      <Select
                        name="districtId"
                        key={`districtId_${selectedDistrict?.value}`}
                        isDisabled={districtOptions.length === 0}
                        options={districtOptions}
                        onChange={(option) => {
                          setDistrict(option);
                          onDistrictSelect(option);
                        }}
                        placeholder="Quận/Huyện"
                        defaultValue={selectedDistrict}
                        className={`mt-1 w-full focus:outline-none text-[15px] border-gray-300 shadow-sm sm:text-[15px] `}
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700 w-full">
                    <h1 className="font-bold mb-1 text-[18px]">Xã/Phường</h1>
                    <div className="inline-block relative w-full">
                      <Select
                        name="wardId"
                        key={`wardId_${selectedWard?.value}`}
                        isDisabled={wardOptions.length === 0}
                        options={wardOptions}
                        placeholder="Phường/Xã"
                        onChange={(option) => {
                          setWard(option);
                          onWardSelect(option);
                        }}
                        defaultValue={selectedWard}
                        classNamePrefix="react-select"
                        className={`react-select-container mt-1 w-full focus:outline-none border-gray-300 shadow-sm text-[15px] `}
                        size="5"
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700">
                    <h1 className="font-bold mb-1 text-[18px]">Địa chỉ</h1>
                    <div className="mt-1">
                      <input
                        id="address"
                        name="address"
                        type="text"
                        ref={addressLine}
                        className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                        placeholder="Ví dụ: Số 1, Đường Võ Văn Ngân"
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700">
                    <h1 className="font-bold mb-1 text-[18px]">Họ và tên</h1>
                    <div className="mt-1">
                      <input
                        id="name"
                        name="name"
                        type="text"
                        ref={fullName}
                        className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                        placeholder="Ví dụ: Nguyễn Văn A"
                      />
                    </div>
                  </label>
                </div>
                <div className="mt-2">
                  <label className="block text-sm font-medium text-gray-700">
                    <h1 className="font-bold mb-1 text-[18px]">Số điện thoại</h1>
                    <div className="mt-1">
                      <input
                        id="phone"
                        name="phone"
                        type="tel"
                        ref={phone}
                        className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm sm:text-[15px] p-[14px] `}
                        placeholder="Ví dụ: 0388 891 635"
                      />
                    </div>
                  </label>
                </div>
              </div>
              <div className="flex justify-start gap-2 py-3 text-right">
                <button
                  type="button"
                  className="inline-flex justify-center border border-transparent bg-orange-500 py-3 text-center text-sm font-medium text-white shadow-sm hover:bg-orange-600 focus:outline-nones w-full"
                  onClick={createAddressAndChooseClickHandle}
                >
                  XÁC NHẬN GIAO ĐẾN ĐỊA CHỈ NÀY
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="border-y border-gray-500 w-full flex gap-3 py-10 text-gray-400">
        <div className="flex justify-center items-start mt-1">
          <AiFillCreditCard className="text-[20px] font-bold"/>
        </div>
        <div className="flex flex-col justify-start items-start">
          <p className="font-bold text-[20px]">Thanh toán</p>
        </div>
      </div>
    </div>
  );
};

export default CheckoutAddress;
