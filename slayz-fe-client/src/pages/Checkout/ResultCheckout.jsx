/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import {AiOutlineShoppingCart} from "@react-icons/all-files/ai/AiOutlineShoppingCart";
import {BiErrorCircle} from "@react-icons/all-files/bi/BiErrorCircle";
import {AiOutlineFileDone} from "@react-icons/all-files/ai/AiOutlineFileDone";
import React from "react";
import {useEffect} from "react";
import {Link, useNavigate, useSearchParams} from "react-router-dom";
import {InvoiceService} from "../../services/invoice.service";
import {Title} from "../../components";

const ResultCheckout = () => {
  const [requestParams, setRequestParams] = useSearchParams();
  window.scrollTo(0, 0);
  if (Number(requestParams.get("resultCode")) !== 0 || Number(requestParams.get("vnp_ResponseCode")) !== 0) {
    return (
      <div className="md:mt-[40px] mt-[40px] h-[80vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
        <Title title={"Thanh toán thất bại"}/>
        <div>
          <BiErrorCircle className="text-[180px]"/>
        </div>
        <div className="font-bold uppercase text-orange-500 text-[30px] text-center w-full">thanh toán thất bại</div>
        <div className="text-center w-full">Giao dịch chưa được xử lý, quý khách vui lòng tiến hành thanh toán lại trong
          phần lịch sử đơn hàng để đơn hàng được xử lý!
        </div>
        <Link to="/" className="underline">
          Quay về trang chủ, tiếp tục mua sắm
        </Link>
      </div>
    );
  }
  if (Number(requestParams.get("vnp_ResponseCode")) === 0) {
    (async function () {
      await InvoiceService.updatePaymentStatus(Number(requestParams.get("vnp_TxnRef")), {status: "PAID"})
    })()
  }
  return (
    <div className="md:mt-[70px] mt-[50px] h-[80vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
      <Title title={"Thanh toán thành công"}/>
      <div>
        <AiOutlineFileDone className="text-[180px]"/>
      </div>
      <div className="font-bold uppercase text-orange-500 text-[30px] text-center w-full">thanh toán thành công</div>
      <div className="text-center w-full">Xin chân thành cảm ơn quý khách hàng đã tin tưởng SlayZ</div>
      <Link to="/" className="underline">
        Quay về trang chủ, tiếp tục mua sắm
      </Link>
    </div>
  );
};

export default ResultCheckout;
