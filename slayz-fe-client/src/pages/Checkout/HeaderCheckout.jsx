import React from "react";
import { Title } from "../../components";
import { useNavigate } from "react-router-dom";
import { IoMdArrowBack } from "@react-icons/all-files/io/IoMdArrowBack";

const HeaderCheckout = ({ step }) => {
  const navigate = useNavigate();
  return (
    <div className="w-full">
      <h1 className="text-center text-3xl font-bold">THANH TOÁN</h1>
      <Title title={"Thanh toán"} />
      <div className="flex justify-between md:items-center py-9 w-full flex-col items-start md:gap-0 gap-6">
        <div
          className="w-full flex gap-3 justify-start items-center my-auto "
          onClick={() => {
            window.scrollTo(0, 0);
            navigate("/user/cart");
          }}
        >
          <IoMdArrowBack className="cursor-pointer" />
          <p className="font-semibold hover:underline cursor-pointer underline">Trở về giỏ hàng</p>
        </div>
        <div className="md:w-[80%] w-full">
          <div className="w-full md:w-[50%] flex relative justify-between mx-auto md:text-md text-sm">
            <div className="border border-gray-400 w-[82%] sm:w-[90%] xl:w-[88%] md:w-[80%] h-[1px] absolute z-[-9999] mt-[19px] ml-[30px]"></div>
            <div className="flex flex-col justify-center items-center w-fit">
              <div className="rounded-full bg-orange-600 text-white w-10 h-10 flex justify-center items-center font-bold">
                1
              </div>
              <div className="font-semibold">Đăng nhập</div>
            </div>
            <div className="flex flex-col justify-center items-center w-fit">
              <div
                className={
                  step >= 2
                    ? "rounded-full bg-orange-600 text-white w-10 h-10 flex justify-center items-center font-bold"
                    : "rounded-full bg-gray-400 text-white w-10 h-10 flex justify-center items-center font-bold"
                }
              >
                2
              </div>
              <div className="font-semibold">Địa chỉ giao hàng</div>
            </div>
            <div className="flex flex-col justify-center items-center w-fit">
              <div
                className={
                  step >= 3
                    ? "rounded-full bg-orange-600 text-white w-10 h-10 flex justify-center items-center font-bold"
                    : "rounded-full bg-gray-400 text-white w-10 h-10 flex justify-center items-center font-bold"
                }
              >
                3
              </div>
              <div className="font-semibold text-gray-400">Thanh toán</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderCheckout;
