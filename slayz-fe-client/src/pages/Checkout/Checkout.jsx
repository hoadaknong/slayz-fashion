/* eslint-disable no-unused-vars */
import React, { useContext, useEffect } from "react";
import { AiOutlineUser } from "@react-icons/all-files/ai/AiOutlineUser";
import { GoLocation } from "@react-icons/all-files/go/GoLocation";

import { Navigate, useLocation, useNavigate } from "react-router-dom";

import { IoMdArrowBack } from "@react-icons/all-files/io/IoMdArrowBack";
import { AiFillCreditCard } from "@react-icons/all-files/ai/AiFillCreditCard";
import { AuthContext } from "../../contexts/AuthContext";
import useCartUser from "../../hooks/useCartUser";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { useRef } from "react";
import { useState } from "react";
import useDelivery from "../../hooks/useDelivery";
import { PaymentService } from "../../services/payment.service";
import HeaderCheckout from "./HeaderCheckout";
import useWindowSize from "../../hooks/useWindowSize";

const Checkout = () => {
  const address = JSON.parse(localStorage.getItem("address"));
  const navigate = useNavigate();
  const {
    authState: { user },
  } = useContext(AuthContext);
  const { cartInfo, cartDetailList, checkOut } = useCartUser();
  const [paymentMethod, setPaymentMethod] = useState(null);
  const [deliveryMethod, setDeliveryMethod] = useState(null);
  const [deliveryMethodName, setDeliveryMethodName] = useState(null);
  const note = useRef();
  const { services, deliveryFee, calculateFee } = useDelivery(true);
  const [paymentType, setPaymentType] = useState(0);
  const addressChangeClickHandle = () => {
    navigate("/checkout/address");
  };

  const paymentMethodChange = (e) => {
    setPaymentMethod(e.target.value);
  };

  const deliveryMethodChange = (e) => {
    setDeliveryMethod(e.target.value);
    calculateFee(Number(e.target.value), cartInfo?.grandTotal);
  };

  const paymentClickHandle = async () => {
    if (deliveryMethod === null) {
      alert("Vui lòng chọn phương thức vận chuyển");
    } else if (paymentMethod === null) {
      alert("Vui lòng chọn phương thức thanh toán");
    } else {
      var listProduct = [];
      for (let i = 0; i < cartDetailList.length; i++) {
        listProduct.push(cartDetailList[i]?.id);
      }
      if (paymentMethod === "COD") {
        const response = await checkOut(
          note.current.value,
          address?.id,
          paymentMethod,
          "PENDING",
          listProduct,
          deliveryFee?.total,
          deliveryMethodName,
        );
        if (response?.id !== undefined) {
          navigate("/order_result");
        }
      }else if(paymentMethod === "VNPAY") {
        const response = await checkOut(
          note.current.value,
          address?.id,
          paymentMethod,
          "PENDING",
          listProduct,
          deliveryFee?.total,
          deliveryMethodName,
        );
        if (response?.id !== undefined) {
          const dataPayment = {
            amount: cartInfo?.grandTotal + deliveryFee?.total,
            extraData: response?.id,
            orderInfo: user.fullName + " thanh toán đơn hàng",
            returnUrl: window.location.origin + "/#/checkout_result",
            type: paymentType,
          };
          const paymentReponse = await PaymentService.createPaymentVnpay(dataPayment);
          if (paymentReponse.status === "OK") {
            window.location.href = paymentReponse.data;
          }
        }
      }
      else {
        const response = await checkOut(
          note.current.value,
          address?.id,
          paymentMethod,
          "PENDING",
          listProduct,
          deliveryFee?.total,
          deliveryMethodName,
        );
        if (response?.id !== undefined) {
          const dataPayment = {
            amount: cartInfo?.grandTotal + deliveryFee?.total,
            extraData: response?.id,
            orderInfo: user.fullName + " thanh toán đơn hàng",
            returnUrl: window.location.origin + "/#/checkout_result",
            type: paymentType,
          };
          const paymentReponse = await PaymentService.createPayment(dataPayment);
          if (paymentReponse.status === "OK") {
            window.location.href = paymentReponse.data;
          }else{

          }
        }
      }
    }
  };
  const size = useWindowSize();
  if(cartDetailList.length === 0){
    return <Navigate to={"/user/cart"}/>
  }
  return (
    <div className="h-fit w-[80%] flex justify-center items-center flex-col mx-auto md:mt-[102px] mt-[80px] pb-[150px]">
      <HeaderCheckout step={3} />
      <div className="border-t border-gray-500 w-full flex gap-3 py-10">
        <div className="flex justify-center items-start mt-1">
          <AiOutlineUser className="text-[20px] font-bold" />
        </div>
        <div className="flex flex-col justify-start items-start">
          <p className="font-bold text-[20px] uppercase">Đăng nhập</p>
          <p>{user.email}</p>
        </div>
      </div>
      <div className="border-t border-gray-500 w-full flex gap-3 py-10 flex-col">
        <div className="flex gap-3">
          <div className="flex justify-center items-start mt-1">
            <GoLocation className="text-[20px] font-bold" />
          </div>
          <div className="flex flex-col justify-start items-start">
            <p className="font-bold text-[20px] uppercase">Địa chỉ giao hàng</p>
          </div>
        </div>
        <div className="flex gap-10 divide-x divide-gray-400">
          {/* Địa chỉ */}
          <div className="h-fit w-full p-4 pl-8 flex bg-white transition-all duration-100 md:flex-row flex-col md:justify-between items-start justify-start md:gap-0 gap-3">
            <div className="w-4/5">
              <div>
                <p className="font-bold">
                  {address?.fullName} - {address?.phone}
                </p>
              </div>
              <div>
                <p>
                  {address?.addressLine}, {address?.ward}, {address?.district}, {address?.province}
                </p>
              </div>
            </div>
            <div className="w-full md:w-1/5 flex justify-center items-center gap-[20px]">
              <div
                className="border border-gray-600 py-2 px-6 hover:text-white hover:bg-black transition-all cursor-pointer md:w-fit w-full text-center"
                onClick={addressChangeClickHandle}
              >
                THAY ĐỔI
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="border-t border-gray-500 w-full flex gap-3 py-10 flex-col">
        <div className="flex gap-3">
          <div className="flex justify-center items-start mt-2">
            <AiFillCreditCard className="text-[20px] font-bold" />
          </div>
          <div className="flex flex-col justify-start items-start">
            <p className="font-bold text-[25px] uppercase">Thanh toán</p>
          </div>
        </div>
        <div className="flex w-full md:flex-row flex-col">
          <div className="flex flex-col gap-10 md:px-8 px-0 mt-5 md:w-3/4 w-full md:pr-10">
            <div className="w-full">
              <p className="font-bold uppercase">Phương thức vận chuyển</p>
              <div className="flex flex-col md:gap-4 mt-3" onChange={deliveryMethodChange}>
                {services?.map((item) => {
                  return (
                    <label
                      key={item.service_id}
                      className="flex gap-2 justify-between items-center border border-gray-300 xl:w-[50%] md:w-[70%] w-full py-3 pl-4 pr-10 hover:drop-shadow bg-white transition-all"
                    >
                      <div className="flex gap-2 justify-start items-center">
                        <input
                          type="radio"
                          name="deliveryMethod"
                          value={item.service_id}
                          className="w-5 h-5"
                          onChange={(e) => {
                            if (e.target.checked) {
                              setDeliveryMethodName(item.short_name);
                            }
                          }}
                        />
                        <p className="uppercase">{item.short_name}</p>
                      </div>
                    </label>
                  );
                })}
              </div>
            </div>
            <div>
              <p className="font-bold uppercase">Phương thức thanh toán</p>
              <div className="flex flex-col gap-4 mt-3" onChange={paymentMethodChange}>
                <label className="flex gap-2 justify-between items-center border border-gray-300 xl:w-[50%] md:w-[70%] w-full py-3 pl-4 pr-10 hover:drop-shadow bg-white transition-all">
                  <div className="flex gap-2 justify-start items-center">
                    <input type="radio" name="paymentMethod" value="COD" className="w-5 h-5 " />
                    <p className="uppercase">Thanh toán khi nhận hàng(COD)</p>
                  </div>
                  <img
                    src="https://cdn-icons-png.flaticon.com/512/639/639365.png"
                    alt="icon cash"
                    className="w-8 mt-[5px]"
                  />
                </label>
                <label
                  className="flex gap-2 justify-between items-center border border-gray-300 xl:w-[50%] md:w-[70%] w-full py-3 pl-4 pr-10 hover:drop-shadow bg-white transition-all"
                  onClick={() => {
                    setPaymentType(0);
                  }}
                >
                  <div className="flex gap-2 justify-start items-center">
                    <input type="radio" name="paymentMethod" value="MOMO" className="w-5 h-5" />
                    <p className="uppercase">Thanh toán bằng Momo</p>
                  </div>
                  <img
                    src="https://img.mservice.io/momo-payment/icon/images/logo512.png"
                    alt="icon cash"
                    className="w-8 mt-[5px]"
                  />
                </label>
                <label
                  className="flex gap-2 justify-between items-center border border-gray-300 xl:w-[50%] md:w-[70%] w-full py-3 pl-4 pr-10 hover:drop-shadow bg-white transition-all"
                  onClick={() => {
                    setPaymentType(1);
                  }}
                >
                  <div className="flex gap-2 justify-start items-center">
                    <input type="radio" name="paymentMethod" value="VNPAY" className="w-5 h-5" />
                    <p className="uppercase">Thanh toán bằng VNPAY</p>
                  </div>
                  <img
                    src="https://vnpay.vn/s1/statics.vnpay.vn/2023/6/0oxhzjmxbksr1686814746087.png"
                    alt="icon cash"
                    className="w-8 mt-[5px]"
                  />
                </label>
                <label
                  className="flex gap-2 justify-between items-center border border-gray-300 xl:w-[50%] md:w-[70%] w-full py-3 pl-4 pr-10 hover:drop-shadow bg-white transition-all"
                  onClick={() => {
                    setPaymentType(1);
                  }}
                >
                  <div className="flex gap-2 justify-start items-center">
                    <input type="radio" name="paymentMethod" value="ATM" className="w-5 h-5" />
                    <p className="uppercase">Thanh toán bằng ATM</p>
                  </div>
                  <img
                    src="https://cdn-icons-png.flaticon.com/512/8983/8983163.png"
                    alt="icon cash"
                    className="w-8 mt-[5px]"
                  />
                </label>
                <label
                  className="flex gap-2 justify-between items-center border border-gray-300 xl:w-[50%] md:w-[70%] w-full py-3 pl-4 pr-10 hover:drop-shadow bg-white transition-all"
                  onClick={() => {
                    setPaymentType(2);
                  }}
                >
                  <div className="flex gap-2 justify-start items-center">
                    <input
                      type="radio"
                      name="paymentMethod"
                      value="VISA/MASTER/JCB"
                      className="w-5 h-5"
                    />
                    <p className="uppercase">Thanh toán bằng VISA/MASTER/JCB</p>
                  </div>
                  <img
                    src="https://logos-world.net/wp-content/uploads/2020/09/Mastercard-Logo.png"
                    alt="icon cash"
                    className="w-10 mt-[5px]"
                  />
                </label>
              </div>
              <label className="flex gap-2 justify-start items-start flex-col w-full mt-7">
                <p className="font-bold uppercase">Ghi chú đơn hàng</p>
                <textarea
                  rows={size.width >= 768 ? "18" : "5"}
                  className="w-full p-3 border border-gray-400 focus:outline-none resize-none"
                  placeholder="Aa"
                  ref={note}
                />
              </label>
            </div>
          </div>
          <div className="md:w-1/4 w-full mt-3">
            <div>
              <p className="font-bold">SẢN PHẨM ({cartInfo?.quantityProduct})</p>
            </div>
            <div className="mt-3 md:mt-10 flex flex-col gap-3 h-fit max-h-[485px] overflow-hidden hover:overflow-y-auto overscroll-none">
              {/* Sản phẩm */}
              {cartDetailList?.map((item) => {
                return (
                  <div
                    className="w-full flex text-[15px] gap-3 justify-start items-center"
                    key={item.id}
                  >
                    <div className="w-fit">
                      <img className="w-[90px] rounded-md" src={item.imageProduct} alt="san pham" />
                    </div>
                    <div className="w-2/3 flex-col flex gap-2">
                      <div>
                        <p className="uppercase font-semibold">{item.productName}</p>

                        <p className="uppercase text-gray-500">
                          Màu: {item.style}, SIZE: {item.size}
                        </p>

                        <p className="uppercase text-gray-500">SL: {item.quantity}</p>
                      </div>
                      <div>
                        <p className="uppercase text-orange-600 font-semibold">
                          {GlobalUtil.numberWithCommas(item.price)}đ
                        </p>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
            <div className="w-full flex flex-col gap-2 mt-8">
              <div className="w-full border border-gray-200 p-6">
                <div>
                  <p className="font-bold">TẠM TÍNH</p>
                </div>
                <div className="flex justify-between mt-4 mb-2">
                  <p>Số lượng</p>
                  <p>{cartInfo?.totalQuantity}</p>
                </div>
                <div className="flex justify-between mb-2">
                  <p>Tạm tính</p>
                  <p>{GlobalUtil.numberWithCommas(cartInfo?.grandTotal)}đ</p>
                </div>
                <div className="flex justify-between border-b border-gray-200 pb-9">
                  <p>Phí vận chuyển</p>
                  <p>{GlobalUtil.numberWithCommas(deliveryFee?.total)}đ</p>
                </div>
                <div className="flex justify-between mb-1 mt-5">
                  <p>Tổng số</p>
                  <p>
                    {deliveryFee?.total !== undefined
                      ? GlobalUtil.numberWithCommas(cartInfo?.grandTotal + deliveryFee?.total)
                      : GlobalUtil.numberWithCommas(cartInfo?.grandTotal)}
                    đ
                  </p>
                </div>
                <div
                  onClick={paymentClickHandle}
                  className="w-full py-3 text-center bg-black text-white mt-10 mb-2 cursor-pointer"
                >
                  THANH TOÁN{" "}
                  {deliveryFee?.total !== undefined
                    ? GlobalUtil.numberWithCommas(cartInfo?.grandTotal + deliveryFee?.total)
                    : GlobalUtil.numberWithCommas(cartInfo?.grandTotal)}
                  đ
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Checkout;
