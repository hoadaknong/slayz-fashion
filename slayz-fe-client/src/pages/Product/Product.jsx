/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import {ProductGallery, Title} from "../../components";
import mainImage from "../../assets/HomePage1.png";
import {useEffect} from "react";
import useProductFilter from "../../hooks/useProductFilter";
import {useSearchParams} from "react-router-dom";
import {Accordion} from "flowbite-react";
import {useState} from "react";
import {AiOutlineClose, AiOutlineHdd} from "react-icons/ai";
import {useDataContext} from "../../contexts/DataProvider";

const Product = (props) => {
  const {
    colors,
    categories,
    sizes,
    filterData,
    pageList,
    setMinPrice,
    setMaxPrice,
    setCategoryFilter,
    setSizeFilter,
    setColorFilter,
    page,
    name,
    setPage,
    isCategoryInit,
    isColorInit,
    isSizeInit,
    setCategoryInit,
    setColorInit,
    setSizeInit,
    setGender,
    setName,
    message,
    setMessage,
    setSortOption,
    maxPrice,
    minPrice,
  } = useProductFilter();
  const [title, setTitle] = useState("Tất cả sản phẩm");
  const {isOpenSideBarFilter, setIsOpenSideBarFilter} = useDataContext();
  const [requestParams, setRequestParams] = useSearchParams();
  const sortOptionOnChange = (e) => {
    setSortOption(e.target.value);
  };
  useEffect(() => {
    setName("");
    setPage(1);
    setMessage("");
    if (props.type === "male") {
      setGender(["MALE", "UNISEX"]);
      setMessage("");
      setTitle("Dành cho Nam");
    }
    if (props.type === "female") {
      setGender(["FEMALE", "UNISEX"]);
      setMessage("");
      setTitle("Dành cho Nữ");
    }
    if (props.type === "unisex") {
      setGender(["UNISEX"]);
      setTitle("Unisex");
    }
    if (props.type === "search") {
      setGender(["MALE", "FEMALE", "UNISEX"]);
      setName(String(requestParams.get("q")));
      setTitle("Kết quả tìm kiếm: " + String(requestParams.get("q")));
    }
    if (props.type === "all") {
      setGender(["MALE", "FEMALE", "UNISEX"]);
      setTitle("Tất cả sản phẩm");
    }
    window.scrollTo(0, 0);
  }, [props.type, requestParams.get("q")]);
  const closeSideBarOnClick = () => {
    setIsOpenSideBarFilter(false);
  };
  const filterSideBarOnClick = () => {
    setIsOpenSideBarFilter(true);
  };
  const onChangeMinPrice = (e) => {
    var value = Number(e.target.value);
    console.log(value);
    if (value <= 10000 || value >= maxPrice) {
      setMinPrice(10000);
    } else {
      setMinPrice(Number(e.target.value));
    }
  };
  const onChangeMaxPrice = (e) => {
    var value = Number(e.target.value);
    console.log(value);
    if (value <= minPrice) {
      setMaxPrice(minPrice + 10000);
    } else {
      setMaxPrice(Number(e.target.value));
    }
  };
  return (<div className="flex flex-col justify-center items-center xl:mt-[45px] mt-[40px] pb-[100px]">
    <Title title={title}/>
    <div
      style={{background: "rgba(0, 0, 0, .4)"}}
      className={isOpenSideBarFilter ? "w-full h-screen absolute z-[3000] top-0 block overscroll-none overflow-auto transition-all duration-500" : `hidden`}
    >
      <div
        className={isOpenSideBarFilter ? "w-3/4 bg-white overflow-y-scroll pb-5 h-full" : "hidden"}
      >
        <div className="flex items-center justify-between py-4 px-2 fixed w-3/4 bg-white shadow-md z-[3001]">
          <div className="font-bold flex justify-start items-center gap-1">
            <AiOutlineHdd className="text-2xl"/> <p>Bộ lọc</p>
          </div>
          <button
            className="hover:bg-gray-300 hover:text-white rounded-full p-2"
            type="button"
            onClick={closeSideBarOnClick}
          >
            <AiOutlineClose className="text-gray-400 hover:text-white"/>
          </button>
        </div>
        <div className="mt-[70px]">
          <Accordion alwaysOpen={true}>
            <Accordion.Panel alwaysOpen={true}>
              <Accordion.Title>DANH MỤC</Accordion.Title>
              <Accordion.Content>
                <div className="flex flex-col gap-2">
                  {categories.map((item) => {
                    return (<label key={item.id} className="flex gap-4 justify-start items-center">
                      <input
                        type="checkbox"
                        name="category"
                        value={item.id}
                        className="w-5 h-5 accent-black"
                        onChange={(e) => {
                          setCategoryFilter((prev) => {
                            if (isCategoryInit) {
                              setCategoryInit(false);
                              if (e.target.checked) {
                                return [Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            } else {
                              if (e.target.checked) {
                                return [...prev, Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            }
                          });
                        }}
                      />
                      <p className="capitalize">{item.name}</p>
                    </label>);
                  })}
                </div>
              </Accordion.Content>
            </Accordion.Panel>
            <Accordion.Panel>
              <Accordion.Title>MÀU SẮC</Accordion.Title>
              <Accordion.Content>
                <div className="flex flex-col gap-2">
                  {colors.map((item) => {
                    return (<label key={item.id} className="flex gap-4 justify-start items-start">
                      <input
                        type="checkbox"
                        name="category"
                        value={item.id}
                        className="w-5 h-5 accent-black mt-[2px]"
                        onChange={(e) => {
                          setColorFilter((prev) => {
                            if (isColorInit) {
                              setColorInit(false);
                              if (e.target.checked) {
                                return [Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            } else {
                              if (e.target.checked) {
                                return [...prev, Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            }
                          });
                        }}
                      />
                      <span
                        className="z-[1000] w-5 h-5  rounded-sm mt-[2px]"
                        style={{backgroundColor: item.code}}
                      ></span>
                      <p className="uppercase">{item.name}</p>
                    </label>);
                  })}
                </div>
              </Accordion.Content>
            </Accordion.Panel>
            <Accordion.Panel>
              <Accordion.Title>KÍCH THƯỚC</Accordion.Title>
              <Accordion.Content>
                <div className="flex flex-col gap-2">
                  {sizes.map((item) => {
                    return (<label key={item.id} className="flex gap-4 justify-start items-start">
                      <input
                        type="checkbox"
                        name="category"
                        value={item.id}
                        className="w-5 h-5 accent-black mt-[2px]"
                        onChange={(e) => {
                          setSizeFilter((prev) => {
                            if (isSizeInit) {
                              setSizeInit(false);
                              if (e.target.checked) {
                                return [Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            } else {
                              if (e.target.checked) {
                                return [...prev, Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            }
                          });
                        }}
                      />
                      <p className="uppercase">{item.name}</p>
                    </label>);
                  })}
                </div>
              </Accordion.Content>
            </Accordion.Panel>
            <Accordion.Panel>
              <Accordion.Title>
                GIÁ TIỀN <span className="text-sm font-normal">(Đơn vị VNĐ)</span>
              </Accordion.Title>
              <Accordion.Content>
                <label>
                  <h1>Từ</h1>
                  <input
                    className="w-full border rounded-none h-[40px] focus:outline-none px-3 border-gray-500 mt-1"
                    type="text"
                    name="minPrice"
                    value={minPrice}
                    onChange={onChangeMinPrice}
                  />
                </label>
                <label>
                  <h1>Đến</h1>
                  <input
                    className="w-full border rounded-none h-[40px] focus:outline-none px-3 border-gray-500 mt-1"
                    type="text"
                    name="maxPrice"
                    value={maxPrice}
                    onChange={onChangeMaxPrice}
                  />
                </label>
              </Accordion.Content>
            </Accordion.Panel>
          </Accordion>
        </div>
      </div>
    </div>
    <img className="object-cover h-[200px] w-full" src={mainImage} alt="banner 1" id="banner"/>
    <div className="w-full flex justify-start items-start xl:gap-2 gap-0 md:mt-8 mt-4 xl:flex-nowrap flex-wrap">
      <div className="xl:w-[18%] xl:pl-10 pl-0 xl:pr-4 pr-0 px-3 w-[90%] mx-auto">
        <div className="flex justify-start gap-2 items-center mb-[10px]">
          <select className="w-full md:text-lg text-sm" onChange={sortOptionOnChange}>
            <option value="newest">Sản phẩm mới</option>
            <option value="price_desc">Giá giảm dần</option>
            <option value="price_asc">Giá tăng dần</option>
            <option value="name_asc">Theo tên A-Z</option>
            <option value="name_desc">Theo tên Z-A</option>
          </select>
        </div>
        <button
          type="button"
          onClick={filterSideBarOnClick}
          className="border border-gray-800 px-10 py-2 block xl:hidden"
        >
          Bộ lọc
        </button>
        <div className="border-t border-gray-300 pt-2 hidden xl:block">
          <Accordion alwaysOpen={true}>
            <Accordion.Panel alwaysOpen={true}>
              <Accordion.Title>DANH MỤC</Accordion.Title>
              <Accordion.Content>
                <div className="flex flex-col gap-2">
                  {categories.map((item) => {
                    return (<label key={item.id} className="flex gap-4 justify-start items-center">
                      <input
                        type="checkbox"
                        name="category"
                        value={item.id}
                        className="w-5 h-5 accent-black"
                        onChange={(e) => {
                          setCategoryFilter((prev) => {
                            if (isCategoryInit) {
                              setCategoryInit(false);
                              if (e.target.checked) {
                                return [Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            } else {
                              if (e.target.checked) {
                                return [...prev, Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            }
                          });
                        }}
                      />
                      <p className="capitalize">{item.name}</p>
                    </label>);
                  })}
                </div>
              </Accordion.Content>
            </Accordion.Panel>
            <Accordion.Panel>
              <Accordion.Title>MÀU SẮC</Accordion.Title>
              <Accordion.Content>
                <div className="flex flex-col gap-2">
                  {colors.map((item) => {
                    return (<label key={item.id} className="flex gap-4 justify-start items-start">
                      <input
                        type="checkbox"
                        name="category"
                        value={item.id}
                        className="w-5 h-5 accent-black mt-[2px]"
                        onChange={(e) => {
                          setColorFilter((prev) => {
                            if (isColorInit) {
                              setColorInit(false);
                              if (e.target.checked) {
                                return [Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            } else {
                              if (e.target.checked) {
                                return [...prev, Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            }
                          });
                        }}
                      />
                      <span
                        className="z-[1000] w-5 h-5  rounded-sm mt-[2px]"
                        style={{backgroundColor: item.code}}
                      ></span>
                      <p className="uppercase">{item.name}</p>
                    </label>);
                  })}
                </div>
              </Accordion.Content>
            </Accordion.Panel>
            <Accordion.Panel>
              <Accordion.Title>KÍCH THƯỚC</Accordion.Title>
              <Accordion.Content>
                <div className="flex flex-col gap-2">
                  {sizes.map((item) => {
                    return (<label key={item.id} className="flex gap-4 justify-start items-start">
                      <input
                        type="checkbox"
                        name="category"
                        value={item.id}
                        className="w-5 h-5 accent-black mt-[2px]"
                        onChange={(e) => {
                          setSizeFilter((prev) => {
                            if (isSizeInit) {
                              setSizeInit(false);
                              if (e.target.checked) {
                                return [Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            } else {
                              if (e.target.checked) {
                                return [...prev, Number(e.target.value)];
                              } else {
                                return prev.filter((e) => {
                                  return e !== item.id;
                                });
                              }
                            }
                          });
                        }}
                      />
                      <p className="uppercase">{item.name}</p>
                    </label>);
                  })}
                </div>
              </Accordion.Content>
            </Accordion.Panel>
            <Accordion.Panel>
              <Accordion.Title>
                GIÁ TIỀN <span className="text-sm font-normal">(Đơn vị VNĐ)</span>
              </Accordion.Title>
              <Accordion.Content>
                <label>
                  <h1>Từ</h1>
                  <input
                    className="w-full border rounded-none h-[40px] focus:outline-none px-3 border-gray-500 mt-1"
                    type="number"
                    name="minPriceLg"
                    value={minPrice}
                    onChange={onChangeMinPrice}
                  />
                </label>
                <label>
                  <h1>Đến</h1>
                  <input
                    className="w-full border rounded-none h-[40px] focus:outline-none px-3 border-gray-500 mt-1"
                    type="number"
                    name="maxPriceLg"
                    value={maxPrice}
                    onChange={onChangeMaxPrice}
                  />
                </label>
              </Accordion.Content>
            </Accordion.Panel>
          </Accordion>
        </div>
      </div>
      <div className="xl:w-[82%] w-[90%] mx-auto">
        {name.trim() !== "" && <div className="flex justify-center items-center w-full">
          <h>Kết quả tìm kiếm cho từ khóa: {String(requestParams.get("q"))}</h>
        </div>}
        <div
          className={name.trim() !== "" ? "flex flex-wrap md:items-center md:justify-start w-full justify-start items-start gap-x-[10px] gap-y-[15px]" : "flex flex-wrap md:items-center md:justify-start w-full justify-start items-start md:my-3 my-0 gap-x-[10px] gap-y-[15px] pt-1"}>
          <div className={message !== "" ? "hidden" : "text-center w-full"}>{message}</div>
          <div className="grid md:grid-cols-4 grid-cols-2 md:gap-4 gap-2 mx-auto mt-1 xl:mt-6 md:w-full w-[90%] pr-5">
            {filterData?.details?.map((item, index) => (<ProductGallery {...item} width={380} key={index}/>))}
          </div>
        </div>
        <div className="flex gap-3 justify-end pr-[60px]">
          {pageList.map((item, index) => {
            return (<div
              key={index}
              className={page !== item ? "flex justify-center items-center font-semibold text-black h-[40px] w-[40px] border border-gray-600 hover:text-white hover:bg-black transition-all cursor-pointer" : "flex justify-center items-center font-semibold  h-[40px] w-[40px] border border-gray-600 text-white bg-black transition-all cursor-pointer"}
              onClick={() => {
                setPage(item);
              }}
            >
              {item}
            </div>);
          })}
        </div>
      </div>
    </div>
  </div>);
};

export default Product;
