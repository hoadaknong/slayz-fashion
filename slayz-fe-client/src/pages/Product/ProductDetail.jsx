/* eslint-disable react/style-prop-object */
/* eslint-disable react-hooks/exhaustive-deps */
import {useState} from "react";
import {Link, useNavigate, useParams} from "react-router-dom";
import {useRef} from "react";
import {useEffect} from "react";
import {FaShippingFast} from "@react-icons/all-files/fa/FaShippingFast";
import {ProductService} from "../../services/product.service";
import {VariantService} from "../../services/variant.service";
import {CartService} from "../../services/cart.service";
import {GlobalUtil} from "../../utils/GlobalUtil";
import {useContext} from "react";
import {AuthContext} from "../../contexts/AuthContext";
import {GiTShirt} from "@react-icons/all-files/gi/GiTShirt";
import {MdPayment} from "@react-icons/all-files/md/MdPayment";
import {AiFillPhone} from "@react-icons/all-files/ai/AiFillPhone";
import {BiBroadcast} from "@react-icons/all-files/bi/BiBroadcast";
import {FaShoppingCart} from "@react-icons/all-files/fa/FaShoppingCart";
import {MdDescription} from "@react-icons/all-files/md/MdDescription";
import {HiThumbUp} from "@react-icons/all-files/hi/HiThumbUp";
import Swal from "sweetalert2";
import {Badge, Progress, Spinner, Tabs} from "flowbite-react";
import {ProductGallery, Rating, Title} from "../../components";
import {FeedbackService} from "../../services/feedback.service";
import {AiFillCheckCircle, AiFillStar} from "react-icons/ai";
import useReviewFilter from "../../hooks/useReviewFilter";

const ProductDetail = () => {
  const {id} = useParams();
  const image = useRef();
  const reviewRef = useRef();
  const [data, setData] = useState();
  const [sizeList, setSizeList] = useState([]);
  const [colorList, setColorList] = useState([]);
  const [imageList, setImageList] = useState([]);
  const [size, setSize] = useState(null);
  const [color, setColor] = useState(null);
  const [title, setTitle] = useState("");
  const [relatedProducts, setRelatedProducts] = useState([]);
  const [num, setNum] = useState(1);
  const quantity = useRef();
  const descriptionRef = useRef();
  const navigate = useNavigate();
  const [saleQuantity, setSaleQuantity] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [avgRating, setAvgRating] = useState(0);
  const [overviewRating, setOverviewRating] = useState([]);
  const [variant, setVariant] = useState({});
  const {setRatingList, setPage, setSortOption, dataReview, setProductId, pageList} =
    useReviewFilter();
  const [message, setMessage] = useState({
    showSize: false,
    showColor: false,
    showQuantity: false,
    showAddToCart: false,
    content: "Size error",
  });
  const {authState} = useContext(AuthContext);
  const fetchData = () => {
    ProductService.getProductByBarcode(id).then((response) => {
      if (response?.status === "OK") {
        const dataProduct = response?.data;
        setProductId(dataProduct.productId);
        setData(response?.data);
        setTitle(response?.data?.title);
        ProductService.getAllImageProductById(dataProduct.productId).then((response) => {
          setImageList(response.data);
        });
        ProductService.getSaleQuantityByProductId(dataProduct?.productId).then((response) => {
          if (response.status === "OK") {
            setSaleQuantity(response.data);
          }
        });
        ProductService.getRelatedProducts(dataProduct.productId).then((response) => {
          setRelatedProducts(response.data);
        });
        descriptionRef.current.innerHTML = response?.data?.description;
        VariantService.getAllColorByProductId(dataProduct?.productId).then((response) => {
          if (response?.status === "OK") {
            setColorList(response?.data);
            VariantService.getAllSizeByProductId(dataProduct?.productId).then((response) => {
              if (response?.status === "OK") {
                setSizeList(response?.data);
                FeedbackService.getAvgRatingByProductId(dataProduct.productId).then((response) => {
                  if (response.status === "OK") {
                    setAvgRating(response.data);
                    FeedbackService.getOverviewByProductId(dataProduct.productId).then(
                      (response) => {
                        if (response.status === "OK") {
                          setOverviewRating(response.data);
                        }
                      },
                    );
                  }
                });
              }
              setIsLoading(false);
            });
          }
        });
      } else {
        navigate("/not_found");
      }
    });
  };

  useEffect(() => {
    let isMount = true;
    if (isMount) {
      quantity.current.value = num;
    }
    return () => {
      isMount = false;
    };
  }, [num]);

  useEffect(() => {
    setIsLoading(true);
    window.scrollTo(0, 0);
    var abort = new AbortController();
    let isMount = true;
    if (isMount) {
      fetchData();
    }
    return () => {
      isMount = false;
      abort.abort();
    };
  }, [id]);

  const addToCartClickHandle = () => {
    if (authState.isAuthenticated === false) {
      Swal.fire({
        title: "ĐĂNG NHẬP",
        text: "Bạn phải đăng nhập khi thêm sản phẩm vào giỏ hàng, bạn có muốn đăng nhập?",
        icon: "info",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#b8b9bb",
        cancelButtonText: "TIẾP TỤC MUA SẮM",
        confirmButtonText: "ĐĂNG NHẬP",
      }).then((result) => {
        if (result.isConfirmed) {
          window.scrollTo(0, 0);
          navigate("/login");
        }
      });
    } else {
      if (color === null) {
        setMessage({
          ...message,
          showColor: true,
          content: "Bạn phải chọn màu trước khi thêm sản phẩm",
        });
      } else if (size?.sizeId === undefined) {
        setMessage({
          ...message,
          showSize: true,
          content: "Bạn phải chọn size trước khi thêm sản phẩm",
        });
      } else {
        VariantService.getVariantByProductIdAndColorIdAndSizeId(
          data.productId,
          color.colorId,
          size.sizeId,
        ).then((response) => {
          if (response.status === "OK") {
            const variantData = response.data;
            const userId = authState.user.id;
            CartService.getCartByUserId(userId).then((response) => {
              if (response.status === "OK") {
                const cartItemData = {
                  cartId: response?.data?.id,
                  variantId: variantData?.id,
                  quantity: num,
                };
                CartService.addProductToCart(cartItemData).then((response) => {
                  if (response.status === "OK") {
                    setMessage({
                      showSize: false,
                      showColor: false,
                      showQuantity: false,
                      showAddToCart: true,
                      content: response?.message,
                    });
                  } else {
                    setMessage({
                      ...message,
                      showQuantity: true,
                      content: response.message,
                    });
                  }
                });
              }
            });
          }
        });
      }
    }
  };
  const decrement = () => {
    if (num > 1) {
      setNum(num - 1);
    } else {
      setNum(1);
    }
  };
  const increment = () => {
    setNum(num + 1);
  };
  const fetchVariant = async (sizeId) => {
    const response = await VariantService.getVariantByProductIdAndColorIdAndSizeId(
      data.productId,
      color.colorId,
      sizeId,
    );
    setVariant(response.data);
  };
  return (
    <div
      className={
        "w-full flex justify-center items-center flex-col md:mt-[80px] mt-[50px] pb-[100px]"
      }
    >
      <Title title={title}/>
      <div className="text-gray-500 p-7 flex md:flex-row flex-col justify-center items-center md:gap-2 gap-0">
        <Link to="/" className="underline">
          Trang chủ
        </Link>{" "}
        <span className="md:block hidden">/</span>{" "}
        <span className="normal-case text-center">{data?.title}</span>
      </div>
      {isLoading && (
        <div className="text-center h-[80vh] flex justify-center items-center">
          <Spinner aria-label="Default status example" size="xl"/>
        </div>
      )}
      <div
        className={
          !isLoading
            ? "flex md:w-[95%] w-full h-fit gap-4 justify-between flex-col md:flex-row"
            : "hidden"
        }
      >
        <div className="flex md:w-1/2 flex-col-reverse md:flex-row w-full md:gap-0 gap-3 md:px-[60px] px-0">
          <div
            className="h-[150px] md:h-[700px] flex flex-row md:flex-col gap-8  md:overscroll-none overflow-hidden overflow-x-scroll hover:overflow-y-scroll md:w-[90px] w-full">
            {imageList.map((item, index) => {
              return (
                <img
                  className="md:w-[70px] w-[100px] border-2 hover:border-gray-700 transition-all duration-200 object-cover"
                  src={item.value}
                  alt="hinh 1 san pham"
                  key={index}
                  onClick={() => {
                    image.current.src = item.value;
                  }}
                />
              );
            })}
          </div>
          <div className="w-full h-fit">
            <img
              className="w-full object-cover"
              src={data?.productImage}
              alt="hinh 1 san pham"
              ref={image}
            />
          </div>
        </div>
        {/* Chi tiết */}
        <div className="md:w-1/2 w-full mx-auto px-3">
          <div className="flex flex-col md:gap-4 gap-1 md:px-0 px-6">
            <div className="uppercase text-xl md:text-[40px] text-start leading-normal">
              {data?.title}
            </div>
            <div>
              <p className="md:text-xl text-md">
                <span className="font-semibold ">Đã bán: </span> {saleQuantity} sản phẩm
              </p>{" "}
            </div>
            <div className="py-2	">
              <p className="text-orange-600 text-xl md:text-[40px]">
                {GlobalUtil.numberWithCommas(data?.standCost)} ₫
              </p>
            </div>
            <div>
              <div className="flex gap-2 md:text-lg text-md">
                <p>
                  CHỌN MÀU: <span className="font-bold">{color?.colorName}</span>
                </p>
                {message.showColor && <p className="text-red-600">{message.content}</p>}
              </div>
              <div className="flex gap-4 my-3">
                {colorList?.map((item) => {
                  return (
                    <img
                      key={item?.id}
                      className={
                        item?.id !== color?.id
                          ? "w-[50px] cursor-pointer hover:border-primary-color border-2 transition-all duration-200"
                          : "border-primary-color border-2 transition-all duration-200 w-[50px] cursor-pointer"
                      }
                      src={item?.image}
                      alt="Kieu dang cua san pham"
                      onClick={() => {
                        setColor(item);
                        VariantService.getSizeByProductIdAndColorId(
                          data?.productId,
                          item?.colorId,
                        ).then((response) => {
                          if (response.status === "OK") {
                            setSizeList(response.data);
                            setSize({});
                            image.current.src = item?.image;
                            setMessage({
                              ...message,
                              showColor: false,
                            });
                          }
                        });
                        setVariant({});
                      }}
                    />
                  );
                })}
              </div>
            </div>
            <div>
              <div className="flex gap-2 md:text-lg text-md">
                <p>
                  CHỌN SIZE: <span className="font-bold">{size?.sizeName}</span>
                </p>{" "}
                {message.showSize && <p className="text-red-500">{message.content}</p>}
              </div>

              <div className="my-3 flex gap-4">
                {sizeList.map((item) => {
                  return item?.isChosen === true ? (
                    <div
                      className={
                        size.sizeId === item.sizeId
                          ? "w-[50px] h-[50px] border border-gray-400 flex justify-center items-center transition-all duration-200 hover:cursor-pointer bg-main-bg hover:bg-main-bg text-white hover:border-gray-300"
                          : "w-[50px] h-[50px] border border-gray-700 flex justify-center items-center transition-all duration-200 hover:cursor-pointer hover:bg-main-bg hover:text-white hover:border-gray-300"
                      }
                      key={item?.id}
                      onClick={() => {
                        setSize(item);
                        setMessage({
                          ...message,
                          showSize: false,
                        });
                        fetchVariant(item?.sizeId);
                      }}
                    >
                      {item?.sizeName}
                    </div>
                  ) : (
                    <div
                      className="w-[50px] h-[50px] border text-gray-400 flex justify-center items-center transition-all duration-200 cursor-not-allowed"
                      key={item?.id}
                    >
                      {item?.sizeName}
                    </div>
                  );
                })}
              </div>
            </div>
            <div>
              <p className="md:text-lg text-md">
                Hiện có:{" "}
                <span
                  className={
                    variant?.inventory
                      ? "font-semibold transition-all opacity-100"
                      : "font-semibold transition-all opacity-0"
                  }
                >
                  {variant?.inventory} sản phẩm
                </span>{" "}
              </p>
            </div>
            <div>
              <p
                className="hover:underline cursor-pointer"
                onClick={() => {
                  Swal.fire({
                    title: "Sweet!",
                    text: "Modal with a custom image.",
                    imageUrl: "https://unsplash.it/400/200",
                    imageWidth: 400,
                    imageHeight: 200,
                    imageAlt: "Custom image",
                  });
                }}
              >
                Hướng dẫn chọn size
              </p>
            </div>
            <div>
              <div>
                <p className="md:text-lg text-md">
                  CHỌN SỐ LƯỢNG: <span className="font-bold"> {num}</span>
                </p>
                {message.showQuantity && <p className="text-red-500">{message.content}</p>}
                {message.showAddToCart && <p className="text-green-600">{message.content}</p>}
              </div>
              <div className="custom-number-input h-10 w-32 xl:mb-0 mb-10">
                <div
                  className="flex xl:flex-row flex-col h-10 w-fit rounded-lg relative bg-transparent mt-3 justify-start items-start gap-3">
                  <div className="flex xl:flex-row h-10 w-fit rounded-lg relative bg-transparent mt-3">
                    <button
                      name="btn-decrement"
                      onClick={decrement}
                      className=" bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 cursor-pointer outline-none"
                    >
                      <span className="m-auto text-2xl font-thin">−</span>
                    </button>
                    <input
                      type="text"
                      className=" focus:outline-none focus:ring-0 border-none text-center w-[60px] bg-gray-300 font-semibold text-md hover:text-black focus:text-black flex items-center text-gray-700 "
                      name="custom-input-number"
                      ref={quantity}
                      readOnly
                    />
                    <button
                      name="btn-increment"
                      onClick={increment}
                      className="bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 cursor-pointer"
                    >
                      <span className="m-auto text-2xl font-thin">+</span>
                    </button>
                  </div>
                  <div className="md:mt-3 mt-0">
                    <button
                      className="transition-all duration-200 hover:shadow-md bg-blue-600 hover:bg-blue-700 text-white p-2 w-[250px] ml-0 md:ml-[100px]"
                      type="button"
                      onClick={addToCartClickHandle}
                    >
                      THÊM VÀO GIỎ HÀNG
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="my-[50px]">
            <ul
              className="py-[15px] bg-gray-100 flex flex-wrap gap-x-[20px] gap-y-2 justify-center items-center mx-3 md:mx-0">
              <li className="flex justify-start items-start gap-2 w-[230px] h-fit p-2">
                <div className="flex justify-center items-center h-full my-auto">
                  <GiTShirt className="text-[35px]"/>
                </div>
                <div className="flex flex-col">
                  <p className="font-bold">Sản phẩm đa dạng</p>
                  <p>Nhiều kiểu dáng</p>
                </div>
              </li>
              <li className="flex justify-start items-start gap-2 w-[230px] h-fit p-2">
                <div className="flex justify-center items-center h-full my-auto">
                  <FaShoppingCart className="text-[35px]"/>
                </div>
                <div className="flex flex-col">
                  <p className="font-bold">Đặt đơn nhanh chóng</p>
                  <p>Tiện lợi</p>
                </div>
              </li>
              <li className="flex justify-start items-start gap-2 w-[230px] h-fit p-2">
                <div className="flex justify-center items-center h-full my-auto">
                  <BiBroadcast className="text-[35px]"/>
                </div>
                <div className="flex flex-col">
                  <p className="max-w-[200px]">Theo dõi trạng thái đơn hàng dễ dàng</p>
                </div>
              </li>
              <li className="flex justify-start items-start gap-2 w-[230px] h-fit p-2">
                <div className="flex justify-center items-center h-full my-auto">
                  <FaShippingFast className="text-[35px]"/>
                </div>
                <div className="flex flex-col">
                  <p className="max-w-[200px]">Quản lý đơn hàng tiện lợi</p>
                </div>
              </li>
              <li className="flex justify-start items-start gap-2 w-[230px] h-fit p-2">
                <div className="flex justify-center items-center h-full my-auto">
                  <MdPayment className="text-[35px]"/>
                </div>
                <div className="flex flex-col">
                  <p className="max-w-[200px]">Thanh toán dễ dàng nhiều hình thức</p>
                </div>
              </li>
              <li className="flex justify-start items-start gap-2 w-[230px] h-fit p-2">
                <div className="flex justify-start items-start h-full my-auto ml-0">
                  <AiFillPhone className="text-[35px]"/>
                </div>
                <div className="flex flex-col">
                  <p>Hotline hỗ trợ</p>
                  <p className="font-semibold">0388 891 635</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      {/* Phần mô tả */}
      <div className={!isLoading ? "flex md:w-[90%] w-full flex-col mt-8" : "hidden"}>
        <Tabs.Group aria-label="Tabs with icons" style="underline">
          <Tabs.Item title="Mô tả" icon={MdDescription}>
            <div
              className="text-justify leading-relaxed flex flex-col justify-center items-center gap-2 overflow-scroll text-[15px] md:text-xl"
              ref={descriptionRef}
            ></div>
          </Tabs.Item>
        </Tabs.Group>
        <Tabs.Group aria-label="Tabs with icons" style="underline">
          <Tabs.Item title="Đánh giá" icon={HiThumbUp}>
            <div className="w-full flex flex-wrap">
              <div
                className="flex flex-col justify-center items-center md:w-1/2 md:border-r-2 border-r-0 border-dotted w-full">
                <p className="font-bold text-lg">Đánh giá sản phẩm</p>
                <p className="text-[40px] font-bold">{avgRating}/5</p>
                <Rating value={avgRating}/>
                <p ref={reviewRef}>
                  (
                  {overviewRating?.length !== 0
                    ? overviewRating
                      .map(({amountOfRating}) => amountOfRating)
                      .reduce((count, item) => count + item)
                    : 0}{" "}
                  đánh giá)
                </p>
              </div>
              <div className="md:w-1/2 flex justify-center items-center flex-col px-10 gap-2 w-full">
                {overviewRating.map((item, index) => {
                  return (
                    <div className="w-full" key={index}>
                      <div className="flex justify-between">
                        <div className="flex gap-1 justify-start items-center text-[20px]">
                          {item.rating} <AiFillStar className="text-yellow-300"/>
                        </div>
                        <div className="flex justify-end items-center">
                          {item.amountOfRating} đánh giá
                        </div>
                      </div>
                      <Progress progress={item.percentage} size="lg"/>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="flex flex-col items-start justify-start gap-4 mt-3">
              <div
                className="flex justify-start items-center gap-4 mt-1 flex-wrap"
                onChange={(e) => {
                  if (e.target.value === "ALL") {
                    setRatingList(() => {
                      return [1, 2, 3, 4, 5];
                    });
                  } else {
                    setRatingList(() => {
                      return [Number(e.target.value)];
                    });
                  }
                }}
              >
                <p className="font-semibold">Lọc theo:</p>
                <label className="flex justify-center items-center gap-1">
                  <input type="radio" name="rating" value="ALL" defaultChecked/> Tất cả
                </label>
                <label className="flex justify-center items-center gap-1">
                  <input type="radio" name="rating" value="5"/> 5{" "}
                  <AiFillStar className="text-yellow-300"/>
                </label>
                <label className="flex justify-center items-center gap-1">
                  <input type="radio" name="rating" value="4"/> 4{" "}
                  <AiFillStar className="text-yellow-300"/>
                </label>
                <label className="flex justify-center items-center gap-1">
                  <input type="radio" name="rating" value="3"/> 3{" "}
                  <AiFillStar className="text-yellow-300"/>
                </label>
                <label className="flex justify-center items-center gap-1">
                  <input type="radio" name="rating" value="2"/> 2{" "}
                  <AiFillStar className="text-yellow-300"/>
                </label>
                <label className="flex justify-center items-center gap-1">
                  <input type="radio" name="rating" value="1"/> 1{" "}
                  <AiFillStar className="text-yellow-300"/>
                </label>
              </div>
              <div className="flex justify-between gap-2 w-full">
                <div className="flex justify-center items-center">
                  <p className="font-semibold">{dataReview.totalRecord} đánh giá</p>
                </div>
                <div className="flex justify-start items-center gap-2">
                  <p className="font-semibold">Sắp xếp:</p>
                  <select
                    onChange={(e) => {
                      setSortOption(e.target.value);
                    }}
                  >
                    <option value="time_desc" defaultChecked>
                      Mới nhất
                    </option>
                    <option value="time_asc">Cũ nhất</option>
                    <option value="rating_desc">Đánh giá cao</option>
                    <option value="rating_asc">Đánh giá thấp</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="md:w-full flex flex-col gap-[40px] mt-4 border-t-2 pt-[40px] ">
              {dataReview?.reviewListData?.map((item) => (
                <Review item={item} key={item.id}/>
              ))}
            </div>
            <div className="flex justify-end items-center gap-2">
              {pageList.map((item) => {
                return (
                  <div
                    className={
                      dataReview.page !== item
                        ? "flex items-center justify-center w-10 h-10 bg-white text-black border-black border cursor-pointer hover:text-white hover:bg-black transition-all"
                        : "flex items-center justify-center w-10 h-10 bg-black text-white cursor-pointer"
                    }
                    onClick={() => {
                      setPage(item);
                      reviewRef.current.scrollIntoView();
                    }}
                    key={item}
                  >
                    {item}
                  </div>
                );
              })}
            </div>
          </Tabs.Item>
        </Tabs.Group>
        <Tabs.Group aria-label="Tabs with icons" style="underline">
          <Tabs.Item title="Sản phẩm liên quan" icon={MdDescription}>
            <div className="grid md:grid-cols-4 grid-cols-2 md:gap-4 gap-2 w-[90%] mx-auto">
              {relatedProducts?.map((item, index) => (
                <ProductGallery {...item} width={380} key={index}/>
              ))}
            </div>
          </Tabs.Item>
        </Tabs.Group>
      </div>
    </div>
  );
};

const Review = ({item}) => {
  return (
    <div className="w-full flex justify-start items-center xl:flex-row flex-col xl:gap-0 gap-2">
      <div className="flex flex-row xl:flex-col justify-center items-center xl:w-1/6 w-full gap-4">
        <div>
          <img
            src={item.userPhoto}
            alt="user_photo"
            className="w-[70px] h-[70px] object-cover rounded-full shadow-md"
          />
        </div>
        <div className="flex flex-col justify-center items-center">
          <p className="text-[15px] font-bold">{item.fullName}</p>
          <p className="text-[15px] text-gray-400">
            {GlobalUtil.dateTimeConvert(item.createdDate)}
          </p>
        </div>
      </div>
      <div className="w-5/6 flex flex-col items-start justify-start gap-3">
        <div>
          <Rating value={item.rating} size="md"/>
        </div>
        <div>
          <Badge icon={AiFillCheckCircle} size="sm" color="success">
            <p className="text-sm">Đã mua hàng</p>
          </Badge>
        </div>
        <div>
          <p className="text-[17px]">{item.comment}</p>
        </div>
        <div className="uppercase text-gray-400">
          <p>Màu - {item.variantColor}</p> <p>Size - {item.variantSize}</p>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
