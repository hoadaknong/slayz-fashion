import { AiOutlineShoppingCart } from "@react-icons/all-files/ai/AiOutlineShoppingCart";
import React from "react";
import { useNavigate } from "react-router-dom";
import { Title } from "../components";

const OrderResult = () => {
  const navigate = useNavigate();
  window.scrollTo(0, 0);
  return (
    <div className="mt-[100px] h-[80vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
      <Title title={"Kết quả thanh toán"} />
      <div>
        <AiOutlineShoppingCart className="text-[180px]" />
      </div>
      <div className="font-bold uppercase text-orange-500 text-[30px]">
        CẢM ƠN QUÝ KHÁCH ĐÃ TIN TƯỞNG VÀ ỦNG HỘ SLAYZ!
      </div>
      <div>Quý khách có thể kiểm tra đơn hàng đã đặt hàng trong phần lịch sử đơn hàng.</div>
      <button
        className="transition-all duration-200 hover:shadow-md bg-none p-3 w-[20%] text-black border border-gray-600"
        type="button"
        onClick={() => {
          navigate("/");
        }}
      >
        TIẾP TỤC MUA SẮM
      </button>
    </div>
  );
};

export default OrderResult;
