/* eslint-disable no-unused-vars */
import React, { useEffect, useRef } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";
import Swal from "sweetalert2";
import { AuthService } from "../services/auth.service";
import { Title } from "../components";

const ResetPassword = () => {
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const [requestParams, setRequestParams] = useSearchParams();
  const navigate = useNavigate();
  const onSubmitHandle = (e) => {
    e.preventDefault();
    const password = passwordRef.current.value;
    const passwordConfirm = passwordConfirmRef.current.value;
    if (password.trim() !== passwordConfirm.trim()) {
      Swal.fire({
        icon: "error",
        title: "Mật khẩu",
        text: "Mật khẩu xác nhận không đúng!",
        footer: '<a href="https://www.facebook.com/slayz.vn">Liên hệ đội ngũ hộ trợ</a>',
      });
      passwordConfirmRef.current.focus();
    } else {
      const token = requestParams.get("token");
      const data = { token, passwordNew: passwordConfirm };
      AuthService.resetPassword(data).then((response) => {
        if (response.status === "OK") {
          Swal.fire({
            icon: "success",
            title: "ĐẶT LẠI MẬT KHẨU",
            text: "Đặt lại mật khẩu thành công",
          });
          navigate("/login");
        }
      });
    }
  };
  useEffect(() => {
    const token = requestParams.get("token");
    AuthService.checkAvailableToken(token).then((response) => {
      if (response.status !== "OK") {
        navigate("/not_found");
      }
    });
  });
  window.scrollTo(0, 0);
  return (
    <form
      onSubmit={onSubmitHandle}
      className="flex justify-center items-start h-fit mt-[120px] xl:mt-[182px] mb-[140px]"
    >
      <Title title={"Quên mật khẩu"} />
      <div className="flex flex-col xl:w-[636px] border border-gray-400 pb-[80px] gap-1 drop-shadow-xl bg-white w-[336px]">
        <label className="mx-auto text-center h-fit mb-8 text-3xl font-semibold content-center w-full px-3 pt-10">
          ĐẶT LẠI MẬT KHẨU
        </label>
        <label className="mx-auto h-fit w-[74%] text-md font-semibold content-center px-3 pt-3 flex flex-col justify-center items-start">
          <p>Mật khẩu mới</p>
          <input
            placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;"
            className="my-2 h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white w-full placeholder:font-normal"
            ref={passwordRef}
            type="password"
            required
          />
        </label>
        <label className="mx-auto h-fit w-[74%] mb-5 text-md font-semibold content-center px-3 pt-1 flex flex-col justify-center items-start">
          <p>Nhập lại mật khẩu mới</p>
          <input
            placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;"
            className="my-2 h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white w-full placeholder:font-normal"
            ref={passwordConfirmRef}
            type="password"
            required
          />
        </label>
        <button
          type="submit"
          className="w-[70%] h-[50px] mx-auto mb-3 bg-orange-500  hover:bg-orange-600 text-white text-center focus:drop-shadow-sm uppercase"
        >
          Đặt lại mật khẩu
        </button>
      </div>
    </form>
  );
};

export default ResetPassword;
