import React, { useRef } from "react";
import { useNavigate } from "react-router-dom";
import { AuthService } from "../services/auth.service";
import Swal from "sweetalert2";
import { Title } from "../components";

const ForgotPassword = () => {
  const inputKeyRef = useRef(null);
  const navigate = useNavigate();
  const onSubmitHandle = (e) => {
    e.preventDefault();
    AuthService.sendResetPasswordLink(inputKeyRef.current.value).then((response) => {
      if (response.status === "OK") {
        navigate("/send_mail_success");
      } else {
        Swal.fire({
          icon: "error",
          title: "Lỗi",
          text: "Không tìm thấy số điện thoại hoặc địa chỉ email quý khách vui lòng thử lại!",
          footer: '<a href="https://www.facebook.com/slayz.vn">Liên hệ đội ngũ hộ trợ</a>',
        });
      }
    });
  };
  window.scrollTo(0, 0);
  return (
    <form
      onSubmit={onSubmitHandle}
      className="flex justify-center items-start h-fit mt-[120px] xl:mt-[182px] mb-[140px]"
    >
      <Title title={"Quên mật khẩu"} />
      <div className="flex flex-col xl:w-[636px] border border-gray-400 pb-[80px] gap-1 drop-shadow-xl bg-white w-[336px]">
        <label className="mx-auto text-center h-fit mb-8 text-3xl font-semibold content-center w-full px-3 pt-10">
          QUÊN THÔNG TIN TÀI KHOẢN
        </label>
        <label className="mx-auto h-fit w-[74%] mb-5 text-md font-semibold content-center px-3 pt-10 flex flex-col justify-center items-start">
          <p>
            Tên đăng nhập
            <span className="font-normal text-sm">(bao gồm email hoặc số điện thoại)</span>
          </p>
          <input
            placeholder="Ví dụ: 0388891635, hoadaknong101@gmail.com,..."
            className="my-2 h-[50px] hover:border-orange-500 border-2 border-black focus:outline-none px-3 focus:border-orange-500 focus:drop-shadow-md bg-white w-full placeholder:font-normal"
            ref={inputKeyRef}
            required
          />
        </label>
        <button
          type="submit"
          className="w-[70%] h-[50px] mx-auto mb-3 bg-orange-500  hover:bg-orange-600 text-white text-center focus:drop-shadow-sm uppercase"
        >
          Đặt lại mật khẩu
        </button>
        <button
          onClick={() => {
            navigate("/login");
          }}
          className="w-[70%] h-[50px] mx-auto mb-3 bg-black  hover:bg-gray-600 text-white text-center focus:drop-shadow-sm uppercase"
        >
          Về Trang đăng nhập
        </button>
      </div>
    </form>
  );
};

export default ForgotPassword;
