/* eslint-disable react/jsx-no-target-blank */
import React from "react";
import { Title } from "../components";

const ResultSendMail = () => {
  window.scrollTo(0, 0);
  return (
    <div className="mt-[100px] h-[64vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
      <Title title={"Gửi thư điện tử thành công"} />
      <div>
        <image
          src="https://res.cloudinary.com/dfnnmsofg/image/upload/v1677726445/image-8_e8yh2p.gif"
          alt="password"
        />
      </div>
      <div className="font-bold uppercase text-orange-500 text-[30px]">
        GỬI LINK ĐẶT LẠI MẬT KHẨU THÀNH CÔNG
      </div>
      <div>Quý khách vui lòng kiểm tra hộp thư để tiến hành đặt lại mật khẩu!</div>

      <a
        className="transition-all duration-200 hover:shadow-md p-3 bg-none w-[20%] text-black border border-gray-600 flex justify-center items-center gap-6"
        href="https://mail.google.com/"
        target="_blank"
      >
        <img
          src="https://cdn-icons-png.flaticon.com/512/5968/5968534.png"
          className="w-10 h-10"
          alt="hihi"
        />
        Mở hộp thư điện tử
      </a>
    </div>
  );
};

export default ResultSendMail;
