/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from "react";
import { IoMdClose } from "@react-icons/all-files/io/IoMdClose";
import { Tooltip } from "@material-tailwind/react";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { CartService } from "../../services/cart.service";
import { useDataContext } from "../../contexts/DataProvider";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";

const CartDetail = (props) => {
  const [num, setNum] = useState(props.quantity);
  const quantity = useRef();
  const { fetchCartData, setFlagChange } = useDataContext();
  const decreaseClickHandle = async (e) => {
    if (Number(quantity.current.value) - 1 <= 0) {
      return;
    }
    const isUpdateSuccess = await updateQuantity(Number(quantity.current.value) - 1);
    if (isUpdateSuccess) {
      if (num > 1) {
        setNum((prev) => {
          return prev - 1;
        });
      } else {
        setNum(1);
      }
      props.changeQuantityOnClick(true);
    }
  };
  const increaseClickHandle = async (e) => {
    const isUpdateSuccess = await updateQuantity(Number(quantity.current.value) + 1);
    if (isUpdateSuccess) {
      setNum((prev) => {
        return prev + 1;
      });
      props.changeQuantityOnClick(true);
    }
  };
  const updateQuantity = async (quantity) => {
    const data = {
      cartDetailId: props?.id,
      quantity: quantity,
    };
    const response = await CartService.updateQuantity(data);
    if (response.status !== "OK") {
      Swal.fire({
        icon: "error",
        title: "Giỏ hàng",
        text: response.message,
        footer:
          '<a href="www.facebook.com/slayz.vn/">Liên hệ SlayZ để biêt thêm thông tin chi tiết</a>',
      });
      return false;
    }
    return true;
  };

  const onClickDeleteCartDetailHandle = () => {
    Swal.fire({
      title: "Bạn có muốn xóa sản phẩm này khỏi giỏ hàng?",
      text: `${props.productName} \n MÀU: ${props.style}, SIZE: ${props.size}`,
      icon: "question",
      imageUrl: `${props.imageProduct}`,
      imageHeight: 150,
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "HỦY BỎ",
      confirmButtonText: "XÓA",
    }).then((result) => {
      if (result.isConfirmed) {
        CartService.removeCartDetailFromCart(props?.id).then((response) => {
          if (response?.status === "OK") {
            window.scrollTo(0, 0);
            fetchCartData();
            setFlagChange((prev) => {
              return !prev;
            });
            Swal.fire(
              "Đã xóa",
              "Sản phẩm đã được xóa khỏi giỏ hàng\nMời quý khách tiếp tục mua sắm!",
              "success",
            );
          }
        });
      }
    });
  };
  useEffect(() => {
    quantity.current.value = num;
  }, [num]);

  return (
    <div className="h-fit flex pt-4 gap-3 w-full flex-col md:flex-row">
      <div className="h-fit mx-auto md:mx-0">
        <img src={props.imageProduct} alt="product" className="w-[200px] object-cover" />
      </div>
      <div className="flex flex-col justify-between pb w-full">
        <div>
          <div className="flex justify-between items-center w-full">
            <Link to={"/products/" + props.barcode}>
              <p className="hover:underline">{props.productName}</p>
            </Link>
            <Tooltip
              content="Xóa sản phẩm"
              placement="top"
              animate={{
                mount: { scale: 1, y: 0 },
                unmount: { scale: 0, y: 25 },
              }}
            >
              <button type="button" onClick={onClickDeleteCartDetailHandle}>
                <IoMdClose className="hover:bg-orange-400 text-[30px] p-1 rounded-full hover:text-white transition-all duration-200" />
              </button>
            </Tooltip>
          </div>
          <div className="mt-1">
            <p className="text-gray-500">
              MÀU: {props.style}, SIZE: {props.size}
            </p>
          </div>
        </div>
        <div>
          <p className="text-orange-600 font-semibold">
            {GlobalUtil.numberWithCommas(props.price)} {" ₫"}
          </p>
        </div>
        <div className="flex justify-between">
          <div className="w-full">
            <div className="custom-number-input h-10 w-full flex md:justify-between md:items-center md:flex-row flex-col items-start md:gap-0 gap-1 mt-1 md:mt-0">
              <div className="flex flex-row h-10 w-fit rounded-lg relative bg-transparent">
                <button
                  name="btn-decrement"
                  onClick={decreaseClickHandle}
                  className=" bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 cursor-pointer outline-none"
                >
                  <span className="m-auto text-2xl font-thin">−</span>
                </button>
                <input
                  type="text"
                  className=" focus:outline-none text-center w-[60px] bg-gray-300 font-semibold text-md hover:text-black focus:text-black flex items-center text-gray-700 border-none focus:border-none focus:ring-0"
                  name="custom-input-number"
                  ref={quantity}
                  readOnly
                />
                <button
                  name="btn-increaseClickHandle"
                  onClick={increaseClickHandle}
                  className="bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 cursor-pointer"
                >
                  <span className="m-auto text-2xl font-thin">+</span>
                </button>
              </div>
              <div>
                <p>
                  TỔNG CỘNG:{" "}
                  <span className="font-semibold">
                    {GlobalUtil.numberWithCommas(props.total)}
                    {" ₫"}
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartDetail;
