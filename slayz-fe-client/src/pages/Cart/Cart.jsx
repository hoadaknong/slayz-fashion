/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState } from "react";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import CartDetail from "./CartDetail";
import { AiOutlineShoppingCart } from "@react-icons/all-files/ai/AiOutlineShoppingCart";
import { useEffect } from "react";
import { CartService } from "../../services/cart.service";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { useDataContext } from "../../contexts/DataProvider";
import { Title } from "../../components";

const Cart = () => {
  const navigate = useNavigate();
  const { cartData, setCartData, fetchCartData, flagChange, setFlagChange } = useDataContext();
  const [cartDetails, setCartDetails] = useState([]);
  const [isChangeQuantity, setIsChangeQuantity] = useState(false);
  const fetchData = () => {
    CartService.getCartByUserId().then((response) => {
      if (response.status === "OK") {
        CartService.getCartById(response.data.id).then((response) => {
          setCartData(response.data);
        });
        CartService.getAllCartDetailByCartId(response.data?.id).then((response) => {
          if (response.status === "OK") {
            setCartDetails(response.data);
          }
        });
      } else {
        navigate("/login");
      }
    });
  };
  useEffect(() => {
    window.scrollTo(0, 0);
    fetchData();
  }, [flagChange]);
  if (cartDetails.length === 0) {
    return (
      <div className="mt-[122px] h-[64vh] w-[80%] mx-auto flex flex-col justify-center items-center gap-4">
        <Title title={"Giỏ hàng"} />
        <div>
          <AiOutlineShoppingCart className="text-[180px]" />
        </div>
        <div>Chưa sản phẩm nào trong giỏ hàng.</div>
        <button
          className="transition-all duration-200 hover:shadow-md bg-none p-3 w-[20%] text-black border border-gray-600"
          type="button"
          onClick={() => {
            navigate("/");
          }}
        >
          QUAY LẠI MUA SẮM
        </button>
      </div>
    );
  }
  return (
    <div className="h-fit md:w-[80%] w-full mx-auto flex flex-col justify-center items-center xl:mt-[142px] mt-[60px] pb-[200px]">
      <Title title={"Giỏ hàng"} />
      <div className="w-full px-3 xl:px-0">
        <h1 className="font-bold">GIỎ HÀNG</h1>
        <p>{cartData?.quantityProduct} sản phẩm</p>
      </div>
      {/* Danh sách sản phẩm */}
      <div className="w-full flex flex-col xl:flex-row xl:justify-start xl:items-start justify-center items-center px-3 xl:px-0">
        {/* Sản phẩm */}
        <div className="flex flex-col divide-y-2 gap-[40px] pt-4 xl:w-2/3 w-full xl:mb-[150px] mb-[50px]">
          {cartDetails.map((item) => {
            return (
              <CartDetail {...item} key={item?.id} changeQuantityOnClick={setIsChangeQuantity} />
            );
          })}
          {cartDetails.length !== 0 && (
            <div type="button" className="w-full py-3">
              <button
                className={
                  isChangeQuantity
                    ? "transition-all w-full py-3 bg-black text-white hover:bg-gray-700"
                    : "w-full py-3 bg-gray-200 text-white"
                }
                onClick={() => {
                  if (isChangeQuantity) {
                    fetchCartData();
                    setFlagChange((prev) => {
                      return !prev;
                    });
                    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
                    setIsChangeQuantity(false);
                  }
                }}
              >
                CẬP NHẬT GIỎ HÀNG
              </button>
            </div>
          )}
        </div>
        {/* Chi tiết */}
        <div className="w-full xl:w-1/3 flex flex-col xl:items-end xl:justify-start items-center justify-center gap-5">
          <div className="p-6 px-[40px] xl:w-[80%] w-[100%] flex flex-col gap-5 border border-gray-500">
            <div className="">
              <p className="font-bold uppercase">TẠM TÍNH</p>
            </div>
            <div className="flex justify-between items-center">
              <p>Số lượng</p>
              <p>{cartData?.totalQuantity}</p>
            </div>
            <div className="flex justify-between items-center">
              <p>Tạm tính</p>
              <p>{GlobalUtil.numberWithCommas(cartData?.grandTotal)} ₫</p>
            </div>
            <div className="flex justify-between items-center">
              <p>VAT</p>
              <p>0%</p>
            </div>
            <div className="flex justify-between items-center border-t border-gray-400 pt-6 pb-2">
              <p>Tổng số</p>
              <p className="font-bold uppercase">
                {GlobalUtil.numberWithCommas(cartData?.grandTotal)} ₫
              </p>
            </div>
          </div>
          <button
            className="transition-all duration-200 hover:shadow-md bg-blue-600 hover:bg-blue-700 text-white p-3 w-full xl:w-[80%]"
            type="button"
            onClick={() => {
              navigate("/checkout/address");
            }}
          >
            THANH TOÁN {GlobalUtil.numberWithCommas(cartData?.grandTotal)} ₫
          </button>
          <div className="flex justify-center items-center w-[80%]">
            <Link to="/" className="hover:underline">
              Tiếp tục mua sắm
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
