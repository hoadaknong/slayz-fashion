import "../index.css";
import { useState, useRef, useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { HiUser } from "@react-icons/all-files/hi/HiUser";
import { HiLockClosed } from "@react-icons/all-files/hi/HiLockClosed";
import { HiEyeOff } from "@react-icons/all-files/hi/HiEyeOff";
import { HiEye } from "@react-icons/all-files/hi/HiEye";
import { AuthContext } from "../contexts/AuthContext";
import React from "react";
import { Title } from "../components";
import { GlobalUtil } from "../utils/GlobalUtil";
import bg from "../assets/HomePage1.png";

const Login = () => {
  const { loginUser } = useContext(AuthContext);
  const isRemember = useRef();
  const navigate = useNavigate();
  const [isHiddenPassword, setIsHiddenPassword] = useState(true);
  const [usernameMessage, setUsernameMessage] = useState("");
  const [passwordMessage, setPasswordMessage] = useState("");
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);
  const hidePasswordClick = () => {
    setIsHiddenPassword(!isHiddenPassword);
  };

  const keyEnter = async (e) => {
    if (e.keyCode === 13) {
      await submitHandler();
    }
  };

  const validate = (username, password) => {
    const usernameValid =
      GlobalUtil.validatePhoneNumber(username) || GlobalUtil.validateEmail(username);
    if (username.trim() === "" && password.trim() === "") {
      usernameRef.current.focus();
      setPasswordMessage("Hãy nhập mật khẩu");
      setUsernameMessage("Hãy nhập tên tài khoản");
      return false;
    } else if (username.trim() === "") {
      usernameRef.current.focus();
      setPasswordMessage("");
      setUsernameMessage("Hãy nhập tên tài khoản");
      return false;
    } else if (password.trim() === "") {
      passwordRef.current.focus();
      setPasswordMessage("Hãy nhập mật khẩu");
      setUsernameMessage("");
      return false;
    } else if (!usernameValid) {
      setPasswordMessage("");
      setUsernameMessage("Email hoặc SĐT không hợp lệ!");
      return false;
    }
    return true;
  };

  const submitHandler = async () => {
    if (isRemember.current.checked === false) {
      window.addEventListener("beforeunload", () => localStorage.removeItem("token"));
    }
    const username = usernameRef.current.value;
    const password = passwordRef.current.value;
    if (validate(username, password)) {
      try {
        const response = await loginUser(username.trim(), password);
        if (response) {
          navigate("/");
        } else {
        }
      } catch (error) {
        console.log(error);
      }
    }
  };
  window.scrollTo(0, 0);
  return (
    <div
      className="flex justify-center items-start h-fit pt-[60px] xl:pt-[182px] pb-[200px] xl:mt-0 mt-10"
      style={{
        backgroundImage: `url(${bg})`,
        backgroundSize: "cover",
      }}
    >
      <Title title={"Đăng nhập"} />
      <div className="flex flex-col 2xl:w-[30%] md:w-[60%] w-[90%] rounded-xl border-0 border-gray-400 pb-[80px] gap-1 drop-shadow-xl bg-white">
        <label className="mx-auto text-center h-fit mb-10 text-3xl font-semibold content-center w-full px-3 pt-10">
          ĐĂNG NHẬP
        </label>
        <div className="flex flex-col justify-start items-start mx-auto md:w-[70%] w-[80%]">
          <div className="flex items-center justify-start w-full">
            <div className="bg-black h-[50px] xl:w-[45px] w-[40px] flex justify-center items-center">
              <HiUser className="text-white text-[25px]" />
            </div>
            <input
              placeholder="Email hoặc số điện thoại"
              name="username"
              className="w-full my-2 h-[50px] hover:border-primary-color border-2 border-black focus:outline-none px-3 focus:drop-shadow-md bg-white xl:placeholder:text-md placeholder:text-sm"
              ref={usernameRef}
              onKeyUp={keyEnter}
              onChange={(e) => {
                if (e.target.value.trim() !== "") {
                  setUsernameMessage("");
                }
              }}
            />
          </div>
          <div>
            <p className="text-red-600 texxt-sm">{usernameMessage}</p>
          </div>
        </div>
        <div className="flex flex-col justify-start items-start mx-auto md:w-[70%] w-[80%]">
          <div className="flex justify-start items-center w-full">
            <div className="bg-black h-[50px] w-[50px] flex justify-center items-center">
              <HiLockClosed className="text-white text-[25px]" />
            </div>
            <input
              type={isHiddenPassword ? "password" : "text"}
              name="password"
              placeholder="Mật khẩu"
              className="w-full ring-0 focus:ring-0 my-2 h-[50px] hover:border-primary-color border-2 border-black focus:outline-none px-3 focus:drop-shadow-md bg-white xl:placeholder:text-md placeholder:text-sm"
              ref={passwordRef}
              onChange={(e) => {
                if (e.target.value.trim() !== "") {
                  setPasswordMessage("");
                }
              }}
            />
            <div
              onClick={hidePasswordClick}
              className="bg-black h-[50px] w-[50px] flex justify-center items-center cursor-pointer"
            >
              {isHiddenPassword ? (
                <HiEyeOff className="text-white text-[22px]" />
              ) : (
                <HiEye className="text-white text-[22px]" />
              )}
            </div>
          </div>
          <div>
            <p className={"text-red-600 texxt-sm"}>{passwordMessage}</p>
          </div>
        </div>
        <div className="w-[80%] md:w-[70%] mx-auto flex flex-col mt-1 xl:flex-row xl:justify-between justify-start items-start">
          <label className="w-fit flex xl:justify-center xl:items-center gap-2 justify-start items-start cursor-pointer">
            <input
              type="checkbox"
              name="remember"
              id=""
              className="w-5 h-5 accent-black"
              defaultChecked={true}
              ref={isRemember}
            />
            Ghi nhớ đăng nhập
          </label>
          <Link to="/forgot_password" className="hover:underline w-fit">
            Quên mật khẩu?
          </Link>
        </div>
        <button
          type="button"
          onClick={submitHandler}
          className="transition-all w-[80%] md:w-[70%] h-[50px] mx-auto my-3 bg-main-bg  hover:bg-hover-main-bg text-white text-center focus:drop-shadow-sm uppercase"
        >
          Đăng nhập
        </button>
        <div className="mx-auto w-[80%] md:w-[70%] flex flex-col justify-start items-center border-t pt-8 mt-4 border-gray-400">
          <button
            type="button"
            className="w-full h-[50px] mx-auto bg-black  hover:bg-gray-600 text-white text-center focus:drop-shadow-sm uppercase transition-all"
            onClick={() => {
              navigate("/register");
            }}
          >
            Đăng ký
          </button>
        </div>
      </div>
    </div>
  );
};

export default Login;
