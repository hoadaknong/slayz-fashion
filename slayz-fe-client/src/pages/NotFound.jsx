import React from "react";
import { Link } from "react-router-dom";
import { Title } from "../components";

const NotFound = () => {
  return (
    <div className="h-[64vh] flex justify-center items-center mt-[102px]">
      <Title title={"Không tìm thấy trang"} />
      <div className="w-fit h-fit flex flex-col justify-center items-center">
        <h1 className="font-bold text-[30px]">KHÔNG TÌM THẤY TRANG</h1>
        <Link to="/" className="hover:underline">
          Quay về trang chủ
        </Link>
      </div>
    </div>
  );
};

export default NotFound;
