const numberWithCommas = (number) => {
  if (number === undefined || number === null) {
    return "0";
  }
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
const dateTimeConvert = (milliseconds) => {
  const date = new Date(milliseconds);
  return (
    date.getDate() +
    "-" +
    (date.getMonth() + 1) +
    "-" +
    date.getUTCFullYear() +
    " " +
    date.getHours() +
    ":" +
    date.getMinutes()
  );
};

const validatePhoneNumber = (phoneNumber) => {
  var phoneRegex = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  if (phoneNumber.match(phoneRegex)) {
    return true;
  } else {
    return false;
  }
};

const validateEmail = (mail) => {
  if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(mail)) return true;
  return false;
};

const checkDateIsOverTwoWeeks = (dateUnixTimeMiliseconds = Date.now()) => {
  const INTERVAL_14_DAYS = 1209600 * 1000;
  const intervalOfOrderDateToNow = Date.now() - dateUnixTimeMiliseconds;
  if (intervalOfOrderDateToNow > INTERVAL_14_DAYS) {
    return true;
  }
  return false;
};

const calculatePercent = (value, total) => {
  return (value / total) * 100;
};

export const GlobalUtil = {
  numberWithCommas,
  dateTimeConvert,
  checkDateIsOverTwoWeeks,
  validatePhoneNumber,
  validateEmail,
  calculatePercent,
};
