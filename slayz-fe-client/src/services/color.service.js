import { api } from "../api/axios";

const getColorById = (id) => {
  return api.get("/api/v1/color/" + id).then((response) => {
    return response.data.data;
  });
};

const getAllColor = () => {
  return api.get("/api/v1/color").then((response) => {
    return response.data;
  });
};

export const ColorService = {
  getAllColor,
  getColorById,
};
