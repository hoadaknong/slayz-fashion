import { api } from "../api/axios";

const login = (data) => {
  return api.post("/api/v1/auth/login", data).then((response) => {
    return response.data;
  });
};

const register = (fullName, email, password, confirmPassword) => {
  const data = { fullName, email, password, confirmPassword };
  return api.post("/api/v1/auth/register", data).then((response) => {
    return response.data;
  });
};

const sendResetPasswordLink = (key) => {
  return api.get("/api/v1/reset_password/send_mail?key=" + key).then((response) => {
    return response.data;
  });
};
const checkAvailableToken = (token) => {
  return api.get("/api/v1/reset_password/check_token/" + token).then((response) => {
    return response.data;
  });
};

const resetPassword = (data) => {
  return api.post("/api/v1/reset_password", data).then((response) => {
    return response.data;
  });
};

const logout = () => {
  return api.get("/api/v1/auth/logout").then((response)=>{
    return response.data
  })
}

export const AuthService = {
  register,
  sendResetPasswordLink,
  checkAvailableToken,
  resetPassword,
  login,
  logout
};
