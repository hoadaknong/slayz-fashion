import { api } from "../api/axios";

const addProductToCart = (data = {}) => {
  return api.post("/api/v1/cart/add_to_cart", data).then((response) => {
    return response.data;
  });
};

const getCartByUserId = () => {
  return api.get("/api/v1/cart/user").then((response) => {
    return response.data;
  });
};

const getAllCartDetailByCartId = (id = 0) => {
  return api.get("/api/v1/cart/cart_detail/" + id).then((response) => {
    return response.data;
  });
};

const getCartById = (id = 0) => {
  return api.get("/api/v1/cart/" + id).then((response) => {
    return response.data;
  });
};

const updateQuantity = (data) => {
  return api.put("api/v1/cart/update_quantity", data).then((response) => {
    return response.data;
  });
};

const removeCartDetailFromCart = (id = 0) => {
  return api.delete("api/v1/cart/cart_detail/" + id).then((response) => {
    return response.data;
  });
};

export const CartService = {
  addProductToCart,
  getCartByUserId,
  getAllCartDetailByCartId,
  getCartById,
  updateQuantity,
  removeCartDetailFromCart,
};
