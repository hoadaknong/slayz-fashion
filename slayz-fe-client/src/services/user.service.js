import { api } from "../api/axios";

const getAllUser = () => {
  return api.get("/api/v1/user").then((response) => {
    return response.data;
  });
};

const getAllUserByStatus = (status) => {
  return api.get("/api/v1/user/status?status=" + status).then((response) => {
    return response.data;
  });
};

const getUserById = (id = 0) => {
  return api.get("/api/v1/user/" + id).then((response) => {
    return response.data;
  });
};

const updateUser = (id, data) => {
  return api.put("/api/v1/user/" + id, data).then((response) => {
    return response.data;
  });
};

const activeUser = (id) => {
  return api.patch("/api/v1/user/active/" + id, null).then((response) => {
    return response.data;
  });
};

const disableUser = (id) => {
  return api.patch("/api/v1/user/disable/" + id, null).then((response) => {
    return response.data;
  });
};

const updateRoleUser = (userId, roleId) => {
  return api.patch(`api/v1/user/${userId}/role/${roleId}`, null).then((response) => {
    return response.data;
  });
};

const updatePassword = (userId, data) => {
  return api.post(`api/v1/user/${userId}/password`, data).then((response) => {
    return response.data;
  });
};

const getCurrentUser = () => {
  return api.get("api/v1/user/current_user").then((response) => {
    return response.data;
  });
};

export const UserService = {
  getAllUser,
  getAllUserByStatus,
  getUserById,
  updateUser,
  activeUser,
  disableUser,
  updateRoleUser,
  updatePassword,
  getCurrentUser,
};
