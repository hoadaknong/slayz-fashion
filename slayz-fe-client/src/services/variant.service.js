import { api } from "../api/axios";

const getAllColorByProductId = (id) => {
  return api.get("/api/v1/variant/color?productId=" + id).then((response) => {
    return response.data;
  });
};

const getAllSizeByProductId = (id) => {
  return api.get("/api/v1/variant/size?productId=" + id).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};
const getSizeByProductIdAndColorId = (productId, colorId) => {
  return api.get("/api/v1/variant/size_chosen?productId=" + productId + "&colorId=" + colorId).then(
    (response) => {
      return response.data;
    },
    (error) => {
      return error;
    },
  );
};

const getVariantByProductIdAndColorIdAndSizeId = (productId, colorId, sizeId) => {
  return api
    .get(
      "/api/v1/variant/find?productId=" + productId + "&colorId=" + colorId + "&sizeId=" + sizeId,
    )
    .then((response) => {
      return response.data;
    });
};

export const VariantService = {
  getAllColorByProductId,
  getAllSizeByProductId,
  getSizeByProductIdAndColorId,
  getVariantByProductIdAndColorIdAndSizeId,
};
