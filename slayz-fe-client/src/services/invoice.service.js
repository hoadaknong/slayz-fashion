import {api} from "../api/axios";

const checkOut = (data = {}) => {
  return api.post("/api/v1/invoice", data).then((response) => {
    return response.data;
  });
};

const updatePaymentStatus = (id, data = {}) => {
  return api.patch("/api/v1/invoice/payment_status/" + id, data).then((response) => {
    console.log(response);
    return response.data;
  });
};

const updateInvoiceStatus = (id, data = {}) => {
  return api.patch("/api/v1/invoice/status/" + id, data).then((response) => {
    console.log(response);
    return response.data;
  });
};

const getInvoiceByUserId = (userId, pageIndex, pageSize, status, paymentStatus) => {
  return api.get(`/api/v1/invoice/users/${userId}?pageIndex=${pageIndex}&pageSize=${pageSize}&status=${status}&paymentStatus=${paymentStatus}`).then((response) => {
    return response.data;
  });
};

const getAllInvoiceDetailByInvoiceId = (invoiceId) => {
  return api.get("/api/v1/invoice/invoice_detail/" + invoiceId).then((response) => {
    return response.data;
  });
};

const getInvoiceById = (id) => {
  return api.get("/api/v1/invoice/" + id).then((response) => {
    return response.data;
  });
};

const getAllInvoiceByStatus = (status, pageIndex, pageSize) => {
  return api.get(`/api/v1/invoice/status/${status}?pageIndex=${pageIndex}&pageSize=${pageSize}`).then((response) => {
    return response.data;
  });
};

export const InvoiceService = {
  checkOut,
  updatePaymentStatus,
  getInvoiceByUserId,
  getAllInvoiceDetailByInvoiceId,
  getInvoiceById,
  updateInvoiceStatus,
  getAllInvoiceByStatus,
};
