import { api } from "../api/axios";

const getAllProduct = () => {
  return api.get("/api/v1/product/product_grid").then((response) => {
    return response.data;
  });
};
const get12TheNewestProduct = () => {
  return api.get("/api/v1/product/newest_product").then((response) => {
    return response.data;
  });
};

const createProduct = (data) => {
  console.log("Create new product");
  return api.post("/api/v1/product", data).then((response) => {
    return response.data;
  });
};

const updateProduct = (id, data) => {
  console.log("Update new product");
  return api.put("/api/v1/product/" + id, data).then((response) => {
    return response.data;
  });
};

const getProductById = (id) => {
  return api.get("/api/v1/product/" + id).then((response) => {
    return response.data;
  });
};

const getProductByBarcode = (barcode) => {
  return api.get("/api/v1/product/barcode/" + barcode).then((response) => {
    return response.data;
  });
};

const deleteProductById = (id) => {
  return api.delete("/api/v1/product/" + id).then((response) => {
    return response.data;
  });
};

const filterData = (data) => {
  return api.post("/api/v1/product/filter", data).then((response) => {
    return response.data;
  });
};
const getSaleQuantityByProductId = (id) => {
  return api.get("/api/v1/product/sale_quantity/" + id).then((response) => {
    return response.data;
  });
};

const getAllImageProductById = (id) => {
  return api.get("/api/v1/image_product/" + id).then((response) => {
    return response.data;
  });
};

const getRelatedProducts = (id) => {
  return api.get("/api/v1/product/" + id + "/related").then((response) => {
    return response.data;
  });
};

export const ProductService = {
  getRelatedProducts,
  getAllProduct,
  createProduct,
  updateProduct,
  deleteProductById,
  getProductById,
  getProductByBarcode,
  filterData,
  getSaleQuantityByProductId,
  get12TheNewestProduct,
  getAllImageProductById,
};
