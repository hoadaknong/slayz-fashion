import { api } from "../api/axios";

const getAllCategory = (pageIndex = 1, pageSize = 100) => {
  return api.get(`/api/v1/category?pageIndex=${pageIndex}&pageSize=${pageSize}`).then((response) => {
    return response.data;
  });
};

export const CategoryService = {
  getAllCategory,
};
