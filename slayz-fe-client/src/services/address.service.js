import {api} from "../api/axios";

const createAddress = (data) => {
  return api.post("/api/v1/address", data).then((response) => {
    return response.data;
  });
};

const getAllAddress = () => {
  return api.get("/api/v1/address").then((response) => {
    return response.data;
  });
};

const updateAddress = (id, data) => {
  return api.put("/api/v1/address/" + id, data).then((response) => {
    return response.data;
  });
};

const getAddressById = (id) => {
  return api.get("/api/v1/address/" + id).then((response) => {
    return response.data;
  });
};

const setDefaultAddress = (id) => {
  return api.patch("/api/v1/address/" + id).then((response) => {
    return response.data;
  });
};

const getAllAddressByUserId = (id, pageIndex = 1, pageSize = 100) => {
  return api.get(`/api/v1/address/user/${id}?pageIndex=${pageIndex}&pageSize=${pageSize}`).then((response) => {
    return response.data;
  });
};

const getDefaultAddressByUserId = (id) => {
  return api.get("/api/v1/address/default_address/" + id).then((response) => {
    return response.data;
  });
};

const removeAddressById = (id) => {
  return api.delete("/api/v1/address/" + id).then((response) => {
    return response.data;
  });
};

export const AddressService = {
  getAllAddress,
  getAllAddressByUserId,
  getDefaultAddressByUserId,
  getAddressById,
  setDefaultAddress,
  updateAddress,
  createAddress,
  removeAddressById,
};
