import { api } from "../api/axios";

const getFeedbackById = (id) => {
  return api.get("/api/v1/review/" + id).then((response) => {
    return response.data;
  });
};

const getAllFeedback = () => {
  return api.get("/api/v1/review").then((response) => {
    return response.data;
  });
};

const createNewReview = (data) => {
  return api.post("/api/v1/review", data).then((response) => {
    return response.data;
  });
};

const deleteReview = (id) => {
  return api.delete("/api/v1/review/" + id).then((response) => {
    return response.data;
  });
};

const disableReview = (id) => {
  return api.post(`/api/v1/review/${id}/disable`, null).then((response) => {
    return response.data;
  });
};

const enableReview = (id) => {
  return api.post(`/api/v1/review/${id}/enable`, null).then((response) => {
    return response.data;
  });
};

const getAllReviewByProductId = (payload) => {
  return api.post("/api/v1/review/product", payload).then((response) => {
    return response.data;
  });
};

const getAllReviewByUserId = (id) => {
  return api.get("/api/v1/review/user/" + id).then((response) => {
    return response.data;
  });
};

const getAvgRatingByProductId = (id) => {
  return api.get("/api/v1/review/avg_rating/" + id).then((response) => {
    return response.data;
  });
};

const getOverviewByProductId = (id) => {
  return api.get("/api/v1/review/overview_rating/" + id).then((response) => {
    return response.data;
  });
};

const getReviewByInvoiceDeatailId = (invoiceDetailId) => {
  return api.get("/api/v1/review/invoice_detail_id/" + invoiceDetailId).then((response) => {
    return response.data;
  });
};

export const FeedbackService = {
  getReviewByInvoiceDeatailId,
  getAllFeedback,
  createNewReview,
  deleteReview,
  disableReview,
  enableReview,
  getAllReviewByProductId,
  getAllReviewByUserId,
  getFeedbackById,
  getAvgRatingByProductId,
  getOverviewByProductId,
};
