import {api} from "../api/axios";

const createPayment = (data = {}) => {
  return api.post("/api/v1/payment/momo", data).then((response) => {
    return response.data;
  });
};
const createPaymentVnpay = (data = {}) => {
  return api.post("/api/v1/payment/vnpay", data).then((response) => {
    return response.data;
  });
};
export const PaymentService = {
  createPayment, createPaymentVnpay
};
