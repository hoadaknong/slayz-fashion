import { api } from "../api/axios";

const getSizeById = (id) => {
  return api.get("/api/v1/size/" + id).then((response) => {
    return response.data.data;
  });
};

const getAllSize = () => {
  return api.get("/api/v1/size").then((response) => {
    return response.data;
  });
};

export const SizeService = {
  getAllSize,
  getSizeById,
};
