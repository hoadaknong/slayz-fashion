/* eslint-disable no-unused-vars */
import {
  Login,
  Register,
  Home,
  ProductDetail,
  NotFound,
  Cart,
  Account,
  HistoryBill,
  AccountDetail,
  AddressForm,
  Checkout,
  CheckoutAddress,
  HistoryBillDetail,
  AddressEdit,
  OrderResult,
  ResultCheckout,
  Product,
  ForgotPassword,
  ResultSendMail,
  ResetPassword,
  ReviewForm,
} from "./pages";
import { HashRouter, Route, Routes} from "react-router-dom";
import MessengerCustomerChat from 'react-messenger-customer-chat';

import "./App.css";
import {AuthRoute, Footer, Header, SideBar} from "./components";
import Address from "./pages/Account/Address";
import TawkMessengerReact from "@tawk.to/tawk-messenger-react";
import {useRef} from "react";
import {Helmet} from "react-helmet";
import {useDataContext} from "./contexts/DataProvider";

function App() {
  const tawkMessengerRef = useRef();

  const handleMinimize = () => {
    tawkMessengerRef.current.minimize();
  };
  const {isOpenSideBarFilter} = useDataContext();

  return (
    <div style={isOpenSideBarFilter ? {height: "100vh", overflow: "hidden"} : {}}>
      <Helmet>
        <meta charSet="utf-8"/>
        <title>SlayZ</title>
        <link rel="canonical" href="http://mysite.com/example"/>
      </Helmet>
      <HashRouter >
        <button onClick={handleMinimize}> Minimize the Chat</button>
        <TawkMessengerReact
          propertyId="63b180b6c2f1ac1e202b1a5f"
          widgetId="1glmltig1"
          ref={tawkMessengerRef}
        />
        <Header/>
        <Routes>
          <Route path="/login" element={<Login/>}/>
          <Route path="/" element={<Home/>}/>
          <Route path="/products">
            <Route index element={<Product type={"all"}/>}/>
            <Route path=":id" element={<ProductDetail/>}/>
            <Route path="search" element={<Product type={"search"}/>}/>
            <Route path="male" element={<Product type={"male"}/>}/>
            <Route path="female" element={<Product type={"female"}/>}/>
            <Route path="unisex" element={<Product type={"unisex"}/>}/>
          </Route>
          <Route path="/register" element={<Register/>}/>
          <Route path="/forgot_password" element={<ForgotPassword/>}/>
          <Route path="/send_mail_success" element={<ResultSendMail/>}/>
          <Route path="/reset_password" element={<ResetPassword/>}/>
          <Route path="/user/cart" element={<Cart/>}/>
          <Route path="/account">
            <Route
              index
              element={
                <AuthRoute>
                  <div
                    className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto md:divide-x divide-x-0">
                    <Account/>
                    <AccountDetail/>
                  </div>
                </AuthRoute>
              }
            />
            <Route path="review">
              <Route
                index
                element={
                  <AuthRoute>
                    <div
                      className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto md:divide-x divide-x-0">
                      <Account/>
                      <ReviewForm/>
                    </div>
                  </AuthRoute>
                }
              />
            </Route>
            <Route path="bill">
              <Route
                index
                element={
                  <AuthRoute>
                    <div
                      className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto md:divide-x divide-x-0">
                      <Account/>
                      <HistoryBill/>
                    </div>
                  </AuthRoute>
                }
              />
              <Route
                path=":id"
                element={
                  <AuthRoute>
                    <div
                      className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto md:divide-x divide-x-0">
                      <Account/>
                      <HistoryBillDetail/>
                    </div>
                  </AuthRoute>
                }
              />
            </Route>
            <Route path="address">
              <Route
                index
                element={
                  <AuthRoute>
                    <div
                      className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto md:divide-x divide-x-0">
                      <Account/>
                      <Address/>
                    </div>
                  </AuthRoute>
                }
              />
              <Route
                path="add"
                element={
                  <AuthRoute>
                    <div
                      className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto md:divide-x divide-x-0">
                      <Account/>
                      <AddressForm/>
                    </div>
                  </AuthRoute>
                }
              />
              <Route
                path=":id"
                element={
                  <AuthRoute>
                    <div
                      className="flex max-w-[1340px] flex-wrap justify-center items-start mx-auto md:divide-x divide-x-0">
                      <Account/>
                      <AddressEdit/>
                    </div>
                  </AuthRoute>
                }
              />
            </Route>
          </Route>
          <Route
            path="/checkout/address"
            element={
              <AuthRoute>
                <CheckoutAddress/>
              </AuthRoute>
            }
          />
          <Route
            path="/checkout"
            element={
              <AuthRoute>
                <Checkout/>
              </AuthRoute>
            }
          />
          <Route
            path="/order_result"
            element={
              <AuthRoute>
                <OrderResult/>
              </AuthRoute>
            }
          />
          <Route
            path="/checkout_result"
            element={
              <AuthRoute>
                <ResultCheckout/>
              </AuthRoute>
            }
          />
          <Route path="/*" element={<NotFound/>}/>
          <Route path="/not_found" element={<NotFound/>}/>
        </Routes>
        <Footer/>
      </HashRouter >
    </div>
  );
}

export default App;
