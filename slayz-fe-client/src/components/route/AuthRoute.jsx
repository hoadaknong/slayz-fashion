import React, { useContext } from "react";
import { AuthContext } from "../../contexts/AuthContext";
import { Navigate } from "react-router-dom";

const AuthRoute = ({ children }) => {
  const { authState } = useContext(AuthContext);
  if (authState.isAuthenticated) {
    return children;
  }
  return <Navigate to="/not_found" />;
};

export default AuthRoute;
