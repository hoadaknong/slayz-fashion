/* eslint-disable react-hooks/exhaustive-deps */
import { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { AuthContext } from "../../contexts/AuthContext";
import "../../index.css";
import logo from "../../assets/logo_new_light_orange.png";
import logoMobile from "../../assets/logo_new_light_orange_bg.png";
import { FiSearch } from "@react-icons/all-files/fi/FiSearch";
import { HiMenu } from "@react-icons/all-files/hi/HiMenu";
import { HiUser } from "@react-icons/all-files/hi/HiUser";
import { HiOutlineLogout } from "@react-icons/all-files/hi/HiOutlineLogout";
import { FaRegAddressBook } from "@react-icons/all-files/fa/FaRegAddressBook";
import { HiShoppingCart } from "@react-icons/all-files/hi/HiShoppingCart";
import { FaFileInvoice } from "@react-icons/all-files/fa/FaFileInvoice";
import { useEffect, useRef } from "react";
import Swal from "sweetalert2";
import { Button, Menu, MenuItem } from "@mui/material";
import Search from "../search/Search";

const Header = () => {
  const navigate = useNavigate();
  const inputRef = useRef(null);
  const { authState, getCurrentUser, logoutUser } = useContext(AuthContext);
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const searchOnClick = (e) => {
    if (inputRef.current.value.trim() !== "") {
      navigate("/products/search?q=" + inputRef.current.value);
      inputRef.current.value = "";
    } else {
      Swal.fire("Hãy nhập từ khóa tìm kiếm!");
      inputRef.current.value = "";
      inputRef.current.focus();
    }
  };
  const logout = () => {
    Swal.fire({
      title: "Đăng xuất",
      text: "Bạn có muốn đăng xuất tài khoản không?",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#ff4949",
      cancelButtonColor: "#a0a0a0",
      cancelButtonText: "HỦY BỎ",
      confirmButtonText: "ĐĂNG XUẤT",
    }).then((result) => {
      if (result.isConfirmed) {
        logoutUser();
        navigate("/login");
      }
    });
  };
  useEffect(() => {
    getCurrentUser();
  }, []);
  return (
    <div className="flex flex-col justify-center items-center fixed z-[1000] w-full top-0 left-0 xl:backdrop-blur-xl shadow-md hover:shadow-lg bg-main-bg backdrop-blur-none xl:bg-white box-border">
      <div className="px-3 py-4 w-full xl:h-[70px] h-[60px] flex justify-start items-center transition-all">
        {/* Mobile */}
        <div className="flex xl:hidden justify-between w-full sticky h-[70px] items-center">
          <div className="justify-center items-center flex">
            <img
              className="cursor-pointer h-[40px] object-cover hover:scale-110 transition-all duration-300"
              src={logoMobile}
              alt="logo"
              onClick={() => {
                navigate("/");
              }}
            />
          </div>
          <div className="">
            <div>
              <Button
                id="basic-button"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
              >
                <HiMenu className="text-[30px] text-white" />
              </Button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                className="z-[1001]"
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <div className="flex flex-col gap-2">
                  <MenuItem>
                    <form
                      onSubmit={(e) => {
                        e.preventDefault();
                        searchOnClick();
                        handleClose();
                      }}
                    >
                      <div className="flex justify-center items-center border-b border-gray-600">
                        <FiSearch
                          onClick={searchOnClick}
                          className="text-black text-3xl hover:rounded-full hover:bg-gray-300 hover:text-white p-1 transition-all"
                        />
                        <input
                          className="bg-transparent p-1 text-black placeholder:text-gray-600 border-none focus:ring-0 py-3"
                          type="text"
                          placeholder="Tìm kiếm"
                          ref={inputRef}
                        />
                      </div>
                    </form>
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    <Link to="/products/male" className="uppercase font-bold font-nunito">
                      Sản phẩm dành cho Nam
                    </Link>
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    <Link to="/products/female" className="uppercase font-bold font-nunito">
                      Sản phẩm dành cho Nữ
                    </Link>
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    <Link to="/products/unisex" className="uppercase font-bold font-nunito">
                      Sản phẩm Unisex
                    </Link>
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    <Link
                      to="/products"
                      className="uppercase font-bold font-nunito text-orange-500"
                    >
                      Tất cả sẩn phẩm
                    </Link>
                  </MenuItem>
                </div>
                {authState.isAuthenticated && (
                  <div className="w-full flex gap-1 font-semibold text-lg flex-col justify-center items-start">
                    <MenuItem
                      onClick={() => {
                        handleClose();
                      }}
                    >
                      <div className="w-full flex justify-start items-center gap-1">
                        <HiShoppingCart />
                        <Link
                          className="mt-[2.5px] uppercase font-bold font-nunito"
                          to="/user/cart"
                        >
                          Giỏ hàng
                        </Link>
                      </div>
                    </MenuItem>
                    <MenuItem
                      onClick={() => {
                        navigate("/account");
                        handleClose();
                      }}
                    >
                      <div className="w-full flex justify-center items-center gap-1">
                        <HiUser />
                        <p className="mt-[2.5px] uppercase font-bold font-nunito">
                          Thông tin cá nhân
                        </p>
                      </div>
                    </MenuItem>
                    <MenuItem
                      onClick={() => {
                        navigate("/account/bill");
                        handleClose();
                      }}
                    >
                      <div className="w-full flex justify-center items-center gap-1">
                        <FaFileInvoice />
                        <p className="mt-[2.5px] uppercase font-bold font-nunito">Đơn hàng</p>
                      </div>
                    </MenuItem>
                    <MenuItem
                      onClick={() => {
                        navigate("/account/address");
                        handleClose();
                      }}
                    >
                      <div className="w-full flex justify-center items-center gap-1">
                        <FaRegAddressBook />
                        <p className="mt-[2.5px] uppercase font-bold font-nunito">Địa chỉ</p>
                      </div>
                    </MenuItem>
                    <MenuItem
                      onClick={() => {
                        logout();
                        handleClose();
                      }}
                    >
                      <div className="w-full flex justify-center items-center gap-1">
                        <HiOutlineLogout />
                        <p className="mt-[2.5px] uppercase font-bold font-nunito">Đăng xuất</p>
                      </div>
                    </MenuItem>
                  </div>
                )}
                {!authState.isAuthenticated && (
                  <MenuItem
                    onClick={() => {
                      handleClose();
                    }}
                  >
                    <div className="w-full flex justify-start items-center gap-1">
                      <Link className="mt-[2.5px] uppercase font-bold font-nunito" to="/login">
                        Đăng nhập
                      </Link>
                    </div>
                  </MenuItem>
                )}
              </Menu>
            </div>
          </div>
        </div>
        {/* Desktop */}
        <div className="hidden xl:flex justify-between w-full sticky h-[70px]">
          <ul className="flex justify-center items-center gap-6 ml-4 font-extrabold text-md">
            <li className="">
              <Link to="/">
                <img
                  className="cursor-pointer h-[40px] object-cover hover:scale-110 transition-all duration-300"
                  src={logo}
                  alt="logo"
                />
              </Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 mt-1">
              <Link to="/products/male">NAM</Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 mt-1">
              <Link to="/products/female">NỮ</Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 mt-1">
              <Link to="/products/unisex">UNISEX</Link>
            </li>
            <li className="transition-all duration-200 hover:scale-105 text-primary-color mt-1">
              <Link to="/products">TẤT CẢ SẢN PHẨM</Link>
            </li>
          </ul>
          <div className="flex justify-center items-center gap-6 text-md font-bold">
            <Search onSubmit={searchOnClick}/>
            {authState.isAuthenticated ? (
              <div className="flex gap-6 mt-2">
                <button
                  className="transition-all duration-200 hover:scale-105 flex justify-center items-center gap-1"
                  onClick={() => {
                    navigate("/account");
                  }}
                >
                  <HiUser />
                  <p className="mt-[1px]">TÀI KHOẢN</p>
                </button>
                <Link
                  className="transition-all duration-200 hover:scale-105 flex justify-center items-center gap-1"
                  to="/user/cart"
                >
                  <HiShoppingCart />
                  <p className="mt-[1px]">GIỎ HÀNG</p>
                </Link>
              </div>
            ) : (
              <Link className="transition-all duration-200 hover:scale-105" to="/login">
                ĐĂNG NHẬP
              </Link>
            )}
          </div>
        </div>
      </div>
      <div className="bg-orange-400 w-full h-3 xl:block hidden"></div>
    </div>
  );
};
export default Header;
