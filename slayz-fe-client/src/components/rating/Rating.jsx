/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { AiFillStar, AiOutlineStar } from "react-icons/ai";
const Rating = (props) => {
  const [ratingValue, setRatingValue] = useState([1, 2, 3, 4, 5]);
  return (
    <div className="flex gap-2">
      {ratingValue.map((value) => {
        if (value <= props.value) {
          return <AiFillStar className="text-[25px] text-yellow-300" key={value} />;
        }
        return <AiOutlineStar className="text-[25px] text-gray-300" key={value} />;
      })}
    </div>
  );
};

export default Rating;
