import React from "react";
import { useNavigate } from "react-router-dom";
import { GlobalUtil } from "../../utils/GlobalUtil";

const SearchChild = ({ title, productImage, listPrice, barcode, closeSearchList, setIsActive }) => {
  const navigate = useNavigate();
  const onClickHandle = () => {
    closeSearchList({ data: [] });
    navigate(`/products/${barcode}`);
    setIsActive(false);
  };
  return (
    <div
      className="flex justify-start items-center cursor-pointer hover:bg-gray-100 transition-all p-2 rounded-md gap-2"
      onClick={onClickHandle}
    >
      <div className="w-10">
        <img src={productImage} alt="hinh thu nho" />
      </div>
      <div className="flex flex-col">
        <div className="text-lg font-semibold">{title}</div>
        <div className="text-orange-500">
          {GlobalUtil.numberWithCommas(listPrice)} <span className="underline">đ</span>
        </div>
      </div>
    </div>
  );
};

export default SearchChild;
