import {FiSearch} from "@react-icons/all-files/fi/FiSearch";
import {AiOutlineClose} from "@react-icons/all-files/ai/AiOutlineClose";
import React, {useEffect, useRef, useState} from "react";
import SearchList from "./SearchList";
import useProductFilter from "../../hooks/useProductFilter";
import Swal from "sweetalert2";
import {useNavigate} from "react-router-dom";

const Search = ({onSubmit}) => {
  const [value, setValue] = useState("");
  const [isActive, setIsActive] = useState(false);
  const typingTimeOut = useRef(null);
  const {filterData, setFilterData, setGender, setName} = useProductFilter();
  const navigate = useNavigate()
  const onChangeHandle = (e) => {
    const input = e.target.value;
    setValue(input);
    if (!onSubmit) return;
    if (typingTimeOut.current) {
      clearTimeout(typingTimeOut.current);
    }
    if (input.trim() !== "") {
      setIsActive(true);
    }
    typingTimeOut.current = setTimeout(() => {
      if (input.trim() === "") {
        setFilterData({data: []});
      } else {
        setGender(["FEMALE", "UNISEX", "MALE"]);
        setName(input);
      }
    }, 400);
  };
  useEffect(() => {
    if (value.trim() === "") {
      setIsActive(false)
    }
  }, [value])
  const searchOnClick = (e) => {
    if (value.trim() !== "") {
      navigate("/products/search?q=" + value);
      setValue("")
    } else {
      Swal.fire("Hãy nhập từ khóa tìm kiếm!");
      setValue("")
    }
  };
  const onClickRemoveAllText = () => {
    setValue("");
    setIsActive(false);
  };
  return (<div>
      <div className="flex justify-start items-center border-b border-gray-600 w-[400px] z-[10000]">
        <FiSearch
          onClick={() => {
            searchOnClick();
          }}
          className="text-black text-3xl hover:rounded-full hover:bg-gray-300 hover:text-white p-1 transition-all"
        />
        <input
          className="bg-transparent p-1 text-black placeholder:text-gray-600 border-none focus:ring-0 w-full"
          type="text"
          placeholder="Tìm kiếm"
          value={value}
          onChange={onChangeHandle}
          maxLength={70}
        />
        {value && (<button
            type="button"
            onClick={onClickRemoveAllText}
            className="p-1 hover:bg-gray-300 hover:text-white transition-all rounded-full"
          >
            <AiOutlineClose/>
          </button>)}

        <SearchList
          result={filterData?.details ? filterData?.details : []}
          setFilter={setFilterData}
          keyWord={value}
          isActive={isActive}
          setIsActive={setIsActive}
          setKeyWord={setValue}
        />
      </div>
      {isActive && (<div
          className="bg-black w-[250%] h-screen absolute opacity-0 top-0 -left-[700px] z-[-1009]"
          onClick={() => {
            setIsActive(false);
          }}
        ></div>)}
    </div>);
};

export default Search;
