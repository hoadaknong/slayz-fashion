import React from "react";
import SearchChild from "./SearchChild";
import {Link} from "react-router-dom";

const SearchList = ({result, setFilter, keyWord, isActive, setIsActive, setKeyWord}) => {
  return (
    <div
      className={
        !isActive
          ? "hidden"
          : "absolute drop-shadow-md nav-item right-64 bg-white dark:bg-[#42464D] rounded-lg top-16 w-[50%] z-[1000]"
      }
    >
      <div className="p-5 hover:bg-gray-100">
        <Link to={`/products/search?q=${keyWord}`} onClick={() => {
          setKeyWord("")
        }}
        >
          <p>Tìm kiếm với từ khóa: "{keyWord}"</p>
        </Link>
      </div>
      <div
        className={
          result.length === 0
            ? "hidden"
            : "w-full flex flex-col gap-1 max-h-[400px] overflow-scroll px-5 pb-5"
        }
      >
        {result.map((child, index) => {
          return (
            <SearchChild
              {...child}
              closeSearchList={setFilter}
              key={index}
              setIsActive={setIsActive}
            />
          );
        })}
      </div>
    </div>
  );
};

export default SearchList;
