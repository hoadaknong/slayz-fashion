import { BrowserRouter as Router, Link } from "react-router-dom";
import "../../../index.css";
import hinh1 from "../../../assets/3Cate1.png";
import hinh2 from "../../../assets/3Cate2.png";
import hinh3 from "../../../assets/3Cate3.png";
const ThreeCategory = () => {
	return (
		<Router>
			<div className="flex">
				<div>
					<Link className="relative" to="/about">
						<h6 class="absolute  text-orange-600 top-2 right-14 font-bold">Nam</h6>
						<h6 class="absolute text-xs top-7 right-4">30+ sản phẩm</h6>
						<img className="h-96 w-64" src={hinh1} alt="hinh cate 1" />
					</Link>
				</div>
				<div>
					<Link className="relative" to="/about">
						<h6 class="absolute  text-orange-600 top-2 right-16 font-bold">Nữ</h6>
						<h6 class="absolute text-xs top-7 right-3">40+ sản phẩm</h6>
						<img className="h-96 w-64" src={hinh2} alt="hinh cate 2" />
					</Link>
				</div>
				<div>
					<Link className="relative" to="/about">
						<h6 class="absolute  text-orange-600 top-2 right-7 font-bold">Phụ Kiện</h6>
						<h6 class="absolute text-xs top-7 right-5">50+ sản phẩm</h6>
						<img className="h-96 w-64" src={hinh3} alt="hinh cate 3" />
					</Link>
				</div>
			</div>
		</Router>
	);
};
export default ThreeCategory;
