import picture from "../../../assets/oneProduct.png";
import infoship from "../../../assets/InfoShip.png";
import { useState } from "react";
import { Link } from "react-router-dom";

const InfomationPanel = () => {
	const [num, setNum] = useState(0);
	const decrement = () => {
		if (num > 0) {
			setNum(num - 1);
		} else {
			setNum(0);
		}
	};

	const increment = () => {
		setNum(num + 1);
	};

	return (
		<div>
			<div className="flex">
				<div>
					<img className=" w-48" src={picture} alt="hinh 1 san pham" />
				</div>
				<div>
					<form action="">
						<div className="uppercase text-2xl">Đầm nữ ngắn ngang đùi hồng regular</div>
						<div>Đã bán: 60 sản phẩm</div>
						<div className="text-orange-600">250.000 đ</div>
						<div className="flex">
							<div>MÃ HÀNG HÓA</div>
							<div>10F21DJAW001</div>
						</div>
						<div>
							<div className="font-bold">CHỌN MÀU</div>
						</div>
						<div>
							<div className="font-bold">CHỌN SIZE</div>
						</div>
						<div>
							<a href="/">Hướng dẫn chọn size</a>
						</div>
						<div>
							<div>SỐ LƯỢNG</div>
							<div className="custom-number-input h-10 w-32">
								<div className="flex flex-row h-10 w-full rounded-lg relative bg-transparent mt-1">
									<button
										name="btn-decrement"
										onClick={decrement}
										className=" bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-l cursor-pointer outline-none"
									>
										<span className="m-auto text-2xl font-thin">−</span>
									</button>
									<input
										type="number"
										className=" focus:outline-none text-center w-full bg-gray-300 font-semibold text-md hover:text-black focus:text-black   flex items-center text-gray-700 "
										name="custom-input-number"
										value={num}
									/>
									<button
										name="btn-increment"
										onClick={increment}
										className="bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-r cursor-pointer"
									>
										<span className="m-auto text-2xl font-thin">+</span>
									</button>
								</div>
							</div>
						</div>
						<div>
							<input
								className="bg-blue-700 text-white p-3 rounded"
								type="submit"
								value={"Thêm vào giỏ hàng"}
							/>
						</div>
					</form>
					<div>
						<img className="h-32" src={infoship} alt="thong tin ship" />
					</div>
					<div>
						<Link className="text-blue-500 underline" href="">
							Chính sách đổi trả
						</Link>
					</div>
					<div className="bg-orange-300 text-transparent rounded h-1">.</div>
				</div>
			</div>
			<div>
				<div>BÌNH LUẬN({num})</div>
				<div>{null ? "Có comment" : "Chưa có nhận xét về sản phẩm này"}</div>
			</div>
		</div>
	);
};

export default InfomationPanel;
