import { Tooltip } from "@material-tailwind/react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../../../index.css";
import { VariantService } from "../../../services/variant.service";
import { GlobalUtil } from "../../../utils/GlobalUtil";
import { useRef } from "react";

const ProductGallery = (props) => {
  const navigate = useNavigate();
  const [colorList, setColorList] = useState([]);
  const imageRef = useRef(null);
  const productClickHandle = () => {
    window.scrollTo(0, 0);
    navigate("/products/" + props.barcode);
  };

  useEffect(() => {
    let isMount = true;
    if (isMount) {
      VariantService.getAllColorByProductId(props?.productId).then((response) => {
        if (response.status === "OK") {
          setColorList(response.data);
        }
      });
    }
    return () => {
      isMount = false;
    };
  }, [props?.productId]);
  return (
    <div
      className={`flex flex-col justify-start items-stretch transition-all duration-300 overflow-hidden`}
    >
      <div className="h-fit w-fit overflow-hidden relative" onClick={productClickHandle}>
        <img
          className="object-contain transition-all duration-500 hover:hover:scale-[1.1] hover:cursor-pointer md:rounded-none rounded-sm"
          src={props.productImage}
          ref={imageRef}
          alt="hinh 1 san pham"
        />
        {props.standCost < props.listPrice && (
          <div className="z-[300] absolute bg-red-600 px-3 py-1 font-extrabold flex justify-center items-center text-white bottom-[10%] right-0">
            <p className="text-[12px] sm:text-[15px] xl:text-xl">
              -{Math.round(100 - GlobalUtil.calculatePercent(props.standCost, props.listPrice))}%
            </p>
          </div>
        )}
      </div>
      <div className="flex flex-col">
        <div className="md:mt-3 mt-2">
          <Tooltip
            content={props.title}
            placement="top"
            animate={{
              mount: { scale: 1, y: 0 },
              unmount: { scale: 0, y: 25 },
            }}
          >
            <div
              className="text-left cursor-pointer hover:underline text-black text-normal md:text-[15px] text-[12px] truncate "
              onClick={productClickHandle}
            >
              {props.title}
            </div>
          </Tooltip>
          <div className="text-orange-600 text-[12px] sm:text-[15px] xl:text-xl font-semibold flex items-center justify-between my-1 flex-row">
            <p className="font-extrabold">{GlobalUtil.numberWithCommas(props.standCost)}₫</p>
            {props.standCost < props.listPrice && (
              <p className="line-through decoration-rose-600 text-gray-400 decoration-1">
                {GlobalUtil.numberWithCommas(props.listPrice)}₫
              </p>
            )}
          </div>
        </div>
        <div className="flex gap-2 my-3 flex-wrap">
          <Tooltip content="GỐC">
            <img
              className="hover:border-orange-600 border transition-all duration-200 cursor-pointer object-cover rounded-md md:w-[45px] w-[35px] md:h-[45px] h-[35px]"
              src={props?.productImage}
              alt="Kieu dang cua san pham"
              onClick={() => {
                imageRef.current.src = props.productImage;
              }}
              onMouseOver={() => {
                imageRef.current.src = props.productImage;
              }}
            />
          </Tooltip>
          {colorList?.map((item) => {
            return (
              <Tooltip content={item.colorName} key={item.id}>
                <img
                  className="hover:border-orange-600 border transition-all duration-200 md:w-[45px] w-[35px] md:h-[45px] h-[35px] cursor-pointer object-cover rounded-md"
                  src={item?.image}
                  alt="Kieu dang cua san pham"
                  onClick={() => {
                    imageRef.current.src = item?.image;
                  }}
                  onMouseOver={() => {
                    imageRef.current.src = item?.image;
                  }}
                />
              </Tooltip>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default ProductGallery;
