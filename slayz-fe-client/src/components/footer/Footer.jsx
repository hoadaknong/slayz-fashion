/* eslint-disable jsx-a11y/iframe-has-title */
import React from "react";
import { SocialIcon } from "react-social-icons";
import { FiPhoneCall } from "@react-icons/all-files/fi/FiPhoneCall";
import "../../index.css";
import LogoMini from "../../assets/logo_new_light_orange.png";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <div className="bg-footer-bg flex justify-center items-center flex-col pb-0 md:pb-10 w-full">
      <div className="bg-orange-500 w-full h-[80px] flex items-center">
        <div className="md:w-4/5 w-[94%] mx-auto flex justify-between items-center">
          <div className="text-gray-200 font-light">
            <p>Hotline đặt hàng miễn phí</p>
          </div>
          <div className="text-white flex items-center gap-1">
            <FiPhoneCall className="text-[20px]" /> <p>0388 891 635</p>
          </div>
        </div>
      </div>
      <div className="bg-footer-bg flex flex-wrap justify-between items-center text-white border-b-2 border-primary-color w-[80%] my-6 py-4 md:py-[60px]">
        <div className="flex flex-col w-fit my-5">
          <div className="flex flex-wrap justify-center items-center md:justify-start md:items-start">
            <div>
              <img
                className="h-14 sm:w-[180px] max-[150px]:w-[100px] w-fit mr-4 object-cover md:mb-0 mb-2"
                src={LogoMini}
                alt="logo mini"
              />
            </div>
            <div>
              <h6 className="sm:text-3xl text-sm max-[150px]:text-[5px] text-primary-color font-extrabold">
                WWW.SLAYZ.COM
              </h6>
              <h6 className="text-xs">Thời trang dành cho GenZ</h6>
            </div>
          </div>
          <div className="text-xs mt-5">
            <div>
              CÔNG TY TNHH SLAYZ VIETNAM | Mã Số Thuế: ABCXYZA | Văn Phòng: Tầng 5 Tòa Nhà Trung Tâm
            </div>
            <div>Số 1 Võ Văn Ngân, Phường Linh Chiểu, TP. Thủ Đức, TP. HCM</div>
          </div>
          <div className="">
            <SocialIcon
              url="https://twitter.com/"
              style={{ height: 25, width: 25, margin: "10px 10px 10px 0" }}
            />
            <SocialIcon url="https://facebook.com/" style={{ height: 25, width: 25, margin: 10 }} />
            <SocialIcon
              url="https://instagram.com/"
              style={{ height: 25, width: 25, margin: 10 }}
            />
            <SocialIcon url="https://youtube.com/" style={{ height: 25, width: 25, margin: 10 }} />
          </div>
        </div>
        <div className="flex flex-col w-full md:justify-start md:items-start justify-center items-center md:w-fit md:mt-0 mt-2">
          <div className="font-bold mb-4 text-second-color"> SẢN PHẨM</div>
          <div className="flex flex-col md:justify-start md:items-start justify-center items-center">
            <div>
              <Link className="hover:underline" to="/products/male">
                Dành Cho Nam
              </Link>
            </div>
            <div>
              <Link className="hover:underline" to="/products/female">
                Dành Cho Nữ
              </Link>
            </div>
            <div>
              <Link className="hover:underline" to="/products/unisex">
                Unisex
              </Link>
            </div>
            <div>
              <Link className="hover:underline" to="/products">
                Tất cả sản phẩm
              </Link>
            </div>
          </div>
        </div>
        <div className="flex flex-col w-full md:justify-start md:items-start justify-center items-center md:w-fit md:mt-0 mt-2 md:text-left text-center">
          <div className="font-bold mb-4 text-second-color">LIÊN HỆ</div>
          <div className="flex flex-col md:justify-start md:items-start justify-center items-center">
            <div>E-mail: slayz@gmail.com</div>
            <div>Hotline: 0972358764</div>
            <div>Địa chỉ: 484 Lê Văn Việt, Tăng Nhơn Phú A, TP.Thủ Đức, TP.HCM</div>
            <div>Facebook: SlayZ Fashion</div>
          </div>
        </div>
        <div className="flex flex-col w-full md:justify-start md:items-start justify-center items-center md:w-fit md:mt-0 mt-2">
          <div className="font-bold mb-4 text-second-color">CHÍNH SÁCH</div>
          <div className="flex flex-col md:justify-start md:items-start justify-center items-center">
            <div>Chính sách đổi trả</div>
            <div>Chính sách bảo hành</div>
            <div>Chính sách bảo mật</div>
            <div>Các câu hỏi thường gặp</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
