import React from "react";
import { FaQuoteLeft } from "@react-icons/all-files/fa/FaQuoteLeft";
const Quote = ({ author, content, image }) => {
  return (
    <div className="bg-[#E4EAEE] flex flex-col justify-center items-center">
      <div className="flex p-6 text-[30px] gap-3 text-[#505050]">
        <FaQuoteLeft />
      </div>
      <div className="flex flex-col md:flex-row justify-center items-center pt-2 pb-8 px-8 md:w-[500px] w-full gap-2">
        <div className="w-1/3">
          <img
            src={image}
            alt="avatar"
            className="object-cover md:h-[150px] md:w-[150px] w-[50px] h-[50px] rounded-full mx-auto"
          />
        </div>
        <div className="md:w-2/3 w-full">
          <p className="md:text-xl text-[15px] text-justify md:leading-8 leading-7 md:px-5 px-0 mb-5">
            <em>{content}</em>
          </p>
          <h1 className="text-primary-color font-bold md:text-xl text-[15px] px-5 italic md:text-left text-center">
            - {author} -
          </h1>
        </div>
      </div>
    </div>
  );
};

export default Quote;
