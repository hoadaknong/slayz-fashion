import React from "react";
import { Helmet } from "react-helmet";

const Title = ({ title }) => {
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <title>{title}</title>
      <link rel="canonical" href="https://www.facebook.com/slayz.vn" />
    </Helmet>
  );
};

export default Title;
