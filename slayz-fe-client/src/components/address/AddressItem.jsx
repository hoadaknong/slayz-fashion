import React from "react";
import {FiEdit} from "@react-icons/all-files/fi/FiEdit";
import {RiDeleteBinLine} from "@react-icons/all-files/ri/RiDeleteBinLine";
import {Tooltip} from "@material-tailwind/react";
import {useNavigate} from "react-router-dom";
import useAddressUser from "../../hooks/useAddressUser";
import Swal from "sweetalert2";

const AddressItem = (props) => {
  const navigate = useNavigate();
  const {removeAddressById} = useAddressUser(null);
  const removeAddress = () => {
    Swal.fire({
      title: "Bạn có muốn xóa địa chỉ này không?",
      text: `${props.addressLine}, ${props.ward}, ${props.district}, ${props.province}`,
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      cancelButtonText: "HỦY BỎ",
      confirmButtonText: "XÓA",
    }).then((result) => {
      if (result.isConfirmed) {
        (async function () {
          await removeAddressById(props.id);
        })()
      }
    });
  };
  return (
    <div
      className="h-fit w-full p-4 border border-gray-500 flex hover:drop-shadow-md bg-white transition-all duration-100 flex-wrap">
      <div className="w-full md:w-4/5">
        <div>
          <p className="font-bold text-[20px] mb-1 ">{props.fullName}</p>
          <p className="italic">
            <span className="font-semibold">SĐT:</span> {props.phone}
          </p>
          <p className="italic">
            <span className="font-semibold">Chi tiết:</span> {props.addressLine}
          </p>
          <p className="italic">
            <span className="font-semibold">Phường/Xã:</span> {props.ward}
          </p>
          <p className="italic">
            <span className="font-semibold">Quận/Huyện:</span> {props.district}
          </p>
          <p className="italic">
            <span className="font-semibold">Tỉnh:</span> {props.province}
          </p>
        </div>
      </div>
      <div className="md:w-1/5 w-full flex justify-center items-center gap-[20px] mt-2 md:mt-0">
        <Tooltip content="Chỉnh sửa">
          <div
            className="flex justify-center items-center w-fit h-fit p-3 transition-all duration-200 hover:text-white hover:bg-slate-400 rounded-full cursor-pointer"
            onClick={() => {
              navigate("/account/address/" + props.id);
            }}
          >
            <FiEdit className="text-[25px]"/>
          </div>
        </Tooltip>
        <Tooltip content="Xóa địa chỉ">
          <div
            className="flex justify-center items-center w-fit h-fit p-3 transition-all duration-200 hover:text-white hover:bg-slate-400 rounded-full cursor-pointer"
            onClick={removeAddress}
          >
            <RiDeleteBinLine className="text-[25px]"/>
          </div>
        </Tooltip>
      </div>
    </div>
  );
};

export default AddressItem;
