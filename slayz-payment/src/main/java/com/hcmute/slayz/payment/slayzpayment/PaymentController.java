package com.hcmute.slayz.payment.slayzpayment;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/payment")
@RequiredArgsConstructor
@Slf4j
public class PaymentController {
  private final InvoiceRepository repository;

  @PostMapping
  public ResponseEntity<?> solve(@RequestBody PaymentPayload payload){
    Invoice invoice = repository
            .findById(Long.parseLong(payload.getExtraData()))
            .orElseThrow(()-> new ResourceNotFoundException("Không tìm thấy hóa đơn!"));
    if(payload.getResultCode().equals(0)){
      invoice.setPaymentStatus("PAID");
    }
    if(payload.getPayType().equals("qr")){
      invoice.setPaymentMethod("MOMO");
    }
    if(payload.getPayType().equals("atm")){
      invoice.setPaymentMethod("ATM");
    }
    if(payload.getPayType().equals("credit")){
      invoice.setPaymentMethod("VISA/MASTER/JCB");
    }
    repository.save(invoice);
    log.info("Payment information: " + payload.getPayType() + ", " + ", " + payload.getExtraData()+ ", " + payload.getResultCode());
    return ResponseEntity.ok("Thanh toán thành công!");
  }

}
