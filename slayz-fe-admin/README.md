# WELCOME TO SLAYZ CMS
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Project Overview

- Name: **SlayZ Fashion CMS UI**
- Author: [Hòa Phạm](https://www.facebook.com/hoaffffff/)
- Collaborator: [Tran Anh Tien](https://www.facebook.com/teddy.tran.756)
- Main tech: **ReactJS**, **TailwindCSS**,

## First script

In the project directory, you can run:
### `yarn install`

 To install all dependencies in package.json file.\

And then you can run:
### `yarn start`

To start the app in the development mode.\
Open [http://localhost:3006](http://localhost:3006) to view it in your browser.

