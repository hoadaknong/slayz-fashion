// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
	apiKey: "AIzaSyCK1_VSn8hc2Ki_taZ5c-aNaY63OqUsJHU",
	authDomain: "slayz-image.firebaseapp.com",
	projectId: "slayz-image",
	storageBucket: "slayz-image.appspot.com",
	messagingSenderId: "902630438654",
	appId: "1:902630438654:web:f37abce1b86afaf52497af",
	measurementId: "G-WJ8XECYCQM",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
