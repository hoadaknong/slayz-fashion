/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { FeedbackService } from "../services/feedback.service";

const useReviewFilter = () => {
  const limit = 5;
  const [ratingList, setRatingList] = useState([1, 2, 3, 4, 5]);
  const [page, setPage] = useState(1);
  const [sortOption, setSortOption] = useState("time_desc");
  const [dataReview, setDataReview] = useState({});
  const [productId, setProductId] = useState(-1);
  const [pageList, setPageList] = useState([1]);
  const fetchData = () => {
    if (productId != null) {
      const data = {
        ratingList,
        page,
        limit,
        sortOption,
        productId,
      };
      FeedbackService.getAllReviewByProductIdFilter(data).then((response) => {
        setDataReview(() => {
          return response.data;
        });
        let pages = [];
        for (let i = 1; i <= response.data?.totalPage; i++) {
          pages.push(i);
        }
        setPageList(pages);
      });
    }
  };
  useEffect(() => {
    fetchData();
  }, [ratingList, page, sortOption, productId]);
  return {
    ratingList,
    setRatingList,
    page,
    setPage,
    sortOption,
    setSortOption,
    dataReview,
    limit,
    setProductId,
    pageList,
  };
};

export default useReviewFilter;
