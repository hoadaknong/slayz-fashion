/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useRef, useState } from "react";
import { InvoiceService } from "../services/invoice.service";
import swal from "sweetalert";

const useInvoiceData = (id, status) => {
  const [loading, setLoading] = useState(true);
  const [invoiceList, setInvoiceList] = useState([]);
  const [invoiceDetailList, setInvoiceDetailList] = useState([]);
  const [invoice, setInvoice] = useState(null);
  const [page, setPage] = useState(1);
  const pageSize = useRef(8);
  const [totalPage, setTotalPage] = useState(100);
  const [invoicesPending, setInvoicesPending] = useState([]);
  const [invoicesCompleted, setInvoicesCompleted] = useState([]);
  const [invoicesCancelled, setInvoicesCancelled] = useState([]);
  const [invoicesInProgress, setInvoicesInProgress] = useState([]);
  const cancelledOrder = () => {
    swal({
      title: "Hủy đơn hàng",
      text: "Khi hủy đơn hàng sẽ không thể khôi phục lại đơn hàng!",
      icon: "info",
      buttons: true,
      dangerMode: true,
    }).then((result) => {
      if (result) {
        InvoiceService.updateInvoiceStatus(id, "CANCELLED").then((response) => {
          if (response.status === "OK") {
            InvoiceService.getInvoiceById(id).then((response) => {
              if (response.status === "OK") {
                setInvoice(response.data);
              }
            });
            swal("Đã cập nhật đơn hàng " + id + "!", {
              icon: "success",
            });
          } else {
            swal("Đã có lỗi xảy ra!", {
              icon: "warning",
            });
          }
        });
      }
    });
  };
  const confirmOrder = () => {
    swal({
      title: "Xác nhận đơn hàng",
      text: "Hãy kiểm tra lại đơn hàng trước khi xác nhận nhé! Bạn có muốn xác nhận đơn hàng không?",
      icon: "info",
      buttons: true,
      dangerMode: true,
    }).then((result) => {
      if (result) {
        InvoiceService.updateInvoiceStatus(id, "IN PROGRESS").then(
          (response) => {
            if (response.status === "OK") {
              InvoiceService.getInvoiceById(id).then((response) => {
                if (response.status === "OK") {
                  setInvoice(response.data);
                }
              });
              swal("Đã cập nhật đơn hàng " + id + "!", {
                icon: "success",
              });
            } else {
              swal("Đã có lỗi xảy ra!", {
                icon: "warning",
              });
            }
          }
        );
      }
    });
  };
  const completedOrder = () => {
    swal({
      title: "Hoàn tất đơn hàng",
      text: "Hãy đảm bảo khách hàng đã nhận được hàng nhé! Bạn có muốn hoàn tất đơn hàng này không?",
      icon: "info",
      buttons: true,
      dangerMode: true,
    }).then((result) => {
      if (result) {
        InvoiceService.updateInvoiceStatus(id, "COMPLETED").then((response) => {
          if (response.status === "OK") {
            InvoiceService.getInvoiceById(id).then((response) => {
              if (response.status === "OK") {
                setInvoice(response.data);
              }
            });
            swal("Đã cập nhật đơn hàng " + id + "!", {
              icon: "success",
            });
          } else {
            swal("Đã có lỗi xảy ra!", {
              icon: "warning",
            });
          }
        });
      }
    });
  };

  useEffect(() => {
    setLoading(true);
    if (id === null && status === null) {
      (async function () {
        const invoiceResponse = await InvoiceService.getAllInvoice(
          page,
          pageSize.current
        );
        if (invoiceResponse.status === "OK") {
          setInvoiceList(invoiceResponse.data.details);
          setTotalPage(invoiceResponse.data.totalPage);
        }
      })();
    }
    if (id !== null) {
      InvoiceService.getInvoiceById(id).then((response) => {
        if (response.status === "OK") {
          setInvoice(response.data);
        }
      });
      InvoiceService.getAllInvoiceDetailByInvoiceId(id).then((response) => {
        if (response.status === "OK") {
          setInvoiceDetailList(response.data);
        }
      });
    }
    if (status !== null) {
      (async function () {
        const invoiceResponse = await InvoiceService.getAllInvoiceByStatus(
          status,
          page,
          pageSize.current
        );
        if (invoiceResponse.status === "OK") {
          setInvoiceList(invoiceResponse.data.details);
          setTotalPage(invoiceResponse.data.totalPage);
        }
      })();
    }
    setLoading(false);
  }, [status, id]);
  useEffect(() => {
    setLoading(true);
    if (status !== null) {
      (async function () {
        const invoiceResponse = await InvoiceService.getAllInvoiceByStatus(
          status,
          page,
          pageSize.current
        );
        if (invoiceResponse.status === "OK") {
          setInvoiceList(invoiceResponse.data.details);
          setTotalPage(invoiceResponse.data.totalPage);
        }
      })();
    } else {
      (async function () {
        const invoiceResponse = await InvoiceService.getAllInvoice(
          page,
          pageSize.current
        );
        if (invoiceResponse.status === "OK") {
          setInvoiceList(invoiceResponse.data.details);
          setTotalPage(invoiceResponse.data.totalPage);
        }
      })();
    }
    setLoading(false);
  }, [page]);

  return {
    invoiceList,
    setInvoiceList,
    invoiceDetailList,
    setInvoiceDetailList,
    invoice,
    setInvoice,
    confirmOrder,
    completedOrder,
    cancelledOrder,
    setPage,
    page,
    totalPage,
    invoicesPending,
    invoicesCompleted,
    invoicesCancelled,
    invoicesInProgress,
    setInvoicesPending,
    setInvoicesCompleted,
    setInvoicesCancelled,
    setInvoicesInProgress,
    loading,
    setLoading,
  };
};

export default useInvoiceData;
