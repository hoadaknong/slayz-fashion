/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect } from "react";
import { useState } from "react";
import { UserService } from "../services/user.service";

const useUserData = (id) => {
  const [user, setUser] = useState();
  useEffect(() => {
    if (id !== null) {
      UserService.getUserById(id).then((response) => {
        if (response.status === "OK") {
          setUser(response.data);
        }
      });
    }
  }, []);
  return { user, setUser };
};

export default useUserData;
