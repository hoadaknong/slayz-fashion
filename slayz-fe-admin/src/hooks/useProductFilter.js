/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState, useRef } from "react";
import { CategoryService } from "../services/category.service";
import { ColorService } from "../services/color.service";
import { ProductService } from "../services/product.service";
import { SizeService } from "../services/size.service";

const useProductFilter = () => {
  const isInit = useRef(true);
  const [loading, setLoading] = useState(false);
  const [limit, setLimit] = useState(5);
  const [data, setData] = useState();
  const [categories, setCategories] = useState([]);
  const [colors, setColors] = useState([]);
  const [sizes, setSizes] = useState([]);
  const [minPrice, setMinPrice] = useState(10);
  const [maxPrice, setMaxPrice] = useState(2000000);
  const [page, setPage] = useState(1);
  const [name, setName] = useState("");
  const [gender, setGender] = useState(["MALE", "FEMALE", "UNISEX"]);
  const [message, setMessage] = useState("");
  const [sortOption, setSortOption] = useState("newest");

  useEffect(() => {
    (async function () {
      setLoading(true);
      const categoryResponse = await CategoryService.getAllCategory(1, 100);
      setCategories(categoryResponse.details);
      const colorResponse = await ColorService.getAllColor();
      setColors(colorResponse.details);
      const sizeResponse = await SizeService.getAllSize();
      setSizes(sizeResponse);
      const data = {
        categories: categoryResponse.details.map((category) => category.id),
        colors: colorResponse.details.map((color) => color.id),
        limit: limit,
        maxPrice: maxPrice,
        minPrice: minPrice,
        name: name,
        offset: page,
        sizes: sizeResponse.map((size) => size.id),
        gender: gender,
        sortOption: sortOption,
      };
      const response = await ProductService.filterData(data);
      if (response.status === "OK") {
        setData(response.data);
      }
      setLoading(false);
    })();
  }, []);
  const pageOnClick = async (pageIndex) => {
    setLoading(true);
    setPage(pageIndex);
    const data = {
      categories: categories.map((category) => category.id),
      colors: colors.map((color) => color.id),
      limit: limit,
      maxPrice: maxPrice,
      minPrice: minPrice,
      name: name,
      offset: pageIndex,
      sizes: sizes.map((size) => size.id),
      gender: gender,
      sortOption: sortOption,
    };
    const response = await ProductService.filterData(data);
    if (response.status === "OK") {
      setData(response.data);
    }
    setLoading(false);
  };
  const applyOnClick = async () => {
    setLoading(true);
    setPage(1);
    const payload = {
      categories: categories.map((category) => category.id),
      colors: colors.map((color) => color.id),
      limit: limit,
      maxPrice: maxPrice,
      minPrice: minPrice,
      name: name,
      offset: 1,
      sizes: sizes.map((size) => size.id),
      gender: gender,
      sortOption: sortOption,
    };
    const response = await ProductService.filterData(payload);
    if (response.status === "OK") {
      setData(response.data);
    }
    setLoading(false);
  };

  return {
    data,
    loading,
    setLoading,
    colors,
    categories,
    sizes,
    minPrice,
    maxPrice,
    setMinPrice,
    setMaxPrice,
    page,
    setPage,
    name,
    setName,
    gender,
    setGender,
    message,
    setMessage,
    sortOption,
    setSortOption,
    limit,
    setLimit,
    applyOnClick,
    pageOnClick,
  };
};

export default useProductFilter;
