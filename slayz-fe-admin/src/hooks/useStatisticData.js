import { useEffect } from "react";
import { useState } from "react";
import { StatisticService } from "../services/statistic.service";

const useStatisticData = () => {
  const [revenue, setRevenue] = useState(0);
  const [totalUser, setTotalUser] = useState(0);
  const [totalQuantityProduct, setTotalQuantityProduct] = useState(0);
  const [totalInvoice, setTotalInvoice] = useState(0);
  const [totalSaleProduct, setTotalSaleProduct] = useState(0);
  const [pieChartInvoice, setPieChartInvoice] = useState([]);

  const [lineSeriesData, setLineSeriesData] = useState([]);
  useEffect(() => {
    StatisticService.getRevenueData().then((response) => {
      if (response.status === "OK") {
        setRevenue(response.data);
      }
    });
    StatisticService.getTotalUser().then((response) => {
      if (response.status === "OK") {
        setTotalUser(response.data);
      }
    });
    StatisticService.getTotalQuantityProduct().then((response) => {
      if (response.status === "OK") {
        setTotalQuantityProduct(response.data);
      }
    });

    StatisticService.getTotalQuantityInvoice().then((response) => {
      if (response.status === "OK") {
        setTotalInvoice(response.data);
      }
    });
    StatisticService.getTotalQuantitySaleProduct().then((response) => {
      if (response.status === "OK") {
        setTotalSaleProduct(response.data);
      }
    });
    StatisticService.getDataLineChartQuantityInvoiceSevenDayRecent().then((responseQuantity) => {
      if (responseQuantity.status === "OK") {
        const lineChartInvoice = responseQuantity.data;
        StatisticService.getDataLineChartGrandTotalSevenDayRecent().then((responseGrand) => {
          if (responseGrand.status === "OK") {
            const lineChartGrandTotal = responseGrand.data;
            setLineSeriesData([
              {
                dataSource: lineChartInvoice,
                xName: "x",
                yName: "y",
                name: "Đơn hàng",
                width: "2",
                marker: { visible: true, width: 10, height: 10 },
                type: "Line",
              },

              {
                dataSource: lineChartGrandTotal,
                xName: "x",
                yName: "y",
                name: "Doanh thu",
                width: "2",
                marker: { visible: true, width: 10, height: 10 },
                type: "Line",
              },
            ]);
          }
        });
      }
    });

    StatisticService.getDataPieChartDataInvoiceData().then((response) => {
      if (response.status === "OK") {
        setPieChartInvoice(response.data);
      }
    });
  }, []);

  return {
    revenue,
    totalUser,
    totalQuantityProduct,
    totalInvoice,
    totalSaleProduct,
    pieChartInvoice,
    lineSeriesData,
  };
};

export default useStatisticData;
