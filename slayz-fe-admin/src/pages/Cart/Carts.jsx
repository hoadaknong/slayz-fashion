import React, { useEffect, useState } from "react";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
} from "@syncfusion/ej2-react-grids";

import { cartGrid } from "./CartData";
import { Header } from "../../components";
import { CartService } from "../../services/cart.service";

const Carts = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    let isFetch = true;
    const fetchData = () => {
      if (isFetch) {
        CartService.getAllCart().then((response) => {
          setData(response.data);
        });
      }
    };
    fetchData();
    return () => {
      isFetch = false;
    };
  }, []);
  document.title = "Giỏ Hàng";
  return (
    <div>
      <div className="my-4 mx-7">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div> Giỏ hàng
            </div>
          }
          title="Giỏ hàng"
        />
      </div>
      <div className="mx-6 p-2 md:p-10 bg-white rounded-3xl">
        <GridComponent
          id="gridcomp"
          dataSource={data}
          allowPaging
          allowSorting
          pageSettings={{ pageSize: 10 }}
        >
          <ColumnsDirective>
            {cartGrid.map((item, index) => (
              <ColumnDirective key={index} {...item} />
            ))}
          </ColumnsDirective>
          <Inject
            services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]}
          />
        </GridComponent>
      </div>
    </div>
  );
};

export default Carts;
