import { Link } from "react-router-dom";
import { GlobalUtil } from "../../utils/GlobalUtil";

export const gridOrderImage = (props) => (
  <div>
    <img className="rounded-xl h-20 md:ml-3" src={props.productImage} alt="order-item" />
  </div>
);

export const gridOrderstatus = (props) => (
  <button
    type="button"
    style={{ background: props.statusBg }}
    className="text-white py-1 px-2 capitalize rounded-2xl text-md"
  >
    {props.status}
  </button>
);

const gridDetailOrder = (props) => (
  <div className="flex justify-center items-center gap-2">
    <Link
      to={`${props.id}`}
      style={{ background: "#00ff71" }}
      className="text-white font-semibold py-2 px-5 capitalize rounded-full text-md hover:drop-shadow-lg"
    >
      Chi tiết
    </Link>
  </div>
);

const gridCartCustomer = (props) => {
  return (
    <div className="flex justify-start items-center gap-2">
      <img
        src={props.userPhoto}
        alt=""
        className="object-contain rounded-full w-[70px] h-[70px] border border-gray-400"
      />
      <h3>{props.fullName}</h3>
    </div>
  );
};

const gridProductCart = (props) => {
  return (
    <div className="flex justify-start items-center gap-2">
      <img src={props.imageProduct} alt="" className="rounded-md w-[120px]" />
      <Link to={`/management/products/${props.productId}`}>
        <h3>{props.productName}</h3>
      </Link>
    </div>
  );
};

const gridCurrency = (props) => (
  <div className="flex">
    <p className="">
      {GlobalUtil.numberWithCommas(props.total)} <span className="underline">đ</span>
    </p>
  </div>
);
const gridCurrencyCart = (props) => (
  <div className="flex">
    <p className="">
      {GlobalUtil.numberWithCommas(props.grandTotal)} <span className="underline">đ</span>
    </p>
  </div>
);

const gridCurrencyPrice = (props) => (
  <div className="flex">
    <p className="">
      {GlobalUtil.numberWithCommas(props.price)} <span className="underline">đ</span>
    </p>
  </div>
);

export const cartDetailGrid = [
  {
    headerText: "Sản phẩm",
    template: gridProductCart,
    width: "150",
    textAlign: "Left",
  },
  {
    field: "style",
    headerText: "Kiểu dáng",
    textAlign: "Left",
    width: "150",
  },
  {
    field: "quantity",
    headerText: "Số lượng",
    textAlign: "Left",
    editType: "numericedit",
    width: "150",
  },
  {
    field: "price",
    headerText: "Đơn giá",
    template: gridCurrencyPrice,
    textAlign: "Left",
    width: "150",
  },
  {
    field: "total",
    headerText: "Tạm tính",
    template: gridCurrency,
    textAlign: "Left",
    width: "150",
  },
];

export const cartGrid = [
  {
    field: "userId",
    headerText: "Mã KH",
    width: "60",
    textAlign: "center",
  },
  {
    headerText: "Khách hàng",
    template: gridCartCustomer,
    width: "150",
    textAlign: "Left",
  },
  {
    field: "quantityProduct",
    headerText: "Mặt hàng",
    textAlign: "Left",
    editType: "numericedit",
    width: "150",
  },
  {
    field: "totalQuantity",
    headerText: "Số lượng",
    textAlign: "Left",
    editType: "numericedit",
    width: "150",
  },
  {
    headerText: "Tạm tính",
    template: gridCurrencyCart,
    width: "150",
  },
  {
    headerText: "Action",
    template: gridDetailOrder,
    textAlign: "Center",
    width: "120",
  },
];
