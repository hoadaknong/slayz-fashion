/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Header } from "../../components";
import { useStateContext } from "../../contexts/ContextProvider";
import { useParams } from "react-router-dom";
import { cartDetailGrid } from "./CartData";
import { HiOutlineCurrencyDollar } from "react-icons/hi";
import { MdProductionQuantityLimits, MdOutlinePayment } from "react-icons/md";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
} from "@syncfusion/ej2-react-grids";
import { CartService } from "../../services/cart.service";
import { UserService } from "../../services/user.service";
import { GlobalUtil } from "../../utils/GlobalUtil";

const CartDetail = () => {
  let isFetch = true;
  const { currentColor } = useStateContext();
  const { id } = useParams();
  const [cartData, setCartData] = useState({});
  const [cartDetailData, setCartDetailData] = useState([]);
  const [userInfo, setUserInfo] = useState([]);

  useEffect(() => {
    if (isFetch) {
      const fetchCartDetailData = () => {
        CartService.getAllCartDetailByCartId(id).then((response) => {
          setCartDetailData(response.data);
        });
      };
      const fetchCartData = () => {
        CartService.getCartById(id).then((response) => {
          if (response.status === "OK") {
            setCartData(response.data);
            UserService.getUserById(response.data.userId).then((res) => {
              if (res.status === "OK") {
                setUserInfo(res.data);
              }
            });
          }
        });
      };
      fetchCartData();
      fetchCartDetailData();
    }

    return () => {
      isFetch = false;
    };
  }, []);

  const widthIcon = "35px";
  return (
    <div>
      <div className="m-2 md:m-10 p-2 md:p-10 bg-white rounded-3xl hover:drop-shadow-md">
        <div className="mb-6">
          <Header title={`Chi tiết giỏ hàng ${id}`} category="Thông tin chi tiết giỏ hàng" />
        </div>
        <div className="flex flex-col justify-center items-center">
          {/* Order information */}
          <div className="flex justify-start gap-4 text-gray-600 w-full">
            <div className="flex justify-center items-center w-1/4 py-6 rounded-3xl border hover:drop-shadow-md bg-white">
              <div className="flex flex-col justify-center items-center">
                <img
                  src={userInfo.photo}
                  alt="user"
                  className="rounded-full w-[100px] border-4 border-white h-[100px] object-cover"
                />
                <div className="w-full flex flex-col justify-start py-3 gap-3 text-center">
                  <h1 className="font-bold text-center text-xl">{userInfo.fullName}</h1>
                  <p>
                    <span className="font-bold text-center">Số điện thoại:</span> {userInfo.phone}
                  </p>
                  <p>
                    <span className="font-bold text-center">Email: </span>
                    {userInfo.email}
                  </p>
                </div>
                <div className="w-full flex justify-center items-center gap-10 border-t pt-6 mt-3">
                  <Link
                    to=""
                    className="inline-flex justify-center rounded-md border border-transparent py-3 px-4 text-sm font-medium shadow-sm hover:drop-shadow-md focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 text-white"
                    style={{ backgroundColor: currentColor }}
                  >
                    Chi tiết
                  </Link>
                  <Link
                    to=""
                    className="inline-flex justify-center rounded-md border bg-white py-3 px-4 text-sm font-medium shadow-sm hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 "
                    style={{ color: currentColor }}
                  >
                    Liên hệ
                  </Link>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-3 w-1/4">
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <MdProductionQuantityLimits
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Số mặt hàng</h1>
                </div>
                <div className="w-full h-full flex justify-start items-center px-7">
                  <h1 className="font-bold text-3xl">
                    {cartData.quantityProduct} <span className="text-gray-200 text-lg"></span>
                  </h1>
                </div>
              </div>
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <HiOutlineCurrencyDollar
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Tạm tính</h1>
                </div>
                <div className="w-full py-7 px-7 flex justify-start items-center h-full">
                  <h1 className="font-bold text-3xl">
                    {GlobalUtil.numberWithCommas(cartData.grandTotal)}{" "}
                    <span className="text-gray-500 text-lg">VNĐ</span>
                  </h1>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-3 w-1/4">
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <MdProductionQuantityLimits
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Tổng số sản phẩm</h1>
                </div>
                <div className="w-full py-7 px-7 flex justify-start items-center h-full">
                  <h1 className="font-bold text-3xl">{cartData.totalQuantity}</h1>
                </div>
              </div>
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <MdOutlinePayment
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Thanh toán</h1>
                </div>
                <div className="w-full px-7 flex justify-start items-center h-full">
                  <div className="w-full font-bold">
                    <div className="flex justify-between">
                      <h1 className="">
                        <span className="text-gray-700">Phương thức:</span>
                      </h1>
                      Chưa xác định
                    </div>
                    <div className="flex justify-between">
                      <h1 className="mt-1">
                        <span className="text-gray-700">Trạng thái:</span>
                      </h1>
                      Chưa thanh toán
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mt-6 w-full rounded-3xl px-8 py-6 border">
            <div className="font-bold text-xl my-3 bg-white rounded-full py-3 w-1/5 flex justify-center items-center border text-gray">
              DANH SÁCH SẢN PHẨM
            </div>
            <div className="flex flex-col gap-3 w-full py-3">
              <GridComponent
                id="gridcomp"
                dataSource={cartDetailData}
                allowPaging
                allowSorting
                pageSettings={{ pageSize: 10 }}
              >
                <ColumnsDirective>
                  {cartDetailGrid.map((item, index) => (
                    <ColumnDirective key={index} {...item} />
                  ))}
                </ColumnsDirective>
                <Inject
                  services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]}
                />
              </GridComponent>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CartDetail;
