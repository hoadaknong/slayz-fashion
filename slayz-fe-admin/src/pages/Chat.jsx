import React, { useEffect } from "react";

const Chat = () => {
  useEffect(() => {
    const script = document.createElement("script");
    script.async = true;
    script.src =
      "https://embed.tawk.to/64e289339a1460755e1a029520157baa154b7749";

    document.head.appendChild(script);

    return () => {
      document.head.removeChild(script);
    };
  }, []);

  return (
    <>
      {/*<iframe*/}
      {/*  className="w-full h-screen px-7"*/}
      {/*  src="https://dashboard.tawk.to/#/chat"*/}
      {/*  sandbox="allow-scripts allow-modal"*/}
      {/*  loading="eager"*/}
      {/*  title="Chat"*/}
      {/*  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture full"*/}
      {/*></iframe>*/}
      <iframe
        src="https://embed.tawk.to/64e289339a1460755e1a029520157baa154b7749"
        width="100%"
        height="100%"
        title="TawkTo"
      ></iframe>
    </>
  );
};

export default Chat;
