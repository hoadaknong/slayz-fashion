import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import { FiSettings } from "react-icons/fi";
import { TooltipComponent } from "@syncfusion/ej2-react-popups";

import { Navbar, Sidebar, ThemeSettings } from "../components";

import {
  Ecommerce,
  Orders,
  Customers,
  Carts,
  Categories,
  Color,
  Comments,
  Discount,
  Products,
  Size,
  SizeEdit,
  ProductNew,
  CategoryEdit,
  ColorEdit,
  DiscountEdit,
  DiscountDetail,
  DiscountNew,
  OrderDetail,
  CartDetail,
  CustomerDetail,
  EditDetail,
  EditAddress,
  NewAddress,
  Statistic,
  Feedback,
  FeedbackDetail,
  Banner,
  BannerForm,
  ProductEditNew,
  ProductEdit,
  ProductFeedbackList,
} from "./";

import { useStateContext } from "../contexts/ContextProvider";
import { useDataContext } from "../contexts/DataProvider";
import Chat from "./Chat";

const Dashboard = () => {
  const {
    activeMenu,
    themeSettings,
    setThemeSettings,
    currentColor,
    currentMode,
    isClicked,
    overlayHandleClick,
  } = useStateContext();
  const { user } = useDataContext();

  if (user === null) {
    return <Navigate to="/login" />;
  }
  return (
    <div className={currentMode === "Dark" ? "dark" : ""}>
      {isClicked?.overlay && (
        <div
          className="bg-black w-full h-screen absolute z-[10000] opacity-0"
          onClick={overlayHandleClick}
        ></div>
      )}
      <div className="flex relative dark:bg-main-dark-bg">
        <div className="fixed right-4 bottom-4" style={{ zIndex: "1000" }}>
          <TooltipComponent content="Cài đặt" position="Top">
            <button
              type="button"
              className="text-3xl p-3 drop-shadow-lg hover:bg-light-gray text-white duration-300 hover:scale-[1.1]"
              onClick={() => setThemeSettings(true)}
              style={{
                background: currentColor,
                borderRadius: "50%",
              }}
            >
              <FiSettings />
            </button>
          </TooltipComponent>
        </div>
        {activeMenu ? (
          <div className="w-72 fixed sidebar dark:bg-secondary-dark-bg bg-white">
            <Sidebar />
          </div>
        ) : (
          <div className="w-0 dark:bg-secondary-dark-bg">
            <Sidebar />
          </div>
        )}
        <div
          className={`dark:bg-main-dark-bg bg-main-bg min-h-screen w-full 
						${activeMenu ? "md:ml-72" : "flex-2"}`}
        >
          <div className="fixed md:static bg-main-bg dark:bg-main-dark-bg navbar w-full">
            <Navbar />
          </div>
          <div>
            {themeSettings && <ThemeSettings />}
            <Routes>
              {/* Dashboard */}
              <Route path="/" element={<Navigate to={"/index"} />} />
              <Route path="/index" element={<Ecommerce />} />

              {/* Quản lý đơn hàng */}
              <Route path="/orders">
                <Route index element={<Orders status={null} />} />
                <Route path=":id" element={<OrderDetail />} />
              </Route>
              <Route path="/orders_in_progress">
                <Route index element={<Orders status={"PENDING"} />} />
                <Route path=":id" element={<OrderDetail />} />
              </Route>
              <Route path="/orders_delivering">
                <Route index element={<Orders status={"IN PROGRESS"} />} />
                <Route path=":id" element={<OrderDetail />} />
              </Route>
              <Route path="/orders_completed">
                <Route index element={<Orders status={"COMPLETED"} />} />
                <Route path=":id" element={<OrderDetail />} />
              </Route>
              <Route path="/orders_cancelled">
                <Route index element={<Orders status={"CANCELLED"} />} />
                <Route path=":id" element={<OrderDetail />} />
              </Route>
              <Route path="/carts">
                <Route index element={<Carts />} />
                <Route path=":id" element={<CartDetail />} />
              </Route>

              {/* Quản lý người dùng */}
              <Route path="/customers">
                <Route index element={<Customers />} />
                <Route path=":id" element={<CustomerDetail />} />
                <Route path="detail/edit/:id" element={<EditDetail />} />
                <Route path="address/edit/:id" element={<EditAddress />} />
                <Route path="address/new/:id" element={<NewAddress />} />
              </Route>
              <Route path="/comments" element={<Comments />} />
              <Route path="/chat" element={<Chat />} />

              {/* Quản lý sản phẩm */}
              <Route path="/products/*">
                <Route index element={<Products />} />
                <Route path="edit/:id" element={<ProductEdit />} />
                <Route path="new" element={<ProductNew />} />
              </Route>
              <Route path="/review/*">
                <Route index element={<Feedback />} />
                <Route path=":id" element={<FeedbackDetail />} />
                <Route path="product/:id" element={<ProductFeedbackList />} />
              </Route>
              {/* Quản lý phân loại sản phẩm */}
              <Route path="/categories/*">
                <Route index element={<Categories />} />
                <Route path="edit/:id" element={<CategoryEdit />} />
              </Route>

              <Route path="/color">
                <Route index element={<Color />} />
                <Route path="edit/:id" element={<ColorEdit />} />
              </Route>
              <Route path="/size">
                <Route index element={<Size />} />
                <Route path="edit/:id" element={<SizeEdit />} />
              </Route>
              <Route path="/discount">
                <Route index element={<Discount />} />
                <Route path="edit/:id" element={<DiscountEdit />} />
                <Route path="new" element={<DiscountNew />} />
                <Route path=":id" element={<DiscountDetail />} />
              </Route>

              <Route path="statistic" element={<Statistic />} />
              <Route path="banner">
                <Route index element={<Banner />} />
                <Route path=":id" element={<BannerForm />} />
                <Route path="new" element={<BannerForm />} />
              </Route>
            </Routes>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
