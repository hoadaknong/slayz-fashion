import React, { useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Header } from "../../components";
import { Link } from "react-router-dom";
import { ColorService } from "../../services/color.service";
import swal from "sweetalert";
const ColorEdit = () => {
  const { id } = useParams();
  const [data, setData] = useState(ColorService.getColorById(id));
  const name = useRef();
  const code = useRef();
  const navigate = useNavigate();
  const [nameCheck, setNameCheck] = useState(true);
  const [codeCheck, setCodeCheck] = useState(true);

  useEffect(() => {
    const fetchData = () => {
      ColorService.getColorById(id).then((response) => {
        setData(response);
      });
    };
    fetchData();
  }, [id]);

  const onSave = () => {
    if (name.current.value.trim() === "" && code.current.value.trim() === "") {
      setNameCheck(false);
      setCodeCheck(false);
      name.current.focus();
    } else if (name.current.value.trim() === "") {
      setNameCheck(false);
      name.current.focus();
    } else if (code.current.value.trim() === "") {
      setCodeCheck(false);
      code.current.focus();
    } else {
      const data = { name: name.current.value, code: code.current.value };
      ColorService.updateColor(id, data).then((response) => {
        swal({
          title: "Thành công",
          text: "Cập nhật thông tin màu thành công!",
          icon: "success",
          button: "OK",
        });
        navigate("/management/color");
      });
    }
  };
  return (
    <>
      <div className=" md:m-10 p-1 md:p-10 bg-white rounded-3xl">
        <div className="flex justify-between items-center mb-6">
          <Header title={`Màu ${id}`} category="Chỉnh sửa thuộc tính màu sắc" />
        </div>
        <div className="flex justify-center items-center bg-gray-50 ">
          <div className="w-full">
            <div>
              <div className="md:col-span-2 md:mt-0">
                <div className="sm:overflow-hidden sm:rounded-md">
                  <div className="space-y-6 bg-white rounded-x">
                    <div>
                      <div className="py-2 flex flex-col gap-2 ">
                        <div>
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1">Tên màu</h1>
                            <div className="mt-1">
                              <textarea
                                id="name"
                                name="name"
                                rows={1}
                                className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                                defaultValue={data.name}
                                ref={name}
                              />
                            </div>
                            {!nameCheck && <p className="mb-1 text-red-500">Hãy nhập tên màu</p>}
                          </label>
                        </div>
                        <div>
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1">Mã màu</h1>
                            <div className="mt-1">
                              <textarea
                                id="code"
                                name="code"
                                rows={1}
                                className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                                defaultValue={data.code}
                                ref={code}
                                disabled
                              />
                            </div>
                            {!codeCheck && <p className="mb-1 text-red-500">Hãy nhập mã màu</p>}
                          </label>
                        </div>
                        <div>
                          <input
                            type="color"
                            className="w-full my-2 h-[50px]"
                            onChange={(event) => {
                              code.current.value = event.target.value;
                            }}
                            defaultValue={data.code}
                          />
                        </div>
                      </div>
                      <div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6">
                        <Link
                          to="/management/color"
                          className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                        >
                          Hủy
                        </Link>
                        <button
                          type="button"
                          className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                          onClick={onSave}
                        >
                          Lưu
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ColorEdit;
