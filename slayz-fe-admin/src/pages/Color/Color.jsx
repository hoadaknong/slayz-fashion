/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useRef} from "react";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
  Toolbar,
  Search,
} from "@syncfusion/ej2-react-grids";
import { Header } from "../../components";
import { useStateContext } from "../../contexts/ContextProvider";
import { useState } from "react";
import ModalAdd from "./ModalAdd";
import { useDataContext } from "../../contexts/DataProvider";
import { ColorService } from "../../services/color.service";
import { Link, useNavigate } from "react-router-dom";
import swal from "sweetalert";
import {Pagination} from "@mui/material";

const Color = () => {
  const navigate = useNavigate();
  const [openModal, setOpenModal] = useState(false);
  const { colorData, setColorData } = useDataContext();
  document.title = "Màu Sắc";
  const { currentColor } = useStateContext();
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(10);
  const pageSize = useRef(10)
  const searchOptions = {
    fields: ["name", "code"],
    ignoreCase: true,
    operator: "contains",
  };
  const toolbarOptions = ["Search", "PdfExport", "ExcelExport"];
  useEffect(() => {
    const fetchColorData = () => {
      ColorService.getAllColor().then((response) => {
        setColorData(response.details);
      });
    };
    fetchColorData();
  }, []);
  const editColorGrid = (props) => {
    return (
      <div className="flex justify-center items-center gap-2">
        <Link
          style={{ background: "#00d084" }}
          className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
          to={`edit/${props.id}`}
        >
          Sửa
        </Link>
        <button
          type="button"
          style={{ background: "#FF3333" }}
          className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
          onClick={() => {
            swal({
              title: "Bạn có muốn xóa?",
              text: "Khi xóa màu " + props.name + ", bạn sẽ không thể khôi phục dữ liệu!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((result) => {
              if (result) {
                const deleteOnClick = (id) => {
                  ColorService.deleteColor(id)
                    .then((response) => {
                      swal("Đã xóa màu " + props.name + "!", {
                        icon: "success",
                      });
                      navigate("/management");
                      navigate("/management/color");
                    })
                    .catch((error) => {
                      swal("Đã xảy ra lỗi", {
                        icon: "error",
                      });
                    });
                };
                deleteOnClick(props.id);
              }
            });
          }}
        >
          Xóa
        </button>
      </div>
    );
  };

  const colorBlock = (props) => {
    return (
      <div className="flex justify-start items-center">
        <div className="flex gap-3 justify-start items-center w-2/4">
          <div className="w-20 h-20 rounded-xl" style={{ backgroundColor: props.code }}></div>
          <p>{props.name}</p>
        </div>
      </div>
    );
  };
  const colorGrid = [
    {
      headerText: "Màu",
      template: colorBlock,
      width: "90",
      textAlign: "Left",
    },
    {
      field: "code",
      headerText: "Mã màu",
      width: "60",
      textAlign: "Left",
    },
    {
      headerText: "Hành động",
      width: "120",
      template: editColorGrid,
      textAlign: "Center",
    },
  ];
  return (
    <>
      {openModal && (
        <ModalAdd
          onClose={() => {
            setOpenModal(false);
          }}
          onSave={() => {
            alert("Save color");
            setOpenModal(false);
          }}
          valueColor={currentColor}
        />
      )}
      {openModal && <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>}
      <div className="flex justify-between items-center my-4 mx-7">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div> Màu sắc/Kiểu dáng
            </div>
          }
          title="Màu sắc/Kiểu dáng"
        />
        <button
          type="button"
          style={{
            backgroundColor: currentColor,
            color: "white",
          }}
          className="font-semibold hover:drop-shadow rounded-full px-6 py-3"
          onClick={() => setOpenModal(true)}
        >
          Thêm màu sản phẩm mới
        </button>
      </div>
      <div className="mx-6 p-1 md:p-10 bg-white rounded-3xl">
        <GridComponent
          id="gridcomp"
          dataSource={colorData}
          allowPaging
          allowSorting
          toolbar={toolbarOptions}
          searchSettings={searchOptions}
          pageSettings={{ pageSize: 7 }}
        >
          <ColumnsDirective>
            {colorGrid.map((item, index) => (
              <ColumnDirective key={index} {...item} />
            ))}
          </ColumnsDirective>
          <Inject
            services={[
              Resize,
              Sort,
              ContextMenu,
              Filter,
              Page,
              ExcelExport,
              Edit,
              PdfExport,
              Toolbar,
              Search,
            ]}
          />
        </GridComponent>
      </div>
    </>
  );
};

export default Color;
