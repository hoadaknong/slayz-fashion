import React, { useRef, useState } from "react";
import { ColorService } from "../../services/color.service";
import swal from "sweetalert";
import { useNavigate } from "react-router-dom";
const ModalAdd = ({ onClose, valueColor }) => {
	const name = useRef();
	const code = useRef();
	const color = useRef();
	const [nameCheck, setNameCheck] = useState(true);
	const [codeCheck, setCodeCheck] = useState(true);
	const navigate = useNavigate();
	const onSave = () => {
		if (name.current.value.trim() === "" && code.current.value.trim() === "") {
			setNameCheck(false);
			setCodeCheck(false);
			name.current.focus();
		} else if (name.current.value.trim() === "") {
			setNameCheck(false);
			name.current.focus();
		} else if (code.current.value.trim() === "") {
			setCodeCheck(false);
			code.current.focus();
		} else {
			const data = { name: name.current.value, code: code.current.value };
			ColorService.createColor(data).then((response) => {
				swal({
					title: "Thành công",
					text: "Thêm mới sản phẩm thành công!",
					icon: "success",
					button: "OK",
				});
				navigate("/management");
				navigate("/management/color");
				onClose();
			});
		}
	};
	return (
		<>
			<div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
				<div className="relative w-1/4 my-0 mx-auto max-w-3xl">
					<div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
						<div className="flex w-full flex-col justify-center items-center rounded-xl border border-gray-200 relative bg-white">
							<div className="flex justify-center gap-2 bg-gray-50 py-2 mb-2 text-right sm:px-6 w-full">
								<h1 className="py-4 font-bold">THÊM MÀU MỚI</h1>
							</div>
							<div className="w-3/4">
								<label className="block text-sm font-medium text-gray-700">
									<h1 className="font-bold mb-1">Tên màu</h1>
									<div className="mt-1">
										<textarea
											id="name"
											name="name"
											rows={1}
											className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${valueColor}] focus:ring-indigo-500 sm:text-sm p-2 focus:outline-none`}
											placeholder="Tên danh mục"
											ref={name}
										/>
									</div>
									{!nameCheck && <p className="mb-1 text-red-500">Hãy nhập tên màu</p>}
								</label>
							</div>
							<div className="w-3/4">
								<label className="block text-sm font-medium text-gray-700">
									<h1 className="font-bold mb-1">Mã màu</h1>
									<div className="mt-1">
										<textarea
											id="code"
											name="code"
											rows={1}
											className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
											defaultValue={valueColor}
											ref={code}
											disabled
										/>
									</div>
									{!codeCheck && <p className="mb-1 text-red-500">Hãy nhập mã màu</p>}
									<input
										type="color"
										ref={color}
										className="w-full my-2"
										onChange={(event) => {
											code.current.value = event.target.value;
										}}
										defaultValue={valueColor}
									/>
								</label>
							</div>

							<div className="flex justify-end gap-2 bg-gray-50 py-3 text-right sm:px-6 w-full mt-4">
								<button
									className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-6 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
									onClick={onClose}
								>
									Hủy
								</button>
								<button
									type="button"
									className="inline-flex justify-center rounded-md border border-transparent bg-blue-500 py-3 px-6 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
									onClick={onSave}
								>
									Lưu
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default ModalAdd;
