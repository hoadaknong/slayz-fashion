import React, { useRef } from "react";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { AuthService } from "../services/auth.service";
import { useDataContext } from "../contexts/DataProvider";
import { backgroundLoginImage } from "../data/dummy";
import { UserService } from "../services/user.service";

const Login = () => {
  document.title = "Đăng nhập";
  const [usernameCheck, setUsernameCheck] = useState(false);
  const [passwordCheck, setPasswordCheck] = useState(false);
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);
  const { setUser } = useDataContext();
  const navigate = useNavigate();
  const validate = (username, password) => {
    if (username.trim() === "" && password.trim() === "") {
      usernameRef.current.focus();
      setUsernameCheck(true);
      setPasswordCheck(true);
      return false;
    } else if (username.trim() === "") {
      usernameRef.current.focus();
      setUsernameCheck(true);
      return false;
    } else if (password.trim() === "") {
      passwordRef.current.focus();
      setPasswordCheck(true);
      return false;
    }
    return true;
  };
  const handleLogin = async (e) => {
    e.preventDefault();
    const username = usernameRef.current.value;
    const password = passwordRef.current.value;
    if (validate(username, password)) {
      const loginResponse = await AuthService.login(username, password);
      console.log(loginResponse);
      if (loginResponse.status === "OK") {
        const userResponse = await UserService.getCurrentUser();
        if (userResponse.status === "OK") {
          setUser(userResponse.data);
          navigate("/");
        }
      }
    }
  };
  return (
    <div
      className="flex justify-center items-center h-screen w-full text-[#03045e]"
      style={{
        backgroundImage: `url(${backgroundLoginImage})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        background: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <section className="h-fit md:w-[30%] w-full px-[50px]">
        <div className="text-center pb-[10px] mb-4">
          <h1 className="text-[30px] mt-1 uppercase font-black">đăng nhập</h1>
          <p className="text-[15px] mt-1 font-light">Trang đăng nhập dành cho quản trị viên.</p>
        </div>
        <form className="flex flex-col gap-2">
          <div className="mb-2">
            <label>
              <p className="text-semibold mb-1 ml-4">Email hoặc SĐT</p>
              <input
                type="text"
                className="w-full px-4 py-[12px] text-base font-normal text-gray-700 border-solid border-gray-300 rounded-full transition ease-in-out m-0 focus:bg-white focus:border-[#0077b6] focus:outline-none border-[1.5px]"
                id=""
                placeholder="Nhập Email hoặc SĐT"
                ref={usernameRef}
                onChange={(event) => {
                  if (event.target.value.trim() !== "") {
                    setUsernameCheck(false);
                  }
                }}
              />
              {usernameCheck && (
                <p className="text-red-600 mt-1 ml-1 text-sm">Vui lòng nhập tài khoản</p>
              )}
            </label>
          </div>
          <div className="mb-4">
            <label>
              <p className="text-semibold mb-1 ml-4">Mật khẩu</p>
              <input
                type="password"
                className="w-full px-4 py-[12px] text-base font-normal text-gray-700 border-solid border-gray-300 rounded-full transition ease-in-out m-0 focus:bg-white focus:border-[#0077b6] focus:outline-none border-[1.5px]"
                id="password"
                placeholder="Nhập mật khẩu"
                ref={passwordRef}
                onChange={(event) => {
                  if (event.target.value.trim() !== "") {
                    setPasswordCheck(false);
                  }
                }}
              />
              {passwordCheck && (
                <p className="text-red-600 mt-1 ml-1 text-sm">Vui lòng nhập mật khẩu</p>
              )}
            </label>
          </div>
          <div className="text-center pt-1 mb-12 pb-1">
            <button
              className="py-[14px] font-medium text-md leading-tight uppercase rounded-full hover:bg-[#0077b6] hover:shadow-lg focus:shadow-lg focus:outline-none focus:ring-0 transition duration-150 ease-in-out w-full mb-3 border border-[#0077b6] hover:text-white text-[#0077b6]"
              type="button"
              data-mdb-ripple="true"
              data-mdb-ripple-color="light"
              onClick={handleLogin}
            >
              Đăng nhập
            </button>
          </div>
        </form>
      </section>
    </div>
  );
};

export default Login;
