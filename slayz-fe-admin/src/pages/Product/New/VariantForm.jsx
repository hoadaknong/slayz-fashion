import React, { useEffect, useRef, useState } from "react";
import { GrClose } from "react-icons/gr";
import { productDefault } from "../../../data/dummy";
import { ColorService } from "../../../services/color.service";
import { SizeService } from "../../../services/size.service";

const VariantForm = ({ id, closeEventHandle, colorBorder }) => {
  const image = useRef();
  const [colorData, setColorData] = useState([]);
  const [sizeData, setSizeData] = useState([]);
  useEffect(() => {
    let isFetchData = true;
    const fetchColorData = () => {
      ColorService.getAllColor().then((response) => {
        if (isFetchData) {
          setColorData(response.details);
        }
      });
    };
    const fetchSizeData = () => {
      SizeService.getAllSize().then((response) => {
        if (isFetchData) {
          setSizeData(response);
        }
      });
    };
    fetchColorData();
    fetchSizeData();
    return () => {
      isFetchData = false;
    };
  }, []);
  return (
    <div id={`variant-form-` + id} className="border-b-1 border-gray-200">
      <div className="grid grid-cols-4 gap-5 py-4">
        <div className="col-span-2">
          <label className="block text-sm font-medium text-gray-700">
            Màu, kiểu dáng
            <select
              className={`mt-1 w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-[${colorBorder}] focus:ring-indigo-500 sm:text-sm variant-style h-10 focus:outline-none`}
            >
              <option disabled selected>
                Chọn kiểu dáng
              </option>
              {colorData.map((item, index) => {
                return (
                  <option height="65" key={index} value={item.id} className="color">
                    {item.name}
                  </option>
                );
              })}
            </select>
          </label>
        </div>
        <div>
          <label className="block text-sm font-medium text-gray-700">
            Size
            <select
              className={`mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-[${colorBorder}] focus:outline-none sm:text-sm variant-size h-[41px]`}
            >
              <option disabled selected>
                Chọn size
              </option>
              {sizeData.map((item, index) => {
                return (
                  <option value={item.id} key={index}>
                    {item.name}
                  </option>
                );
              })}
            </select>
          </label>
        </div>
        <div>
          <label>
            Số lượng
            <input
              type="number"
              className={`block w-full border rounded-md border-gray-300 shadow-sm focus:border-[${colorBorder}] sm:text-sm pl-2 variant-quantity focus:outline-none h-[41px]`}
              min={1}
              max={100000}
              defaultValue={1}
            />
          </label>
        </div>
      </div>
      <div className=" grid grid-cols-4 gap-5 pb-4">
        <div className="col-span-3">
          <label className="block text-sm font-medium text-gray-700">Photo</label>
          <div className="flex items-center space-x-6">
            <label className="block">
              <input
                type="file"
                className={`block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100 mt-1 variant-photo`}
                onChange={(event) => {
                  image.current.src = URL.createObjectURL(event.target.files[0]);
                }}
              />
            </label>
          </div>
          <div className="shrink-0 mt-2">
            <img
              className="h-60 w-60 object-cover rounded-xl"
              src={productDefault}
              alt=""
              ref={image}
            />
          </div>
        </div>
        <div className="flex justify-end p-1">
          {id !== 1 ? (
            <button
              type="button"
              className="rounded-full hover:drop-shadow-lg h-10 w-10 flex justify-center items-center"
              style={{ backgroundColor: "white" }}
              onClick={closeEventHandle}
            >
              <GrClose size="15px" style={{ color: "white" }} color="#666666" />
            </button>
          ) : (
            <div></div>
          )}
        </div>
      </div>
    </div>
  );
};

export default VariantForm;
