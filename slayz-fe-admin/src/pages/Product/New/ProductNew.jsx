/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from "react";
import ReactROM from "react-dom";
import { Link, useNavigate } from "react-router-dom";
import { Header } from "../../../components";
import { productDefault } from "../../../data/dummy";
import VariantForm from "./VariantForm";
import { ProductService } from "../../../services/product.service";
import { useStateContext } from "../../../contexts/ContextProvider";
import swal from "sweetalert";
import { useDataContext } from "../../../contexts/DataProvider";
import { CategoryService } from "../../../services/category.service";
import {
  HtmlEditor,
  Image,
  Inject,
  QuickToolbar,
  RichTextEditorComponent,
  Toolbar,
} from "@syncfusion/ej2-react-richtexteditor";
import Select from "react-select";
import TabVariant from "./TabVariant";

const ProductNew = () => {
  const [styleFormList, setStyleFormList] = useState([1]);
  const [count, setCount] = useState(1);
  const [images, setImages] = useState([]);
  const thumbnailRef = useRef();
  const { currentColor } = useStateContext();
  const navigate = useNavigate();
  const descriptionRef = useRef();
  const slug = useRef();
  const [categoryOption, setCategoryOption] = useState(null);
  const [genderData, setGenderData] = useState([
    { value: "MALE", label: "Nam" },
    { value: "FEMALE", label: "Nữ" },
    { value: "UNISEX", label: "Unisex" },
  ]);
  const [genderOption, setGenderOption] = useState(genderData[0]);

  const { categoryData, setCategoryData } = useDataContext();
  const increaseVariantForm = () => {
    let tmpList = styleFormList;
    tmpList.push(styleFormList[tmpList.length - 1] + 1);
    setStyleFormList(tmpList);
    setCount(count + 1);
  };

  useEffect(() => {
    renderVariantFormList();
    CategoryService.getAllCategory().then((response) => {
      setCategoryData(() => {
        return response.details.map(({ id, name }) => ({
          value: id,
          label: name,
        }));
      });
    });
  }, [count]);
  const renderVariantFormList = () => {
    ReactROM.render(
      <div>
        {styleFormList.map((i, index) => {
          return (
            <VariantForm
              key={index}
              colorBorder={currentColor}
              id={i}
              closeEventHandle={() => {
                if (count !== 1) {
                  const newList = styleFormList.filter((item) => item !== i);
                  setStyleFormList(newList);
                  setCount(count - 1);
                  renderVariantFormList();
                }
              }}
            />
          );
        })}
      </div>,
      document.getElementById("style-form-render")
    );
  };

  const getData = (e) => {
    e.preventDefault();
    let error = false;
    let title = document.getElementById("title").value;
    let category = categoryOption.value;
    let barcode = document.getElementById("barcode").value;
    let variantStyle = document.getElementsByClassName("variant-style");
    let variantSize = document.getElementsByClassName("variant-size");
    let variantQuantity = document.getElementsByClassName("variant-quantity");
    let variantPhoto = document.getElementsByClassName("variant-photo");
    let gender = genderOption?.value;
    let standCost = Number(document.getElementById("standCost").value);
    let listPrice = Number(document.getElementById("listPrice").value);
    let description = descriptionRef.current.value;
    let thumbnail = document.getElementById("thumbnail").files[0];
    let photos = images;
    var data = new FormData();
    data.append("title", title);
    data.append("category", category);
    data.append("barcode", barcode);
    data.append("standCost", standCost);
    data.append("listPrice", listPrice);
    data.append("description", description);
    data.append("gender", gender);
    data.append("thumbnail", thumbnail);
    for (let i = 0; i < photos.length; i++) {
      data.append("photos", photos[i]);
    }
    for (let i = 0; i < variantStyle.length; i++) {
      let colorVariants = Number(variantStyle[i].value);
      let sizeVariants = Number(variantSize[i].value);
      let quantityVariants = Number(variantQuantity[i].value);
      let photoVariants = variantPhoto[i].files[0];
      if (
        colorVariants === undefined ||
        sizeVariants === undefined ||
        quantityVariants === undefined ||
        photoVariants === undefined ||
        quantityVariants < 1
      ) {
        error = true;
        break;
      }
      data.append("colorVariants", colorVariants);
      data.append("sizeVariants", sizeVariants);
      data.append("quantityVariants", quantityVariants);
      data.append("photoVariants", photoVariants);
    }
    if (error) {
      swal({
        title: "LỖI BIẾN THỂ SẢN PHẨM",
        text: "Hãy kiểm tra đảm bảo rằng tất cả biến thể đều được chọn màu sắc, kích cỡ, số lượng và hình ảnh.",
        icon: "error",
        button: "OK",
      });
    } else {
      swal({
        text: "Đang xử lý...",
      });
      ProductService.createProduct(data)
        .then((response) => {
          if (response.status === "OK") {
            swal({
              title: "Thành công",
              text: "Thêm mới sản phẩm thành công!",
              icon: "success",
              button: "OK",
            });
            navigate("/management/products");
          } else {
            swal({
              title: "Lỗi",
              text: response.message,
              icon: "error",
              button: "OK",
            });
          }
        })
        .catch((error) => {
          console.log(error);
          swal({
            title: "Lỗi",
            text: error.message,
            icon: "error",
            button: "OK",
          });
        });
    }
  };
  return (
    <div className=" md:m-10 rounded-3xl">
      <div className="flex justify-between items-center mb-6">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Sản phẩm <div className="w-1 h-1 rounded-full bg-gray-400"></div>
              Thêm sản phẩm
            </div>
          }
          title="Thêm sản phẩm mới"
        />
      </div>
      <div className="flex justify-center items-center">
        <div className="w-full">
          <div>
            <div className="mt-1 md:col-span-2 md:mt-0">
              <form
                className="sm:overflow-hidden sm:rounded-md"
                onSubmit={getData}
              >
                <div className="space-y-3">
                  <div className="px-6 py-8 rounded-xl border border-gray-200 flex flex-col gap-2 bg-white">
                    <div>
                      <h1 className="font-bold mb-3">Thông tin cơ bản</h1>
                      <label
                        htmlFor="title"
                        className="block text-sm font-medium text-gray-700"
                      >
                        Tiêu đề
                        <div className="mt-1">
                          <textarea
                            id="title"
                            name="title"
                            rows={3}
                            className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-[${currentColor}] sm:text-sm p-2 focus:outline-none`}
                            placeholder="Tên sản phẩm"
                            minLength={4}
                            maxLength={255}
                            required
                            onChange={(e) => {
                              slug.current.value =
                                String(e.target.value)
                                  .normalize("NFD")
                                  .replace(/[\u0300-\u036f]/g, "") //remove diacritics
                                  .toLowerCase()
                                  .replace(/\s+/g, "-") //spaces to dashes
                                  .replace(/&/g, "-and-") //ampersand to and
                                  .replace(/[^\w\-]+/g, "") //remove non-words
                                  .replace(/\-\-+/g, "-") //collapse multiple dashes
                                  .replace(/^-+/, "") //trim starting dash
                                  .replace(/-+$/, "") + ".html";
                            }}
                          />
                        </div>
                      </label>
                    </div>
                    <div>
                      <label className="block text-sm font-medium text-gray-700">
                        Loại sản phẩm
                        <Select
                          options={categoryData}
                          onChange={(option) => {
                            setCategoryOption(option);
                          }}
                          placeholder="Chọn loại sản phẩm"
                          className={`mt-1 w-full border rounded-md focus:outline-none text-[15px] border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] hover:border-[${currentColor}]`}
                        />
                      </label>
                    </div>
                    <div>
                      <label className="block text-sm font-medium text-gray-700">
                        Giới tính
                        <Select
                          options={genderData}
                          onChange={(option) => {
                            console.log(option);
                            setGenderOption(option);
                          }}
                          placeholder="Chọn giới tính"
                          defaultValue={genderData[0]}
                          className={`mt-1 w-full border rounded-md focus:outline-none text-[15px] border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] hover:border-[${currentColor}]`}
                        />
                      </label>
                    </div>
                    <div>
                      <label className="block text-sm font-medium text-gray-700">
                        Mã sản phẩm
                        <textarea
                          id="barcode"
                          name="barcode"
                          rows={2}
                          className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-[${currentColor}] focus:outline-none sm:text-sm p-2 `}
                          placeholder="Mã sản phẩm"
                          ref={slug}
                          required
                        />
                      </label>
                    </div>
                  </div>
                  <div
                    className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white"
                    id="style-form"
                  >
                    <div className="flex justify-between">
                      <h1 className="font-bold mb-2">Kiểu dáng, màu sắc</h1>
                    </div>
                    <div id="style-form-render"></div>
                  </div>
                  <div>
                    <button
                      type="button"
                      className="block w-full mr-4 py-3 px-4 rounded-full border-0 text-sm font-semibold bg-violet-200 text-violet-700 hover:bg-violet-300"
                      onClick={() => {
                        increaseVariantForm();
                      }}
                    >
                      Thêm biến thể
                    </button>
                  </div>
                  <div className="bg-white flex flex-col shadow px-6 py-8 rounded-xl border border-gray-200 gap-2">
                    <h1 className="font-bold mb-1">Giá bán</h1>
                    <div>
                      <label>
                        Giá giảm
                        <input
                          id="standCost"
                          name="standCost"
                          type="number"
                          className={`mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-[${currentColor}] focus:outline-none sm:text-sm h-9 pl-2`}
                          placeholder="0"
                          defaultValue={10000}
                          required
                          min={10000}
                          max={10000000}
                        />
                      </label>
                    </div>
                    <div>
                      <label>
                        Giá bán
                        <input
                          id="listPrice"
                          name="listPrice"
                          type="number"
                          min={10000}
                          max={10000000}
                          className={`mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-[${currentColor}] focus:outline-none sm:text-sm h-9 pl-2`}
                          placeholder="0"
                          defaultValue={10001}
                          required
                        />
                      </label>
                    </div>
                    <p className="mt-2 text-sm text-gray-500">
                      Đơn vị tiền tệ sử dụng hiện tại là VNĐ. Giá ở trên là giá
                      mặc định bạn có thể thay đổi theo ý muốn.
                    </p>
                  </div>
                  <div className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white">
                    <h1 className="font-bold mb-1">Mô tả</h1>
                    <label className="block text-sm font-medium text-gray-700">
                      Thông tin mô tả tổng quát về sản phẩm
                      <div className="mt-1">
                        <RichTextEditorComponent
                          height={600}
                          ref={descriptionRef}
                        >
                          <Inject
                            services={[
                              HtmlEditor,
                              Toolbar,
                              Image,
                              QuickToolbar,
                            ]}
                          />
                        </RichTextEditorComponent>
                      </div>
                    </label>
                    <p className="mt-2 text-sm text-gray-500">
                      Bạn có thể mô tả thông tin chi về sản phẩm để hiển thị
                      trong phần mô tả.
                    </p>
                  </div>
                  <div className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white">
                    <label className="block text-md font-bold text-gray-700">
                      Hình thu nhỏ - Một hình ảnh
                    </label>
                    <div className="flex items-center space-x-6 py-2">
                      <label className="block">
                        <span className="sr-only">Chọn file</span>
                        <input
                          type="file"
                          className="block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100"
                          id="thumbnail"
                          onChange={(event) => {
                            thumbnailRef.current.src = URL.createObjectURL(
                              event.target.files[0]
                            );
                          }}
                          required
                        />
                      </label>
                    </div>
                    <div className="shrink-0 mt-2">
                      <img
                        className="h-60 w-60 object-cover rounded-xl"
                        src={productDefault}
                        alt=""
                        ref={thumbnailRef}
                      />
                    </div>
                    <p className="mt-4 text-sm text-gray-500">
                      Hình ảnh này sẽ được hiển thị đầu tiên cho khách hàng.
                    </p>
                  </div>
                  <div className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white">
                    <p className="block text-md font-bold text-gray-700 mt-4">
                      Hình ảnh - Album hình ảnh
                    </p>
                    <div className="flex items-center space-x-6 py-2">
                      <label className="block">
                        <span className="sr-only">Chọn file</span>
                        <input
                          type="file"
                          className="block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100"
                          multiple="multiple"
                          id="photos"
                          onChange={(event) => {
                            setImages((old) => {
                              return [...old, ...event.target.files];
                            });
                          }}
                          required
                        />
                      </label>
                    </div>
                    <div className="flex flex-wrap gap-7 shrink-0 mt-2">
                      {images.length === 0 ? (
                        <img
                          className="h-60 w-60 object-cover rounded-xl"
                          src={productDefault}
                          alt=""
                        />
                      ) : (
                        images.map((image, index) => {
                          return (
                            <img
                              key={index}
                              className="w-[290px] h-[290px] object-cover rounded-2xl"
                              src={URL.createObjectURL(image)}
                              alt=""
                              onClick={() => {
                                if (window.confirm("Bạn có muốn xóa?")) {
                                  setImages((current) => {
                                    return current.filter(
                                      (element) => element !== image
                                    );
                                  });
                                }
                              }}
                            />
                          );
                        })
                      )}
                    </div>
                    <p className="mt-6 text-sm text-gray-500">
                      Những hình ảnh này sẽ được hiển thị trong phần chi tiết
                      của sản phẩm mỗi khi người dùng click vào sản phẩm này.
                    </p>
                  </div>
                </div>
                <div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6">
                  <Link
                    to="/management/products"
                    className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  >
                    Hủy
                  </Link>
                  <input
                    type="submit"
                    className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    value="Lưu"
                  />
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductNew;
