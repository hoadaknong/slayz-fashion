import React, { useEffect, useState } from "react";

const TabVariant = () => {
  const [tabIndex, setTabIndex] = useState(0);
  const [tabAmount, setTabAmount] = useState(1);
  const [tabs, setTabs] = useState([{ index: 0, title: "New tab", content: "Content" }]);
  const [setCurrentTitle] = useState(tabs[0].title);
  useEffect(() => {
    if (tabs.length < tabAmount)
      setTabs((prev) => {
        return [...prev, { index: tabAmount - 1, title: "New tab", content: "Content" }];
      });
  }, [tabAmount, tabs]);
  useEffect(() => {
    setTabIndex(tabs[0].index);
  }, [tabs]);

  const onChangeTitle = (title) => {
    setTabs((prev) => {
      prev[tabIndex].title = title;
      return prev;
    });
    setCurrentTitle(title);
  };

  const onChangeContent = (content) => {
    setTabs((prev) => {
      prev[tabIndex].content = content;
      return prev;
    });
  };

  const removeTab = () => {
    if (tabAmount > 1) {
      setTabs((prev) => {
        return [...prev.filter((tab) => tab.index !== tabIndex)];
      });
      setTabAmount(tabAmount - 1);
    }
  };
  return (
    <>
      <div className="flex">
        {tabs.map((tab) => {
          return (
            <TabTitle
              key={tab.index}
              title={tab.title}
              index={tabIndex}
              onClick={() => setTabIndex(tab.index)}
              isActive={tab.index === tabIndex}
              removeTab={removeTab}
            />
          );
        })}
        <TabTitle
          title={"+"}
          onClick={() => {
            setTabAmount(tabAmount + 1);
          }}
        />
      </div>
      {tabs.map((tab) => {
        return (
          <TabContent
            key={tab.index}
            title={tab.title}
            content={tab.content}
            onChangeTitle={onChangeTitle}
            onChangeContent={onChangeContent}
            isActive={tab.index === tabIndex}
          />
        );
      })}
    </>
  );
};

const TabTitle = ({ title, isActive, onClick, removeTab }) => {
  return (
    <div
      className={
        !isActive
          ? "flex justify-between gap-3 items-center font-semibold px-3 py-1 rounded-t-md border border-gray-700 w-fit border-b-0 cursor-pointer hover:text-white hover:bg-black "
          : "flex justify-between gap-3 items-center font-semibold px-3 py-1 rounded-t-md border border-gray-700 w-fit border-b-0 text-white bg-black cursor-pointer"
      }
      onClick={onClick}
    >
      <div>{title}</div>
      {title !== "+" && (
        <div className="rounded-full px-[7px] hover:text-black hover:bg-white" onClick={removeTab}>
          x
        </div>
      )}
    </div>
  );
};

const TabContent = ({ title, content, onChangeTitle, onChangeContent, isActive }) => {
  return (
    <div className={isActive ? "flex flex-col gap-2 p-5 border border-gray-700" : "hidden"}>
      <input
        type="text"
        name="title"
        onChange={(e) => {
          onChangeTitle(e.target.value);
        }}
        defaultValue={title}
        className="py-2 border border-black px-2"
      />
      <textarea
        onChange={(e) => {
          onChangeContent(e.target.value);
        }}
        className="py-2 border border-black px-2"
        defaultValue={content}
      ></textarea>
      <button type="submit" className="py-2 border border-black px-2">
        Submit
      </button>
    </div>
  );
};

export default TabVariant;
