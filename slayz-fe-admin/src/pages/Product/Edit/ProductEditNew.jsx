/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useRef, useState} from "react";
import {Link, useNavigate, useParams} from "react-router-dom";
import {Header} from "../../../components";
import swal from "sweetalert";
import {ProductService} from "../../../services/product.service";
import {useStateContext} from "../../../contexts/ContextProvider";
import {useDataContext} from "../../../contexts/DataProvider";
import {ImageService} from "../../../services/image.service";
import {CategoryService} from "../../../services/category.service";
import {
  HtmlEditor,
  Image,
  Inject,
  QuickToolbar,
  RichTextEditorComponent,
  Toolbar,
} from "@syncfusion/ej2-react-richtexteditor";
import ColorVariant from "./ColorVariant";
import SizeVariant from "./SizeVariant";
import {SizeService} from "../../../services/size.service";

const ProductEditNew = () => {
  const [styleFormList, setStyleFormList] = useState([]);
  const { id } = useParams();
  const [images, setImages] = useState([]);
  const [data, setData] = useState({});
  const navigate = useNavigate();
  const { currentColor } = useStateContext();
  const { categoryData, setCategoryData } = useDataContext();
  const [imageData, setImageData] = useState([]);
  const thumbnailRef = useRef();
  const descriptionRef = useRef();
  const [variants, setVariants] = useState([]);
  const slug = useRef();
  const sizeIndex = useRef(0);
  const [sizes,setSizes] = useState([])
  const increaseVariantForm = () => {
    alert("Add new variant");
  };

  useEffect(() => {
    (async function () {
      const response = await SizeService.getAllSize();
      setSizes(response);
    })();
    (async function () {
      const productResponse = await ProductService.getProductById(id);
      setData(productResponse.data);
      document.getElementById("gender").value = productResponse.data.gender;
      const imageResponse = await ImageService.getAllImageByProductId(id);
      setImageData(imageResponse.data);
      const categoryResponse = await CategoryService.getAllCategory();
      setCategoryData(categoryResponse.details);
      const variantResponse = await ProductService.getAllVariantByProductId(id);
      setVariants(() => {
        let result = [];
        let variantList = variantResponse.data;
        let checkColor = [];
        for (let i = 0; i < variantList.length; i++) {
          if (checkColor.includes(variantList[i].colorId)) {
            continue;
          }
          let color = variantList[i].colorId;
          checkColor.push(color);
          let colorVariant = {
            colorId: color,
            productId: Number(id),
            image: variantList[i].image,
            sizeVariants: [],
          };
          for (let j = 0; j < variantList.length; j++) {
            if (color === variantList[j].colorId) {
              colorVariant.sizeVariants.push({
                sizeI: sizeIndex.current++,
                sizeId: variantList[j].sizeId,
                inventory: variantList[j].inventory,
              });
            }
          }
          result.push(colorVariant);
        }
        console.log(result);
        return result;
      });
    })();
  }, []);

  const getData = (e) => {
    e.preventDefault();
    let error = false;
    let title = document.getElementById("title").value;
    let category = Number(document.getElementById("category").value);
    let barcode = document.getElementById("barcode").value;
    let variantIdOld = document.getElementsByClassName("variant-id-old");
    let variantQuantityOld = document.getElementsByClassName(
      "variant-quantity-old"
    );
    let standCost = Number(document.getElementById("standCost").value);
    let listPrice = Number(document.getElementById("listPrice").value);
    let description = descriptionRef.current.value;
    let thumbnail = document.getElementById("thumbnail").files[0];
    let photos = images;
    let variantStyle = document.getElementsByClassName("variant-style");
    let variantSize = document.getElementsByClassName("variant-size");
    let variantQuantity = document.getElementsByClassName("variant-quantity");
    let variantPhoto = document.getElementsByClassName("variant-photo");
    let gender = document.getElementById("gender").value;
    var data = new FormData();
    data.append("title", title);
    data.append("category", category);
    data.append("barcode", barcode);
    data.append("standCost", standCost);
    data.append("listPrice", listPrice);
    data.append("description", description);
    data.append("thumbnail", thumbnail);
    data.append("gender", gender);
    for (let i = 0; i < photos.length; i++) {
      data.append("photos", photos[i]);
    }
    for (let i = 0; i < imageData.length; i++) {
      data.append("photosOld", imageData[i].value);
    }
    for (let i = 0; i < variantIdOld.length; i++) {
      let idVariantsOld = Number(variantIdOld[i].value);
      let quantityVariantsOld = Number(variantQuantityOld[i].value);
      if (quantityVariantsOld < 1) {
        error = true;
        break;
      }
      data.append("idVariantsOld", idVariantsOld);
      data.append("quantityVariantsOld", quantityVariantsOld);
    }

    for (let i = 0; i < variantStyle.length; i++) {
      let colorVariants = Number(variantStyle[i].value);
      let sizeVariants = Number(variantSize[i].value);
      let quantityVariants = Number(variantQuantity[i].value);
      let photoVariants = variantPhoto[i].files[0];
      if (
        isNaN(colorVariants) ||
        isNaN(sizeVariants) ||
        quantityVariants === 0 ||
        photoVariants === undefined
      ) {
        error = true;
        break;
      }
      data.append("colorVariants", colorVariants);
      data.append("sizeVariants", sizeVariants);
      data.append("quantityVariants", quantityVariants);
      data.append("photoVariants", photoVariants);
    }
    if (error) {
      swal({
        title: "LỖI BIẾN THỂ SẢN PHẨM",
        text: "Hãy kiểm tra đảm bảo rằng tất cả biến thể đều được chọn màu sắc, kích cỡ, số lượng và hình ảnh.",
        icon: "error",
        button: "OK",
      });
    } else {
      swal({
        text: "Đang xử lý...",
      });
      ProductService.updateProduct(id, data).then((response) => {
        if (response.status === "OK") {
          swal({
            title: "Thành công",
            text: response.message,
            icon: "success",
            button: "OK",
          });
          navigate("/management/products");
        } else {
          swal({
            title: "Lỗi",
            text: response.message,
            icon: "error",
            button: "OK",
          });
        }
      });
    }
  };
  return (
    <div className=" md:m-10 rounded-3xl">
      <div className="flex justify-between items-center mb-3">
        <Header title={data.title} category="Chỉnh sửa thông tin sản phẩm" />
      </div>
      <div className="flex justify-center items-center">
        <div className="w-full">
          <div>
            <div className="mt-1 md:col-span-2 md:mt-0">
              <form
                className="sm:overflow-hidden sm:rounded-md"
                onSubmit={getData}
              >
                <div className="space-y-3">
                  <div className="px-6 py-8 rounded-xl border border-gray-200 flex flex-col gap-2 bg-white ">
                    <div>
                      <h1 className="font-bold mb-1">Thông tin cơ bản</h1>
                      <label
                        htmlFor="title"
                        className="block text-sm font-medium text-gray-700"
                      >
                        Tiêu đề
                        <div className="mt-1">
                          <textarea
                            id="title"
                            name="title"
                            rows={3}
                            className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-indigo-500 sm:text-sm p-2 focus:outline-none`}
                            placeholder="Tên sản phẩm"
                            defaultValue={data.title}
                            required
                            onChange={(e) => {
                              slug.current.value =
                                String(e.target.value)
                                  .normalize("NFD")
                                  .replace(/[\u0300-\u036f]/g, "") //remove diacritics
                                  .toLowerCase()
                                  .replace(/\s+/g, "-") //spaces to dashes
                                  .replace(/&/g, "-and-") //ampersand to and
                                  .replace(/[^\w\-]+/g, "") //remove non-words
                                  .replace(/\-\-+/g, "-") //collapse multiple dashes
                                  .replace(/^-+/, "") //trim starting dash
                                  .replace(/-+$/, "") + ".html";
                            }}
                          />
                        </div>
                      </label>
                    </div>
                    <div>
                      <label className="block text-sm font-medium text-gray-700">
                        Loại sản phẩm
                        <select
                          id="category"
                          className={`mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-[${currentColor}] focus:ring-[${currentColor}] focus:outline-none sm:text-sm`}
                          required
                        >
                          {categoryData.map((item) => {
                            return (
                              <option
                                key={item.id}
                                value={item.id}
                                selected={data.categoryId === item.id}
                              >
                                {item.name}
                              </option>
                            );
                          })}
                        </select>
                      </label>
                    </div>
                    <div>
                      <label className="block text-sm font-medium text-gray-700">
                        Giới tính
                        <select
                          id="gender"
                          className={`mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-[${currentColor}] focus:ring-[${currentColor}] focus:outline-none sm:text-sm`}
                          required
                        >
                          <option value="MALE">Nam</option>
                          <option value="FEMALE">Nữ</option>
                          <option value="UNISEX">Cả nam và nữ</option>
                        </select>
                      </label>
                    </div>
                    <div>
                      <label
                        htmlFor="first-name"
                        className="block text-sm font-medium text-gray-700"
                      >
                        Barcode
                        <textarea
                          id="barcode"
                          name="barcode"
                          rows={1}
                          className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-indigo-500 sm:text-sm p-2 focus:outline-none`}
                          placeholder="Mã sản phẩm"
                          defaultValue={data.barcode}
                          ref={slug}
                          readOnly
                        />
                      </label>
                      <p className="text-sm text-gray-500">
                        Slug được hiển thị trên đường link của website.
                      </p>
                    </div>
                  </div>
                  <div className="bg-white flex flex-col shadow px-6 py-8 rounded-xl border border-gray-200 gap-2">
                    <h1 className="text-md font-bold mb-1">
                      Giá bán của sản phẩm
                    </h1>
                    <div>
                      <label>
                        Giá nhập
                        <input
                          id="standCost"
                          name="standCost"
                          type="number"
                          className={`mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-[${currentColor}] focus:outline-none sm:text-sm h-[50px] pl-2`}
                          placeholder="0"
                          defaultValue={data.standCost}
                          required
                          min={10000}
                          max={10000000}
                        />
                      </label>
                    </div>
                    <div>
                      <label>
                        Giá bán
                        <input
                          id="listPrice"
                          name="listPrice"
                          type="number"
                          className={`mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-[${currentColor}] focus:outline-none sm:text-sm h-[50px] pl-2`}
                          placeholder="0"
                          defaultValue={data.listPrice}
                          required
                          min={10001}
                          max={10000000}
                        />
                      </label>
                    </div>
                    <p className="mt-2 text-sm text-gray-500">
                      Đơn vị tiền tệ sử dụng hiện tại là VNĐ.
                    </p>
                  </div>
                  <div className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white">
                    <h1 className="text-md font-bold mb-1">Mô tả</h1>
                    <label className="block text-sm font-medium text-gray-700">
                      Thông tin mô tả tổng quát về sản phẩm
                      <div className="mt-1">
                        <RichTextEditorComponent
                          height={600}
                          ref={descriptionRef}
                          value={data.description}
                        >
                          <Inject
                            services={[
                              HtmlEditor,
                              Toolbar,
                              Image,
                              QuickToolbar,
                            ]}
                          />
                        </RichTextEditorComponent>
                      </div>
                    </label>
                    <p className="mt-2 text-sm text-gray-500">
                      Bạn có thể mô tả thông tin chi về sản phẩm để hiển thị
                      trong phần mô tả.
                    </p>
                  </div>
                  <div className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white">
                    <label className="block text-md font-bold text-gray-700">
                      Hình thu nhỏ - Một hình ảnh
                    </label>
                    <div className="flex items-center space-x-6 py-2">
                      <label className="block">
                        <span className="sr-only">Chọn file</span>
                        <input
                          type="file"
                          className="block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100"
                          id="thumbnail"
                          onChange={(event) => {
                            thumbnailRef.current.src = URL.createObjectURL(
                              event.target.files[0]
                            );
                          }}
                        />
                      </label>
                    </div>
                    <div className="shrink-0 mt-2">
                      <img
                        className="h-60 w-60 object-cover rounded-xl"
                        src={data.productImage}
                        alt=""
                        ref={thumbnailRef}
                      />
                    </div>
                    <p className="mt-4 text-sm text-gray-500">
                      Hình ảnh này sẽ được hiển thị đầu tiên cho khách hàng.
                    </p>
                  </div>
                  <div className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white">
                    <label className="block text-md font-bold text-gray-700">
                      Hình ảnh chi tiết - Nhiều hình ảnh
                    </label>
                    <div className="flex items-center space-x-6 py-2">
                      <label className="block">
                        <span className="sr-only">Chọn file</span>
                        <input
                          type="file"
                          className="block w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100"
                          multiple="multiple"
                          id="photos"
                          onChange={(event) => {
                            setImages((old) => {
                              return [...old, ...event.target.files];
                            });
                          }}
                        />
                      </label>
                    </div>
                    <div className="flex gap-[50px] flex-wrap mt-2 mx-auto px-10">
                      {imageData.map((item) => {
                        return (
                          <img
                            src={item.value}
                            key={item.id}
                            alt=""
                            className="w-[435px] h-[435px] object-cover rounded-xl"
                            onClick={() => {
                              swal({
                                title: "Bạn có muốn xóa hình này không?",
                                text: "Khi xóa hình chưa khi chưa lưu thì bạn có thể khôi phục bằng cách tải lại trang",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                              }).then((result) => {
                                if (result) {
                                  setImageData((prev) => {
                                    return prev.filter((e) => e !== item);
                                  });
                                }
                              });
                            }}
                          />
                        );
                      })}
                      {images.map((item, index) => {
                        return (
                          <img
                            src={URL.createObjectURL(item)}
                            key={index}
                            alt=""
                            className="w-[435px] h-[435px] object-cover rounded-xl"
                            onClick={() => {
                              swal({
                                title: "Bạn có muốn xóa hình này không?",
                                text: "Khi xóa hình chưa khi chưa lưu thì bạn có thể khôi phục bằng cách tải lại trang",
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                              }).then((result) => {
                                if (result) {
                                  setImages((prev) => {
                                    return prev.filter((e) => e !== item);
                                  });
                                }
                              });
                            }}
                          />
                        );
                      })}
                    </div>
                    <p className="mt-6 text-sm text-gray-500">
                      Những hình ảnh này sẽ được hiển thị trong phần chi tiết
                      của sản phẩm mỗi khi người dùng click vào sản phẩm này.
                    </p>
                  </div>
                </div>
                <div
                  className="shadow px-6 py-8 rounded-xl border border-gray-200 bg-white my-3 w-full"
                  id="style-form"
                >
                  <div className="flex justify-between border-b">
                    <h1 className="text-md font-bold mb-2">
                      Kiểu dáng, màu sắc
                    </h1>
                  </div>
                  <div className="w-full">
                    {variants.map((e, index) => {
                      return (
                        <div key={index} className="border-b">
                          <div className="flex justify-start items-start gap-4">
                            <ColorVariant
                              color={e.colorId}
                              productId={id}
                              defaultImage={e.image}
                            />
                            <div className="flex w-2/5 justify-start item-start flex-col mt-5 gap-4">
                              {e?.sizeVariants?.map((element, i) => {
                                return (
                                  <SizeVariant
                                    sizes={sizes}
                                    size={element.sizeId}
                                    inventory={element.inventory}
                                    setSize={(value)=>{
                                      setVariants((prev) => {
                                        let newSizes = prev[index].sizeVariants[i].sizeId = value;
                                        prev = [...prev, {}];
                                        prev.pop();
                                        return prev
                                      });
                                    }}
                                    setInventory={(value)=>{
                                      setVariants((prev) => {
                                        let newSizes = prev[index].sizeVariants[i].inventory = value;
                                        return [...prev];
                                      });
                                    }}
                                    removeOnClick={() => {
                                      setVariants((prev) => {
                                        let newSizes = prev[index].sizeVariants.filter(value => value.sizeI !== element.sizeI);
                                        prev[index].sizeVariants = newSizes;
                                        return [...prev];
                                      });
                                    }}
                                    key={i}
                                  />
                                );
                              })}
                              <button
                                type="button"
                                className="block w-full mr-4 py-3 px-4 rounded-full border-0 text-sm font-semibold bg-violet-200 text-violet-700 hover:bg-violet-300 my-2"
                                onClick={() => {
                                  setVariants((prev) => {
                                    prev[index].sizeVariants.push({
                                      sizeI: sizeIndex.current++,
                                      sizeId: 30,
                                      inventory: 10,
                                    });
                                    return [...prev];
                                  });
                                }}
                              >
                                Thêm size
                              </button>
                            </div>
                          </div>
                          <div className="w-full justify-end flex">
                            <button
                              type="button"
                              className="hover:drop-shadow-lg flex justify-center items-center py-2 px-4 hover:bg-red-700 bg-red-500 text-white border mt-2 mb-4 rounded-md w-full"
                            >
                              Xóa biến thể
                            </button>
                          </div>
                        </div>
                      );
                    })}
                  </div>
                </div>
                <div>
                  <button
                    type="button"
                    className="block w-full mr-4 py-3 px-4 rounded-full border-0 text-sm font-semibold bg-violet-200 text-violet-700 hover:bg-violet-300 my-2"
                    onClick={()=>{
                      setVariants((prev) => {
                        prev.push({
                          colorId: 0,
                          productId: Number(id),
                          image: "",
                          sizeVariants: [],
                        })
                        return [...prev];
                      });
                    }}
                  >
                    Thêm biến thể
                  </button>
                </div>
                <div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6">
                  <Link
                    to="/management/products"
                    className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  >
                    Hủy
                  </Link>
                  <button
                    type="submit"
                    className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  >
                    Lưu
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ProductEditNew;
