import React, { useEffect, useState } from "react";
import { ColorService } from "../../../services/color.service";

const ColorVariant = ({
  productId,
  color,
  defaultImage,
}) => {
  const [colorData, setColorData] = useState([]);
  const [image, setImage] = useState(defaultImage);
  useEffect(() => {
    (async function () {
      const response = await ColorService.getAllColor();
      setColorData(response.details);
    })();
  }, []);
  return (
    <div className="w-1/2">
      <div className="py-4">
        <div className="col-span-2">
          <input type="hidden" className="variant-id-old" />
          <label className="block text-sm font-medium text-gray-700">
            Màu, kiểu dáng
            <select
              className="mt-1 w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm  h-10 focus:outline-none variant-style-old"
              disabled
            >
              <option disabled selected>
                Chọn kiểu dáng
              </option>
              {colorData.map((item) => {
                return (
                  <option
                    height="65"
                    key={item.id}
                    value={item.id}
                    className="color"
                    selected={color === item.id}
                  >
                    {item.name}
                  </option>
                );
              })}
            </select>
          </label>
        </div>
      </div>
      <div className=" grid grid-cols-4 gap-5 pb-4">
        <div className="col-span-3">
          <label className="block text-sm font-medium text-gray-700">
            Hình ảnh
          </label>
          <div className="shrink-0 mt-2">
            <img
              className="h-60 w-60 object-cover rounded-xl"
              src={image}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
};
export default ColorVariant;
