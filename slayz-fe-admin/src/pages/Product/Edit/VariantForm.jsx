import React, { useEffect, useRef, useState } from "react";
import { productDefault } from "../../../data/dummy";
import { ColorService } from "../../../services/color.service";
import { SizeService } from "../../../services/size.service";

const VariantForm = ({ id, closeEventHandle, colorId, sizeId, quantity, photo, colorBorder }) => {
  const image = useRef();
  const [colorData, setColorData] = useState([]);
  const [sizeData, setSizeData] = useState([]);
  useEffect(() => {
    let isFetchData = true;
    const fetchColorData = () => {
      ColorService.getAllColor().then((response) => {
        if (isFetchData) {
          setColorData(response.details);
        }
      });
    };
    const fetchSizeData = () => {
      SizeService.getAllSize().then((response) => {
        if (isFetchData) {
          setSizeData(response);
        }
      });
    };
    fetchColorData();
    fetchSizeData();
    return () => {
      isFetchData = false;
    };
  }, []);
  return (
    <div id={`variant-form-` + id} className="border-b-1 border-gray-200">
      <div className="grid grid-cols-4 gap-5 py-4">
        <div className="col-span-2">
          <input type="hidden" className="variant-id-old" value={id} />
          <label className="block text-sm font-medium text-gray-700">
            Màu, kiểu dáng
            <select
              className="mt-1 w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm  h-10 focus:outline-none variant-style-old"
              disabled
            >
              <option disabled selected>
                Chọn kiểu dáng
              </option>
              {colorData.map((item) => {
                return (
                  <option
                    height="65"
                    key={item.id}
                    value={item.id}
                    className="color"
                    selected={colorId === item.id}
                  >
                    {item.name}
                  </option>
                );
              })}
            </select>
          </label>
        </div>
        <div>
          <label className="block text-sm font-medium text-gray-700">
            Size
            <select
              className="mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm variant-size-old h-[41px]"
              disabled
            >
              <option disabled selected>
                Chọn size
              </option>
              {sizeData.map((item) => {
                return (
                  <option value={item.id} key={item.id} selected={sizeId === item.id}>
                    {item.name}
                  </option>
                );
              })}
            </select>
          </label>
        </div>
        <div>
          <label>
            Số lượng
            <input
              type="number"
              className={`block w-full border rounded-md border-gray-300 shadow-sm focus:border-[${colorBorder}] focus:ring-indigo-500 sm:text-sm pl-2 focus:outline-none variant-quantity-old h-[41px]`}
              placeholder="0"
              defaultValue={quantity}
              min={1}
              max={100000}
            />
          </label>
        </div>
      </div>
      <div className=" grid grid-cols-4 gap-5 pb-4">
        <div className="col-span-3">
          <label className="block text-sm font-medium text-gray-700">Hình ảnh</label>
          <div className="shrink-0 mt-2">
            <img
              className="h-60 w-60 object-cover rounded-xl"
              src={photo ? photo : productDefault}
              alt=""
              ref={image}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default VariantForm;
