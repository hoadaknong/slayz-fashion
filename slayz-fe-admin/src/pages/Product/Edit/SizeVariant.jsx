import React from "react";

const SizeVariant = ({
  size,
  inventory,
  removeOnClick,
  setInventory,
  setSize,
  sizes,
}) => {
  return (
    <div className="flex justify-start items-center gap-4 w-full">
      <label className="block text-sm font-medium text-gray-700 w-1/2">
        Size
        <select
          className="block w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm variant-size-old h-[41px]"
          onChange={(e) => {
            setSize(Number(e.target.value));
          }}
        >
          <option disabled selected>
            Chọn size
          </option>
          {sizes.map((item) => {
            return (
              <option value={item.id} key={item.id} selected={size === item.id}>
                {item.name}
              </option>
            );
          })}
        </select>
      </label>
      <label className="block text-sm font-medium text-gray-700 w-1/2">
        Số lượng
        <input
          type="number"
          className={`block w-full border rounded-md border-gray-300 shadow-sm focus:ring-indigo-500 sm:text-sm pl-2 focus:outline-none variant-quantity-old h-[41px]`}
          placeholder="0"
          defaultValue={inventory}
          min={1}
          max={100000}
          onChange={(e) => {
            setInventory(Number(e.target.value));
          }}
        />
      </label>
      <button
        type="button"
        className="hover:drop-shadow-lg flex justify-center items-center py-2 px-4 hover:bg-red-700 bg-red-500 text-white border mt-5 rounded-md"
        onClick={removeOnClick}
      >
        Xóa
      </button>
    </div>
  );
};

export default SizeVariant;
