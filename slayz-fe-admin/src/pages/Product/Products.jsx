import React from "react";
import {
  ColumnDirective,
  ColumnsDirective,
  ContextMenu,
  Edit,
  ExcelExport,
  Filter,
  GridComponent,
  Inject,
  Page,
  PdfExport,
  Resize,
  Search,
  Sort,
  Toolbar,
} from "@syncfusion/ej2-react-grids";

import { Header } from "../../components";
import { Link, useNavigate } from "react-router-dom";
import { useStateContext } from "../../contexts/ContextProvider";
import { ProductService } from "../../services/product.service";
import swal from "sweetalert";
import { AiOutlineFolderAdd } from "react-icons/ai";
import useProductFilter from "../../hooks/useProductFilter";
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Pagination,
  Select,
  TextField,
} from "@mui/material";
import Loading from "react-fullscreen-loading";

const Products = () => {
  document.title = "Sản Phẩm";
  const toolbarOptions = ["Search", "PdfExport", "ExcelExport"];
  const { currentColor } = useStateContext();
  const navigate = useNavigate();
  const {
    data,
    setMinPrice,
    setMaxPrice,
    page,
    setPage,
    setGender,
    maxPrice,
    minPrice,
    limit,
    setLimit,
    loading,
    name,
    setName,
    setLoading,
    applyOnClick,
    pageOnClick,
  } = useProductFilter();

  const searchOptions = {
    fields: ["title", "category"],
    ignoreCase: true,
    operator: "contains",
  };
  const gridEditProduct = (props) => (
    <div className="flex justify-center items-center gap-2">
      <Link
        style={{ background: "#00d084" }}
        className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
        to={`edit/${props.productId}`}
      >
        Sửa
      </Link>
      <button
        type="button"
        style={{ background: "#FF3333" }}
        className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
        onClick={() => {
          swal({
            title: "Bạn có muốn xóa?",
            text:
              "Khi xóa sản phẩm " +
              props.title +
              ", bạn sẽ không thể khôi phục dữ liệu!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((result) => {
            if (result) {
              const deleteOnClick = (id) => {
                ProductService.deleteProductById(id)
                  .then((response) => {
                    swal("Đã xóa sản phẩm " + props.title + "!", {
                      icon: "success",
                    }).then((r) => {
                      return r;
                    });
                    navigate("/management");
                    navigate("/management/products");
                  })
                  .catch((error) => {
                    swal("Đã xảy ra lỗi", {
                      icon: "error",
                    }).then((r) => {
                      return r;
                    });
                  });
              };
              deleteOnClick(props.productId);
            }
          });
        }}
      >
        Xóa
      </button>
    </div>
  );
  const productGridImage = (props) => (
    <div className="flex justify-start items-center gap-2">
      <img
        className="rounded-lg w-[150px]"
        src={props.productImage}
        alt="product"
      />
      <div className="max-w-[350px]">
        <p className="uppercase truncate hover:text-clip">{props.title}</p>
      </div>
    </div>
  );
  const productHeader = (props) => {
    return (
      <div>
        <p className=" text-gray-600 text-[15px] uppercase">
          {props.headerText}
        </p>
      </div>
    );
  };
  const productGrid = [
    {
      field: "title",
      headerText: "Sản phẩm",
      headerTextAlign: "Left",
      template: productGridImage,
      textAlign: "Center",
      headerTemplate: productHeader,
      width: "350",
    },
    {
      field: "category",
      headerText: "Phân loại",
      width: "90",
      textAlign: "Left",
      headerTemplate: productHeader,
    },
    {
      field: "standCost",
      headerText: "Giá nhập",
      width: "90",
      textAlign: "Left",
      headerTemplate: productHeader,
    },
    {
      field: "listPrice",
      headerText: "Giá bán",
      width: "90",
      textAlign: "Left",
      headerTemplate: productHeader,
    },
    {
      field: "inventory",
      headerText: "Trong kho",
      width: "100",
      textAlign: "Left",
      headerTemplate: productHeader,
    },
    {
      field: "barcode",
      headerText: "Barcode",
      width: "100",
      textAlign: "Left",
      headerTemplate: productHeader,
    },
    {
      field: "id",
      headerText: "Hành động",
      template: gridEditProduct,
      textAlign: "Center",
      width: "120",
      headerTemplate: productHeader,
    },
  ];
  return (
    <div className="my-4 mx-8 rounded-3xl h-fit flex flex-col justify-between items-center">
      <Loading
        loading={loading}
        background="rgba(0, 0, 0, 0.33)"
        loaderColor="rgba(0, 167, 255, 1)"
      />
      <div className="w-full flex justify-between items-center">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Sản phẩm
            </div>
          }
          title="Sản phẩm"
        />
        <Link
          to="/management/products/new"
          style={{
            backgroundColor: currentColor,
            color: "white",
          }}
          className="font-semibold hover:drop-shadow rounded-full px-6 py-3 flex gap-2 justify-center items-center"
        >
          <AiOutlineFolderAdd
            style={{ color: "white", fontSize: "20px" }}
            color="white"
          />
          <span>Thêm Sản Phẩm</span>
        </Link>
      </div>
      <div className="flex items-center justify-start gap-4 w-full bg-white rounded-xl my-3 p-3 py-5">
        {/*  Nơi chứa bộ lọc*/}
        <h1 className="py-2 px-5 border flex justify-center items-center rounded">
          Bộ lọc
        </h1>
        <TextField
          id="outlined-basic"
          size="small"
          label="Tên sản phẩm"
          variant="outlined"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
        />
        <TextField
          id="outlined-basic"
          size="small"
          label="Giá dưới"
          variant="outlined"
          type="number"
          variant="outlined"
          value={minPrice}
          onChange={(e) => {
            setMinPrice(Number(e.target.value));
          }}
        />
        <TextField
          id="outlined-basic"
          size="small"
          label="Giá trên"
          variant="outlined"
          type="number"
          value={maxPrice}
          onChange={(e) => {
            setMaxPrice(Number(e.target.value));
          }}
        />
        <Box sx={{ minWidth: 200 }}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label-page-size">
              Hiển thị
            </InputLabel>
            <Select
              labelId="demo-simple-select-label-page-size"
              id="demo-simple-select-page-size"
              size="small"
              value={limit}
              label="Hiển thị"
              onChange={(e) => {
                setLimit(Number(e.target.value));
              }}
            >
              <MenuItem value={"5"}>5</MenuItem>
              <MenuItem value={"10"}>10</MenuItem>
              <MenuItem value={"15"}>15</MenuItem>
              <MenuItem value={"20"}>20</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <button
          style={{
            backgroundColor: currentColor,
            color: "white",
          }}
          className="hover:drop-shadow rounded-lg px-6 py-2 flex gap-2 justify-center items-center"
          type="button"
          onClick={async () => {
            await applyOnClick();
          }}
        >
          Áp dụng
        </button>
      </div>
      <div className="p-10 bg-white rounded-3xl">
        <GridComponent
          dataSource={data?.details || []}
          toolbar={toolbarOptions}
          searchSettings={searchOptions}
          allowPdfExport={true}
          allowExcelExport={true}
        >
          <ColumnsDirective>
            {productGrid.map((item, index) => (
              <ColumnDirective key={index} {...item} />
            ))}
          </ColumnsDirective>
          <Inject
            services={[
              Resize,
              Sort,
              ContextMenu,
              Filter,
              Page,
              ExcelExport,
              Edit,
              PdfExport,
              Toolbar,
              Search,
            ]}
          />
        </GridComponent>
      </div>
      <div className="p-4 w-full flex justify-end">
        <Pagination
          count={data?.totalPage}
          size="large"
          color="primary"
          page={page}
          onChange={async (event, page) => {
            await pageOnClick(Number(page));
          }}
        />
      </div>
    </div>
  );
};

export default Products;
