/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { GoPrimitiveDot } from "react-icons/go";

import { Header } from "../components";
import { GlobalUtil } from "../utils/GlobalUtil";
import { MdOutlineSupervisorAccount } from "react-icons/md";
import { BsBoxSeam } from "react-icons/bs";
import { FiBarChart } from "react-icons/fi";
import { HiOutlineRefresh } from "react-icons/hi";
import useStatisticData from "../hooks/useStatisticData";
import { Pie } from "../components";

import {
  ChartComponent,
  SeriesCollectionDirective,
  SeriesDirective,
  Inject,
  LineSeries,
  DateTime,
  Tooltip,
  Category, DataLabel, Legend
} from "@syncfusion/ej2-react-charts";
import { LinePrimaryXAxisSlayZ, LinePrimaryYAxisSlayZ } from "../data/dummy";
import { useEffect } from "react";
import { UserService } from "../services/user.service";
import { useNavigate } from "react-router-dom";
const Ecommerce = () => {
  document.title = "Ecommerce";
  const navigate = useNavigate();
  const {
    revenue,
    totalUser,
    totalQuantityProduct,
    totalInvoice,
    totalSaleProduct,
    lineSeriesData,
    pieChartInvoice,
  } = useStatisticData();
  useEffect(() => {
    UserService.getCurrentUser().then((response) => {
      if (response.status !== "OK") {
        navigate("/login");
      }
    });
  }, []);
  const legendSettings = { visible: true };
  const marker = { dataLabel: { visible: true } };
  const tooltip = { enable: true, shared: false };
  return (
    <div className="flex flex-col justify-center gap-3 items-start">
      <div className="w-full px-6 mt-3">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div> Bảng điều khiển
            </div>
          }
          title="Trang Chủ"
        />
      </div>
      <div className="flex flex-wrap lg:flex-nowrap justify-start w-full pl-4">
        <div className="bg-white dark:text-gray-200 border dark:bg-secondary-dark-bg h-44 rounded-xl w-full lg:w-80 p-8 pt-9 m-3 bg-no-repeat bg-gradient-to-r from-green-300 to-blue-300 hover:drop-shadow-md transition-all">
          <div className="flex justify-between items-center">
            <div className="flex flex-col gap-1">
              <p className="font-bold text-gray-600 dark:text-white uppercase">Doanh thu</p>
              <p className="text-2xl text-white font-bold">
                {GlobalUtil.numberWithCommas(revenue)} <span className="underline">đ</span>
              </p>
            </div>
          </div>
        </div>
        <div className="flex m-3 flex-wrap justify-start gap-6 items-center">
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md">
            <button
              type="button"
              style={{
                color: "#03C9D7",
                backgroundColor: "#E5FAFB",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <MdOutlineSupervisorAccount />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">{totalUser}</span>
              <span className={`text-sm text-[#19bcc5] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <p className="text-sm text-gray-500 mt-1 uppercase">Người dùng</p>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md">
            <button
              type="button"
              style={{
                color: "rgb(255, 244, 229)",
                backgroundColor: "rgb(254, 201, 15)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <BsBoxSeam />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(totalQuantityProduct)}
              </span>
              <span className={`text-sm text-[#fdc913] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <p className="text-sm text-gray-500 mt-1 uppercase">Sản phẩm</p>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md">
            <button
              type="button"
              style={{
                color: "rgb(228, 106, 118)",
                backgroundColor: "rgb(255, 244, 229)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <FiBarChart />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(totalInvoice)}
              </span>
              <span className={`text-sm text-[#d4707a] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <p className="text-sm text-gray-500 mt-1 uppercase">Đơn hàng</p>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md">
            <button
              type="button"
              style={{
                color: "rgb(0, 194, 146)",
                backgroundColor: "rgb(235, 250, 242)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <HiOutlineRefresh />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(totalSaleProduct)}
              </span>
              <span className={`text-sm text-[#1cb587] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <p className="text-sm text-gray-500 mt-1 uppercase">Đã bán</p>
          </div>
        </div>
      </div>
      <div className="flex flex-wrap justify-start w-full gap-5 mb-10">
        <div className=" bg-white dark:text-gray-200 border text-gray-500 dark:bg-secondary-dark-bg p-10 rounded-2xl md:w-2/3 hover:drop-shadow-md w-full ml-7">
          <div className="flex justify-center items-center uppercase font-semibold">
            Đơn hàng 7 ngày gần đây
          </div>
          <ChartComponent
            id="charts"
            primaryXAxis={LinePrimaryXAxisSlayZ}
            primaryYAxis={LinePrimaryYAxisSlayZ}
            tooltip={tooltip}
            legendSettings={legendSettings}
          >
            <Inject services={[DataLabel, LineSeries, DateTime, Tooltip, Category,Legend]} />
            <SeriesCollectionDirective>
              <SeriesDirective {...lineSeriesData[0]}  marker={marker}/>;
            </SeriesCollectionDirective>
          </ChartComponent>
        </div>
        <div className="w-[29%] bg-white rounded-2xl border dark:bg-secondary-dark-bg p-10 dark:text-gray-200 text-gray-500">
          <div className="flex justify-center items-center uppercase font-semibold">
            Tổng quan đơn hàng
          </div>
          <Pie id="chart-pie" data={pieChartInvoice} legendVisiblity height="full" />
        </div>
      </div>
    </div>
  );
};

export default Ecommerce;
