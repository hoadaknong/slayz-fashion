import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { api } from "../../api/axios";
import { GlobalUtil } from "../../utils/GlobalUtil";

const ModalAdd = ({ open, onClose, title, status }) => {
  const [nameCheck, setNameCheck] = useState(false);
  const [descriptionCheck, setDescriptionCheck] = useState(false);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  useEffect(() => {
    let isFetchData = true;
    return () => {
      isFetchData = false;
    };
  }, []);
  if (!open) {
    return null;
  }
  const downloadFileOnClick = async () => {
    if (startDate === null) {
      swal("Hãy chọn ngày bắt đầu!", {
        icon: "warning",
      });
    }
    if (endDate === null) {
      swal("Hãy chọn ngày kết thúc!", {
        icon: "warning",
      });
    }
    try {
      const response = await api.post(
        `api/v1/invoice/export_xlsx_range_date`,
        { status, startDate: startDate, endDate: endDate },
        {
          responseType: "blob",
          headers: {
            Authorization: JSON.stringify(localStorage.getItem("token")),
          },
        }
      );
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute(
        "download",
        `invoice_${status}_${GlobalUtil.dateTimeConvert(Date.now())}.xlsx`
      );
      document.body.appendChild(link);
      link.click();
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-1/4 my-0 mx-auto max-w-3xl">
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <form
              action="#"
              className="flex w-full flex-col justify-center items-center rounded-xl border border-gray-200 relative bg-white"
            >
              <div className="flex justify-center gap-2 bg-gray-50 py-2 mb-2 text-right sm:px-6 w-full">
                <h1 className="py-4 font-bold uppercase">
                  XUÂT DANH MỤC HÓA ĐƠN {title}
                </h1>
              </div>

              <div className="w-3/4">
                <label
                  htmlFor="title"
                  className="block text-sm font-medium text-gray-700"
                >
                  <h1 className="font-bold mb-1">Ngày bắt đầu</h1>
                  <div className="mt-1">
                    <input
                      className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                      type="datetime-local"
                      value={startDate}
                      onChange={(e) => {
                        setStartDate(e.target.value);
                      }}
                    />
                  </div>
                </label>
              </div>
              <div className="w-3/4">
                <label
                  htmlFor="title"
                  className="block text-sm font-medium text-gray-700"
                >
                  <h1 className="font-bold mb-1">Ngày kết thúc</h1>
                  <div className="mt-1">
                    <input
                      className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                      type="datetime-local"
                      value={endDate}
                      onChange={(e) => {
                        setEndDate(e.target.value);
                      }}
                    />
                  </div>
                </label>
              </div>
              <div className="flex justify-end gap-2 bg-gray-50 py-3 text-right sm:px-6 w-full mt-4">
                <button
                  className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-6 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  onClick={() => {
                    setDescriptionCheck(false);
                    setNameCheck(false);
                    onClose();
                  }}
                >
                  Hủy
                </button>
                <button
                  onClick={downloadFileOnClick}
                  type="button"
                  className="inline-flex justify-center rounded-md border border-transparent bg-blue-500 py-3 px-6 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                >
                  Xuất
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default ModalAdd;
