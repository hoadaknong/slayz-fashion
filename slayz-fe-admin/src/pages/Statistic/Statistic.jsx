/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import { GoPrimitiveDot } from "react-icons/go";

import { Header, Pie } from "../../components";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { MdOutlineSupervisorAccount } from "react-icons/md";
import { BsBoxSeam } from "react-icons/bs";
import { FiBarChart } from "react-icons/fi";
import { HiOutlineRefresh } from "react-icons/hi";
import useStatisticData from "../../hooks/useStatisticData";
import { api } from "../../api/axios";

import {
  Category,
  ChartComponent,
  DataLabel,
  DateTime,
  Inject,
  Legend,
  LineSeries,
  SeriesCollectionDirective,
  SeriesDirective,
  Tooltip,
} from "@syncfusion/ej2-react-charts";
import { TbFileInvoice } from "react-icons/tb";
import { LinePrimaryXAxisSlayZ, LinePrimaryYAxisSlayZ } from "../../data/dummy";
import useInvoiceData from "../../hooks/useInvoiceData";
import { InvoiceService } from "../../services/invoice.service";
import { SiMicrosoftexcel } from "react-icons/si";
import ModalAdd from "./ModalAdd";
import { AiOutlineSetting } from "react-icons/ai";

const Statistic = () => {
  const {
    revenue,
    totalUser,
    totalQuantityProduct,
    totalInvoice,
    totalSaleProduct,
    lineSeriesData,
    pieChartInvoice,
  } = useStatisticData();
  const {
    invoicesPending,
    invoicesCompleted,
    invoicesCancelled,
    invoicesInProgress,
    setInvoicesPending,
    setInvoicesCompleted,
    setInvoicesCancelled,
    setInvoicesInProgress,
  } = useInvoiceData(null, null);
  const [isAll, setIsAll] = useState(false);
  const [isPending, setIsPending] = useState(false);
  const [isCancelled, setIsCancelled] = useState(false);
  const [isCompleted, setIsCompleted] = useState(false);
  const [isInProgress, setIsInProgress] = useState(false);
  useEffect(() => {
    (async function () {
      const pending = await InvoiceService.getAllInvoiceByStatus(
        "PENDING",
        1,
        100000000
      );
      const inProgress = await InvoiceService.getAllInvoiceByStatus(
        "IN PROGRESS",
        1,
        100000000
      );
      const cancelled = await InvoiceService.getAllInvoiceByStatus(
        "CANCELLED",
        1,
        100000000
      );
      const completed = await InvoiceService.getAllInvoiceByStatus(
        "COMPLETED",
        1,
        100000000
      );
      setInvoicesPending(pending.data.details);
      setInvoicesCompleted(inProgress.data.details);
      setInvoicesCancelled(cancelled.data.details);
      setInvoicesInProgress(completed.data.details);
    })();
  }, []);
  const legendSettings = { visible: true };
  const marker = { dataLabel: { visible: true } };
  const tooltip = { enable: true, shared: false };
  const downloadFileOnClick = async (status = "all") => {
    try {
      const response = await api.get(
        `api/v1/invoice/export_xlsx/status/${status}`,
        {
          responseType: "blob",
          headers: {
            Authorization: JSON.stringify(localStorage.getItem("token")),
          },
        }
      );
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute(
        "download",
        `invoice_${status}_${GlobalUtil.dateTimeConvert(Date.now())}.xlsx`
      );
      document.body.appendChild(link);
      link.click();
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="flex flex-col justify-center gap-3 items-start">
      <div id="modal-category">
        <ModalAdd
          open={isAll}
          onClose={() => {
            setIsAll(false);
          }}
          title={"tất cả"}
          status={"all"}
        />
        {isAll && (
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        )}
        <ModalAdd
          open={isPending}
          onClose={() => {
            setIsPending(false);
          }}
          title={"chưa xử lý"}
          status={"pending"}
        />
        {isPending && (
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        )}
        <ModalAdd
          open={isInProgress}
          onClose={() => {
            setIsInProgress(false);
          }}
          title={"đã xử lý"}
          status={"IN PROGRESS"}
        />
        {isInProgress && (
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        )}
        <ModalAdd
          open={isCompleted}
          onClose={() => {
            setIsCompleted(false);
          }}
          title={"đã giao"}
          status={"COMPLETED"}
        />
        {isCompleted && (
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        )}
        <ModalAdd
          open={isCancelled}
          onClose={() => {
            setIsCancelled(false);
          }}
          title={"đã hủy"}
          status={"CANCELLED"}
        />
        {isCancelled && (
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        )}
      </div>
      <div className="w-full px-6 mt-3">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Thống kê
            </div>
          }
          title="Thống kê"
        />
      </div>
      <div className="flex flex-wrap lg:flex-nowrap justify-start w-full pl-4">
        <div className="bg-white dark:text-gray-200 border dark:bg-secondary-dark-bg h-44 rounded-xl w-full lg:w-1/5 p-8 pt-9 m-3 bg-no-repeat bg-gradient-to-r from-green-300 to-blue-300 hover:drop-shadow-md transition-all">
          <div className="flex justify-between items-center">
            <div className="flex flex-col gap-1">
              <p className="font-bold text-gray-600 dark:text-white uppercase">
                Doanh thu
              </p>
              <p className="text-2xl text-white font-bold">
                {GlobalUtil.numberWithCommas(revenue)}{" "}
                <span className="underline">đ</span>
              </p>
            </div>
          </div>
        </div>
        <div className="flex m-3 flex-wrap justify-start gap-6 items-center w-full md:w-4/5">
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "#03C9D7",
                backgroundColor: "#E5FAFB",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <MdOutlineSupervisorAccount />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">{totalUser}</span>
              <span className={`text-sm text-[#19bcc5] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <p className="text-sm text-gray-500 mt-1 uppercase">Người dùng</p>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "rgb(255, 244, 229)",
                backgroundColor: "rgb(254, 201, 15)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <BsBoxSeam />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(totalQuantityProduct)}
              </span>
              <span className={`text-sm text-[#fdc913] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <p className="text-sm text-gray-500 mt-1 uppercase">Sản phẩm</p>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "rgb(228, 106, 118)",
                backgroundColor: "rgb(255, 244, 229)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <FiBarChart />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(totalInvoice)}
              </span>
              <span className={`text-sm text-[#d4707a] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <div className="w-full flex justify-between items-center">
              <p className="text-sm text-gray-500 mt-1 uppercase">Đơn hàng</p>
              <div className="flex gap-2">
                <button
                  className="w-[100px] px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    await downloadFileOnClick("all");
                  }}
                >
                  Tải File
                  <SiMicrosoftexcel className="text-green-700" />
                </button>
                <button
                  className="px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    setIsAll(true);
                  }}
                >
                  <AiOutlineSetting className="text-green-700 text-lg" />
                </button>
              </div>
            </div>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "rgb(0, 194, 146)",
                backgroundColor: "rgb(235, 250, 242)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <HiOutlineRefresh />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(totalSaleProduct)}
              </span>
              <span className={`text-sm text-[#1cb587] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <p className="text-sm text-gray-500 mt-1 uppercase">Đã bán</p>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "rgb(255, 244, 229)",
                backgroundColor: "rgb(254, 201, 15)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <TbFileInvoice />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(invoicesPending.length)}
              </span>
              <span className={`text-sm text-[#fdc913] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <div className="w-full flex justify-between items-center">
              <p className="text-sm text-gray-500 mt-1 uppercase">
                Đơn chưa xử lý
              </p>
              <div className="flex gap-2">
                <button
                  className="w-[100px] px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    await downloadFileOnClick("pending");
                  }}
                >
                  Tải File
                  <SiMicrosoftexcel className="text-green-700" />
                </button>
                <button
                  className="px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    setIsPending(true);
                  }}
                >
                  <AiOutlineSetting className="text-green-700 text-lg" />
                </button>
              </div>
            </div>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "rgb(255, 244, 229)",
                backgroundColor: "rgb(3, 169, 244)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <TbFileInvoice />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(invoicesInProgress.length)}
              </span>
              <span className={`text-sm text-[#03a9f4] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <div className="w-full flex justify-between items-center">
              <p className="text-sm text-gray-500 mt-1 uppercase">
                Đơn đã xử lý
              </p>
              <div className="flex gap-2">
                <button
                  className="w-[100px] px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    await downloadFileOnClick("IN PROGRESS");
                  }}
                >
                  Tải File
                  <SiMicrosoftexcel className="text-green-700" />
                </button>
                <button
                  className="px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    setIsInProgress(true);
                  }}
                >
                  <AiOutlineSetting className="text-green-700 text-lg" />
                </button>
              </div>
            </div>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "rgb(255, 244, 229)",
                backgroundColor: "rgb(98, 204, 102)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <TbFileInvoice />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(invoicesCompleted.length)}
              </span>
              <span className={`text-sm text-[#4bc750] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <div className="w-full flex justify-between items-center">
              <p className="text-sm text-gray-500 mt-1 uppercase">
                đơn Đã giao
              </p>
              <div className="flex gap-2">
                <button
                  className="w-[100px] px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    await downloadFileOnClick("COMPLETED");
                  }}
                >
                  Tải File
                  <SiMicrosoftexcel className="text-green-700" />
                </button>
                <button
                  className="px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    setIsCompleted(true);
                  }}
                >
                  <AiOutlineSetting className="text-green-700 text-lg" />
                </button>
              </div>
            </div>
          </div>
          {/*  */}
          <div className="bg-white dark:text-gray-200 h-44 text-gray-500 dark:bg-secondary-dark-bg md:w-[290px] px-4 pt-6 rounded-2xl border hover:drop-shadow-md w-1/2">
            <button
              type="button"
              style={{
                color: "rgb(255, 244, 229)",
                backgroundColor: "rgb(248, 96, 85)",
              }}
              className="text-2xl opacity-0.9 p-4 rounded-full hover:drop-shadow-xl"
            >
              <TbFileInvoice />
            </button>
            <p className="mt-3 flex justify-start items-center">
              <span className="text-lg font-semibold">
                {GlobalUtil.numberWithCommas(invoicesCancelled.length)}
              </span>
              <span className={`text-sm text-[#f44336] ml-2`}>
                <GoPrimitiveDot />
              </span>
            </p>
            <div className="w-full flex justify-between items-center">
              <p className="text-sm text-gray-500 mt-1 uppercase">ĐƠn đã hủy</p>
              <div className="flex gap-2">
                <button
                  className="w-[100px] px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    await downloadFileOnClick("CANCELLED");
                  }}
                >
                  Tải File
                  <SiMicrosoftexcel className="text-green-700" />
                </button>
                <button
                  className="px-2 py-[6px] border rounded-md hover:shadow flex justify-between items-center text-md"
                  onClick={async () => {
                    setIsCancelled(true);
                  }}
                >
                  <AiOutlineSetting className="text-green-700 text-lg" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-wrap justify-start w-full gap-5 mb-10">
        <div className=" bg-white dark:text-gray-200 border text-gray-500 dark:bg-secondary-dark-bg p-10 rounded-2xl md:w-2/3 hover:drop-shadow-md w-full ml-7">
          <div className="flex justify-center items-center uppercase font-semibold">
            Đơn hàng 7 ngày gần đây
          </div>
          <ChartComponent
            id="charts"
            primaryXAxis={LinePrimaryXAxisSlayZ}
            primaryYAxis={LinePrimaryYAxisSlayZ}
            tooltip={tooltip}
            legendSettings={legendSettings}
          >
            <Inject
              services={[
                DataLabel,
                LineSeries,
                DateTime,
                Tooltip,
                Category,
                Legend,
              ]}
            />
            <SeriesCollectionDirective>
              <SeriesDirective {...lineSeriesData[0]} marker={marker} />;
            </SeriesCollectionDirective>
          </ChartComponent>
        </div>
        <div className="w-[29%] bg-white rounded-2xl border dark:bg-secondary-dark-bg p-10 dark:text-gray-200 text-gray-500">
          <div className="flex justify-center items-center uppercase font-semibold">
            Tổng quan đơn hàng
          </div>
          <Pie
            id="chart-pie"
            data={pieChartInvoice}
            legendVisiblity
            height="full"
          />
        </div>
      </div>
    </div>
  );
};
export default Statistic;
