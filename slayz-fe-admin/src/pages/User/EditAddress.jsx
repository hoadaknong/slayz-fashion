import React, { useEffect, useRef, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Header } from "../../components";
import Select from "react-select";
import { useStateContext } from "../../contexts/ContextProvider";
import { AddressService } from "../../services/address.service";
import useLocationForm from "./useLocationForm";
import swal from "sweetalert";

const EditAddress = () => {
  const { id } = useParams();
  const [address, setAddress] = useState();
  const { state, onCitySelect, onDistrictSelect, onWardSelect } = useLocationForm(true, id);

  const addressLine = useRef();
  const fullName = useRef();
  const phone = useRef();

  const {
    cityOptions,
    districtOptions,
    wardOptions,
    selectedCity,
    selectedDistrict,
    selectedWard,
  } = state;
  const [province, setProvince] = useState(selectedCity);
  const [district, setDistrict] = useState(selectedDistrict);
  const [ward, setWard] = useState(selectedWard);
  const { currentColor } = useStateContext();
  const fetchAddress = () => {
    AddressService.getAddressById(id).then((response) => {
      if (response.status === "OK") {
        setAddress(response.data);
        addressLine.current.value = response.data?.addressLine;
        fullName.current.value = response.data?.fullName;
        phone.current.value = response.data?.phone;
      }
    });
  };
  const navigate = useNavigate();
  const onSave = () => {
    swal({
      text: "Đang xử lý...",
    });
    const createData = {
      fullName: fullName.current.value,
      phone: phone.current.value,
      addressLine: addressLine.current.value,
      province: province?.label ? province?.label : selectedCity?.label,
      district: district?.label ? district?.label : selectedDistrict?.label,
      ward: ward?.label ? ward?.label : selectedWard?.label,
      provinceId: province?.value ? province?.value : selectedCity?.value,
      districtId: district?.value ? district?.value : selectedDistrict?.value,
      wardId: ward?.value ? ward?.value : selectedWard?.value,
      userId: address.userId,
    };
    AddressService.updateAddress(id, createData).then((response) => {
      if (response.status === "OK") {
        swal({
          title: "Chỉnh sửa địa chỉ",
          text: "Chỉnh sửa địa chỉ thành công!",
          icon: "success",
          button: "OK",
        });
        navigate(-1);
      } else {
        swal({
          title: "Lỗi",
          text: response.message,
          icon: "error",
          button: "OK",
        });
      }
    });
  };
  useEffect(() => {
    let isFetch = true;
    if (isFetch) {
      fetchAddress();
    }
    return () => {
      isFetch = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <div className=" md:m-10 p-1 md:p-10 bg-white rounded-3xl">
        <div className="flex justify-between items-center mb-4">
          <Header title={`Chỉnh sửa địa chỉ`} category="Chỉnh sửa thông tin địa chỉ" />
        </div>
        <div className="flex justify-center items-center bg-gray-50 ">
          <div className="w-full">
            <div>
              <div className="md:col-span-2 md:mt-0">
                <div className="sm:overflow-hidden sm:rounded-md">
                  <div className="space-y-6 bg-white rounded-x">
                    <div>
                      <div className="py-2 flex flex-col gap-2 ">
                        <div>
                          <label className="block text-sm font-medium text-gray-700 w-full">
                            <h1 className="font-bold mb-1 text-[18px]">Tỉnh/Thành phố</h1>
                            <div className="inline-block relative w-full">
                              <Select
                                name="cityId"
                                key={`cityId_${selectedCity?.value}`}
                                isDisabled={cityOptions.length === 0}
                                options={cityOptions}
                                onChange={(option) => {
                                  setProvince(option);
                                  onCitySelect(option);
                                }}
                                placeholder="Tỉnh/Thành"
                                defaultValue={selectedCity}
                                className={`mt-1 w-full border rounded-md focus:outline-none text-[15px] border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] hover:border-[${currentColor}]`}
                              />
                            </div>
                          </label>
                        </div>
                        <div className="mt-2">
                          <label className="block text-sm font-medium text-gray-700 w-full">
                            <h1 className="font-bold mb-1 text-[18px]">Quận/Huyện</h1>
                            <div className="inline-block relative w-full">
                              <Select
                                name="districtId"
                                key={`districtId_${selectedDistrict?.value}`}
                                isDisabled={districtOptions.length === 0}
                                options={districtOptions}
                                onChange={(option) => {
                                  setDistrict(option);
                                  onDistrictSelect(option);
                                }}
                                placeholder="Quận/Huyện"
                                defaultValue={selectedDistrict}
                                className={`mt-1 w-full border rounded-md focus:outline-none text-[15px] border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] hover:border-[${currentColor}]`}
                              />
                            </div>
                          </label>
                        </div>
                        <div className="mt-2">
                          <label className="block text-sm font-medium text-gray-700 w-full">
                            <h1 className="font-bold mb-1 text-[18px]">Xã/Phường</h1>
                            <div className="inline-block relative w-full">
                              <Select
                                name="wardId"
                                key={`wardId_${selectedWard?.value}`}
                                isDisabled={wardOptions.length === 0}
                                options={wardOptions}
                                placeholder="Phường/Xã"
                                onChange={(option) => {
                                  setWard(option);
                                  onWardSelect(option);
                                }}
                                defaultValue={selectedWard}
                                className={`mt-1 w-full border rounded-md focus:outline-none text-[15px] border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] hover:border-[${currentColor}]`}
                                size="5"
                              />
                            </div>
                          </label>
                        </div>
                        <div className="mt-2">
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1 text-[18px]">Địa chỉ</h1>
                            <div className="mt-1">
                              <input
                                id="address"
                                name="address"
                                type="text"
                                ref={addressLine}
                                className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] p-[10px] hover:border-[${currentColor}] shadow`}
                                placeholder="Ví dụ: Số 1, Đường Võ Văn Ngân"
                              />
                            </div>
                          </label>
                        </div>
                        <div className="mt-2">
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1 text-[18px]">Họ và tên</h1>
                            <div className="mt-1">
                              <input
                                id="name"
                                name="name"
                                type="text"
                                ref={fullName}
                                className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] p-[10px] hover:border-[${currentColor}] shadow`}
                                placeholder="Ví dụ: Nguyễn Văn A"
                              />
                            </div>
                          </label>
                        </div>
                        <div className="mt-2">
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1 text-[18px]">Số điện thoại</h1>
                            <div className="mt-1">
                              <input
                                id="phone"
                                name="phone"
                                type="tel"
                                ref={phone}
                                className={`mt-1 w-full border rounded-md focus:outline-none text-[15px]  border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[15px] p-[10px] hover:border-[${currentColor}] shadow`}
                                placeholder="Ví dụ: 0388 891 635"
                              />
                            </div>
                          </label>
                        </div>
                      </div>
                      <div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6">
                        <Link
                          to="/management/categories"
                          className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                          onClick={() => {
                            navigate(-1);
                          }}
                        >
                          Hủy
                        </Link>
                        <button
                          type="button"
                          className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                          onClick={onSave}
                        >
                          Lưu
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditAddress;
