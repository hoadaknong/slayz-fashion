import React, { useEffect, useRef, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Header } from "../../components";
import { useStateContext } from "../../contexts/ContextProvider";
import { UserService } from "../../services/user.service";
import swal from "sweetalert";

const EditDetail = () => {
  const { id } = useParams();
  const { currentColor } = useStateContext();
  const [user, setUser] = useState();
  const navigate = useNavigate();
  const fullName = useRef();
  const email = useRef();
  const phone = useRef();
  const dob = useRef();
  const gender = useRef();
  const photo = useRef();
  const file = useRef();

  const onChangeFile = (e) => {
    photo.current.src = URL.createObjectURL(e.target.files[0]);
  };

  const onSave = () => {
    var data = new FormData();
    data.append("dob", dob.current.value);
    data.append("phone", phone.current.value);
    data.append("fullName", fullName.current.value);
    data.append("gender", gender.current.value);
    data.append("photo", file.current.files[0] ? file.current.files[0] : null);
    swal({
      text: "Đang xử lý...",
    });
    UserService.updateUser(id, data).then((response) => {
      if (response.status === "OK") {
        swal({
          title: "Chỉnh sửa thông tin",
          text: "Cập nhật thông tin người dùng thành công!",
          icon: "success",
          button: "OK",
        });
        navigate(-1);
      }
    });
  };
  const fetchUserData = () => {
    UserService.getUserById(id).then((response) => {
      if (response.status === "OK") {
        setUser(response.data);
        fullName.current.value = response.data?.fullName;
        email.current.value = response.data?.email;
        phone.current.value = response.data?.phone;
        dob.current.value = response.data?.dob;
        gender.current.value = response.data?.gender;
        photo.current.src = response.data?.photo;
      }
    });
  };
  useEffect(() => {
    let isFetch = true;
    if (isFetch) {
      fetchUserData();
      fullName.current.value = user?.fullName;
      email.current.value = user?.email;
      phone.current.value = user?.phone;
      dob.current.value = user?.dob;
      gender.current.value = user?.gender;
      photo.current.src = user?.photo;
    }

    return () => {
      isFetch = false;
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <div className="md:mx-10 mt-6 p-2 md:p-10 bg-white rounded-3xl flex-row h-fit">
        <div className="flex justify-between items-center mb-6">
          <Header title={`Thông tin người dùng`} category="Chỉnh sửa thông tin người dùng" />
        </div>
        <div className="flex justify-center items-center bg-gray-50 ">
          <div className="w-full">
            <div>
              <div className="md:col-span-2 md:mt-0">
                <div className="sm:overflow-hidden sm:rounded-md">
                  <div className="space-y-6 bg-white rounded-x">
                    <div className="flex">
                      <div className="w-1/3 flex justify-center flex-col items-center gap-7">
                        <img
                          className="w-[300px] h-[300px] object-contain rounded-full drop-shadow-xl"
                          src=""
                          ref={photo}
                          alt="image_profile"
                        />
                        <label
                          className={`transition-all duration-200 bg-[${currentColor}] rounded-full w-[200px] h-[50px] flex justify-center items-center text-white hover:scale-[1.03]`}
                        >
                          Chọn hình ảnh mới
                          <input
                            type="file"
                            className="hidden"
                            ref={file}
                            onChange={onChangeFile}
                          />
                        </label>
                      </div>
                      <div className="w-2/3">
                        <div className="py-2 flex flex-col gap-2 ">
                          <div>
                            <label className="block text-sm font-medium text-gray-700">
                              <h1 className="font-bold mb-1 text-[18px]">Họ và tên</h1>
                              <div className="mt-1">
                                <input
                                  id="name"
                                  name="name"
                                  type="text"
                                  ref={fullName}
                                  className={`mt-1 w-full border rounded-md focus:outline-none text-[20px]  border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[20px] p-4 hover:border-[${currentColor}]`}
                                  placeholder="Ví dụ: Nguyễn Văn A"
                                />
                              </div>
                            </label>
                          </div>
                          <div className="mt-2">
                            <label className="block text-sm font-medium text-gray-700">
                              <h1 className="font-bold mb-1 text-[18px]">Email</h1>
                              <div className="mt-1">
                                <input
                                  id="email"
                                  name="email"
                                  type="email"
                                  ref={email}
                                  className={`mt-1 w-full border rounded-md focus:outline-none text-[20px]  border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[20px] p-4 hover:border-[${currentColor}]`}
                                  placeholder="Ví dụ: nguyenvana@gmail.com"
                                  disabled
                                />
                              </div>
                            </label>
                          </div>
                          <div className="mt-2">
                            <label className="block text-sm font-medium text-gray-700">
                              <h1 className="font-bold mb-1 text-[18px]">Số điện thoại</h1>
                              <div className="mt-1">
                                <input
                                  id="phone"
                                  name="phone"
                                  type="tel"
                                  ref={phone}
                                  className={`mt-1 w-full border rounded-md focus:outline-none text-[20px]  border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[20px] p-4 hover:border-[${currentColor}]`}
                                  placeholder="Ví dụ: 0388 891 635"
                                />
                              </div>
                            </label>
                          </div>
                          <div className="flex gap-3 w-full mt-2">
                            <label className="block text-sm font-medium text-gray-700 w-1/2">
                              <h1 className="font-bold mb-1 text-[18px]">Ngày sinh</h1>
                              <div className="mt-1">
                                <input
                                  id="dob"
                                  name="dob"
                                  type="date"
                                  ref={dob}
                                  className={`mt-1 w-full border rounded-md focus:outline-none text-[20px]  border-gray-300 shadow-sm focus:border-[${currentColor}] sm:text-[20px] p-4 hover:border-[${currentColor}]`}
                                />
                              </div>
                              <p className="ml-1 mt-1 text-gray-400 text-sm">
                                Định dạng: Tháng/Ngày/Năm
                              </p>
                            </label>
                            <label className="block text-sm font-medium text-gray-700 w-1/2">
                              <h1 className="font-bold mb-1 text-[18px]">Giới tính</h1>
                              <div className="inline-block relative w-full">
                                <select
                                  className={`block appearance-none w-full bg-white border border-gray-300 p-4 mt-1 rounded leading-tight focus:outline-none h-[57px] focus:border-[${currentColor}] hover:border-[${currentColor}] text-[20px]`}
                                  name="gender"
                                  id="gender"
                                  ref={gender}
                                >
                                  <option value="Nam">Nam</option>
                                  <option value="Nữ">Nữ</option>
                                  <option value="Khác">Khác</option>
                                </select>
                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                  <svg
                                    className="fill-current h-4 w-4"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 20 20"
                                  >
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                                  </svg>
                                </div>
                              </div>
                            </label>
                          </div>
                        </div>
                        <div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6 mt-3">
                          <Link
                            to="/management/categories"
                            className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                            onClick={() => {
                              navigate(-1);
                            }}
                          >
                            Trở về
                          </Link>
                          <button
                            type="button"
                            className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                            onClick={onSave}
                          >
                            Lưu
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EditDetail;
