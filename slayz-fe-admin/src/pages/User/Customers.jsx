/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from "react";
import {
  ColumnDirective,
  ColumnsDirective,
  Filter,
  GridComponent,
  Inject,
  Page,
  Sort,
  Toolbar,
} from "@syncfusion/ej2-react-grids";

import { Header, LoadingSpinner } from "../../components";
import { UserService } from "../../services/user.service";
import { UserGrid } from "./UserGrid";
import { useStateContext } from "../../contexts/ContextProvider";
import { useNavigate } from "react-router-dom";
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Pagination,
  Select,
  TextField,
} from "@mui/material";
import Loading from "react-fullscreen-loading";

const Customers = () => {
  const { currentColor } = useStateContext();
  const [dataGrid, setDataGrid] = useState([]);
  const [tabIndex, setTabIndex] = useState(1);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(5);
  const [fullName, setFullName] = useState("");
  const [phone, setPhone] = useState("");
  const [gender, setGender] = useState("all");
  const [email, setEmail] = useState("");
  const [status, setStatus] = useState("all");
  const [totalPage, setTotalPage] = useState(10);
  const [isLoading, setLoading] = useState(true);
  const navigate = useNavigate();

  const fetchUserData = () => {
    (async function () {
      const response = await UserService.getAllUser(
        page,
        pageSize,
        fullName,
        phone,
        gender,
        email,
        status
      );
      if (response.status === "OK") {
        setDataGrid(response.data.details);
        setTotalPage(response.data.totalPage);
      } else {
        navigate("/login");
      }
    })();
    setLoading(false);
  };

  useEffect(() => {
    setPage(1);
    fetchUserData();
  }, [pageSize, fullName, phone, gender, email, status]);

  useEffect(() => {
    fetchUserData();
  }, [page]);

  return (
    <>
      {isLoading === true && <LoadingSpinner />}
      <div className="mx-6 mt-2 rounded-3xl h-fit px-3 flex justify-between items-center">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Bảng Điều Khiển{" "}
              <div className="w-1 h-1 rounded-full bg-gray-400"></div>
              Khách Hàng
            </div>
          }
          title="Danh sách người dùng"
        />
      </div>
      <div className="mt-4 mx-2 md:mx-5 md:mb-2 md:pb-10 md:px-10 pt-5 bg-white rounded-3xl">
        <div className="flex items-center justify-start gap-4 mb-2 mt-3">
          <h1 className="py-2 px-5 border flex justify-center items-center rounded">
            Bộ lọc
          </h1>
          <TextField
            id="outlined-basic"
            size="small"
            label="Họ và tên"
            variant="outlined"
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          />
          <TextField
            id="outlined-basic"
            size="small"
            label="Số điện thoại"
            variant="outlined"
            onChange={(e) => {
              setPhone(e.target.value);
            }}
          />
          <TextField
            id="outlined-basic"
            size="small"
            label="Email"
            variant="outlined"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <Box sx={{ minWidth: 200 }}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Giới tính</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={gender}
                size="small"
                label="Giới tính"
                onChange={(e) => {
                  setGender(e.target.value);
                }}
              >
                <MenuItem value={"all"}>Tất cả</MenuItem>
                <MenuItem value={"Nam"}>Nam</MenuItem>
                <MenuItem value={"Nữ"}>Nữ</MenuItem>
                <MenuItem value={"Khác"}>Khác</MenuItem>
              </Select>
            </FormControl>
          </Box>
          <Box sx={{ minWidth: 200 }}>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label-page-size">
                Hiển thị
              </InputLabel>
              <Select
                labelId="demo-simple-select-label-page-size"
                id="demo-simple-select-page-size"
                value={pageSize}
                size="small"
                label="Hiển thị"
                onChange={(e) => {
                  setPageSize(Number(e.target.value));
                }}
              >
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={15}>15</MenuItem>
                <MenuItem value={20}>20</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </div>
        <div className="border-b mb-4 border-gray-200 dark:border-gray-700">
          <ul
            className="flex flex-wrap text-sm font-medium text-center"
            id="myTab"
          >
            <li className="mr-2">
              <button
                className={
                  tabIndex === 1
                    ? `inline-block p-4 rounded-t-lg border-b-2 border-[${currentColor}] flex gap-2 justify-center items-center`
                    : `inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-[${currentColor}] dark:hover:text-gray-300 flex gap-2 justify-center items-center`
                }
                type="button"
                onClick={() => {
                  setTabIndex(1);
                  setStatus("all");
                }}
              >
                Tất cả
              </button>
            </li>
            <li className="mr-2">
              <button
                className={
                  tabIndex === 2
                    ? `inline-block p-4 rounded-t-lg border-b-2 border-[${currentColor}] flex gap-2 justify-center items-center`
                    : `inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-[${currentColor}] dark:hover:text-gray-300 flex gap-2 justify-center items-center`
                }
                onClick={() => {
                  setTabIndex(2);
                  setStatus("active");
                }}
                type="button"
              >
                Đã kích hoạt
              </button>
            </li>
            <li className="mr-2">
              <button
                className={
                  tabIndex === 3
                    ? `inline-block p-4 rounded-t-lg border-b-2 border-[${currentColor}] flex gap-2 justify-center items-center`
                    : `inline-block p-4 rounded-t-lg border-b-2 border-transparent hover:text-gray-600 hover:border-[${currentColor}] dark:hover:text-gray-300 flex gap-2 justify-center items-center`
                }
                onClick={() => {
                  setTabIndex(3);
                  setStatus("disable");
                }}
                type="button"
              >
                Vô hiệu hóa
              </button>
            </li>
          </ul>
        </div>
        <GridComponent dataSource={dataGrid}>
          <ColumnsDirective>
            {UserGrid.map((item, index) => (
              <ColumnDirective key={index} {...item} />
            ))}
          </ColumnsDirective>
          <Inject services={[Page, Toolbar, Sort, Filter]} />
        </GridComponent>
      </div>
      <div className="p-4 w-full flex justify-center">
        <Pagination
          count={totalPage}
          size="large"
          color="primary"
          page={page}
          onChange={(event, page) => setPage(page)}
        />
      </div>
    </>
  );
};

export default Customers;
