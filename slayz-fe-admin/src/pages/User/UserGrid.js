import { Link } from "react-router-dom";
import swal from "sweetalert";
import { useDataContext } from "../../contexts/DataProvider";
import { UserService } from "../../services/user.service";

const customerGridStatus = (props) => {
  let bg = "";
  let status = "";
  if (props.status === true) {
    bg = "#23c55e";
    status = "Đang kích hoạt";
  } else {
    bg = "red";
    status = "Đang khóa";
  }
  return (
    <div className="flex gap-2 justify-start items-center text-gray-700 capitalize">
      <p style={{ background: bg }} className="rounded-full h-3 w-3" />
      <p>{status}</p>
    </div>
  );
};

const customerGridImage = (props) => (
  <div className="image flex gap-4 py-3">
    <img
      className="object-contain rounded-full w-[40px] h-[40px] border border-gray-400"
      src={props.photo}
      alt="employee"
    />
    <div className="flex justify-center items-center">
      <Link to={`/management/customers/${props.id}`}>{props.fullName}</Link>
    </div>
  </div>
);

const UserAction = (props) => {
  const { setUserData } = useDataContext();
  const { user } = useDataContext();
  const disableOnClick = () => {
    if (props.id !== user.id) {
      swal({
        title: "Vô hiệu hóa",
        text: "Người dùng sẽ không thể truy cập vào hệ thống.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      }).then((result) => {
        if (result) {
          UserService.disableUser(props.id).then((response) => {
            if (response.status === "OK") {
              UserService.getAllUser().then((response) => {
                setUserData(response.data);
              });
              swal({
                title: "Vô hiệu hóa",
                text: "Đã vô hiệu hóa người dùng, hiện người dùng sẽ không thể đăng nhập vào hệ thống!",
                icon: "warning",
                button: "OK",
              });
            }
          });
        }
      });
    } else {
      swal({
        title: "Vô hiệu hóa",
        text: "Bạn không thể vô hiệu hóa chính mình!",
        icon: "warning",
        button: "OK",
      });
    }
  };
  const enableOnClick = () => {
    swal({
      title: "Kích hoạt",
      text: "Người dùng sẽ được phép truy cập vào hệ thống.",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then((result) => {
      if (result) {
        UserService.activeUser(props.id).then((response) => {
          UserService.getAllUser().then((response) => {
            setUserData(response.data);
          });
          swal({
            title: "Kích hoạt",
            text: "Đã kích hoạt người dùng, hiện người dùng có thể truy cập vào hệ thống bình thường!",
            icon: "success",
            button: "OK",
          });
        });
      }
    });
  };
  return (
    <div className="flex gap-2">
      {props.status === true ? (
        <button
          type="button"
          className={
            props.id !== user.id
              ? "w-[90px] h-[30px] rounded-lg bg-red-500 text-white"
              : "w-[90px] h-[30px] rounded-lg bg-gray-300 text-white"
          }
          onClick={disableOnClick}
        >
          Vô hiệu hóa
        </button>
      ) : (
        <button
          type="button"
          className="w-[90px] h-[30px] rounded-lg bg-green-500 text-white"
          onClick={enableOnClick}
        >
          Kích hoạt
        </button>
      )}
      <Link
        className="w-[90px] h-[30px] rounded-lg bg-blue-500 text-white flex justify-center items-center"
        type="button"
        to={`${props.id}`}
      >
        Chi tiết
      </Link>
    </div>
  );
};

export const UserGrid = [
  { field: "roleName", headerText: "Quyền", width: "100", textAlign: "Left" },
  {
    field: "fullName",
    headerText: "Người dùng",
    width: "200",
    template: customerGridImage,
    textAlign: "Left",
  },
  { field: "email", headerText: "Email", width: "200", textAlign: "Left" },
  { field: "dob", headerText: "Ngày sinh", width: "150", textAlign: "Center" },
  {
    field: "gender",
    headerText: "Giới tính",
    width: "100",
    textAlign: "Center",
  },
  { field: "phone", headerText: "Số ĐT", width: "150", textAlign: "Center" },
  {
    field: "status",
    headerText: "Trạng thái",
    width: "130",
    format: "yMd",
    textAlign: "Center",
    template: customerGridStatus,
  },
  // {
  //   field: "id",
  //   headerText: "ID",
  //   width: "60",
  //   textAlign: "Center",
  //   isPrimaryKey: true,
  // },
  {
    headerText: "Hành động",
    width: "200",
    template: UserAction,
    textAlign: "Center",
  },
];
