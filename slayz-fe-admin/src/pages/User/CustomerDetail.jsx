import React, { useEffect, useRef, useState } from "react";
import { AiOutlineMail, AiOutlinePhone } from "react-icons/ai";
import { FiEdit } from "react-icons/fi";
import { GrUserSettings } from "react-icons/gr";
import { HiOutlineStatusOffline, HiOutlineStatusOnline } from "react-icons/hi";
import { useNavigate, useParams } from "react-router-dom";
import swal from "sweetalert";
import { Header } from "../../components";
import { useStateContext } from "../../contexts/ContextProvider";
import { AddressService } from "../../services/address.service";
import { UserService } from "../../services/user.service";

const CustomerDetail = () => {
  const { id } = useParams();
  const { currentColor } = useStateContext();
  const [userData, setUserData] = useState();
  const [defaultAddress, setDefaultAddress] = useState();
  const [roles, setRoles] = useState([]);
  const navigate = useNavigate();
  const roleSelect = useRef();
  useEffect(() => {
    let isFetch = true;
    UserService.getAllRole().then((response) => {
      if (isFetch) {
        setRoles(response.data?.filter((e) => e.name !== "ROLE_ADMIN"));
        UserService.getUserById(id).then((response) => {
          if (isFetch) {
            setUserData(response.data);
            roleSelect.current.value = response.data.roleId;
          }
        });
      }
    });

    AddressService.getDefaultAddressByUserId(id).then((response) => {
      if (isFetch) {
        setDefaultAddress(response.data);
      }
    });
    return () => {
      isFetch = false;
    };
  }, [id]);
  const editDetailHandle = (id) => {
    navigate("/management/customers/detail/edit/" + id);
  };
  const editAddressHandle = (addressId) => {
    if (defaultAddress === null) {
      navigate("/management/customers/address/new/" + userData?.id);
    } else {
      navigate("/management/customers/address/edit/" + addressId);
    }
  };
  return (
    <>
      <div className="md:mx-10 mt-6 p-2 md:p-10 bg-white rounded-3xl flex flex-wrap gap-5 flex-row h-fit">
        <div className="mb-3 w-full">
          <Header category="Quản lý người dùng" title="Chi Tiết Người Dùng" />
        </div>
        <div className="flex w-1/4 rounded-lg border flex-col justify-center items-center">
          <div className="flex flex-col justify-center items-center w-full border-b py-4 gap-2">
            <div>
              <img
                className="object-cover rounded-full drop-shadow-lg w-[200px] h-[200px]"
                src={userData?.photo}
                alt="image_user"
              />
            </div>
            <div className="mt-3 flex flex-col justify-center items-center w-[90%]">
              <h1 className="font-bold text-gray-600 text-[26px] text-center">
                {userData?.fullName}
              </h1>
            </div>
            <div className="flex justify-center items-center gap-2">
              {userData?.status === true ? (
                <div className="mt-2 flex justify-center items-center gap-2 rounded-full w-[200px] h-[55px] border border-gray-200">
                  <div className="w-3 h-3 bg-green-500 rounded-full"></div>
                  <h1 className="">Đã kích hoạt</h1>
                </div>
              ) : (
                <div className="mt-2 flex justify-center items-center gap-2 rounded-full w-[200px] h-[55px] border border-gray-200">
                  <div className="w-3 h-3 bg-red-500 rounded-full"></div>
                  <h1 className="">Vô hiệu hóa</h1>
                </div>
              )}
            </div>
          </div>
          <div className="bg-[#f9fafc] w-full flex flex-col justify-center items-center py-7 h-full">
            <div className="w-fit flex flex-col gap-3">
              <div className="flex gap-2 justify-start items-center w-fit">
                <AiOutlineMail /> {userData?.email}
              </div>
              <div className="flex gap-2 justify-start items-center w-fit">
                <AiOutlinePhone /> {userData?.phone}
              </div>
            </div>
          </div>
        </div>
        <div className="flex w-[30%] flex-col gap-3">
          <div className="flex w-full rounded-lg border flex-col py-6 px-8 h-fit">
            <div className="flex justify-between items-center">
              <h1 className="text-gray-700 font-bold text-xl">
                Thông Tin Chi Tiết
              </h1>
              <button
                type="button"
                className="flex justify-center items-center w-9 h-9 transition-all duration-200 hover:text-white hover:bg-slate-400 rounded-full"
                onClick={() => editDetailHandle(id)}
              >
                <FiEdit className="" />
              </button>
            </div>
            <div className="flex flex-col gap-5 mt-6">
              <div className="flex justify-between">
                <h1 className="text-gray-400 font-semibold">Họ và tên</h1>
                <h1 className="text-gray-700 font-semibold">
                  {userData?.fullName}
                </h1>
              </div>
              <div className="flex justify-between">
                <h1 className="text-gray-400 font-semibold">Ngày sinh</h1>
                <h1 className="text-gray-700 font-semibold">{userData?.dob}</h1>
              </div>
              <div className="flex justify-between">
                <h1 className="text-gray-400 font-semibold">Số điện thoại</h1>
                <h1 className="text-gray-700 font-semibold">
                  {userData?.phone}
                </h1>
              </div>
              <div className="flex justify-between mb-2">
                <h1 className="text-gray-400 font-semibold">Giới tính</h1>
                <h1 className="text-gray-700 font-semibold">
                  {userData?.gender}
                </h1>
              </div>
            </div>
          </div>
          <div className="flex w-full rounded-lg border flex-col py-6 px-8 h-fit">
            <div className="flex justify-between items-center">
              <h1 className="text-gray-700 font-bold text-xl">
                Trạng Thái Tài Khoản
              </h1>
              {userData?.status === true ? (
                <HiOutlineStatusOnline />
              ) : (
                <HiOutlineStatusOffline />
              )}
            </div>
            <div className="flex flex-col gap-5 mt-6">
              <div className="flex justify-between">
                <h1 className="text-gray-400 font-semibold">Trạng thái</h1>
                <div className="flex justify-center items-center gap-2 pb-6">
                  {userData?.status === true ? (
                    <>
                      <span className="animate-ping h-3 w-3 rounded-full bg-green-700 opacity-75"></span>
                      <h1 className="text-gray-700 font-semibold">
                        Đã kích hoạt
                      </h1>
                    </>
                  ) : (
                    <>
                      <span className="animate-ping h-3 w-3 rounded-full bg-red-600 opacity-75"></span>
                      <h1 className="text-gray-700 font-semibold">
                        Vô hiệu hóa
                      </h1>
                    </>
                  )}
                </div>
              </div>
            </div>
            <div className="flex justify-end items-center gap-2">
              {userData?.status === true ? (
                <>
                  <button
                    disabled={userData?.roleId === 2}
                    type="button"
                    className={
                      userData?.roleId !== 2
                        ? "transition-all ease-in-out delay-50 duration-200 hover:drop-shadow-md hover:scale-[1.01] bg-red-500 text-white w-full h-[45px] rounded-[10px] mt-1"
                        : "transition-all ease-in-out delay-50 duration-200 hover:drop-shadow-md hover:scale-[1.01] bg-gray-300 text-white w-full h-[45px] rounded-[10px] mt-1"
                    }
                    onClick={() => {
                      swal({
                        title: "Vô hiệu hóa",
                        text:
                          "Người dùng " +
                          userData.fullName +
                          " sẽ không thể truy cập vào hệ thống.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                      }).then((result) => {
                        if (result) {
                          UserService.disableUser(id).then((response) => {
                            if (response.status === "OK") {
                              UserService.getUserById(id).then((response) => {
                                if (response.status === "OK") {
                                  setUserData(response.data);
                                }
                              });
                              swal({
                                title: "Vô hiệu hóa",
                                text: "Đã vô hiệu hóa người dùng, hiện người dùng sẽ không thể đăng nhập vào hệ thống!",
                                icon: "warning",
                                button: "OK",
                              });
                            }
                          });
                        }
                      });
                    }}
                  >
                    Vô hiệu hóa
                  </button>
                </>
              ) : (
                <>
                  <button
                    type="button"
                    className="transition-all ease-in-out delay-50 duration-200 hover:drop-shadow-md hover:scale-[1.01] bg-green-500 text-white w-full h-[45px] rounded-[10px] mt-1"
                    onClick={() => {
                      swal({
                        title: "Kích hoạt",
                        text:
                          "Người dùng " +
                          userData.fullName +
                          " sẽ truy cập vào hệ thống như bình thường.",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                      }).then((result) => {
                        if (result) {
                          UserService.activeUser(id).then((response) => {
                            if (response.status === "OK") {
                              UserService.getUserById(id).then((response) => {
                                if (response.status === "OK") {
                                  setUserData(response.data);
                                }
                              });
                              swal({
                                title: "Kích hoạt người dùng",
                                text: "Đã kích hoạt người dùng, hiện người dùng đã có thể sử dụng hệ thống bình thường!",
                                icon: "success",
                                button: "OK",
                              });
                            }
                          });
                        }
                      });
                    }}
                  >
                    Kích hoạt
                  </button>
                </>
              )}
            </div>
          </div>
        </div>
        <div className="flex w-[42%] flex-col gap-3">
          <div className="flex w-full rounded-lg border flex-col py-6 px-8 h-fit">
            <div className="flex justify-between items-center">
              <h1 className="text-gray-700 font-bold text-xl">Địa chỉ</h1>
              <button
                type="button"
                className="flex justify-center items-center w-9 h-9 transition-all duration-200 hover:text-white hover:bg-slate-400 rounded-full"
                onClick={() => editAddressHandle(defaultAddress?.id)}
              >
                <FiEdit className="" />
              </button>
            </div>
            <div className="flex flex-col gap-5 mt-6">
              <div className="flex justify-between">
                <h1 className="text-gray-400 font-semibold">Địa chỉ</h1>
                <h1 className="text-gray-700 font-semibold">
                  {defaultAddress?.addressLine}
                </h1>
              </div>
              <div className="flex justify-between">
                <h1 className="text-gray-400 font-semibold">Tỉnh/Thành phố</h1>
                <h1 className="text-gray-700 font-semibold">
                  {defaultAddress?.province}
                </h1>
              </div>
              <div className="flex justify-between">
                <h1 className="text-gray-400 font-semibold">Quận/Huyện</h1>
                <h1 className="text-gray-700 font-semibold">
                  {defaultAddress?.district}
                </h1>
              </div>
              <div className="flex justify-between mb-2">
                <h1 className="text-gray-400 font-semibold">Xã/Phường</h1>
                <h1 className="text-gray-700 font-semibold">
                  {defaultAddress?.ward}
                </h1>
              </div>
            </div>
          </div>
          <div className="flex w-full rounded-lg border flex-col py-6 px-8 h-fit">
            <div className="flex justify-between items-center">
              <h1 className="text-gray-700 font-bold text-xl">Phân Quyền</h1>
              <GrUserSettings />
            </div>
            <div className="flex flex-col gap-5 mt-5">
              <div className="inline-block relative w-full">
                <select
                  className={`block appearance-none w-full bg-white border border-gray-300 hover:border-[${currentColor}] px-4 py-2 pr-8 rounded leading-tight focus:outline-none focus:shadow-outline`}
                  name="role"
                  id="role"
                  ref={roleSelect}
                >
                  {userData?.roleId === 2 ? (
                    <option value="2">Quản trị viên</option>
                  ) : (
                    <>
                      {roles.map((e) => {
                        return (
                          <option value={e.id} key={e.id}>
                            {e.description}
                          </option>
                        );
                      })}
                    </>
                  )}
                </select>
                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                  <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                  >
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                  </svg>
                </div>
              </div>
            </div>
            <button
              disabled={userData?.roleId === 2}
              type="button"
              className={
                userData?.roleId !== 2
                  ? "transition-all ease-in-out delay-50 duration-200 hover:drop-shadow-md hover:scale-[1.01] bg-green-500 text-white w-full h-[45px] rounded-[10px] mt-4"
                  : "transition-all ease-in-out delay-50 duration-200 hover:drop-shadow-md hover:scale-[1.01] bg-gray-300 text-white w-full h-[45px] rounded-[10px] mt-4"
              }
              onClick={() => {
                const roleId = Number(roleSelect.current.value);
                UserService.updateRoleUser(id, roleId).then((response) => {
                  if (response.status === "OK") {
                    UserService.getUserById(id).then((response) => {
                      if (response.status === "OK") {
                        setUserData(response.data);
                        swal({
                          title: "Phân quyền",
                          text: "Cập nhật quyền người dùng thành công!",
                          icon: "success",
                          button: "OK",
                        });
                      }
                    });
                  }
                });
              }}
            >
              Cập nhật quyền
            </button>
          </div>
        </div>
      </div>
      <div className="flex justify-end px-10 py-5">
        <button
          className="w-[150px] h-[50px] rounded-full text-white transition-all hover:drop-shadow-md hover:scale-[1.01]"
          type="button"
          style={{ background: currentColor }}
          onClick={() => {
            navigate(-1);
          }}
        >
          Trở về
        </button>
      </div>
    </>
  );
};

export default CustomerDetail;
