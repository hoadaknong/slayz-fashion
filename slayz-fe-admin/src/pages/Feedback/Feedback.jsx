import React from "react";
import { Header, Rating } from "../../components";
import { styled } from "@mui/material/styles";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
} from "@syncfusion/ej2-react-grids";
import { useState } from "react";
import { useEffect } from "react";
import { FeedbackService } from "../../services/feedback.service";
import { Link, useNavigate } from "react-router-dom";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { Pagination } from "@mui/material";
import { StatisticService } from "../../services/statistic.service";
import { AiFillStar } from "react-icons/ai";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import useProductFilter from "../../hooks/useProductFilter";
import ProductFeedback from "./ProductFeedback";

const Feedback = () => {
  const [reviewList, setReviewList] = useState([]);
  const [pageR, setPageR] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const [reviewMetric, setReviewMetric] = useState({});
  const navigate = useNavigate();
  const {
    data,
    page,
    setPage,
    setMinPrice,
    setMaxPrice,
    setGender,
    maxPrice,
    minPrice,
    limit,
    setLimit,
    loading,
    name,
    setName,
    setLoading,
    applyOnClick,
    pageOnClick,
  } = useProductFilter();
  const productFeedbackTemplate = (props) => {
    return (
      <div className="flex gap-2">
        <img
          className="rounded-md w-[60px]"
          src={props.productImage}
          alt="product_image"
        />
        <div className="flex flex-col justify-center items-start">
          <div className="underline text-blue-500">
            <Link to={`/management/products/edit/${props.productId}`}>
              {props.productName}
            </Link>
          </div>
          <div className="">
            SIZE: {props.variantSize}, MÀU: {props.variantColor}
          </div>
        </div>
      </div>
    );
  };
  const customerFeedbackTemplate = (props) => {
    return (
      <div className="w-fit flex flex-col justify-center items-center gap-1 pt-1">
        <img
          className="object-cover w-[50px] h-[50px] rounded-full"
          src={props.userPhoto}
          alt="user_photo"
        />
        <div className="underline text-blue-500">
          <Link to={`/management/customers/${props.userId}`}>
            {props.fullName}
          </Link>
        </div>
        <div>{GlobalUtil.dateTimeConvert(props.createdDate)}</div>
      </div>
    );
  };
  const actionFeedback = (props) => {
    return (
      <div className="flex justify-center items-center">
        <button
          onClick={() => {
            navigate("/management/review/" + props.id);
          }}
          className="w-[100px] h-[50px] bg-blue-500 rounded-full text-white hover:shadow-md"
          type="button"
        >
          Chi tiết
        </button>
      </div>
    );
  };
  const ratingTemplate = (props) => {
    return (
      <div>
        <Rating value={props.rating} />
      </div>
    );
  };
  const feedbackGrid = [
    {
      field: "createdDate",
      headerText: "Khách hàng",
      template: customerFeedbackTemplate,
      width: "60",
      textAlign: "Left",
    },
    {
      headerText: "Sản phẩm",
      width: "120",
      template: productFeedbackTemplate,
      textAlign: "Left",
    },
    {
      field: "rating",
      headerText: "Rating",
      width: "90",
      template: ratingTemplate,
      textAlign: "Left",
    },
    {
      field: "comment",
      headerText: "Nội dung",
      width: "60",
      textAlign: "Left",
    },
    {
      headerText: "Hành động",
      width: "60",
      template: actionFeedback,
      textAlign: "Center",
    },
  ];
  useEffect(() => {
    FeedbackService.getAllFeedback(pageR, 5).then((response) => {
      if (response.status === "OK") {
        setReviewList(response.data.details);
        setTotalPage(response.data.totalPage);
      }
    });
  }, [pageR]);
  useEffect(() => {
    StatisticService.getReviewMetric().then((response) => {
      if (response.status === "OK") {
        setReviewMetric(response.data);
      }
    });
  }, [pageR]);
  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === "light" ? "#1a90ff" : "#308fe8",
    },
  }));
  return (
    <div className="my-4 mx-4 rounded-3xl h-fit px-3 flex justify-between items-center flex-col">
      <div className="w-full">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Đánh giá sản phẩm
            </div>
          }
          title="Đánh giá sản phẩm"
        />
      </div>
      <div className="p-1 md:p-10 bg-white rounded-3xl flex w-full flex-wrap gap-4 justify-between my-4 mx-7">
        <div className="w-full flex flex-wrap">
          <div className="flex flex-col justify-center items-center md:w-1/2 md:border-r-2 border-r-0 border-dotted w-full">
            <p className="font-bold text-lg">Đánh giá sản phẩm</p>
            <p className="text-[40px] font-bold">{reviewMetric?.avgVote}/5</p>
            <Rating value={reviewMetric?.avgVote} />
            <p>({reviewMetric?.reviewCount} đánh giá)</p>
          </div>
          <div className="md:w-1/2 flex justify-center items-center flex-col px-10 gap-2 w-full">
            <div className="w-full">
              <div className="flex justify-between">
                <div className="flex gap-1 justify-start items-center text-[20px]">
                  {1} <AiFillStar className="text-yellow-300" />
                </div>
                <div className="flex justify-end items-center">
                  {reviewMetric?.oneStar} đánh giá{" "}
                </div>
              </div>
              <BorderLinearProgress
                variant="determinate"
                value={
                  (parseInt(reviewMetric?.oneStar) /
                    parseInt(reviewMetric?.reviewCount)) *
                  100
                }
              />
            </div>
            <div className="w-full">
              <div className="flex justify-between">
                <div className="flex gap-1 justify-start items-center text-[20px]">
                  {2} <AiFillStar className="text-yellow-300" />
                </div>
                <div className="flex justify-end items-center">
                  {reviewMetric?.twoStar} đánh giá{" "}
                </div>
              </div>
              <BorderLinearProgress
                variant="determinate"
                value={
                  (parseInt(reviewMetric?.twoStar) /
                    parseInt(reviewMetric?.reviewCount)) *
                  100
                }
              />
            </div>
            <div className="w-full">
              <div className="flex justify-between">
                <div className="flex gap-1 justify-start items-center text-[20px]">
                  {3} <AiFillStar className="text-yellow-300" />
                </div>
                <div className="flex justify-end items-center">
                  {reviewMetric?.threeStar} đánh giá{" "}
                </div>
              </div>
              <BorderLinearProgress
                variant="determinate"
                value={
                  (parseInt(reviewMetric?.threeStar) /
                    parseInt(reviewMetric?.reviewCount)) *
                  100
                }
              />
            </div>
            <div className="w-full">
              <div className="flex justify-between">
                <div className="flex gap-1 justify-start items-center text-[20px]">
                  {4} <AiFillStar className="text-yellow-300" />
                </div>
                <div className="flex justify-end items-center">
                  {reviewMetric?.fourStar} đánh giá{" "}
                </div>
              </div>
              <BorderLinearProgress
                variant="determinate"
                value={
                  (parseInt(reviewMetric?.fourStar) /
                    parseInt(reviewMetric?.reviewCount)) *
                  100
                }
              />
            </div>
            <div className="w-full">
              <div className="flex justify-between">
                <div className="flex gap-1 justify-start items-center text-[20px]">
                  {5} <AiFillStar className="text-yellow-300" />
                </div>
                <div className="flex justify-end items-center">
                  {reviewMetric?.fiveStar} đánh giá{" "}
                </div>
              </div>
              <BorderLinearProgress
                variant="determinate"
                value={
                  (parseInt(reviewMetric?.fiveStar) /
                    parseInt(reviewMetric?.reviewCount)) *
                  100
                }
              />
            </div>
          </div>
        </div>
      </div>
      <div className="p-1 md:px-10 md:py-5 md:pb-8 bg-white rounded-3xl flex w-full flex-wrap gap-4 justify-between my-1 mx-7 divide-y">
        {data?.details.map((product, index) => {
          return (
            <ProductFeedback
              key={index}
              id={product.productId}
              name={product.title}
              image={product.productImage}
              listPrice={product.listPrice}
              standCost={product.standCost}
            />
          );
        })}
      </div>
      <div className="p-4 w-full flex justify-start">
        <Pagination
          count={data?.totalPage}
          size="large"
          color="primary"
          page={page}
          onChange={async (event, page) => {
            window.scroll(0, 0);
            await pageOnClick(Number(page));
          }}
        />
      </div>
      {/*<div className="p-1 md:p-10 bg-white rounded-3xl flex w-full flex-wrap gap-4 justify-between my-4 mx-7">*/}
      {/*  <GridComponent dataSource={reviewList} allowSorting>*/}
      {/*    <ColumnsDirective>*/}
      {/*      {feedbackGrid.map((item, index) => (*/}
      {/*        <ColumnDirective key={index} {...item} />*/}
      {/*      ))}*/}
      {/*    </ColumnsDirective>*/}
      {/*    <Inject*/}
      {/*      services={[*/}
      {/*        Resize,*/}
      {/*        Sort,*/}
      {/*        ContextMenu,*/}
      {/*        Filter,*/}
      {/*        Page,*/}
      {/*        ExcelExport,*/}
      {/*        Edit,*/}
      {/*        PdfExport,*/}
      {/*      ]}*/}
      {/*    />*/}
      {/*  </GridComponent>*/}
      {/*</div>*/}
      {/*<div className="p-4 w-full flex justify-end">*/}
      {/*  <Pagination*/}
      {/*    count={totalPage}*/}
      {/*    size="large"*/}
      {/*    color="primary"*/}
      {/*    page={pageR}*/}
      {/*    onChange={(event, page) => setPageR(page)}*/}
      {/*  />*/}
      {/*</div>*/}
    </div>
  );
};

export default Feedback;
