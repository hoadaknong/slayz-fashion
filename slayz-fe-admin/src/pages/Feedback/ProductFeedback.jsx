import React, { useEffect, useState } from "react";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { FeedbackService } from "../../services/feedback.service";
import { Rating } from "../../components";
import { AiFillStar, AiOutlineFolderAdd } from "react-icons/ai";
import { styled } from "@mui/material/styles";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { Link } from "react-router-dom";
import { useStateContext } from "../../contexts/ContextProvider";

const ProductFeedback = ({ id, name, image, listPrice, standCost }) => {
  const { currentColor } = useStateContext();
  const [avgRating, setAvgRating] = useState(0);
  const [overviewRating, setOverviewRating] = useState([]);
  useEffect(() => {
    FeedbackService.getAvgRatingByProductId(id).then((response) => {
      if (response.status === "OK") {
        setAvgRating(response.data);
        FeedbackService.getOverviewByProductId(id).then((response) => {
          if (response.status === "OK") {
            setOverviewRating(response.data);
          }
        });
      }
    });
  }, [id]);
  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === "light" ? "#1a90ff" : "#308fe8",
    },
  }));
  return (
    <div className="w-full pt-3 pb-3.5">
      <div className="flex justify-start items-start gap-3">
        <img className="rounded-lg w-[180px]" src={image} alt="product" />
        <div className="flex flex-col gap-3 w-full">
          <p className="underline mt-1">
            <Link
              to={`/management/products/edit/${id}`}
              onClick={() => {
                window.scroll(0, 0);
              }}
            >
              {name}
            </Link>
          </p>
          <div className="w-full flex">
            <div className="flex flex-col justify-center items-center md:w-1/2 md:border-r-2 border-r-0 border-dotted w-full">
              <p className="font-bold text-md">Đánh giá sản phẩm</p>
              <p className="text-[30px] font-bold">{avgRating}/5</p>
              <Rating value={avgRating} />
              <p>
                (
                {overviewRating?.length !== 0
                  ? overviewRating
                      .map(({ amountOfRating }) => amountOfRating)
                      .reduce((count, item) => count + item)
                  : 0}{" "}
                đánh giá)
              </p>
            </div>
            <div className="md:w-1/2 flex justify-center items-center flex-col px-10 gap-2 w-full">
              {overviewRating.map((item, index) => {
                return (
                  <div className="w-full" key={index}>
                    <div className="flex justify-between">
                      <div className="flex gap-1 justify-start items-center text-[20px]">
                        {item.rating} <AiFillStar className="text-yellow-300" />
                      </div>
                      <div className="flex justify-end items-center">
                        {item.amountOfRating} đánh giá
                      </div>
                    </div>
                    <BorderLinearProgress
                      value={item.percentage}
                      variant="determinate"
                    />
                  </div>
                );
              })}
            </div>
            <div className="flex justify-center items-center">
              <Link
                to={`/management/review/product/${id}`}
                style={{
                  backgroundColor: currentColor,
                  color: "white",
                }}
                className="font-semibold hover:drop-shadow rounded-full px-2 py-3 flex gap-2 justify-center items-center w-[100px]"
              >
                Chi tiết
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductFeedback;
