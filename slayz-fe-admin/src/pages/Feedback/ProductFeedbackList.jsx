import React, { useEffect, useState } from "react";
import { GlobalUtil } from "../../utils/GlobalUtil";
import { FeedbackService } from "../../services/feedback.service";
import { Header, Rating } from "../../components";
import { AiFillStar, AiOutlineFolderAdd } from "react-icons/ai";
import { styled } from "@mui/material/styles";
import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useStateContext } from "../../contexts/ContextProvider";
import { Pagination } from "@mui/material";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
} from "@syncfusion/ej2-react-grids";
import useReviewFilter from "../../hooks/useReviewFilter";

const ProductFeedbackList = () => {
  const { id } = useParams();
  const { currentColor } = useStateContext();
  const [avgRating, setAvgRating] = useState(0);
  const [overviewRating, setOverviewRating] = useState([]);
  const navigate = useNavigate();
  const { page, setPage, dataReview, setProductId } = useReviewFilter();
  const productFeedbackTemplate = (props) => {
    return (
      <div className="flex gap-2">
        <img
          className="rounded-md w-[60px]"
          src={props.productImage}
          alt="product_image"
        />
        <div className="flex flex-col justify-center items-start">
          <div className="underline text-blue-500">
            <Link to={`/management/products/edit/${props.productId}`}>
              {props.productName}
            </Link>
          </div>
          <div className="">
            SIZE: {props.variantSize}, MÀU: {props.variantColor}
          </div>
        </div>
      </div>
    );
  };
  const customerFeedbackTemplate = (props) => {
    return (
      <div className="w-fit flex flex-col justify-center items-center gap-1 pt-1">
        <img
          className="object-cover w-[50px] h-[50px] rounded-full"
          src={props.userPhoto}
          alt="user_photo"
        />
        <div className="underline text-blue-500">
          <Link to={`/management/customers/${props.userId}`}>
            {props.fullName}
          </Link>
        </div>
        <div>{GlobalUtil.dateTimeConvert(props.createdDate)}</div>
      </div>
    );
  };
  const actionFeedback = (props) => {
    return (
      <div className="flex justify-center items-center">
        <button
          onClick={() => {
            navigate("/management/review/" + props.id);
          }}
          className="w-[100px] h-[50px] bg-blue-500 rounded-full text-white hover:shadow-md"
          type="button"
        >
          Chi tiết
        </button>
      </div>
    );
  };
  const ratingTemplate = (props) => {
    return (
      <div>
        <Rating value={props.rating} />
      </div>
    );
  };
  const feedbackGrid = [
    {
      field: "createdDate",
      headerText: "Khách hàng",
      template: customerFeedbackTemplate,
      width: "60",
      textAlign: "Left",
    },
    {
      headerText: "Sản phẩm",
      width: "120",
      template: productFeedbackTemplate,
      textAlign: "Left",
    },
    {
      field: "rating",
      headerText: "Rating",
      width: "90",
      template: ratingTemplate,
      textAlign: "Left",
    },
    {
      field: "comment",
      headerText: "Nội dung",
      width: "60",
      textAlign: "Left",
    },
    {
      headerText: "Hành động",
      width: "60",
      template: actionFeedback,
      textAlign: "Center",
    },
  ];
  useEffect(() => {
    setProductId(id);
    FeedbackService.getAvgRatingByProductId(id).then((response) => {
      if (response.status === "OK") {
        setAvgRating(response.data);
        FeedbackService.getOverviewByProductId(id).then((response) => {
          if (response.status === "OK") {
            setOverviewRating(response.data);
          }
        });
      }
    });
  }, [id]);
  const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor:
        theme.palette.grey[theme.palette.mode === "light" ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === "light" ? "#1a90ff" : "#308fe8",
    },
  }));

  return (
    <div className="my-4 mx-4 rounded-3xl h-fit px-3 flex justify-between items-center flex-col">
      <div className="w-full">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Chi tiết đánh giá sản phẩm{" "}
              <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              {dataReview.reviewListData?.length > 0
                ? dataReview.reviewListData[0].productName
                : ""}
            </div>
          }
          title={"Chi tiết đánh giá sản phẩm"}
        />
      </div>
      <div className="flex justify-start items-start gap-5 w-full mt-4">
        <div className="flex flex-col gap-5 w-full">
          <div className="w-full flex bg-white p-3 rounded-2xl py-5">
            <div className="flex flex-col justify-center items-center md:w-1/2 md:border-r-2 border-r-0 border-dotted w-full">
              <p className="font-bold text-lg">Đánh giá sản phẩm</p>
              <p className="text-[40px] font-bold">{avgRating}/5</p>
              <Rating value={avgRating} />
              <p>
                (
                {overviewRating?.length !== 0
                  ? overviewRating
                      .map(({ amountOfRating }) => amountOfRating)
                      .reduce((count, item) => count + item)
                  : 0}{" "}
                đánh giá)
              </p>
            </div>
            <div className="md:w-1/2 flex justify-center items-center flex-col px-10 gap-2 w-full">
              {overviewRating.map((item, index) => {
                return (
                  <div className="w-full" key={index}>
                    <div className="flex justify-between">
                      <div className="flex gap-1 justify-start items-center text-[20px]">
                        {item.rating} <AiFillStar className="text-yellow-300" />
                      </div>
                      <div className="flex justify-end items-center">
                        {item.amountOfRating} đánh giá
                      </div>
                    </div>
                    <BorderLinearProgress
                      value={item.percentage}
                      variant="determinate"
                    />
                  </div>
                );
              })}
            </div>
          </div>
          <div className="w-full flex bg-white p-3 rounded-2xl py-2 flex-col px-5 pt-6">
            {dataReview?.reviewListData?.length > 0 ? (
              <>
                <GridComponent
                  dataSource={dataReview?.reviewListData}
                  allowSorting
                >
                  <ColumnsDirective>
                    {feedbackGrid.map((item, index) => (
                      <ColumnDirective key={index} {...item} />
                    ))}
                  </ColumnsDirective>
                  <Inject
                    services={[
                      Resize,
                      Sort,
                      ContextMenu,
                      Filter,
                      Page,
                      ExcelExport,
                      Edit,
                      PdfExport,
                    ]}
                  />
                </GridComponent>
                <div className="p-4 w-full flex justify-end">
                  <Pagination
                    count={dataReview?.totalPage}
                    size="large"
                    color="primary"
                    page={page}
                    onChange={(event, page) => setPage(page)}
                  />
                </div>
              </>
            ) : (
              <div className="text-center mb-3">Sản phẩm hiện tại chưa có đánh giá nào!</div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductFeedbackList;
