/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { Header, Rating } from "../../components";
import { FeedbackService } from "../../services/feedback.service";
import { useState } from "react";
import Swal from "sweetalert2";
import { GlobalUtil } from "../../utils/GlobalUtil";

const FeedbackDetail = () => {
  const { id } = useParams();
  const [feedbackData, setFeedbackData] = useState({});
  const fetchData = () => {
    FeedbackService.getFeedbackById(id).then((response) => {
      if (response.status === "OK") {
        setFeedbackData(response.data);
      }
    });
  };
  const disableReview = () => {
    Swal.fire({
      title: "Vô hiệu hóa chế độ hiển thị?",
      text: "Khi vô hiệu hóa thì đánh giá sản phẩm sẽ không được hiện trong trang của sản phẩm!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Vô hiệu hóa",
      cancelButtonText: "Hủy",
    }).then((result) => {
      if (result.isConfirmed) {
        FeedbackService.disableReview(id).then((response) => {
          if (response.status === "OK") {
            fetchData();
            Swal.fire(
              "Vô hiệu hóa!",
              "Đã vô hiệu hóa bài đánh giá này!",
              "success"
            );
          }
        });
      }
    });
  };

  const enableReview = () => {
    Swal.fire({
      title: "Kích hoạt chế độ hiển thị?",
      text: "Khi kích hoạt thì đánh giá sẽ được hiện trên trang của sản phẩm",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Kích hoạt",
      cancelButtonText: "Hủy",
    }).then((result) => {
      if (result.isConfirmed) {
        FeedbackService.enableReview(id).then((response) => {
          if (response.status === "OK") {
            fetchData();
            Swal.fire("Kích hoạt", "Đã kích hoạt bài đánh giá này!", "success");
          }
        });
      }
    });
  };
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="mx-6 my-4 rounded-3xl h-fit px-3 flex flex-col justify-between items-center gap-3">
      <div className="w-full flex justify-between items-center mb-4">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Chi tiết đánh giá sản phẩm
            </div>
          }
          title="Đánh giá"
        />
      </div>
      <div className="w-full gap-2 bg-white rounded-lg h-[70px] flex justify-between items-center px-3">
        <div className="flex justify-center items-center gap-3">
          <p className="text-[30px] font-bold">
            {Number(feedbackData.rating).toFixed(1)}
          </p>
          <Rating value={feedbackData.rating} />
        </div>
        {feedbackData.enable ? (
          <button
            type="button"
            className="w-[150px] bg-red-500 text-white rounded-md h-9 hover:shadow-md"
            onClick={disableReview}
          >
            Vô hiệu hóa
          </button>
        ) : (
          <button
            type="button"
            className="w-[150px] bg-green-500 text-white rounded-md h-9 hover:shadow-md"
            onClick={enableReview}
          >
            Kích hoạt
          </button>
        )}
      </div>
      <div className="w-full flex gap-2">
        <div className="w-1/4 bg-white flex flex-col justify-start items-start rounded-xl">
          <div className="p-6 font-bold uppercase border-b-1 w-full">
            Thông tin người dùng
          </div>
          <div className="w-full flex flex-col justify-center items-center gap-4 mt-[65px]">
            <img
              className="w-[160px] h-[160px] object-cover rounded-full"
              src={feedbackData.userPhoto}
              alt="user_photo"
            />
            <Link
              to={`/management/customers/${feedbackData.userId}`}
              className="underline text-blue-500"
            >
              {feedbackData.fullName}
            </Link>
            <div>{GlobalUtil.dateTimeConvert(feedbackData.createdDate)}</div>
          </div>
        </div>
        <div className="w-1/4 bg-white flex flex-col justify-start items-start rounded-xl">
          <div className="p-6 font-bold uppercase border-b-1 w-full">
            Nội dung đánh giá
          </div>
          <div className="p-6 font-normal italic">{feedbackData.comment}</div>
        </div>
        <div className="w-2/4 bg-white flex rounded-xl flex-col">
          <div className="p-6 font-bold uppercase border-b-1 w-full">
            Sản phẩm
          </div>
          <div className="flex justify-start items-center p-6 gap-3">
            <img
              className="w-[200px] object-cover rounded-xl"
              src={feedbackData.productImage}
              alt="product_photo"
            />
            <div className="flex flex-col py-4">
              <Link
                to={`/management/products/edit/${feedbackData.productId}`}
                className="underline text-blue-500"
              >
                {feedbackData.productName}
              </Link>
              <div className="">
                SIZE: {feedbackData.variantSize}, MÀU:{" "}
                {feedbackData.variantColor}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeedbackDetail;
