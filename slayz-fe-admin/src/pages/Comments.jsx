import React from "react";
import {
	GridComponent,
	ColumnsDirective,
	ColumnDirective,
	Resize,
	Sort,
	ContextMenu,
	Filter,
	Page,
	ExcelExport,
	PdfExport,
	Edit,
	Inject,
} from "@syncfusion/ej2-react-grids";
import { productsData, productGrid } from "../data/dummy";
import { Header } from "../components";

const Comments = () => {
	document.title = "Bình Luận";
	return (
		<div className=" md:m-10 p-1 md:p-10 bg-white rounded-3xl hover:drop-shadow-lg">
			<div className="flex justify-between items-center mb-6">
				<Header title="Bình Luận" category="Quản lý bình luận sản phẩm" />
			</div>

			<GridComponent
				id="gridcomp"
				dataSource={productsData}
				allowPaging
				allowSorting
				pageSettings={{ pageSize: 5 }}
			>
				<ColumnsDirective>
					{productGrid.map((item, index) => (
						<ColumnDirective key={index} {...item} />
					))}
				</ColumnsDirective>
				<Inject
					services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]}
				/>
			</GridComponent>
		</div>
	);
};

export default Comments;
