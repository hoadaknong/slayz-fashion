/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
} from "@syncfusion/ej2-react-grids";

import { ordersGrid } from "./OrderData";
import { Header } from "../../components";
import useInvoiceData from "../../hooks/useInvoiceData";
import { Pagination } from "@mui/material";
import Loading from "react-fullscreen-loading";

const Orders = ({ status }) => {
  document.title = "Đơn Hàng";
  const { page, setPage, invoiceList, totalPage, loading } = useInvoiceData(
    null,
    status
  );
  useEffect(() => {
    setPage(1);
  }, [status]);
  return (
    <div className="flex flex-col">
      <Loading
        loading={loading}
        background="rgba(0, 0, 0, 0.33)"
        loaderColor="rgba(0, 167, 255, 1)"
      />
      <div className="mb-6 mx-6 mt-3">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Tất cả đơn hàng
            </div>
          }
          title="Đơn hàng"
        />
      </div>
      {invoiceList.length !== 0 && (
        <div className="bg-white rounded-3xl p-10 mx-6">
          <GridComponent id="gridcomp" dataSource={invoiceList} allowSorting>
            <ColumnsDirective>
              {ordersGrid.map((item, index) => (
                <ColumnDirective key={index} {...item} />
              ))}
            </ColumnsDirective>
            <Inject
              services={[
                Resize,
                Sort,
                ContextMenu,
                Filter,
                Page,
                ExcelExport,
                Edit,
                PdfExport,
              ]}
            />
          </GridComponent>
        </div>
      )}
      {invoiceList.length !== 0 && (
        <div className="p-4 w-full flex justify-start">
          <Pagination
            count={totalPage}
            size="large"
            color="primary"
            page={page}
            onChange={(event, page) => setPage(page)}
          />
        </div>
      )}
      {invoiceList.length === 0 && (
        <div className="bg-white rounded-3xl p-10 mx-6 text-center py-[100px] text-lg">
          Không tìm thấy đơn hàng{" "}
          {status === null
            ? ""
            : status === "PENDING"
            ? "đợi xử lý!"
            : status === "IN PROGRESS"
            ? "đã xử lý!"
            : status === "COMPLETED"
            ? "đã hoàn thành!"
            : "đã hủy!"}
        </div>
      )}
    </div>
  );
};

export default Orders;
