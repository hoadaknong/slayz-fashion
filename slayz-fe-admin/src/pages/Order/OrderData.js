import { Link } from "react-router-dom";
import { GlobalUtil } from "../../utils/GlobalUtil";

export const gridOrderImage = (props) => (
  <div>
    <img className="rounded-xl h-20 md:ml-3" src={props.productImage} alt="order-item" />
  </div>
);

export const gridOrderstatus = (props) => {
  var bgColor;
  var status;
  if (props.status === "CANCELLED") {
    bgColor = "#ff472a";
    status = "ĐÃ HỦY";
  } else if (props.status === "PENDING") {
    bgColor = "#ffef00";
    status = "ĐỢI XÁC NHẬN";
  } else if (props.status === "IN PROGRESS") {
    bgColor = "#0693e3";
    status = "ĐÃ XỬ LÝ";
  } else {
    bgColor = "#18b81f";
    status = "HOÀN THÀNH";
  }
  return (
    <button
      type="button"
      style={{ background: bgColor }}
      className="text-white py-1 px-2 capitalize rounded-2xl text-md"
    >
      {status}
    </button>
  );
};

const gridPaymentStatus = (props) => {
  var bgColor;
  var status;
  if (props.paymentStatus === "PENDING") {
    bgColor = "#ffef00";
    status = "CHƯA THANH TOÁN";
  } else {
    bgColor = "#18b81f";
    status = "ĐÃ THANH TOÁN";
  }
  return (
    <button
      type="button"
      style={{ background: bgColor }}
      className="text-white py-1 px-2 capitalize rounded-2xl text-md"
    >
      {status}
    </button>
  );
};

const gridCurrency = (props) => (
  <div className="flex">
    <p className="">
      {GlobalUtil.numberWithCommas(props.total)} <span className="underline">đ</span>
    </p>
  </div>
);

const gridCurrencyPrice = (props) => (
  <div className="flex">
    <p className="">
      {GlobalUtil.numberWithCommas(props.price)} <span className="underline">đ</span>
    </p>
  </div>
);

const gridDetailOrder = (props) => (
  <div className="flex justify-center items-center gap-2">
    <Link
      to={`${props.id}`}
      style={{ background: "#00ff71" }}
      className="text-white font-semibold py-2 px-5 capitalize rounded-full text-md hover:drop-shadow-lg"
    >
      Chi tiết
    </Link>
  </div>
);

const gridPaymentMethod = (props) => {
  if (props.paymentMethod === "MOMO") {
    return (
      <div className="flex justify-start items-center gap-1">
        <img
          src="https://img.mservice.io/momo-payment/icon/images/logo512.png"
          alt="momo"
          className="w-[40px] h-[40px] object-cover"
        />
        {props.paymentMethod}
      </div>
    );
  }
  if (props.paymentMethod === "ATM") {
    return (
      <div className="flex justify-start items-center gap-1">
        <img
          src="https://cdn-icons-png.flaticon.com/512/8983/8983163.png"
          alt="momo"
          className="w-[40px] h-[40px] object-cover"
        />
        {props.paymentMethod}
      </div>
    );
  }
  if (props.paymentMethod === "VISA/MASTER/JCB") {
    return (
      <div className="flex justify-start items-center gap-1">
        <img
          src="https://logos-world.net/wp-content/uploads/2020/09/Mastercard-Logo.png"
          alt="momo"
          className="w-[40px] object-cover"
        />
        {"VISA/MASTER..."}
      </div>
    );
  }
  return (
    <div className="flex justify-start items-center gap-1">
      <img
        src="https://cdn-icons-png.flaticon.com/512/3812/3812106.png"
        alt="cod"
        className="w-[40px] h-[40px] object-cover"
      />
      {props.paymentMethod}
    </div>
  );
};

export const ordersGrid = [
  {
    headerText: "TRẠNG THÁI",
    template: gridOrderstatus,
    field: "status",
    textAlign: "Center",
    width: "120",
  },
  {
    field: "fullNameUser",
    headerText: "KHÁCH HÀNG",
    width: "150",
    textAlign: "Left",
  },
  {
    field: "total",
    headerText: "TỔNG TIỀN",
    template: gridCurrency,
    width: "90",
  },

  {
    field: "paymentMethod",
    headerText: "THANH TOÁN",
    template: gridPaymentMethod,
    width: "90",
    textAlign: "Left",
  },
  {
    field: "paymentStatus",
    headerText: "THANH TOÁN",
    width: "120",
    template: gridPaymentStatus,
    textAlign: "Left",
  },
  {
    field: "id",
    headerText: "MÃ ĐƠN",
    width: "70",
    textAlign: "Left",
  },

  {
    field: "address",
    headerText: "ĐỊA CHỈ",
    width: "150",
    textAlign: "Left",
  },
  {
    headerText: "HÀNH ĐỘNG",
    template: gridDetailOrder,
    textAlign: "Center",
    width: "120",
  },
];

const imageProductGrid = (props) => (
  <div className="flex gap-2 justify-start items-center">
    <img className="max-w-[120px]" src={props.image} alt="product" />
    <p className="uppercase">{props.name}</p>
  </div>
);

export const orderDetailGrid = [
  {
    field: "name",
    headerText: "SẢN PHẨM",
    template: imageProductGrid,
    width: "200",
    textAlign: "Left",
  },
  {
    field: "color",
    headerText: "KIỂU DÁNG",
    width: "150",
    textAlign: "Left",
  },
  {
    field: "size",
    headerText: "SIZE",
    width: "90",
    textAlign: "Left",
  },
  {
    field: "price",
    headerText: "ĐƠN GIÁ",
    template: gridCurrencyPrice,
    width: "90",
  },
  {
    field: "quantity",
    headerText: "SỐ LƯỢNG",
    width: "70",
    textAlign: "Left",
  },
  {
    field: "total",
    headerText: "TỔNG CỘNG",
    template: gridCurrency,
    width: "90",
  },
];
