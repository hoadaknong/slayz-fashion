/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { Link } from "react-router-dom";
import { Header } from "../../components";
import phoneIcon from "../../data/phone_icon.png";
import addressIcon from "../../data/address_icon.png";
import calendarIcon from "../../data/calendar_icon.png";
import { useStateContext } from "../../contexts/ContextProvider";
import { useParams } from "react-router-dom";
import { HiOutlineCurrencyDollar, HiOutlineStatusOnline } from "react-icons/hi";
import {
  MdProductionQuantityLimits,
  MdOutlinePayment,
  MdPermContactCalendar,
} from "react-icons/md";
import useInvoiceData from "../../hooks/useInvoiceData";
import { GlobalUtil } from "../../utils/GlobalUtil";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
} from "@syncfusion/ej2-react-grids";

import { orderDetailGrid } from "./OrderData";

const OrderDetail = () => {
  const { currentColor } = useStateContext();
  const { id } = useParams();
  const { invoice, invoiceDetailList, confirmOrder, cancelledOrder, completedOrder } =
    useInvoiceData(id);
  const widthIcon = "35px";
  const classContactElement = "flex gap-4 rounded-xl border px-3 py-3 bg-white";
  const countTotalQuantity = (list) => {
    let count = 0;
    if (list?.length > 0) {
      for (let i = 0; i < list?.length; i++) {
        count += list[i].quantity;
      }
    }
    return count;
  };
  return (
    <div>
      <div className="mx-7 my-4 flex justify-between items-center">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div> Đơn hàng
              <div className="w-1 h-1 rounded-full bg-gray-400"></div> Chi tiết đơn hàng
            </div>
          }
          title={`Chi tiết đơn hàng ${id}`}
        />
        <div className="flex gap-1">
          {invoice?.status === "PENDING" && (
            <button
              className="text-white rounded-md px-3 py-3 hover:drop-shadow-md transition-all hover:scale-[1.01]"
              type="button"
              style={{ background: currentColor }}
              onClick={() => confirmOrder()}
            >
              Xác nhận đơn hàng
            </button>
          )}
          {invoice?.status === "IN PROGRESS" && (
            <button
              className="text-white rounded-md px-3 py-3 hover:drop-shadow-md transition-all hover:scale-[1.01]"
              type="button"
              style={{ background: currentColor }}
              onClick={completedOrder}
            >
              Xác nhận đã nhận hàng
            </button>
          )}
          {invoice?.status !== "CANCELLED" && invoice?.status !== "COMPLETED" && (
            <button
              className="text-white rounded-md px-3 py-3 hover:drop-shadow-md transition-all hover:scale-[1.01]"
              type="button"
              style={{ background: "#f44336" }}
              onClick={cancelledOrder}
            >
              Hủy đơn hàng
            </button>
          )}
        </div>
      </div>
      <div className="mx-6 p-2 md:p-10 bg-white rounded-3xl">
        <div className="flex flex-col justify-center items-center">
          {/* Order information */}
          <div className="flex justify-start gap-4 text-gray-600 w-full">
            <div className="flex w-[450px] py-6 rounded-3xl border hover:drop-shadow-md bg-white">
              <div className="flex flex-col justify-center items-center w-full">
                <img
                  src={invoice?.userPhoto}
                  alt="user"
                  className="rounded-full w-[100px] border-4 border-white h-[100px] object-cover"
                />
                <div className="w-full flex flex-col justify-start py-3 gap-3 text-center">
                  <h1 className="font-bold text-center text-xl">{invoice?.fullNameUser}</h1>
                  <p>
                    <span className="font-bold text-center">Số điện thoại:</span>{" "}
                    {GlobalUtil.formatPhoneNumber(invoice?.phoneUser)}
                  </p>
                  <p>
                    <span className="font-bold text-center">Email: </span>
                    {invoice?.emailUser}
                  </p>
                </div>
                <div className="w-3/4 flex justify-center items-center gap-10 border-t pt-6 mt-3">
                  <Link
                    to={`/management/customers/${invoice?.userId}`}
                    className="inline-flex justify-center rounded-md border border-transparent py-3 px-4 text-sm font-medium shadow-sm hover:drop-shadow-md focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 text-white"
                    style={{ backgroundColor: currentColor }}
                  >
                    Chi tiết
                  </Link>
                  <Link
                    to=""
                    className="inline-flex justify-center rounded-md border bg-white py-3 px-4 text-sm font-medium shadow-sm hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 "
                    style={{ color: currentColor }}
                  >
                    Liên hệ
                  </Link>
                </div>
              </div>
            </div>
            <div className="flex flex-col w-1/4  rounded-3xl border hover:drop-shadow-md bg-white">
              <div className="flex px-6 py-3 border-b gap-2 justify-start items-center">
                <MdPermContactCalendar
                  style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                />
                <h1 className="font-bold text-xl">Thông tin đặt hàng</h1>
              </div>
              <div className="flex flex-col py-4 px-6 gap-6">
                <div className={classContactElement}>
                  <div className="flex justify-center items-center w-1/6">
                    <img src={phoneIcon} alt="" width="50" />
                  </div>
                  <div className="flex flex-col justify-center">
                    <h1 className="font-bold text-xl">Số điện thoại</h1>
                    <p>{GlobalUtil.formatPhoneNumber(invoice?.phone)}</p>
                  </div>
                </div>
                <div className={classContactElement}>
                  <div className="flex justify-center items-center w-1/6">
                    <img src={addressIcon} alt="" width="50" />
                  </div>
                  <div className="flex flex-col justify-center mx-w-[250px]">
                    <h1 className="font-bold text-xl">Địa chỉ</h1>
                    <p className="max-w-[250px]">{invoice?.address}</p>
                  </div>
                </div>
                <div className={classContactElement + " mb-2"}>
                  <div className="flex justify-center items-center w-1/6">
                    <img src={calendarIcon} alt="" width="50" />
                  </div>
                  <div className="flex flex-col justify-center">
                    <h1 className="font-bold text-xl">Ngày đặt</h1>
                    <p>{invoice?.date}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-3 w-1/4">
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <MdProductionQuantityLimits
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Số lượng</h1>
                </div>
                <div className="w-full h-full flex justify-start items-center px-7 ">
                  <div className="flex flex-col gap-2 w-full text-xl">
                    <div className="flex justify-between w-full">
                      <h1 className="font-bold">Sản phẩm:</h1>
                      <h1 className="font-bold">{invoiceDetailList?.length} </h1>
                    </div>
                    <div className="flex justify-between w-full">
                      <h1 className="font-bold">Tổng số:</h1>
                      <h1 className="font-bold">{countTotalQuantity(invoiceDetailList)}</h1>
                    </div>
                  </div>
                </div>
              </div>
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <HiOutlineCurrencyDollar
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Tổng tiền</h1>
                </div>
                <div className="w-full py-7 px-7 flex justify-start items-center h-full">
                  <h1 className="font-bold text-3xl">
                    {GlobalUtil.numberWithCommas(invoice?.total)}{" "}
                    <span className="text-gray-500 text-lg">VNĐ</span>
                  </h1>
                </div>
              </div>
            </div>
            <div className="flex flex-col gap-3 w-1/4">
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <HiOutlineStatusOnline
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Trạng thái</h1>
                </div>
                <div className="w-full py-7 px-7 flex justify-start items-center h-full">
                  <h1 className="font-bold text-xl">{invoice?.status}</h1>
                </div>
              </div>
              <div className="flex flex-col justify-start items-center border rounded-3xl h-1/2 bg-white hover:drop-shadow-md">
                <div className="flex px-6 py-3 border-b w-full justify-start items-center gap-2">
                  <MdOutlinePayment
                    style={{ color: currentColor, width: widthIcon, height: widthIcon }}
                  />
                  <h1 className="font-bold text-xl">Thanh toán</h1>
                </div>
                <div className="w-full px-7 flex justify-start items-center h-full">
                  <div className="w-full flex flex-col gap-2">
                    <div className="flex justify-between w-full">
                      <h1 className="font-bold text-lg">Phương thức:</h1>
                      <h1 className="font-bold text-lg">{invoice?.paymentMethod}</h1>
                    </div>
                    <div className="flex justify-between w-full">
                      <h1 className="font-bold text-lg">
                        <span className="text-gray-700">Trạng thái:</span>
                      </h1>
                      <h1 className="font-bold text-lg">{invoice?.paymentStatus}</h1>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mt-6 w-full rounded-3xl px-8 py-6 border">
            <div className="font-bold text-xl my-3 bg-white rounded-full py-3 w-1/5 flex justify-center items-center border text-gray uppercase">
              Danh sách sản phẩm
            </div>
            <div className="flex flex-col gap-3 w-full py-3">
              <GridComponent
                id="gridcomp"
                dataSource={invoiceDetailList}
                allowPaging
                allowSorting
                pageSettings={{ pageSize: 10 }}
              >
                <ColumnsDirective>
                  {orderDetailGrid.map((item, index) => (
                    <ColumnDirective key={index} {...item} />
                  ))}
                </ColumnsDirective>
                <Inject
                  services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]}
                />
              </GridComponent>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderDetail;
