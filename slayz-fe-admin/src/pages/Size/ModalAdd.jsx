import React, { useRef, useState } from "react";
import { SizeService } from "../../services/size.service";
import swal from "sweetalert";
import { useNavigate } from "react-router-dom";

const ModalAdd = ({ onClose }) => {
  const name = useRef();
  const description = useRef();
  const [nameCheck, setNameCheck] = useState(true);
  const [descriptionCheck, setDescriptionCheck] = useState(true);
  const navigate = useNavigate();
  const onSave = () => {
    if (name.current.value.trim() === "" && description.current.value.trim() === "") {
      setNameCheck(false);
      setDescriptionCheck(false);
      name.current.focus();
    } else if (name.current.value.trim() === "") {
      setNameCheck(false);
      name.current.focus();
    } else if (description.current.value.trim() === "") {
      setDescriptionCheck(false);
      description.current.focus();
    } else {
      const data = {
        name: name.current.value.trim(),
        description: description.current.value.trim(),
      };
      SizeService.createSize(data).then((response) => {
        if (response.status === "OK") {
          swal({
            title: "Thành công",
            text: "Thêm mới sản phẩm thành công!",
            icon: "success",
            button: "OK",
          });
          navigate("/management");
          navigate("/management/size");
          onClose();
        } else {
          swal({
            title: "Lỗi",
            text: response.messaage,
            icon: "error",
            button: "OK",
          });
        }
      });
    }
  };

  return (
    <>
      <div className="flex justify-center items-center overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-1/4 my-0 mx-auto max-w-3xl">
          <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
            <div className="flex w-full flex-col justify-center items-center rounded-xl border border-gray-200 relative bg-white">
              <div className="flex justify-center gap-2 bg-gray-50 py-2 mb-2 text-right sm:px-6 w-full">
                <h1 className="py-4 font-bold">THÊM KÍCH THƯỚC MỚI</h1>
              </div>

              <div className="w-3/4">
                <label className="block text-sm font-medium text-gray-700">
                  <h1 className="font-bold mb-1">Tên kích cỡ</h1>
                  <div className="mt-1">
                    <textarea
                      id="name"
                      name="name"
                      rows={1}
                      className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                      placeholder="Tên danh mục"
                      ref={name}
                      onChange={(e) => {
                        if (e.target.value.trim() !== "") {
                          setNameCheck(true);
                        } else {
                          setNameCheck(false);
                        }
                      }}
                    />
                  </div>
                  {!nameCheck && <p className="text-red-600 mb-2">Vui lòng nhập tên kích cỡ</p>}
                </label>
              </div>
              <div className="w-3/4">
                <label className="block text-sm font-medium text-gray-700">
                  <h1 className="font-bold mb-1">Mô tả</h1>
                  <div className="mt-1">
                    <textarea
                      id="description"
                      name="description"
                      rows={5}
                      ref={description}
                      className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                      placeholder="Mô tả về danh mục"
                      onChange={(e) => {
                        if (e.target.value.trim() !== "") {
                          setDescriptionCheck(true);
                        } else {
                          setDescriptionCheck(false);
                        }
                      }}
                    />
                  </div>
                  {!descriptionCheck && (
                    <p className="text-red-600 mb-2">Vui lòng nhập mô tả của kích cỡ</p>
                  )}
                </label>
              </div>

              <div className="flex justify-end gap-2 bg-gray-50 py-3 text-right sm:px-6 w-full mt-4">
                <button
                  className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-6 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  onClick={onClose}
                >
                  Hủy
                </button>
                <button
                  type="button"
                  className="inline-flex justify-center rounded-md border border-transparent bg-blue-500 py-3 px-6 text-sm font-medium text-white shadow-sm hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                  onClick={onSave}
                >
                  Lưu
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ModalAdd;
