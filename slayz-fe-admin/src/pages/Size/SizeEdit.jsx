import React, { useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Header } from "../../components";
import { Link } from "react-router-dom";
import { SizeService } from "../../services/size.service";
import swal from "sweetalert";
const SizeEdit = () => {
	const { id } = useParams();
	const navigate = useNavigate();
	const [data, setData] = useState(SizeService.getSizeById(id));
	const name = useRef();
	const description = useRef();
	useEffect(() => {
		let isFetchData = true;
		const fetchData = () => {
			SizeService.getSizeById(id).then((response) => {
				if (isFetchData) {
					setData(response);
				}
			});
		};
		fetchData();
		return () => {
			isFetchData = false;
		};
	}, [id]);
	const onSave = () => {
		if (name.current.value.trim() === "" || description.current.value.trim() === "") {
			alert("Hãy nhập đầy đủ dữ liệu");
		} else {
			const newData = {
				name: name.current.value.trim(),
				description: description.current.value.trim(),
			};
			SizeService.updateSize(id, newData).then((response) => {
				if (response.status === "OK") {
					swal({
						title: "Thành công",
						text: "Cập nhật thông tin kích cỡ thành công!",
						icon: "success",
						button: "OK",
					});
					navigate("/management/size");
				}
			});
		}
	};
	return (
		<>
			<div className=" md:m-10 p-1 md:p-10 bg-white rounded-3xl">
				<div className="flex justify-between items-center mb-6">
					<Header title={`Size ${data.name}`} category="Chỉnh sửa thông tin kích cỡ" />
				</div>
				<div className="flex justify-center items-center bg-gray-50 ">
					<div className="w-full">
						<div>
							<div className="md:col-span-2 md:mt-0">
								<div className="sm:overflow-hidden sm:rounded-md">
									<div className="space-y-6 bg-white rounded-x">
										<div>
											<div className="py-2 flex flex-col gap-2 ">
												<div>
													<label className="block text-sm font-medium text-gray-700">
														<h1 className="font-bold mb-1">Tên Size</h1>
														<div className="mt-1">
															<textarea
																id="name"
																name="name"
																rows={1}
																className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
																ref={name}
																defaultValue={data.name}
															/>
														</div>
													</label>
												</div>
												<div>
													<label className="block text-sm font-medium text-gray-700">
														<h1 className="font-bold mb-1">Mô tả</h1>
														<div className="mt-1">
															<textarea
																id="description"
																name="description"
																rows={5}
																className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
																ref={description}
																defaultValue={data.description}
															/>
														</div>
													</label>
												</div>
											</div>
											<div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6">
												<Link
													to="/management/size"
													className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
												>
													Hủy
												</Link>
												<button
													type="button"
													className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
													onClick={onSave}
												>
													Lưu
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default SizeEdit;
