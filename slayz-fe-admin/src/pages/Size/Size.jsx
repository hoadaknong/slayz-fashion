/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from "react";
import sizeImage from "../../data/size_icon.png";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
  Toolbar,
  Search,
} from "@syncfusion/ej2-react-grids";
import {Header} from "../../components";
import {useStateContext} from "../../contexts/ContextProvider";
import ModalAdd from "./ModalAdd";
import {SizeService} from "../../services/size.service";
import {useDataContext} from "../../contexts/DataProvider";
import {Link, useNavigate} from "react-router-dom";
import swal from "sweetalert";

const Size = () => {
  document.title = "Kích Cỡ Sản Phẩm";
  const {currentColor} = useStateContext();
  const [openModal, setOpenModal] = useState(false);
  const {sizeData, setSizeData} = useDataContext();
  const navigate = useNavigate();
  const fetchData = () => {
    SizeService.getAllSize().then((response) => {
      setSizeData(response);
    });
  };
  useEffect(() => {
    fetchData();
  }, []);
  const searchOptions = {
    fields: ["name", "description"],
    ignoreCase: true,
    operator: "contains",
  };
  const toolbarOptions = ["Search", "PdfExport", "ExcelExport"];
  const sizeTemplate = (props) => (
    <div className="flex justify-start items-center">
      <div className="w-1/4 flex gap-2 justify-start items-center">
        <img src={sizeImage} width="60" alt="size icon"/>
        <div>
          <p>{props.name}</p>
        </div>
      </div>
    </div>
  );

  const editSizeGrid = (props) => (
    <div className="flex justify-center items-center gap-2">
      <Link
        style={{background: "#00d084"}}
        className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
        to={`edit/${props.id}`}
      >
        Sửa
      </Link>
      <button
        type="button"
        style={{background: "#FF3333"}}
        className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
        onClick={() => {
          swal({
            title: "Bạn có muốn xóa?",
            text: "Khi xóa size " + props.name + ", bạn sẽ không thể khôi phục dữ liệu!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          }).then((result) => {
            if (result) {
              const deleteOnClick = (id) => {
                SizeService.deleteSize(id)
                  .then((response) => {
                    swal("Đã xóa size " + props.name + "!", {
                      icon: "success",
                    });
                    navigate("/management");
                    navigate("/management/size");
                  })
                  .catch((error) => {
                    swal("Đã xảy ra lỗi", {
                      icon: "error",
                    });
                  });
              };
              deleteOnClick(props.id);
            }
          });
        }}
      >
        Xóa
      </button>
    </div>
  );

  const sizeGrid = [
    {
      headerText: "Size",
      template: sizeTemplate,
      width: "80",
      textAlign: "Left",
    },
    {
      field: "description",
      headerText: "Mô tả",
      width: "100",
      textAlign: "Left",
    },
    {
      headerText: "Hành động",
      width: "120",
      template: editSizeGrid,
      textAlign: "Center",
    },
  ];
  return (
    <>
      {openModal && (
        <ModalAdd
          open={openModal}
          onClose={() => {
            setOpenModal(false);
          }}
          onSave={() => {
            alert("Save size");
            setOpenModal(false);
          }}
        />
      )}
      {openModal && <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>}
      <div className="flex justify-between items-center my-4 mx-7">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div> Kích thước
            </div>
          }
          title="Kích thước"
        />
        <button
          type="button"
          style={{
            backgroundColor: currentColor,
            color: "white",
          }}
          className="font-semibold hover:drop-shadow rounded-full px-6 py-3"
          onClick={() => setOpenModal(true)}
        >
          Thêm kích cỡ mới
        </button>
      </div>
      <div className="mx-6 p-1 md:p-10 bg-white rounded-3xl">
        <GridComponent
          id="gridcomp"
          dataSource={sizeData || []}
          allowPaging
          allowSorting
          toolbar={toolbarOptions}
          searchSettings={searchOptions}
          pageSettings={{pageSize: 7}}
        >
          <ColumnsDirective>
            {sizeGrid.map((item, index) => (
              <ColumnDirective key={index} {...item} />
            ))}
          </ColumnsDirective>
          <Inject
            services={[
              Resize,
              Sort,
              ContextMenu,
              Filter,
              Page,
              ExcelExport,
              Edit,
              PdfExport,
              Toolbar,
              Search,
            ]}
          />
        </GridComponent>
      </div>
    </>
  );
};

export default Size;
