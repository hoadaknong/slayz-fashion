import React from "react";
import {
	GridComponent,
	ColumnsDirective,
	ColumnDirective,
	Resize,
	Sort,
	ContextMenu,
	Filter,
	Page,
	ExcelExport,
	PdfExport,
	Edit,
	Inject,
} from "@syncfusion/ej2-react-grids";
import { discountData, discountGrid } from "./DiscountData";
import { Header } from "../../components";

import { useStateContext } from "../../contexts/ContextProvider";
import { Link } from "react-router-dom";

const Discount = () => {
	document.title = "Giảm Giá";
	const { currentColor } = useStateContext();
	return (
		<div className=" md:m-10 p-1 md:p-10 bg-white rounded-3xl">
			<div className="flex justify-between items-center mb-6">
				<Header title="Giảm giá" category="Quản giảm giá, voucher của sản phẩm" />
				<Link
					to="new"
					style={{
						backgroundColor: currentColor,
						color: "white",
					}}
					className="font-semibold hover:drop-shadow rounded-full px-6 py-3"
				>
					Thêm voucher mới
				</Link>
			</div>

			<GridComponent
				id="gridcomp"
				dataSource={discountData}
				allowPaging
				allowSorting
				pageSettings={{ pageSize: 10 }}
			>
				<ColumnsDirective>
					{discountGrid.map((item, index) => (
						<ColumnDirective key={index} {...item} />
					))}
				</ColumnsDirective>
				<Inject
					services={[Resize, Sort, ContextMenu, Filter, Page, ExcelExport, Edit, PdfExport]}
				/>
			</GridComponent>
		</div>
	);
};

export default Discount;
