import discountImage from "../../data/NEW_ARRIVALS_NAM_SLIDER.png";
import { Link } from "react-router-dom";

const editDiscountGrid = (props) => (
	<div className="flex justify-center items-center gap-2">
		<Link
			style={{ background: "#00d084" }}
			className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
			to={`edit/${props.id}`}
		>
			Sửa
		</Link>
		<button
			type="button"
			style={{ background: "#FF3333" }}
			className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
			onClick={() => {
				alert("Xóa một mục: " + props.id);
			}}
		>
			Xóa
		</button>
	</div>
);

export const discountTypeList = [
	{
		id: 1,
		name: "Giảm theo phần trăm",
	},
	{
		id: 2,
		name: "Giảm theo giá tiền",
	},
];

export const discountGrid = [
	{
		field: "id",
		headerText: "ID",
		width: "50",
		textAlign: "Center",
	},
	{
		field: "code",
		headerText: "Mã",
		width: "130",
		textAlign: "Left",
	},
	{
		field: "type",
		headerText: "Loại",
		width: "130",
		textAlign: "Left",
	},
	{
		field: "description",
		headerText: "Mô tả",
		width: "120",
		textAlign: "Left",
	},
	{
		field: "value",
		headerText: "Giá trị",
		width: "120",
		textAlign: "Left",
	},
	{
		field: "valueRequirement",
		headerText: "Giá trị tối thiểu",
		width: "120",
		textAlign: "Left",
	},
	{
		headerText: "Hành động",
		width: "120",
		template: editDiscountGrid,
		textAlign: "Center",
	},
];

export const discountData = [
	{
		id: 1,
		type: "Giảm theo phần trăm",
		code: "NAMHOCMOI2022",
		value: 0.2,
		maxValue: 40000,
		valueRequirement: 400000,
		description: "Mừng năm học mới",
		image: discountImage,
	},
	{
		id: 2,
		type: "Giảm theo số tiền",
		code: "NAMHOCMOI2022",
		value: 40000,
		maxValue: 40000,
		valueRequirement: 400000,
		description: "Mừng năm học mới",
		image: discountImage,
	},
	{
		id: 3,
		type: "Giảm theo phần trăm",
		code: "NAMHOCMOI2022",
		value: 0.2,
		maxValue: 40000,
		valueRequirement: 400000,
		description: "Mừng năm học mới",
		image: discountImage,
	},
	{
		id: 4,
		type: "Giảm theo số tiền",
		code: "NAMHOCMOI2022",
		value: 40000,
		maxValue: 40000,
		valueRequirement: 400000,
		description: "Mừng năm học mới",
		image: discountImage,
	},
];
