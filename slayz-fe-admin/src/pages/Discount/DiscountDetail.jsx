import React from "react";
import { Link } from "react-router-dom";
import { Header, Selection } from "../../components";
import { productDefault } from "../../data/dummy";
import { discountTypeList } from "./DiscountData";
import { useParams } from "react-router-dom";

const DiscountDetail = () => {
	const { id } = useParams();
	document.title = "Khuyến mại";
	return (
		<div className=" md:m-10 p-1 md:p-10 bg-white rounded-3xl">
			<div className="flex justify-between items-center mb-6">
				<Header title={`Khuyến mại ${id}`} category="Xem thông tin khuyến mại" />
			</div>
			<div className="flex justify-center items-center bg-gray-50 ">
				<div className="w-full">
					<div>
						<div className="mt-1 md:col-span-2 md:mt-0">
							<div className="sm:overflow-hidden sm:rounded-md">
								<div className="space-y-6 bg-white sm:p-6">
									<div className="px-6 py-8 rounded-xl border border-gray-200 flex flex-col gap-2 ">
										<div>
											<h1 className="font-bold mb-1">Thông tin cơ bản</h1>
											<label htmlFor="title" className="block text-sm font-medium text-gray-700">
												Tiêu đề
												<div className="mt-1">
													<textarea
														id="title"
														name="title"
														rows={3}
														className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
														placeholder="Tên sản phẩm"
														defaultValue={""}
														disabled
													/>
												</div>
											</label>
										</div>
										<div>
											<label className="block text-sm font-medium text-gray-700">
												Loại khuyến mại
												<Selection data={discountTypeList} id={"type"} name={"type"} />
											</label>
										</div>
										<div>
											<label
												htmlFor="first-name"
												className="block text-sm font-medium text-gray-700"
											>
												Code
												<textarea
													id="code"
													name="code"
													rows={1}
													className="mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
													placeholder="Mã khuyến mại"
													value={id}
													disabled
												/>
											</label>
										</div>
									</div>
									<div className="flex flex-col shadow px-6 py-8 rounded-xl border border-gray-200 gap-2">
										<h1 className="font-bold mb-1">Giảm giá</h1>
										<div>
											<label>
												Giá tối thiệu
												<input
													id="valueRequirement"
													name="valueRequirement"
													type="number"
													className="mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm h-9 pl-2"
													placeholder="0"
													disabled
												/>
											</label>
										</div>
										<div>
											<label>
												Giá trị
												<input
													id="value"
													name="value"
													type="number"
													className="mt-1 block w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm h-9 pl-2"
													placeholder="0"
													disabled
												/>
											</label>
										</div>
									</div>
									<div>
										<label className="block text-sm font-medium text-gray-700">
											Hình thu nhỏ - Một hình ảnh
										</label>
										<div className="flex items-center space-x-6 py-2">
											<div className="shrink-0">
												<img
													className="h-16 w-16 object-cover rounded-full"
													src={productDefault}
													alt=""
												/>
											</div>
										</div>
									</div>
								</div>
								<div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6">
									<Link
										to="/management/discount"
										className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
									>
										Trở về
									</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default DiscountDetail;
