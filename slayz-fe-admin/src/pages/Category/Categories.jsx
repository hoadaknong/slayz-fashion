/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef } from "react";
import { useState } from "react";
import ModalAdd from "./ModalAdd";
import swal from "sweetalert";
import {
  GridComponent,
  ColumnsDirective,
  ColumnDirective,
  Resize,
  Sort,
  ContextMenu,
  Filter,
  Page,
  ExcelExport,
  PdfExport,
  Edit,
  Inject,
  Toolbar,
  Search,
} from "@syncfusion/ej2-react-grids";

import { Header } from "../../components";
import { useStateContext } from "../../contexts/ContextProvider";
import { CategoryService } from "../../services/category.service";
import { Link, useNavigate } from "react-router-dom";
import { useDataContext } from "../../contexts/DataProvider";
import { Pagination } from "@mui/material";
import Loading from "react-fullscreen-loading";

const Categories = () => {
  document.title = "Phân Loại Sản Phẩm";
  const navigate = useNavigate();
  const { currentColor } = useStateContext();
  const [openModal, setOpenModal] = useState(false);
  const [categoryData, setCategoryData] = useState([]);
  const searchOptions = {
    fields: ["name", "description"],
    ignoreCase: true,
    operator: "contains",
  };
  const toolbarOptions = ["Search", "PdfExport", "ExcelExport"];
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const pageSize = useRef(10);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    (async function () {
      setLoading(true);
      const response = await CategoryService.getAllCategory(
        page,
        pageSize.current
      );
      setCategoryData(response.details);
      setTotalPage(response.totalPage);
      setLoading(false);
    })();
  }, [page]);

  const editCategoryGrid = (props) => {
    return (
      <div className="flex justify-start items-center gap-2">
        <Link
          style={{ background: "#00d084" }}
          className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
          to={`edit/${props.id}`}
        >
          Sửa
        </Link>
        <button
          type="button"
          style={{ background: "#FF3333" }}
          className="text-white font-bold py-2 px-6 capitalize rounded-full text-sm hover:drop-shadow-lg"
          onClick={async () => {
            await swal({
              title: "Bạn có muốn xóa?",
              text:
                "Khi xóa loại " +
                props.name +
                ", bạn sẽ không thể khôi phục dữ liệu!",
              icon: "warning",
              buttons: true,
              dangerMode: true,
            }).then((result) => {
              if (result) {
                const deleteOnClick = (id) => {
                  CategoryService.deleteCategory(id)
                    .then(async (response) => {
                      await swal("Đã xóa loại " + props.name + "!", {
                        icon: "success",
                      });
                      navigate("/management");
                      navigate("/management/categories");
                    })
                    .catch(async (error) => {
                      await swal("Đã xảy ra lỗi", {
                        icon: "error",
                      });
                    });
                };
                deleteOnClick(props.id);
              }
            });
          }}
        >
          Xóa
        </button>
      </div>
    );
  };

  const categoryGrid = [
    {
      field: "name",
      headerText: "Loại",
      width: "90",
      textAlign: "Left",
    },
    {
      field: "parentName",
      headerText: "Mục lớn",
      width: "90",
      textAlign: "Left",
    },
    {
      field: "description",
      headerText: "Mô tả",
      width: "150",
      textAlign: "Left",
    },
    {
      field: "description",
      headerText: "Hành động",
      template: editCategoryGrid,
      width: "120",
      textAlign: "Left",
    },
  ];

  return (
    <>
      <Loading
        loading={loading}
        background="rgba(0, 0, 0, 0.33)"
        loaderColor="rgba(0, 167, 255, 1)"
      />
      <div id="modal-category">
        <ModalAdd
          open={openModal}
          onClose={() => {
            setOpenModal(false);
          }}
        />
        {openModal && (
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        )}
      </div>
      <div className="flex justify-between items-center my-4 mx-8">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Phân loại
            </div>
          }
          title="Phân loại"
        />
        <button
          type="button"
          style={{
            backgroundColor: currentColor,
            color: "white",
          }}
          className="font-semibold hover:drop-shadow rounded-full px-6 py-3"
          onClick={() => {
            setOpenModal(true);
          }}
        >
          Thêm loại sản phẩm mới
        </button>
      </div>
      <div className="p-1 md:p-10 md:pb-4 bg-white rounded-3xl mx-6">
        <div id="grid-data">
          <GridComponent
            id="gridcomp"
            dataSource={categoryData}
            toolbar={toolbarOptions}
            searchSettings={searchOptions}
          >
            <ColumnsDirective>
              {categoryGrid.map((item, index) => (
                <ColumnDirective key={index} {...item} />
              ))}
            </ColumnsDirective>
            <Inject
              services={[
                Resize,
                Sort,
                ContextMenu,
                Filter,
                Page,
                ExcelExport,
                Edit,
                PdfExport,
                Toolbar,
                Search,
              ]}
            />
          </GridComponent>
        </div>
        <div className="p-4 w-full flex justify-end">
          <Pagination
            count={totalPage}
            size="large"
            color="primary"
            page={page}
            onChange={(event, page) => setPage(page)}
          />
        </div>
      </div>
    </>
  );
};

export default Categories;
