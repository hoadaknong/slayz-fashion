import React, { useEffect, useState } from "react";
import { Header, Selection } from "../../components";
import { Link, useNavigate, useParams } from "react-router-dom";
import { CategoryService } from "../../services/category.service";
import swal from "sweetalert";

const CategoryEdit = () => {
  const { id } = useParams();
  const [data, setData] = useState({});
  const navigate = useNavigate();
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    (async function () {
      const response = await CategoryService.getAllCategory(1, 1000);
      setCategories(response.details);
    })();
    (async function () {
      const response = await CategoryService.getCategoryById(id);
      setData(response);
    })();
  }, [id]);
  const onSave = () => {
    const category = {
      name: document.getElementById("name").value,
      description: document.getElementById("description").value,
      parentId: document.getElementById("parentCategory").value,
    };
    CategoryService.updateCategory(id, category)
      .then((response) => {
        swal({
          title: "Thành công",
          text: "Cập nhật thông tin loại sản phẩm thành công!",
          icon: "success",
          button: "OK",
        });
        navigate("/management/categories");
      })
      .catch((error) => {
        alert("Đã xảy ra lỗi!!!");
      });
  };
  return (
    <>
      <div className="px-6 py-3">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Sản phẩm <div className="w-1 h-1 rounded-full bg-gray-400"></div>
              Loại sản phẩm
            </div>
          }
          title={`Danh mục ${data.name}`}
        />
      </div>

      <div className="px-6 bg-white rounded-3xl mx-6 py-6 mt-4">
        <div className="flex justify-center items-center bg-gray-50 ">
          <div className="w-full">
            <div>
              <div className="md:col-span-2 md:mt-0">
                <div className="sm:overflow-hidden sm:rounded-md">
                  <div className="space-y-6 bg-white rounded-x">
                    <div>
                      <div className="py-2 flex flex-col gap-2 ">
                        <div>
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1">Tên danh mục</h1>
                            <div className="mt-1">
                              <textarea
                                id="name"
                                name="name"
                                rows={3}
                                className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                                placeholder="Tên danh mục"
                                defaultValue={data.name}
                              />
                            </div>
                          </label>
                        </div>
                        <div>
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1">Mô tả</h1>
                            <div className="mt-1">
                              <textarea
                                id="description"
                                name="description"
                                rows={5}
                                className="mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm p-2"
                                placeholder="Mô tả về danh mục"
                                defaultValue={data.description}
                              />
                            </div>
                          </label>
                        </div>
                        <div>
                          <label className="block text-sm font-medium text-gray-700">
                            <h1 className="font-bold mb-1">Danh mục lớn</h1>
                            <div className="mt-1 pb-2 pt-1">
                              <Selection
                                id={"parentCategory"}
                                name={"parentCategory"}
                                data={categories}
                                parentId={data.parentId}
                                isDefault={true}
                              />
                            </div>
                          </label>
                        </div>
                      </div>
                      <div className="flex justify-end gap-2 px-4 py-3 text-right sm:px-6">
                        <Link
                          to="/management/categories"
                          className="inline-flex justify-center rounded-md border border-transparent bg-red-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                        >
                          Hủy
                        </Link>
                        <button
                          type="button"
                          className="inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                          onClick={onSave}
                        >
                          Lưu
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CategoryEdit;
