import React, { useEffect, useState } from "react";
import { Header } from "../../components";
import { useStateContext } from "../../contexts/ContextProvider";
import { useNavigate, useParams } from "react-router-dom";
import { ImageService } from "../../services/image.service";
import Loading from "react-fullscreen-loading";
import { BannerService } from "../../services/banner.service";
import { GlobalUtil } from "../../utils/GlobalUtil";
import swal from "sweetalert";

const BannerForm = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const [image, setImage] = useState(null);
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [linkTo, setLinkTo] = useState("");
  const [type, setType] = useState("slide");
  const { currentColor } = useStateContext();
  const [loading, setLoading] = useState(false);
  const formSubmit = async (e) => {
    e.preventDefault();
    if(image=== null){
      swal({
        title: "Lỗi",
        text: "Hãy tải lên hình ảnh!",
        icon: "error",
        button: "OK",
      });
      return;
    }
    const payload = {
      image,
      startDate,
      endDate,
      linkTo,
      type,
    };
    if (id !== undefined) {
      const response = await BannerService.updateBanner(id, payload);
      if (response.status === "OK") {
        navigate(-1);
      }
    } else {
      const response = await BannerService.createBanner(payload);
      if (response.status === "OK") {
        navigate(-1);
      }
    }
  };
  useEffect(() => {
    (async function () {
      if (id !== undefined) {
        const response = await BannerService.getBannerById(id);
        if (response.status === "OK") {
          const data = response.data;
          setImage(data.image);
          setStartDate(
            GlobalUtil.dateTimeConvertToSetValueInput(data.startDate)
          );
          setEndDate(GlobalUtil.dateTimeConvertToSetValueInput(data.endDate));
          setLinkTo(data.linkTo);
          setType(data.type);
        }
      }
    })();
  }, [id]);
  return (
    <div className="mx-6 my-4 rounded-3xl h-fit px-3 flex flex-col justify-between items-center">
      <Loading
        loading={loading}
        background="rgba(0, 0, 0, 0.33)"
        loaderColor="rgba(0, 167, 255, 1)"
      />
      <div className="w-full flex justify-between items-center mb-4">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Banner
            </div>
          }
          title={"Thêm Banner Mới"}
        />
      </div>
      <div className="p-1 md:p-10 bg-white rounded-3xl w-full">
        {image && (
          <img
            src={image}
            alt="banner"
            className="max-w-3xl mx-auto mb-3 mt-2"
          />
        )}
        <form onSubmit={formSubmit}>
          <label>
            <p>Chọn hình ảnh:</p>
            <input
              className="w-full text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400"
              type="file"
              onChange={async (e) => {

                setLoading(true);
                const response = await ImageService.saveImage(
                  e.target.files[0]
                );
                console.log(response);
                if (response.status === 200) setImage(response.data.secure_url);
                setLoading(false);
              }}
            />
          </label>
          <label className="block text-sm font-medium text-gray-700 mt-2">
            Đặt đường link sản phẩm
            <div className="mt-1">
              <input
                id="title"
                name="title"
                className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-indigo-500 sm:text-sm p-2 focus:outline-none`}
                placeholder="Tên sản phẩm"
                value={linkTo}
                onChange={(event) => {
                  setLinkTo(event.target.value);
                }}
                required
              />
            </div>
          </label>
          <label className="block text-sm font-medium text-gray-700 mt-2">
            Loại banner:
            <select
              className={`mt-1 block w-full rounded-md border border-gray-300 bg-white py-2 px-2 shadow-sm focus:border-[${currentColor}] focus:ring-[${currentColor}] focus:outline-none sm:text-sm`}
              value={type}
              onChange={(event) => {
                setType(event.target.value);
              }}
              required
            >
              <option value="slide">Slide</option>
              <option value="ad">Quảng cáo</option>
              <option value="about">Thông tin về chúng tôi</option>
            </select>
          </label>
          <div className="flex justify-start items-center gap-3">
            <label className="block text-sm font-medium text-gray-700 mt-2 w-1/4">
              Ngày bắt đầu
              <div className="mt-1">
                <input
                  className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-indigo-500 sm:text-sm p-2 focus:outline-none`}
                  type="date"
                  value={startDate}
                  onChange={(event) => {
                    setStartDate(event.target.value);
                  }}
                  required
                />
              </div>
            </label>
            <label className="block text-sm font-medium text-gray-700 mt-2 w-1/4">
              Ngày kết thúc
              <div className="mt-1">
                <input
                  className={`mt-1 w-full border rounded-md border-gray-300 shadow-sm focus:border-[${currentColor}] focus:ring-indigo-500 sm:text-sm p-2 focus:outline-none`}
                  type="date"
                  value={endDate}
                  onChange={(event) => {
                    setEndDate(event.target.value);
                  }}
                  required
                />
              </div>
            </label>
          </div>
          <button
            type="submit"
            className="mt-5 inline-flex justify-center rounded-md border border-transparent bg-sky-500 py-3 px-8 text-sm font-medium text-white shadow-sm hover:bg-sky-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
          >
            Lưu
          </button>
        </form>
      </div>
    </div>
  );
};

export default BannerForm;
