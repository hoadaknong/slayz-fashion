import React, { useEffect, useState } from "react";
import { Header } from "../../components";
import BannerGrid from "./BannerGrid";
import { BannerService } from "../../services/banner.service";
import { useStateContext } from "../../contexts/ContextProvider";
import { useNavigate } from "react-router-dom";
import swal from "sweetalert";

const Banner = () => {
  const [banner, setBanner] = useState([]);
  const { currentColor } = useStateContext();
  const navigate = useNavigate();
  useEffect(() => {
    (async ()=>{
      await fetchData();
    })()
  }, []);
  const fetchData = async () => {
    const response = await BannerService.getAllBanner();
    if (response.status === "OK") {
      setBanner(response.data);
    }
  };
  const deleteBanner = async (id) => {
    swal({
      title: "Bạn có muốn xóa?",
      text: "Khi xóa loại banner này hay không bạn sẽ không thể khôi phục dữ liệu!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then(async (result) => {
      if (result) {
        const response = await BannerService.deleteBanner(id);
        if (response.status === "OK") {
          await swal("Xóa banner thành công!", {
            icon: "success",
          });
          await fetchData()
        }
      }
    });
  };
  const editBannerOnClick = (id)=>{
    navigate(`${id}`)
  }
  return (
    <div className="mx-6 my-4 rounded-3xl h-fit px-3 flex flex-col justify-between items-center">
      <div className="w-full flex justify-between items-center mb-4">
        <Header
          category={
            <div className="flex justify-start items-center gap-3 mt-3">
              Trang chủ <div className="w-1 h-1 rounded-full bg-gray-400"></div>{" "}
              Banner
            </div>
          }
          title={"Banner"}
        />
        <button
          type="button"
          style={{
            backgroundColor: currentColor,
            color: "white",
          }}
          className="font-semibold hover:drop-shadow rounded-full px-6 py-3"
          onClick={() => {
            navigate("new");
          }}
        >
          Thêm banner mới
        </button>
      </div>
      <div className="p-1 md:p-10 bg-white rounded-3xl flex w-full flex-wrap gap-4 justify-between">
        {banner.map((banner, index) => {
          return (
            <BannerGrid
              {...banner}
              key={index}
              onDeleteOnClick={async () => {
                await deleteBanner(banner.id);
              }}
              onEditOnClick={()=>{
                editBannerOnClick(banner.id)
              }}
            />
          );
        })}
      </div>
    </div>
  );
};

export default Banner;
