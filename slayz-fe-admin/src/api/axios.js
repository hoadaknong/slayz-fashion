import axios from "axios";
import jwt_decode from "jwt-decode";

// const BACK_END_URL = `${process.env.REACT_APP_BACK_END_PROTOCOL}://${process.env.REACT_APP_BACK_END_URL}:${process.env.REACT_APP_BACK_END_PORT}`;
const BACK_END_URL = `${process.env.REACT_APP_BACK_END_PROTOCOL}://${process.env.REACT_APP_BACK_END_URL}`;
const API_NO_AUTH = axios.create({
  baseURL: BACK_END_URL,
  withCredentials: true,
});

export const api = axios.create({
  baseURL: BACK_END_URL,
  withCredentials: true,
});

api.interceptors.request.use(async (config) => {
  try {
    const token = JSON.parse(localStorage.getItem("token"));
    if (token !== null) {
      let now = new Date();
      const decodedToken = jwt_decode(token);
      const isExpiredToken = decodedToken.exp * 1000 < now.getTime();
      if (isExpiredToken) {
        const refreshRequest = await API_NO_AUTH.get("/api/v1/auth/refresh");
        const refreshResponse = refreshRequest.data;
        if (refreshResponse.status === "OK") {
          localStorage.setItem(
            "token",
            JSON.stringify(refreshResponse.data.accessToken)
          );
        }
        config.headers[
          "Authorization"
        ] = `Bearer ${refreshResponse.data.accessToken}`;
      } else {
        config.headers["Authorization"] = `Bearer ${token}`;
      }
    } else {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  } catch (error) {
    //console.log("Lỗi token");
  } finally {
    return config;
  }
});
