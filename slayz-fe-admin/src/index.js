import React from "react";
import ReactROM from "react-dom";
import { HashRouter } from "react-router-dom";

import "./index.css";
import App from "./App";

import { ContextProvider } from "./contexts/ContextProvider";
import { DataProvider } from "./contexts/DataProvider";

ReactROM.render(
  <React.StrictMode>
    <DataProvider>
      <ContextProvider>
        <HashRouter>
          <App />
        </HashRouter>
      </ContextProvider>
    </DataProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
