import { api } from "../api/axios";

const getAllProduct = () => {
  return api.get("/api/v1/product/product_grid").then((response) => {
    return response.data;
  });
};

const createProduct = (data) => {
  return api.post("/api/v1/product", data).then((response) => {
    return response.data;
  });
};

const updateProduct = (id, data) => {
  return api.put("/api/v1/product/" + id, data).then((response) => {
    return response.data;
  });
};

const getProductById = (id) => {
  return api.get("/api/v1/product/" + id).then((response) => {
    return response.data;
  });
};

const getProductByBarcode = (barcode) => {
  return api.get("/api/v1/product/barcode/" + barcode).then((response) => {
    return response.data;
  });
};

const getAllVariantByProductId = (productId) => {
  return api.get("/api/v1/variant/product/" + Number(productId)).then((response) => {
    return response.data;
  });
};

const getSaleQuantityByProductId = (id) => {
  return api.get("/api/v1/product/sale_quantity/" + id).then((response) => {
    return response.data;
  });
};

const deleteProductById = (id) => {
  return api.delete("/api/v1/product/" + id).then((response) => {
    return response.data;
  });
};

const getProductByVariantId = (variantId) => {
  return api.get("/api/v1/product/variant/" + variantId).then((response) => {
    return response.data;
  });
};
const filterData = (data) => {
  return api.post("/api/v1/product/filter", data).then((response) => {
    return response.data;
  });
};
export const ProductService = {
  getAllProduct,
  createProduct,
  updateProduct,
  deleteProductById,
  getProductById,
  getProductByBarcode,
  getSaleQuantityByProductId,
  getAllVariantByProductId,
  getProductByVariantId,
  filterData
};
