import { api } from "../api/axios";

const getAllCart = () => {
  return api.get("/api/v1/cart").then((response) => {
    return response.data;
  });
};

const getAllCartDetailByCartId = (id) => {
  return api.get("/api/v1/cart/cart_detail/" + id).then((response) => {
    return response.data;
  });
};

const getCartById = (id) => {
  return api.get("/api/v1/cart/" + id).then((response) => {
    return response.data;
  });
};

export const CartService = { getAllCart, getAllCartDetailByCartId, getCartById };
