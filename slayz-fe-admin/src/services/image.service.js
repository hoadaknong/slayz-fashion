import { api } from "../api/axios";
import axios from "axios";

const preset_key = "ml_default";
const cloud_name = "dfnnmsofg";
const getAllImageByProductId = (productId) => {
  return api.get("/api/v1/image_product/" + productId).then((response) => {
    return response.data;
  });
};

const saveImage = (file) => {
  const form = new FormData();
  form.append("file", file);
  form.append("upload_preset", preset_key);
  return axios
    .post(`https://api.cloudinary.com/v1_1/${cloud_name}/image/upload`, form)
    .then((data) => {
      return data;
    })
    .catch((error) => {
      return error;
    });
};

export const ImageService = {
  getAllImageByProductId,
  saveImage,
};
