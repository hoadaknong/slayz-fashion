import { api } from "../api/axios";

const createCategory = (category) => {
  return api.post("/api/v1/category", category).then((response) => {
    return response.data;
  });
};

const getCategoryById = (id) => {
  return api.get("/api/v1/category/" + id).then((response) => {
    return response.data.data;
  });
};

const updateCategory = (id, category) => {
  return api.put("/api/v1/category/" + id, category).then((response) => {
    return response.data;
  });
};

const deleteCategory = (id) => {
  return api.delete("/api/v1/category/" + id).then((response) => {
    return response.data;
  });
};

const getAllCategory = (pageIndex = 1, pageSize = 10) => {
  return api
    .get(`/api/v1/category?pageIndex=${pageIndex}&pageSize=${pageSize}`)
    .then((response) => {
      return response.data.data;
    });
};

export const CategoryService = {
  getAllCategory,
  createCategory,
  getCategoryById,
  updateCategory,
  deleteCategory,
};
