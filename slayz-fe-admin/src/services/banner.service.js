import { api } from "../api/axios";

const getAllBanner = ()=>{
  return api.get("/api/v1/banner").then(response =>{
    return response.data;
  });
}

const createBanner = (payload)=>{
  return api.post("/api/v1/banner",payload).then(response =>{
    return response.data;
  });
}

const getBannerById = (id)=>{
  return api.get("/api/v1/banner/" + id).then(response =>{
    return response.data;
  });
}

const getAllBannerByType = (type)=>{
  return api.get("/api/v1/banner/type?type=" + type).then(response =>{
    return response.data;
  });
}

const updateBanner = (id,payload)=>{
  return api.put("/api/v1/banner/" + id, payload).then(response =>{
    return response.data;
  });
}

const deleteBanner = (id)=>{
  return api.delete("/api/v1/banner/" + id).then(response =>{
    return response.data;
  });
}

export const BannerService = {
  getAllBanner,
  createBanner,
  getBannerById,
  getAllBannerByType,
  updateBanner,
  deleteBanner
}