import swal from "sweetalert";
import { api } from "../api/axios";
import jwt_decode from "jwt-decode";
import axios from "axios";

const API_NO_AUTH = axios.create({ baseURL: "http://localhost:8080", withCredentials: true });

const login = (username, password) => {
  const data = { username, password };
  return api.post("/api/v1/auth/admin/login", data).then((response) => {
    if (response?.data?.data?.accessToken) {
      localStorage.setItem("token", JSON.stringify(response?.data?.data?.accessToken));
      api.interceptors.request.use(async (config) => {
        config.headers["Authorization"] = `Bearer ${response?.data?.data?.accessToken}`;
        return config;
      });
      api.interceptors.request.use(async (config) => {
        try {
          let now = new Date();
          const decodedToken = jwt_decode(response?.data?.data?.accessToken);
          const isExpiredToken = decodedToken.exp * 1000 < now.getTime();
          console.log("Is Expired Token: " + isExpiredToken);
          if (isExpiredToken) {
            const refreshRequest = await API_NO_AUTH.get("/api/v1/auth/refresh");
            const refreshResponse = refreshRequest.data;
            if (refreshResponse.data.status === "OK") {
              localStorage.setItem("token", JSON.stringify(refreshResponse.data.accessToken));
            }
            config.headers["Authorization"] = `Bearer ${refreshResponse.data.accessToken}`;
          }
        } catch (error) {
          console.log(error);
        } finally {
          return config;
        }
      });
      return response.data;
    } else {
      swal({
        title: "LỖI ĐĂNG NHẬP",
        text: response.data.message,
        icon: "error",
        button: "OK",
      });
    }
    return null;
  });
};

const logout = () => {
  return api.get("/api/v1/auth/logout").then((response) => {
    return response.data;
  });
};

export const AuthService = {
  login,
  logout,
};
