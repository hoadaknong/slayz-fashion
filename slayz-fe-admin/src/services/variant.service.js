import { api } from "../api/axios";

const createNewVariant = (payload) => {
  api.post("/api/v1/variant", payload).then((response) => {
    return response.data;
  });
};

const updateVariant = (payload) => {
  api.put("/api/v1/variant", payload).then((response) => {
    return response.data;
  });
};

const getAllVariant = (productId) => {
  api.get("/api/v1/variant/product/" + productId).then((response) => {
    return response.data;
  });
};

export const VariantService = {
  createNewVariant,
  updateVariant,
  getAllVariant,
};
