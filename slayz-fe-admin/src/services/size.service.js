import { api } from "../api/axios";

const createSize = (size) => {
  return api.post("/api/v1/size", size).then((response) => {
    return response.data;
  });
};

const getSizeById = (id) => {
  return api.get("/api/v1/size/" + id).then((response) => {
    return response.data.data;
  });
};

const updateSize = (id, size) => {
  return api.put("/api/v1/size/" + id, size).then((response) => {
    return response.data;
  });
};

const deleteSize = (id) => {
  return api.delete("/api/v1/size/" + id).then((response) => {
    return response.data;
  });
};

const getAllSize = () => {
  return api.get("/api/v1/size").then((response) => {
    return response.data.data;
  });
};

export const SizeService = {
  getAllSize,
  createSize,
  getSizeById,
  updateSize,
  deleteSize,
};
