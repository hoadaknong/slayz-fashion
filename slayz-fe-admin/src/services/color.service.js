import { api } from "../api/axios";

const createColor = (color) => {
  return api.post("/api/v1/color", color).then((response) => {
    return response.data;
  });
};

const getColorById = (id) => {
  return api.get("/api/v1/color/" + id).then((response) => {
    return response.data.data;
  });
};

const updateColor = (id, color) => {
  return api.put("/api/v1/color/" + id, color).then((response) => {
    return response.data;
  });
};

const deleteColor = (id) => {
  return api.delete("/api/v1/color/" + id).then((response) => {
    return response.data;
  });
};

const getAllColor = (pageIndex=1,pageSize=1000) => {
  return api.get("/api/v1/color").then((response) => {
    return response.data.data;
  });
};

export const ColorService = {
  getAllColor,
  createColor,
  getColorById,
  updateColor,
  deleteColor,
};
