import { api } from "../api/axios";

const getFeedbackById = (id) => {
  return api.get("/api/v1/review/" + id).then((response) => {
    return response.data;
  });
};

const getAllFeedback = (pageIndex, pageSize) => {
  return api
    .get(`/api/v1/review?pageIndex=${pageIndex}&pageSize=${pageSize}`)
    .then((response) => {
      return response.data;
    });
};

const createNewReview = (data) => {
  return api.post("/api/v1/review", data).then((response) => {
    return response.data;
  });
};

const deleteReview = (id) => {
  return api.delete("/api/v1/review/" + id).then((response) => {
    return response.data;
  });
};

const disableReview = (id) => {
  return api.post(`/api/v1/review/${id}/disable`, null).then((response) => {
    return response.data;
  });
};

const enableReview = (id) => {
  return api.post(`/api/v1/review/${id}/enable`, null).then((response) => {
    return response.data;
  });
};

const getAllReviewByProductId = (id) => {
  return api.get("/api/v1/review/product/" + id).then((response) => {
    return response.data;
  });
};

const getAllReviewByUserId = (id) => {
  return api.get("/api/v1/review/user/" + id).then((response) => {
    return response.data;
  });
};

const getAvgRatingByProductId = (id) => {
  return api.get("/api/v1/review/avg_rating/" + id).then((response) => {
    return response.data;
  });
};

const getOverviewByProductId = (id) => {
  return api.get("/api/v1/review/overview_rating/" + id).then((response) => {
    return response.data;
  });
};

const getAllReviewByProductIdFilter = (payload) => {
  return api.post("/api/v1/review/product", payload).then((response) => {
    return response.data;
  });
};

export const FeedbackService = {
  getAllFeedback,
  createNewReview,
  deleteReview,
  disableReview,
  enableReview,
  getAllReviewByProductId,
  getAllReviewByUserId,
  getFeedbackById,
  getAvgRatingByProductId,
  getOverviewByProductId,
  getAllReviewByProductIdFilter,
};
