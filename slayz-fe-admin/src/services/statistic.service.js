import { api } from "../api/axios";

const getDataLineChartGrandTotalSevenDayRecent = () => {
  return api
    .get("/api/v1/statistic/line_chart/grand_total")
    .then((response) => {
      return response.data;
    });
};

const getDataLineChartQuantityInvoiceSevenDayRecent = () => {
  return api
    .get("/api/v1/statistic/line_chart/quantity_invoice")
    .then((response) => {
      return response.data;
    });
};

const getDataPieChartDataInvoiceData = () => {
  return api.get("/api/v1/statistic/pie_chart_invoice").then((response) => {
    return response.data;
  });
};

const getTotalQuantityInvoiceByStatus = (status) => {
  return api
    .get("/api/v1/statistic/quantity_invoice/" + status)
    .then((response) => {
      return response.data;
    });
};

const getRevenueData = () => {
  return api.get("/api/v1/statistic/revenue").then((response) => {
    return response.data;
  });
};

const getTotalProduct = () => {
  return api.get("/api/v1/statistic/total_product").then((response) => {
    return response.data;
  });
};

const getTotalQuantityProduct = () => {
  return api
    .get("/api/v1/statistic/total_product_quantity")
    .then((response) => {
      return response.data;
    });
};

const getTotalQuantityInvoice = () => {
  return api
    .get("/api/v1/statistic/total_quantity_invoice")
    .then((response) => {
      return response.data;
    });
};

const getTotalQuantitySaleProduct = () => {
  return api.get("/api/v1/statistic/total_quantity_sale").then((response) => {
    return response.data;
  });
};

const getTotalUser = () => {
  return api.get("/api/v1/statistic/total_user").then((response) => {
    return response.data;
  });
};
const getReviewMetric = () => {
  return api.get("/api/v1/statistic/review").then((response) => {
    return response.data;
  });
};
export const StatisticService = {
  getDataLineChartGrandTotalSevenDayRecent,
  getDataLineChartQuantityInvoiceSevenDayRecent,
  getDataPieChartDataInvoiceData,
  getTotalQuantityInvoiceByStatus,
  getRevenueData,
  getTotalProduct,
  getTotalQuantityProduct,
  getTotalQuantityInvoice,
  getTotalQuantitySaleProduct,
  getTotalUser,
  getReviewMetric,
};
