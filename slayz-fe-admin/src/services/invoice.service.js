import { api } from "../api/axios";

const FileDownload = require("js-file-download");

const getAllInvoice = (pageIndex, pageSize) => {
  return api
    .get(`/api/v1/invoice?pageIndex=${pageIndex}&pageSize=${pageSize}`)
    .then((response) => {
      return response.data;
    });
};

const getInvoiceByUserId = (userId) => {
  return api.get("/api/v1/invoice/users/" + userId).then((response) => {
    return response.data;
  });
};

const getAllInvoiceDetailByInvoiceId = (invoiceId) => {
  return api
    .get("/api/v1/invoice/invoice_detail/" + invoiceId)
    .then((response) => {
      return response.data;
    });
};

const getInvoiceById = (id) => {
  return api.get("/api/v1/invoice/" + id).then((response) => {
    return response.data;
  });
};

const getAllInvoiceByStatus = (status, page = 1, pageSize = 10) => {
  return api
    .get(
      `/api/v1/invoice/status/${status}?pageIndex=${page}&pageSize=${pageSize}`
    )
    .then((response) => {
      return response.data;
    });
};

const updateInvoiceStatus = (id, status) => {
  return api
    .patch("/api/v1/invoice/status/" + id, { status })
    .then((response) => {
      return response.data;
    });
};

export const InvoiceService = {
  getAllInvoice,
  getInvoiceByUserId,
  getAllInvoiceDetailByInvoiceId,
  getInvoiceById,
  getAllInvoiceByStatus,
  updateInvoiceStatus,
};
