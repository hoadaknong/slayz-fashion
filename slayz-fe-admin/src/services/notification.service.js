import { api } from "../api/axios";

const getAllNotification = (pageIndex=1, pageSize=20,status="all") => {
  return api.get(`/api/v1/notification?pageSize=${pageSize}&pageIndex=${pageIndex}&status=${status}`).then((response) => {
    return response.data;
  });
};

const setIsCheckedNotification = (id) => {
  return api.patch(`/api/v1/notification/${id}`).then((response) => {
    return response.data;
  });
};

export const NotificationService = {
  getAllNotification,
  setIsCheckedNotification,
};
