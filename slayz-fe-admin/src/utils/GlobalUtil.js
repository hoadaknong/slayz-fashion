function numberWithCommas(number = 0) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function formatPhoneNumber(phoneNumberString) {
  var cleaned = ("" + phoneNumberString).replace(/\D/g, "");
  var match = cleaned.match(/^(\d{4})(\d{3})(\d{3})$/);
  if (match) {
    return match[1] + " " + match[2] + " " + match[3];
  }
  return null;
}

const dateTimeConvert = (milliseconds) => {
  const date = new Date(milliseconds);
  return (
    date.getDate() +
    "-" +
    (date.getMonth() + 1) +
    "-" +
    date.getUTCFullYear() +
    " " +
    date.getHours() +
    ":" +
    date.getMinutes()
  );
};
const dateTimeConvertWithoutTime = (milliseconds) => {
  const date = new Date(milliseconds);
  return (
    date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getUTCFullYear()
  );
};

const dateTimeConvertToSetValueInput = (milliseconds) => {
  const date = new Date(milliseconds);
  if (date.getMonth() + 1 > 9) {
    return (
      date.getUTCFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
    );
  }
  return (
    date.getUTCFullYear() + "-0" + (date.getMonth() + 1) + "-" + date.getDate()
  );
};
export const GlobalUtil = {
  numberWithCommas,
  formatPhoneNumber,
  dateTimeConvert,
  dateTimeConvertWithoutTime,
  dateTimeConvertToSetValueInput,
};
