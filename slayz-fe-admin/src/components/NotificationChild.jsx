import React from "react";
import { useNavigate } from "react-router-dom";
import { GlobalUtil } from "../utils/GlobalUtil";
import { NotificationService } from "../services/notification.service";
import { useDataContext } from "../contexts/DataProvider";

const NotificationChild = ({ item }) => {
  const navigate = useNavigate();
  const { fetchNotification } = useDataContext();
  const onClickNotification = async () => {
    const checkNotification = await NotificationService.setIsCheckedNotification(item.id);
    fetchNotification();
    if (checkNotification.status === "OK") {
      if (item.type === "INVOICE") {
        navigate(`/management/orders_in_progress/${item.idOfType}`);
      }
      if (item.type === "REVIEW") {
        navigate(`/management/review/${item.idOfType}`);
      }
    }
  };
  return (
    <div
      className={
        item.isChecked
          ? "flex items-center leading-8 gap-5 border-b-1 border-color p-3 rounded-lg hover:bg-gray-300 cursor-pointer"
          : "flex items-center leading-8 gap-5 border-b-1 border-color p-3 bg-slate-200 rounded-lg hover:bg-gray-300 cursor-pointer"
      }
      onClick={onClickNotification}
    >
      <img className="rounded-full h-10 w-10" src={item.userPhoto} alt={item.message} />
      <div>
        <p className="font-semibold dark:text-gray-200 text-gray-600">
          {item.type === "INVOICE" ? "Đơn hàng mới" : "Đánh giá mới"}
        </p>
        <p className="text-gray-500 text-sm dark:text-gray-400"> {item.message} </p>
        <div className="flex justify-end text-slate-500 text-[15px]">
          <p>{GlobalUtil.dateTimeConvert(item.createdDate)}</p>
        </div>
      </div>
    </div>
  );
};

export default NotificationChild;
