/* eslint-disable no-unused-vars */
import React from "react";
import {useState} from "react";
import {AiFillStar, AiOutlineStar} from "react-icons/ai";

const Rating = (props) => {
  const [ratingArr, setRatingArr] = useState([1, 2, 3, 4, 5]);
  return (
    <div className="flex gap-2">
      {ratingArr.map((e, i) => {
        if (e <= props.value) {
          return <AiFillStar className="text-[40px] text-yellow-400" key={i}/>;
        }
        return <AiOutlineStar className="text-[40px] text-gray-500" key={i}/>;
      })}
    </div>
  );
};

export default Rating;
