import React from "react";

const Header = ({ category, title }) => {
  document.title = title;
  return (
    <div className="mb-1">
      <p className="text-3xl font-extrabold tracking-tight text-slate-900 dark:text-gray-200">
        {title}
      </p>
      <div className="text-gray-400 mt-1">{category}</div>
    </div>
  );
};

export default Header;
