/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from "react";
import {AiOutlineMenu} from "react-icons/ai";
import {RiNotification3Line} from "react-icons/ri";
import {MdArrowBackIosNew, MdKeyboardArrowDown} from "react-icons/md";
import {TooltipComponent} from "@syncfusion/ej2-react-popups";
import {Notification, UserProfile} from ".";
import {useStateContext} from "../contexts/ContextProvider";
import {useDataContext} from "../contexts/DataProvider";
import {useLocation, useNavigate} from "react-router-dom";

const NavButton = ({title, customFunc, icon, color, dotColor}) => {
  return (
    <TooltipComponent content={title} position="BottomCenter">
      <button
        type="button"
        onClick={customFunc}
        style={{color}}
        className="relative text-xl rounded-full p-3 hover:bg-light-gray"
      >
        <span
          style={{background: dotColor}}
          className="absolute inline-flex rounded-full h-2 w-2 right-2 top-2"
        />
        {icon}
      </button>
    </TooltipComponent>
  );
};

const Navbar = (props) => {
  const {
    activeMenu,
    setActiveMenu,
    isClicked,
    handleClick,
    screenSize,
    setScreenSize,
    currentColor,
  } = useStateContext();

  const {user} = useDataContext();
  useEffect(() => {
    const handleResize = () => setScreenSize(window.innerWidth);
    window.addEventListener("resize", handleResize);

    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, [screenSize]);
  const navigate = useNavigate();
  useEffect(() => {
    if (screenSize <= 900) {
      setActiveMenu(false);
    } else {
      setActiveMenu(true);
    }
  }, [screenSize]);
  const location = useLocation();
  const handleActiveMenu = () => setActiveMenu(!activeMenu);
  return (
    <div
      className="flex justify-between py-2 md:mx-6 md:my-3 relative bg-white dark:bg-secondary-dark-bg md:px-2 rounded-md items-center">
      <div className="flex justify-start items-center">
        <div className={!activeMenu ? "block" : "hidden"}>
          <NavButton
            title="Menu"
            customFunc={handleActiveMenu}
            color={currentColor}
            icon={<AiOutlineMenu/>}
          />
        </div>
        {location.pathname !== "/" && (
          <div>
            <div
              className="flex justify-center items-center gap-2 text-gray-300 hover:bg-gray-200 hover:text-white px-4 rounded-md py-2 transition-all cursor-pointer hover:dark:text-gray-600"
              onClick={() => {
                navigate(-1);
              }}
            >
              <MdArrowBackIosNew/>
              <h1>Trở về</h1>
            </div>
          </div>
        )}
      </div>

      <div className="flex">
        {/*<NavButton*/}
        {/*  title="Notifications"*/}
        {/*  dotColor="#03C9D7"*/}
        {/*  customFunc={() => handleClick("notification")}*/}
        {/*  color={currentColor}*/}
        {/*  icon={<RiNotification3Line />}*/}
        {/*/>*/}
        <TooltipComponent content="Profile" position="BottomCenter">
          <div
            className="flex items-center gap-2 cursor-pointer p-1 hover:bg-light-gray rounded-lg"
            onClick={() => handleClick("userProfile")}
          >
            <img
              src={user?.photo}
              className={`object-cover rounded-full w-8 h-8 border border-[${currentColor}]`}
              alt=".jpg"
            />
            <p>
              <span className="text-gray-400 text-14">Xin chào, </span>
              <span className="text-gray-400 font-bold ml-1 text-14">{user.fullName}</span>
            </p>
            <MdKeyboardArrowDown className="text-gray-400 text-14"/>
          </div>
        </TooltipComponent>
        {isClicked.notification && <Notification/>}
        {isClicked.userProfile && <UserProfile/>}
      </div>
    </div>
  );
};

export default Navbar;
