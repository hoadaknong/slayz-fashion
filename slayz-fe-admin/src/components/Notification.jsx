import React from "react";
import { useDataContext } from "../contexts/DataProvider";
import { useState } from "react";
import { useStateContext } from "../contexts/ContextProvider";
import NotificationChild from "./NotificationChild";

const Notification = () => {
  const { notifications, setPage, setNotificationStatus } = useDataContext();
  const [isAll, setIsAll] = useState(true);
  const { currentColor } = useStateContext();
  const loadMoreOnClick = (e) => {
    e.preventDefault();
    setPage((prev) => {
      return prev + 1;
    });
  };
  return (
    <div className="nav-item absolute right-5 md:right-40 top-16 bg-white dark:bg-[#42464D] py-8 px-4  rounded-lg w-96 shadow-lg">
      <div className="flex justify-start items-center gap-2">
        <div className="flex gap-3">
          <p className="font-semibold text-lg dark:text-gray-200 text-gray-600">
            Thông báo
          </p>
        </div>
        <button
          className={
            isAll
              ? `px-3 py-1 bg-[${currentColor}] rounded-md text-white`
              : "px-3 py-1 bg-gray-300 rounded-md"
          }
          onClick={() => {
            setIsAll(true);
            setNotificationStatus("all");
            setPage(1);
          }}
        >
          Tất cả
        </button>
        <button
          className={
            !isAll
              ? `px-3 py-1 bg-[${currentColor}] rounded-md text-white`
              : "px-3 py-1 bg-gray-300 rounded-md"
          }
          onClick={() => {
            setIsAll(false);
            setNotificationStatus("false");
            setPage(1);
          }}
        >
          Chưa đọc
        </button>
      </div>
      {isAll && (
        <div className="mt-3 flex flex-col gap-2 h-[60vh] overflow-scroll px-2">
          {notifications?.map((item, index) => {
            return <NotificationChild item={item} key={index} />;
          })}
          <button onClick={loadMoreOnClick}>Xem thêm</button>
        </div>
      )}
      {!isAll && (
        <div className="mt-3 flex flex-col gap-2 h-[60vh] overflow-scroll px-2">
          {notifications?.map((item, index) => {
            if (item.isChecked === false) {
              return <NotificationChild item={item} key={index} />;
            }
            return null;
          })}
        </div>
      )}
    </div>
  );
};

export default Notification;
