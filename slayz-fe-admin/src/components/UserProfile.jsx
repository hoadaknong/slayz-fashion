import React from "react";
import { MdOutlineCancel } from "react-icons/md";

import { Button } from ".";
import { userProfileData } from "../data/dummy";
import { useStateContext } from "../contexts/ContextProvider";
import { useDataContext } from "../contexts/DataProvider";
import { AuthService } from "../services/auth.service";
import swal from "sweetalert";

const UserProfile = () => {
  const { currentColor, handleClick } = useStateContext();
  const { user, setUser } = useDataContext();

  const logoutHandleClick = async () => {
    handleClick("userProfile");
    const logoutResponse = await AuthService.logout();
    if (logoutResponse.status === "OK") {
      setUser(null);
      swal({
        title: "ĐĂNG XUẤT",
        text: logoutResponse.message,
        icon: "success",
        button: "OK",
      });
    }
  };
  const roleConvert = (roleName) => {
    if (roleName === "ROLE_ADMIN") {
      return "Quản trị viên";
    } else if (roleName === "ROLE_STAFF") {
      return "Nhân viên";
    } else {
      return "Người dùng";
    }
  };
  return (
    <div className="drop-shadow-xl nav-item absolute right-1 top-16 bg-white dark:bg-[#42464D] p-8 rounded-lg w-96">
      <div className="flex justify-between items-center">
        <p className="font-semibold text-lg dark:text-gray-200 text-gray-400">Hồ sơ người dùng</p>
        <Button
          icon={<MdOutlineCancel />}
          color="rgb(153, 171, 180)"
          bgHoverColor="light-gray"
          size="2xl"
          borderRadius="50%"
        />
      </div>
      <div className="flex gap-5 items-center mt-4 border-color border-b-1 pb-4 overflow-hidden">
        <img
          className="object-cover rounded-full h-24 w-24 border-2"
          src={user?.photo}
          alt="user-profile"
        />
        <div className="flex flex-col justify-start items-start gap-1">
          <p className="font-semibold text-xl dark:text-gray-200 text-gray-500">{user?.fullName}</p>
          <p className="text-gray-500 text-sm dark:text-gray-400">
            {roleConvert(user?.role?.name)}
          </p>
          <p className="text-gray-500 text-sm font-semibold dark:text-gray-400">{user?.email}</p>
        </div>
      </div>
      <div>
        {userProfileData?.map((item, index) => (
          <div
            key={index}
            className="flex gap-5 border-b-1 border-color p-4 hover:bg-light-gray cursor-pointer  dark:hover:bg-[#42464D]"
          >
            <button
              type="button"
              style={{ color: item.iconColor, backgroundColor: item.iconBg }}
              className=" text-xl rounded-lg p-3 hover:bg-light-gray"
            >
              {item.icon}
            </button>

            <div>
              <p className="font-semibold dark:text-gray-200 text-gray-500">{item.title}</p>
              <p className="text-gray-500 text-sm dark:text-gray-400">{item.desc}</p>
            </div>
          </div>
        ))}
      </div>
      <div className="mt-5">
        <Button
          color="white"
          bgColor={currentColor}
          text="Đăng xuất"
          borderRadius="10px"
          width="full"
          onClick={logoutHandleClick}
        />
      </div>
    </div>
  );
};

export default UserProfile;
