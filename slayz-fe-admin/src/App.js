/* eslint-disable no-unused-vars */
import React from "react";
import {Route, Routes} from "react-router-dom";
import {Dashboard, Login, NotFound} from "./pages";
import "./App.css";

const App = () => {
  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/management/*" element={<Dashboard />} />
      <Route path="*" element={<Dashboard />} />
      <Route path="/*" element={<NotFound />} />
    </Routes>
  );
};

export default App;
