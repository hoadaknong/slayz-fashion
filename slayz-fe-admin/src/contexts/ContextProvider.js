import React, { createContext, useContext, useState } from "react";
const StateContext = createContext();

const initialState = {
  chat: false,
  cart: false,
  userProfile: false,
  notification: false,
  overlay: false
};

export const ContextProvider = ({ children }) => {
  //To solve sibar's appearance
  const [activeMenu, setActiveMenu] = useState(true);

  //To solve icon navbar click
  const [isClicked, setIsClicked] = useState(initialState);

  //To solve screen size
  const [screenSize, setScreenSize] = useState(undefined);

  //To solve current color theme
  const [currentColor, setCurrentColor] = useState("#03C9D7");

  //To solve current mode light or dark
  const [currentMode, setCurrentMode] = useState("Light");

  //To solve theme setting appearance
  const [themeSettings, setThemeSettings] = useState(false);

  //To solve theme setting appearance
  const [isAuthen, setIsAuthen] = useState(false);

  //To store variant form
  const [styleFormList, setStyleFormList] = useState([1]);

  //To get product data
  const [productData, setProductData] = useState({});

  //To get id product
  const [productId, setProductId] = useState(null);

  //To get user
  const [user, setUser] = useState(null);

  //To path
  const [navigateLength, setNavigateLength] = useState(0);

  const setMode = (e) => {
    setCurrentMode(e.target.value);
    localStorage.setItem("themeMode", e.target.value);
    setThemeSettings(false);
  };

  const setColor = (color) => {
    setCurrentColor(color);
    localStorage.setItem("colorMode", color);
    setThemeSettings(false);
  };
  const overlayHandleClick = ()=>{
    setIsClicked({
      ...initialState
    });
  }
  const handleClick = (clicked) => {
    if (isClicked[clicked]) {
      setIsClicked({
        ...initialState,
        [clicked]: false,
        "overlay":false,
      });
    } else {
      setIsClicked({
        ...initialState,
        [clicked]: true,
        "overlay": true,
      });
    }
  };
  return (
    <StateContext.Provider
      value={{
        activeMenu,
        setActiveMenu,
        isClicked,
        setIsClicked,
        handleClick,
        screenSize,
        setScreenSize,
        currentColor,
        currentMode,
        themeSettings,
        setThemeSettings,
        setMode,
        setColor,
        isAuthen,
        setIsAuthen,
        styleFormList,
        setStyleFormList,
        productData,
        setProductData,
        productId,
        setProductId,
        user,
        setUser,
        navigateLength,
        setNavigateLength,
        overlayHandleClick,
      }}
    >
      {children}
    </StateContext.Provider>
  );
};

export const useStateContext = () => useContext(StateContext);
