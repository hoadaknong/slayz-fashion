/* eslint-disable react-hooks/exhaustive-deps */
import React, { createContext, useContext, useEffect, useState } from "react";
import { InvoiceService } from "../services/invoice.service";
import { UserService } from "../services/user.service";
import { NotificationService } from "../services/notification.service";

const DataContext = createContext();

export const DataProvider = ({ children }) => {
  const [user, setUser] = useState({});
  //Category data
  const [categoryData, setCategoryData] = useState([]);

  //Size data
  const [sizeData, setSizeData] = useState([]);

  //Discount data
  const [discountData, setDiscountData] = useState([]);

  //Color data
  const [colorData, setColorData] = useState([]);

  //Product data
  const [productData, setProductData] = useState([]);

  //User data
  const [userData, setUserData] = useState([]);

  //Invoice data
  const [invoiceData, setInvoiceData] = useState([]);

  //Notification list
  const [notifications, setNotifications] = useState([]);

  //Limit per page
  const [limit, setLimit] = useState(5);

  //Notification page
  const [page, setPage] = useState(1);

  const [notificationStatus, setNotificationStatus] = useState("all");

  const fetchInvoiceData = () => {
    InvoiceService.getAllInvoice().then((response) => {
      if (response.status === "OK") {
        setInvoiceData(response.data);
      }
    });
  };
  const fetchNotification = async () => {
    const notificationResponse = await NotificationService.getAllNotification(
      page,
      4,
      notificationStatus
    );
    setNotifications((prev) => {
      if (notificationResponse.status === "OK") {
        return [...prev, ...notificationResponse.data.details];
      } else {
        return [];
      }
    });
  };
  useEffect(() => {
    (async function () {
      const userResponse = await UserService.getCurrentUser();
      setUser(() => {
        if (userResponse.status === "OK") {
          return userResponse.data;
        } else {
          return null;
        }
      });
    })();
  }, []);

  const data = {
    user,
    categoryData,
    sizeData,
    discountData,
    colorData,
    productData,
    userData,
    invoiceData,
    notifications,
    limit,
    page,
    notificationStatus,
  };
  const setData = {
    setUser,
    setCategoryData,
    setSizeData,
    setDiscountData,
    setColorData,
    setProductData,
    setUserData,
    setInvoiceData,
    fetchInvoiceData,
    setNotifications,
    fetchNotification,
    setLimit,
    setPage,
    setNotificationStatus,
  };

  return (
    <DataContext.Provider value={{ ...data, ...setData }}>
      {children}
    </DataContext.Provider>
  );
};

export const useDataContext = () => useContext(DataContext);
